<?php

/* GEDELTURBundle:Default:plantilla.html.twig */
class __TwigTemplate_d25750ad6c8086219baca87d3eb0038c979bfc02ee68eab735b693bf8a4c07e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html.twig");

        $this->blocks = array(
            'user' => array($this, 'block_user'),
            'usuario' => array($this, 'block_usuario'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 14
    public function block_user($context, array $blocks = array())
    {
        // line 15
        echo " ";
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_FULLY")) {
            echo " 
 
 Bienvenido: ";
            // line 17
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "getNombre", array(), "method"))), "html", null, true);
            echo "  ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "getApellido", array(), "method"))), "html", null, true);
            echo "
 
 ";
        }
        // line 20
        echo "
 ";
    }

    // line 23
    public function block_usuario($context, array $blocks = array())
    {
        // line 24
        echo "
<!--<li><a  href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("usuarios_edit", array("id" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "getId", array(), "method"))), "html", null, true);
        echo "\"><i class=\"fa fa-wrench\"></i>    Editar</a>
                        </li>-->

 ";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Default:plantilla.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 25,  54 => 24,  51 => 23,  46 => 20,  38 => 17,  32 => 15,  40 => 10,  37 => 9,  29 => 14,);
    }
}

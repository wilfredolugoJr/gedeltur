<?php

/* layout.html.twig */
class __TwigTemplate_d725eb60533f924ed5d6445c2a7a69ecb7bcee0d76bd3b0da0335fccf40e4bd9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'grafica' => array($this, 'block_grafica'),
            'evento' => array($this, 'block_evento'),
            'superacion' => array($this, 'block_superacion'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<!--[if IE 8 ]><html class=\"ie ie8\" lang=\"en\"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang=\"en\" class=\"no-js\"> <![endif]-->
<html lang=\"en\">

<head>

  <!-- Basic -->
  <title>GEDELTUR</title>

  <!-- Define Charset -->
  <meta charset=\"utf-8\">

  <!-- Responsive Metatag -->
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">

  <!-- Page Description and Author -->
  <meta name=\"description\" content=\"Margo - Responsive HTML5 Template\">
  <meta name=\"author\" content=\"iThemesLab\">

  <!-- Bootstrap CSS  -->










    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/css/dataTables.bootstrap.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/css/dataTables.responsive.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/css/font-awesome.min.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/css/bootstrap-datetimepicker.css"), "html", null, true);
        echo "\" />











  <link rel=\"stylesheet\" href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/asset/css/bootstrap.min.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"screen\">

  <!-- Font Awesome CSS -->
  <link rel=\"stylesheet\" href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/font-awesome.min.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"screen\">

  <!-- Slicknav -->
  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/slicknav.css"), "html", null, true);
        echo "\" media=\"screen\">

  <!-- Margo CSS Styles  -->
  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/style.css"), "html", null, true);
        echo "\" media=\"screen\">

  <!-- Responsive CSS Styles  -->
  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/responsive.css"), "html", null, true);
        echo "\" media=\"screen\">

  <!-- Css3 Transitions Styles  -->
  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/animate.css"), "html", null, true);
        echo "\" media=\"screen\">

  <!-- Color CSS Styles  -->
  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/colors/red.css"), "html", null, true);
        echo "\" title=\"red\" media=\"screen\" />

  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/colors/jade.css"), "html", null, true);
        echo "\" title=\"jade\" media=\"screen\" />

  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/colors/green.css"), "html", null, true);
        echo "\" title=\"green\" media=\"screen\" />

  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/colors/blue.css"), "html", null, true);
        echo "\" title=\"blue\" media=\"screen\" />

  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/colors/beige.css"), "html", null, true);
        echo "\" title=\"beige\" media=\"screen\" />

  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/colors/cyan.css"), "html", null, true);
        echo "\" title=\"cyan\" media=\"screen\" />

  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/colors/orange.css"), "html", null, true);
        echo "\" title=\"orange\" media=\"screen\" />

  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/colors/peach.css"), "html", null, true);
        echo "\" title=\"peach\" media=\"screen\" />

  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/colors/pink.css"), "html", null, true);
        echo "\" title=\"pink\" media=\"screen\" />

  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/colors/purple.css"), "html", null, true);
        echo "\" title=\"purple\" media=\"screen\" />

  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/colors/sky-blue.css"), "html", null, true);
        echo "\" title=\"sky-blue\" media=\"screen\" />

  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/css/colors/yellow.css"), "html", null, true);
        echo "\" title=\"yellow\" media=\"screen\" />
  
  
  <!-- registrar
  <link href=\"asset/global/plugins/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\">-->
  <link href=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/asset/global/plugins/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
  <!-- Global styles END --> 
  <!-- Page level plugin styles START --> 
  <link href=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/asset/global/plugins/fancybox/source/jquery.fancybox.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

  <link href=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/asset/global/plugins/uniform/css/uniform.default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">

  <!-- Page level plugin styles END --><!-- Theme styles START 
  <link href=\"asset/global/css/components.css\" rel=\"stylesheet\"> --> 
  
  <link href=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/asset/frontend/layout/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
  
  
  <link href=\"";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/asset/frontend/layout/css/style-responsive.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

  <link href=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/asset/frontend/layout/css/themes/red.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"style-color\">

  <link href=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/asset/frontend/layout/css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

















  <!-- registrar -->
  
   <script type=\"text/javascript\" src=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/jquery.js"), "html", null, true);
        echo "\" >
  </script>

  <!-- Margo JS  -->
  <script type=\"text/javascript\" src=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/jquery-2.1.4.min.js"), "html", null, true);
        echo "\"></script>
  <script type=\"text/javascript\" src=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/jquery.migrate.js"), "html", null, true);
        echo "\"></script>

  <script type=\"text/javascript\" src=\"";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/modernizrr.js"), "html", null, true);
        echo "\"></script>

  <script type=\"text/javascript\" src=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/asset/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

  <script type=\"text/javascript\" src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/jquery.fitvids.js"), "html", null, true);
        echo "\"></script>

  <script type=\"text/javascript\" src=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/owl.carousel.min.js"), "html", null, true);
        echo "\"></script>


  <script type=\"text/javascript\" src=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/nivo-lightbox.min.js"), "html", null, true);
        echo "\"></script>

  <script type=\"text/javascript\" src=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/jquery.isotope.min.js"), "html", null, true);
        echo "\"></script>

  <script type=\"text/javascript\" src=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/jquery.appear.js"), "html", null, true);
        echo "\"></script>

  <script type=\"text/javascript\" src=\"";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/count-to.js"), "html", null, true);
        echo "\"></script>

  <script type=\"text/javascript\" src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/jquery.textillate.js"), "html", null, true);
        echo "\"></script>

  <script type=\"text/javascript\" src=\"";
        // line 155
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/jquery.lettering.js"), "html", null, true);
        echo "\"></script>

  <script type=\"text/javascript\" src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/jquery.easypiechart.min.js"), "html", null, true);
        echo "\"></script>

  <script type=\"text/javascript\" src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/jquery.nicescroll.min.js"), "html", null, true);
        echo "\"></script>

  <script type=\"text/javascript\" src=\"";
        // line 161
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/jquery.parallax.js"), "html", null, true);
        echo "\"></script>

  <script type=\"text/javascript\" src=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/mediaelement-and-player.js"), "html", null, true);
        echo "\"></script>

  <script type=\"text/javascript\" src=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/jquery.slicknav.js"), "html", null, true);
        echo "\"></script>






    ";
        // line 172
        $this->displayBlock('grafica', $context, $blocks);
        // line 175
        echo "
    <script src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/dataTables.bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/moment-with-locales.js"), "html", null, true);
        echo "\"></script>





    <script type=\"text/javascript\">
        jQuery(document).ready(function(\$) {
            \$(\".scroll\").click(function(event){
                event.preventDefault();
                \$('html,body').animate({scrollTop:\$(this.hash).offset().top},1200);
            });
        });
    </script>









       

 

  <!--[if IE 8]><script src=\"http://html5shiv.googlecode.com/svn/trunk/html5.js\"></script><![endif]-->
  <!--[if lt IE 9]><script src=\"http://html5shiv.googlecode.com/svn/trunk/html5.js\"></script><![endif]-->

</head>

<body>

  <!-- Full Body Container -->

  
   
  <div id=\"container\">


    <!-- Start Header Section -->
    <div class=\"hidden-header\"></div>
    <header class=\"clearfix\">

      <!-- Start Top Bar -->
      <div class=\"top-bar\">
        <div class=\"container\">
        
        
          <div class=\"row\">
          
          
         <div class=\"col-md-3\"  >
              <!-- Start Contact Info -->
              <ul class=\"contact-details\">
              
               

        

                ";
        // line 239
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_FULLY")) {
            echo " 
 
                   Bienvenido: ";
            // line 241
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "getNombre", array(), "method"))), "html", null, true);
            echo "   ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "getApellido", array(), "method"))), "html", null, true);
            echo "

                  ";
            // line 243
            if (($this->env->getExtension('security')->isGranted("ROLE_USER") || $this->env->getExtension('security')->isGranted("ROLE_ADMIN"))) {
                // line 244
                echo "                      <li> <a href=\"";
                echo $this->env->getExtension('routing')->getPath("cambiar_password");
                echo "\"><span class=\"glyphicon glyphicon-edit\"></span> Cambiar Contraseña</a></li>
                  ";
            }
            // line 246
            echo "                    ";
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 247
                echo " 
                <li><a href=\"";
                // line 248
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\"><span class=\"glyphicon glyphicon-folder-open\"></span> Administrar</a>
                
                </li>


                 ";
            }
            // line 254
            echo "
                   <li><a href=\"";
            // line 255
            echo $this->env->getExtension('routing')->getPath("logout");
            echo "\"><span class=\"glyphicon glyphicon-off\"></span> Salir</a>
                
                </li>

                ";
        } else {
            // line 260
            echo "                 <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("usuarios_registro");
            echo "\"><span class=\"glyphicon glyphicon-user\"></span>
 Registrarse</a>
                </li>

                <li><a href=\"";
            // line 264
            echo $this->env->getExtension('routing')->getPath("_bienvenid");
            echo "\"><span class=\"glyphicon glyphicon-lock\"></span>Autentificar</a>
                
                </li>
 
                  ";
        }
        // line 269
        echo "
                
              </ul>
              <!-- End Contact Info -->
            </div>
          
          
            <div class=\"col-md-5\">
              <!-- Start Contact Info -->
              <ul class=\"contact-details\">
                <li><a href=\"#\"><i class=\"fa fa-map-marker\"></i> Martí-270/final, Pinar de Río, Cuba</a>
                </li>
                <li><a href=\"#\"><i class=\"fa fa-envelope-o\"></i> gedeltur@upr.edu.cu</a>
                </li>
                <li><a href=\"#\"><i class=\"fa fa-phone\"></i> 48779361</a>
                </li>
              </ul>
              <!-- End Contact Info -->
            </div>
            
            
            
            
            
            
            
            
            
            
            <!-- .col-md-6 -->
            <div class=\"col-md-4\">
              <!-- Start Social Links -->
              <ul class=\"social-list\">
                <li>
                  <a  data-placement=\"bottom\" title=\"Directorio\" href=\"http://directorio.upr.edu.cu\" target=\"_blank\">
                  <img style=\"max-height: 20px; max-width: 20px; margin-top: 12px; margin-left: 5px; margin-right: 5px;\"  src=\"";
        // line 304
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/images/iconos/directorio.svg"), "html", null, true);
        echo "\"></a>
                </li>
                <li>
                 <a  data-placement=\"bottom\" title=\"Teléfonos\" href=\"http://telefonos.upr.edu.cu\" target=\"_blank\">
                  <img style=\"max-height: 20px; max-width: 20px; margin-top: 12px; margin-left: 5px; margin-right: 5px;\"  src=\"";
        // line 308
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/images/iconos/telef.svg"), "html", null, true);
        echo "\"></a>
                </li>
                <li>
                 <a  data-placement=\"bottom\" title=\"Correo\" href=\"http://correo.upr.edu.cu\" target=\"_blank\">
                  <img style=\"max-height: 20px; max-width: 20px; margin-top: 12px; margin-left: 5px; margin-right: 5px;\"  src=\"";
        // line 312
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/images/iconos/mail_p.svg"), "html", null, true);
        echo "\"></a>
                </li>
                <li>
                 <a  data-placement=\"bottom\" title=\"Lupa\" href=\"http://lupa.upr.edu.cu\" target=\"_blank\">
                  <img style=\"max-height: 20px; max-width: 20px; margin-top: 12px; margin-left: 5px; margin-right: 5px;\"  src=\"";
        // line 316
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/images/iconos/lupa.svg"), "html", null, true);
        echo "\"></a>
                </li>

                <li>
                  <a  data-placement=\"bottom\" title=\"Noticias\"href=\"http://noticias.upr.edu.cu\" target=\"_blank\">
                  <img style=\"max-height: 20px; max-width: 20px; margin-top: 12px; margin-left: 5px; margin-right: 5px;\"  src=\"";
        // line 321
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/images/iconos/news.svg"), "html", null, true);
        echo "\"></a>
                </li>
               
              </ul>
              <!-- End Social Links -->
            </div>
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            <!-- .col-md-6 -->
          </div>
          <!-- .row -->
        </div>
        <!-- .container -->
      </div>
      <!-- .top-bar -->
      <!-- End Top Bar -->


      <!-- Start  Logo & Naviagtion  -->
      <div class=\"navbar navbar-default navbar-top\">
        <div class=\"container\">
          <div class=\"navbar-header\">
            <!-- Stat Toggle Nav Link For Mobiles -->
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
              <i class=\"fa fa-bars\"></i>
            </button>
            <!-- End Toggle Nav Link For Mobiles -->
            <a class=\"navbar-brand\" href=\"\">
              <h1 style=\"color:#FFF\">GEDELTUR</h1>

            </a>
          </div>
          <div class=\"navbar-collapse collapse\">
            <!-- Stat Search
            <div class=\"search-side\">
              <a class=\"show-search\"><i class=\"fa fa-search\"></i></a>
              <div class=\"search-form\">
                <form autocomplete=\"off\" role=\"search\" method=\"get\" class=\"searchform\" action=\"#\">
                  <input type=\"text\" value=\"\" name=\"s\" id=\"s\" placeholder=\"Search the site...\">
                </form>
              </div>
            </div>
            <!-- End Search -->
            <!-- Start Navigation List -->
            <ul class=\"nav navbar-nav navbar-right\">
              <li>
                <a  href=\"";
        // line 381
        echo $this->env->getExtension('routing')->getPath("gedeltur_homepage");
        echo "\">Inicio</a>
                 

              </li>

                


               
              <li>
                <a href=\"#\" class=\"scroll\">Pregrado</a>
                <ul class=\"dropdown\">
                  <li><a href=\"";
        // line 393
        echo $this->env->getExtension('routing')->getPath("asignaturas");
        echo "\" >Asignaturas</a>
                  </li>

                </ul>
              </li>
              <li>
                <a  href=\"#\" >Postgrado</a>
                <ul class=\"dropdown\">
                  <li><a href=\"";
        // line 401
        echo $this->env->getExtension('routing')->getPath("diplomados");
        echo "\" >Diplomados</a>
                  </li>
                  <li><a href=\"";
        // line 403
        echo $this->env->getExtension('routing')->getPath("maestrias");
        echo "\" >Maestrías</a>
                  </li>
                  <li><a href=\"";
        // line 405
        echo $this->env->getExtension('routing')->getPath("especialidades");
        echo "\" >Especialidades</a>
                  </li>

                </ul>
              </li>
              <li>
                <a  href=\"";
        // line 411
        echo $this->env->getExtension('routing')->getPath("cursos");
        echo "\" >Cursos</a>

              </li>
              <li>
                <a title=\"Investigacion, Desarrollo e Inovación\" href=\"#\" > I+D+I </a>

                <ul class=\"dropdown\">

                  <li><a href=\"";
        // line 419
        echo $this->env->getExtension('routing')->getPath("pro");
        echo "\" >Proyectos</a>
                  </li>
                  <li><a href=\"";
        // line 421
        echo $this->env->getExtension('routing')->getPath("li");
        echo "\" >Líneas de Investigación</a>
                  </li>
                
                 
                 </ul>
              </li>

              <li><a title=\"Servicios Científico Técnicos\" href=\"";
        // line 428
        echo $this->env->getExtension('routing')->getPath("servicios");
        echo "\" >SCT</a>
              </li>

                <li><a href=\"#\" >Recursos Humanos</a>
                 <ul class=\"dropdown\">
                  <li><a href=\"";
        // line 433
        echo $this->env->getExtension('routing')->getPath("trabajadores");
        echo "\" >Trabajadores</a>
                  </li>
                  <li><a href=\"";
        // line 435
        echo $this->env->getExtension('routing')->getPath("estudiantes");
        echo "\" >Estudiantes</a>
                  </li>
                  <li><a href=\"";
        // line 437
        echo $this->env->getExtension('routing')->getPath("colaboradores");
        echo "\" >Colaboradores</a>
                  </li>

                </ul>
                </li>


                  ";
        // line 444
        $this->displayBlock('evento', $context, $blocks);
        // line 448
        echo "
                 ";
        // line 449
        if (($this->env->getExtension('security')->isGranted("ROLE_USER") || $this->env->getExtension('security')->isGranted("ROLE_ADMIN"))) {
            // line 450
            echo "




                <li><a href=\"";
            // line 455
            echo $this->env->getExtension('routing')->getPath("correo_nuevo");
            echo "\" >Foro</a>
                </li>

                ";
        }
        // line 459
        echo "
                <li>
                    <a  href=\"";
        // line 461
        echo $this->env->getExtension('routing')->getPath("sobre");
        echo "\">Acerca de</a>

                    <ul class=\"dropdown\">
                        <li>
                            <a target=\"_blank\" href=\"";
        // line 465
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("Ayuda/GEDELTUR.html"), "html", null, true);
        echo "\">Ayuda</a>


                        </li>

                    </ul>



                </li>
                
            </ul>

            <!-- End Navigation List -->
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class=\"wpb-mobile-menu\">
          <li>

            <a class=\"active\" href=\"\">GEDELTUR</a>
            <ul class=\"dropdown\">




                <li>
                    <a href=\"\">Usuario</a>
                    <ul class=\"dropdown\">


                        ";
        // line 497
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 498
            echo "
                            Bievenido: ";
            // line 499
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "getNombre", array(), "method"))), "html", null, true);
            echo "   ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "getApellido", array(), "method"))), "html", null, true);
            echo "

                            ";
            // line 501
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 502
                echo "
                                <li><a href=\"";
                // line 503
                echo $this->env->getExtension('routing')->getPath("home");
                echo "\"><span class=\"glyphicon glyphicon-folder-open\"></span> Administrar</a>

                                </li>


                            ";
            }
            // line 509
            echo "
                            <li><a href=\"";
            // line 510
            echo $this->env->getExtension('routing')->getPath("logout");
            echo "\"><span class=\"glyphicon glyphicon-off\"></span> Salir</a>

                            </li>

                        ";
        } else {
            // line 515
            echo "                            <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("usuarios_registro");
            echo "\"><span class=\"glyphicon glyphicon-user\"></span>
                                    Registrarse</a>
                            </li>

                            <li><a href=\"";
            // line 519
            echo $this->env->getExtension('routing')->getPath("_bienvenid");
            echo "\"><span class=\"glyphicon glyphicon-lock\"></span> Login</a>

                            </li>

                        ";
        }
        // line 524
        echo "

                    </ul>

                </li>


                <li>
                    <a class=\"active\" href=\"";
        // line 532
        echo $this->env->getExtension('routing')->getPath("gedeltur_homepage");
        echo "\">Inicio</a>

                </li>
                <li>
                    <a href=\"\">Pregrado</a>
                    <ul class=\"dropdown\">


                        <li><a href=\"";
        // line 540
        echo $this->env->getExtension('routing')->getPath("asignaturas");
        echo "\">Asignaturas</a>
                        </li>

                    </ul>
                </li>
                <li>
                    <a  href=\"\">Postgrado</a>
                    <ul class=\"dropdown\">

                        <li><a href=\"";
        // line 549
        echo $this->env->getExtension('routing')->getPath("diplomados");
        echo "\" >Diplomados</a>
                        </li>
                        <li><a href=\"";
        // line 551
        echo $this->env->getExtension('routing')->getPath("maestrias");
        echo "\" >Maestrías</a>
                        </li>
                        <li><a href=\"";
        // line 553
        echo $this->env->getExtension('routing')->getPath("especialidades");
        echo "\" >Especialidades</a>
                        </li>


                    </ul>
                </li>
                <li>
                    <a href=\"";
        // line 560
        echo $this->env->getExtension('routing')->getPath("cursos");
        echo "\">Cursos</a>

                </li>
                <li><a href=\"";
        // line 563
        echo $this->env->getExtension('routing')->getPath("pro");
        echo "\" >Proyectos</a>
                </li>
                <li><a href=\"";
        // line 565
        echo $this->env->getExtension('routing')->getPath("li");
        echo "\" >Lineas de Investigación</a>
                </li>


                <li><a title=\"Sercios Científico Técnicos\" href=\"";
        // line 569
        echo $this->env->getExtension('routing')->getPath("servicios");
        echo "\" >SCT</a>
                </li>

                <li><a href=\"#\" >Recursos Humanos</a>
                    <ul class=\"dropdown\">
                        <li><a href=\"";
        // line 574
        echo $this->env->getExtension('routing')->getPath("trabajadores");
        echo "\" >Trabajador</a>
                        </li>
                        <li><a href=\"";
        // line 576
        echo $this->env->getExtension('routing')->getPath("estudiantes");
        echo "\" >Estudiantes</a>
                        </li>
                        <li><a href=\"";
        // line 578
        echo $this->env->getExtension('routing')->getPath("colaboradores");
        echo "\" >Colaboradores</a>
                        </li>

                    </ul>
                </li>



                ";
        // line 586
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 587
            echo "




                    <li><a href=\"";
            // line 592
            echo $this->env->getExtension('routing')->getPath("correo_nuevo");
            echo "\" >Foro</a>
                    </li>

                ";
        }
        // line 596
        echo "        </ul>
        <!-- Mobile Menu End -->


      <!-- End Header Logo & Naviagtion -->

    </header>
    <!-- End Header Section -->

 
        
        
        </br>
         
         <div class=\"container\">

        ";
        // line 612
        $this->displayBlock('superacion', $context, $blocks);
        // line 616
        echo "
           </div>





    <!-- Start Team Member Section -->
    
 <!-- Trabajadores -->



    <!--   End Trabajadores -->



    <!-- Start Footer Section -->
    </div>
    

    <div class=\"row links\">
        <div class=\"container\">
                            <div class=\"row titlelink\" id=\"universidad\">
                    <h2>Universidad</h2>
                </div>
                <div class=\"row linkul\">
                                <div class=\"col-md-4 col-sm-4 nopadding\">
            <ul>
                            <li>
                    <a href=\"http://vrfp.upr.edu.cu/\" target=\"_blank\">VRFP</a>
                </li>
                        <li>
                    <a href=\"http://vrip.upr.edu.cu/\" target=\"_blank\">VRIIP</a>
                </li>
                        <li>
                    <a href=\"http://ri.upr.edu.cu/\" target=\"_blank\">RI</a>
                </li>
                        <li>
                    <a href=\"http://dict.upr.edu.cu/\" target=\"_blank\">Biblioteca</a>
                </li>
                        <li>
                    <a href=\"http://ceces.upr.edu.cu/\" target=\"_blank\">CECES</a>
                </li>
                        <li>
                    <a href=\"http://cemarnaweb.upr.edu.cu/\" target=\"_blank\">CEMARNA</a>
                </li>
                        <li>
                    <a href=\"http://cedecom.upr.edu.cu/\" target=\"_blank\">CEDECOM</a>
                </li>
                </ul>
        </div>
                        <div class=\"col-md-4 col-sm-4 nopadding\">
            <ul>
                            <li>
                    <a href=\"http://progintec.upr.edu.cu/\" target=\"_blank\">ProGintec</a>
                </li>
                        <li>
                    <a href=\"http://di.upr.edu.cu/\" target=\"_blank\">UPRedes</a>
                </li>
                        <li>
                    <a href=\"http://www.upr.edu.cu/\" target=\"_blank\">Internet</a>
                </li>
                        <li>
                    <a href=\"http://gedeltur.upr.edu.cu/\" target=\"_blank\">Gedeltur</a>
                </li>
                        <li>
                    <a href=\"http://feu.upr.edu.cu/\" target=\"_blank\">FEU</a>
                </li>
                        <li>
                    <a href=\"http://sgc.upr.edu.cu/\" target=\"_blank\">Gestion de la Calidad</a>
                </li>
                        <li>
                    <a href=\"http://ceetes.upr.edu.cu/\" target=\"_blank\">CEETES</a>
                </li>
                </ul>
        </div>
                        <div class=\"col-md-4 col-sm-4 nopadding\">
            <ul>
                            <li>
                    <a href=\"http://cum.upr.edu.cu/\" target=\"_blank\">CUM</a>
                </li>
                        <li>
                    <a href=\"http://cef.upr.edu.cu/\" target=\"_blank\">Centro de Estudios Forestales</a>
                </li>
                        <li>
                    <a href=\"http://cvforestal.upr.edu.cu/\" target=\"_blank\">Comunidad Virtual de Recursos Forestales</a>
                </li>
                        <li>
                    <a href=\"http://observatorio.progintec.upr.edu.cu/\" target=\"_blank\">Observatorio Tecnológico de la UPR</a>
                </li>
                        <li>
                    <a href=\"http://interfaz.upr.edu.cu/\" target=\"_blank\">Observatorio Interfaz Universidad - Empresa</a>
                </li>
                        <li>
                    <a href=\"https://cv.progintec.upr.edu.cu/paginas/index.php\" target=\"_blank\">CV</a>
                </li>
                        <li>
                    <a href=\"http://preguntas.upr.edu.cu/\" target=\"_blank\">blog</a>
                </li>
                </ul>
        </div>
            
                </div>
                            <div class=\"row titlelink\" id=\"reduniv\">
                    <h2>RedUniv</h2>
                </div>
                <div class=\"row linkul\">
                                <div class=\"col-md-4 col-sm-4 nopadding\">
            <ul>
                            <li>
                    <a href=\"http://intranet.mes.gob.cu/\" target=\"_blank\">MES</a>
                </li>
                        <li>
                    <a href=\"http://www.jovenclub.cu/\" target=\"_blank\">Joven Club</a>
                </li>
                        <li>
                    <a href=\"http://www.sld.cu/\" target=\"_blank\">Infomed</a>
                </li>
                </ul>
        </div>
                        <div class=\"col-md-4 col-sm-4 nopadding\">
            <ul>
                            <li>
                    <a href=\"http://www.rimed.cu/\" target=\"_blank\">MINED</a>
                </li>
                        <li>
                    <a href=\"http://www.uci.cu/\" target=\"_blank\">UCI</a>
                </li>
                        <li>
                    <a href=\"http://www.cubarte.cult.cu/\" target=\"_blank\">Cubarte</a>
                </li>
                </ul>
        </div>
                        <div class=\"col-md-4 col-sm-4 nopadding\">
            <ul>
                            <li>
                    <a href=\"http://www.habananuestra.cu/\" target=\"_blank\">Hist. Habana</a>
                </li>
                        <li>
                    <a href=\"http://www.uh.cu/sitios/reddees/\" target=\"_blank\">Red-dees</a>
                </li>
                        <li>
                    <a href=\"http://www.reduniv.edu.cu/\" target=\"_blank\">reduniv</a>
                </li>
                </ul>
        </div>
                        <div class=\"col-md-4 col-sm-4 nopadding\">
            <ul>
                            <li>
                    <a href=\"http://redcuba.reduniv.edu.cu/\" target=\"_blank\">Red Cuba</a>
                </li>
                </ul>
        </div>
            
                </div>
                            <div class=\"row titlelink\" id=\"de-interes\">
                    <h2>De interés</h2>
                </div>
                <div class=\"row linkul\">
                                <div class=\"col-md-4 col-sm-4 nopadding\">
            <ul>
                            <li>
                    <a href=\"http://www.ecured.cu/\" target=\"_blank\">EcuRed</a>
                </li>
                        <li>
                    <a href=\"http://segurinfo.upr.edu.cu/\" target=\"_blank\">SegurInfo</a>
                </li>
                        <li>
                    <a href=\"http://hsaiz.upr.edu.cu/\" target=\"_blank\">Hermanos Saíz</a>
                </li>
                        <li>
                    <a href=\"http://pinux.upr.edu.cu/\" target=\"_blank\">Pinux</a>
                </li>
                        <li>
                    <a href=\"http://coodes.upr.edu.cu/\" target=\"_blank\">COODES</a>
                </li>
                        <li>
                    <a href=\"http://cifam.upr.edu.cu/\" target=\"_blank\">CIFAM</a>
                </li>
                </ul>
        </div>
                        <div class=\"col-md-4 col-sm-4 nopadding\">
            <ul>
                            <li>
                    <a href=\"http://cfores.upr.edu.cu/\" target=\"_blank\">CEFORES</a>
                </li>
                        <li>
                    <a href=\"http://relcoop.upr.edu.cu/\" target=\"_blank\">RELCOOP</a>
                </li>
                        <li>
                    <a href=\"http://pialpr.upr.edu.cu/\" target=\"_blank\">Proyecto PIALPR</a>
                </li>
                        <li>
                    <a href=\"http://www.ahs.pinarte.cult.cu/\" target=\"_blank\">Asociación Hermanos Saíz</a>
                </li>
                        <li>
                    <a href=\"http://www.granma.cu/discursos-raul\" target=\"_blank\">Discursos de Raúl</a>
                </li>
                        <li>
                    <a href=\"http://www.granma.cu/reflexiones-fidel\" target=\"_blank\">Reflexiones del compañero Fidel</a>
                </li>
                </ul>
        </div>
                        <div class=\"col-md-4 col-sm-4 nopadding\">
            <ul>
                            <li>
                    <a href=\"http://www.esceg.cu/\" target=\"_blank\">Escuela Superior de Cuadros del Estado y Gobierno</a>
                </li>
                        <li>
                    <a href=\"http://www.rguama.icrt.cu/\" target=\"_blank\">Radio Guamá</a>
                </li>
                        <li>
                    <a href=\"http://composer.upr.edu.cu/\" target=\"_blank\">Composer</a>
                </li>
                        <li>
                    <a href=\"http://mendive.upr.edu.cu/\" target=\"_blank\">Revista Mendive</a>
                </li>
                        <li>
                    <a href=\"http://podium.upr.edu.cu/\" target=\"_blank\"> Revista Podium</a>
                </li>
                </ul>
        </div>
            
                </div>
                    </div>
    </div>

    <div id=\"footer\" class=\"footer-style\">
  <div class=\"container\">
    <p><a href=\"http://elnodo.upr.edu.cu/\" target=\"_blank\">UPR</a> © 2016 | Contáctenos en <a href=\"mailto:wilfredo.lugo@nauta.cu\">wilfredo.lugo@nauta.cu</a></p>
  </div>
</div>
    <!-- End Footer Section -->

 </div> 

  <!-- End Full Body Container -->

  <!-- Go To Top Link -->
  <a href=\"#\" class=\"back-to-top\"><i class=\"fa fa-angle-up\"></i></a>

  <div id=\"loader\">
    <div class=\"spinner\">
      <div class=\"dot1\"></div>
      <div class=\"dot2\"></div>
    </div>
  </div>

  <!-- Style Switcher -->


  <script type=\"text/javascript\" src=\"";
        // line 868
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/js/script.js"), "html", null, true);
        echo "\"></script>
  
  <!-- <script src=\"asset/global/plugins/jquery.min.js\" type=\"text/javascript\"></script>-->
    <script src=\"";
        // line 871
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/asset/global/plugins/jquery-migrate.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
    
    
     <script src=\"";
        // line 874
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/asset/global/plugins/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>      
   <!-- <script src=\"asset/frontend/layout/scripts/back-to-top.js\" type=\"text/javascript\"></script> -->
    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script src=\"";
        // line 879
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/asset/global/plugins/fancybox/source/jquery.fancybox.pack.js\" type=\"text/javascript"), "html", null, true);
        echo "\"></script><!-- pop up--> 


    <script src=\"";
        // line 882
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/asset/frontend/layout/scripts/layout.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>


    <script src=\"";
        // line 885
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/highcharts.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>


  <script type=\"text/javascript\">
      jQuery(document).ready(function() {
          jQuery('#dataTables-example').DataTable({
              responsive: true
          });
      });
  </script>




</body>

</html>";
    }

    // line 172
    public function block_grafica($context, array $blocks = array())
    {
        // line 173
        echo "
    ";
    }

    // line 444
    public function block_evento($context, array $blocks = array())
    {
        // line 445
        echo "

                  ";
    }

    // line 612
    public function block_superacion($context, array $blocks = array())
    {
        // line 613
        echo "

        ";
    }

    public function getTemplateName()
    {
        return "layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1286 => 613,  1283 => 612,  1277 => 445,  1274 => 444,  1269 => 173,  1266 => 172,  1245 => 885,  1239 => 882,  1233 => 879,  1225 => 874,  1219 => 871,  1213 => 868,  959 => 616,  957 => 612,  939 => 596,  932 => 592,  925 => 587,  923 => 586,  912 => 578,  907 => 576,  902 => 574,  894 => 569,  887 => 565,  882 => 563,  876 => 560,  866 => 553,  861 => 551,  856 => 549,  844 => 540,  833 => 532,  823 => 524,  815 => 519,  807 => 515,  799 => 510,  796 => 509,  787 => 503,  784 => 502,  782 => 501,  775 => 499,  772 => 498,  770 => 497,  735 => 465,  728 => 461,  724 => 459,  717 => 455,  710 => 450,  708 => 449,  705 => 448,  703 => 444,  693 => 437,  688 => 435,  683 => 433,  675 => 428,  665 => 421,  660 => 419,  649 => 411,  640 => 405,  635 => 403,  630 => 401,  619 => 393,  604 => 381,  541 => 321,  533 => 316,  526 => 312,  519 => 308,  512 => 304,  475 => 269,  459 => 260,  451 => 255,  448 => 254,  439 => 248,  436 => 247,  433 => 246,  427 => 244,  425 => 243,  418 => 241,  413 => 239,  345 => 177,  341 => 176,  338 => 175,  336 => 172,  326 => 165,  321 => 163,  316 => 161,  311 => 159,  306 => 157,  301 => 155,  296 => 153,  291 => 151,  286 => 149,  281 => 147,  276 => 145,  270 => 142,  265 => 140,  260 => 138,  255 => 136,  250 => 134,  246 => 133,  239 => 129,  216 => 109,  211 => 107,  206 => 105,  200 => 102,  192 => 97,  187 => 95,  181 => 92,  173 => 87,  168 => 85,  163 => 83,  158 => 81,  153 => 79,  143 => 75,  138 => 73,  133 => 71,  128 => 69,  123 => 67,  118 => 65,  112 => 62,  106 => 59,  100 => 56,  94 => 53,  88 => 50,  82 => 47,  67 => 35,  63 => 34,  59 => 33,  55 => 32,  22 => 1,  668 => 543,  664 => 542,  612 => 493,  608 => 492,  591 => 478,  587 => 477,  570 => 463,  566 => 462,  493 => 392,  489 => 391,  471 => 376,  467 => 264,  446 => 357,  442 => 356,  360 => 277,  349 => 178,  337 => 260,  283 => 209,  264 => 193,  245 => 177,  222 => 156,  219 => 155,  213 => 146,  205 => 140,  194 => 137,  188 => 133,  184 => 132,  174 => 124,  172 => 123,  169 => 122,  166 => 121,  148 => 77,  141 => 100,  135 => 97,  131 => 96,  81 => 49,  74 => 45,  33 => 6,  30 => 5,);
    }
}

<?php

/* GEDELTURBundle:Default:login.html.twig */
class __TwigTemplate_219019d7deee87ebbbcbe95e5afd4ba2756b167b70958358502ccd5faed16b88 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>

    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">

    <title>Auntentificación</title>

    <!-- Bootstrap Core CSS -->
    <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/vocacional/bower_components/bootstrap/dist/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- MetisMenu CSS -->
    <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/vocacional/bower_components/metisMenu/dist/metisMenu.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- Custom CSS -->
    <link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/vocacional/dist/css/sb-admin-2.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- Custom Fonts -->
    <link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/vocacional/bower_components/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
    <![endif]-->

</head>

<body>

<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-4 col-md-offset-4\">
            <div class=\"login-panel panel panel-default\">
                <div  style=\"background-color: #26A6D1;\" class=\"panel-heading\">
                    <h3 style=\"color: #fff;\" class=\"panel-title\">GEDELTUR</h3>
                </div>
                <div class=\"panel-body\">
                    <form action=\"";
        // line 45
        echo $this->env->getExtension('routing')->getPath("login_check");
        echo "\" method=\"post\">
                        <fieldset>
                            <div class=\"form-group\">
                                <input class=\"form-control\" placeholder=\"Usuario\" name=\"_username\" type=\"TEXT\" autofocus for=\"username\" value=\"";
        // line 48
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : null), "html", null, true);
        echo "\">
                            </div>
                            <div class=\"form-group\">
                                <input class=\"form-control\" placeholder=\"Contraseña\" name=\"_password\" type=\"password\" value=\"\">

                               

                            </div>
                            <div class=\"checkbox\">
                                <label>
                                    <input name=\"_remember_me\" type=\"checkbox\" value=\"Remember Me\">Recordar
                                </label>

                            </div>
                            <!-- Change this to a button or input when using this as a form -->
                            <button class=\"btn btn-lg btn-success btn-block\" type=\"submit\">Aceptar</button>

                        </fieldset>
                    </form>

                    ";
        // line 68
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 69
            echo "                   <div  style=\"text-align: center;color: #ff0000\" class=\"error\">Usuario o Contraseña Incorrectos";
            echo "</div>

                        ";
        }
        // line 72
        echo "                </div>
            </div>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/vocacional/bower_components/jquery/dist/jquery.min.js"), "html", null, true);
        echo "\"></script>

<!-- Bootstrap Core JavaScript -->
<script src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/vocacional/bower_components/bootstrap/dist/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/vocacional/bower_components/metisMenu/dist/metisMenu.min.js"), "html", null, true);
        echo "\"></script>

<!-- Custom Theme JavaScript -->
<script src=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/vocacional/dist/js/sb-admin-2.js"), "html", null, true);
        echo "\"></script>

</body>

</html>
";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Default:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 88,  135 => 85,  129 => 82,  123 => 79,  114 => 72,  108 => 69,  106 => 68,  83 => 48,  77 => 45,  53 => 24,  47 => 21,  41 => 18,  35 => 15,  19 => 1,);
    }
}

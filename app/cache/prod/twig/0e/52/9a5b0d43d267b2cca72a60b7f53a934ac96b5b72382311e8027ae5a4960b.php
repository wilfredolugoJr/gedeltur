<?php

/* GEDELTURBundle:Default:principal.html.twig */
class __TwigTemplate_0e529a5b0d43d267b2cca72a60b7f53a934ac96b5b72382311e8027ae5a4960b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout.html.twig");

        $this->blocks = array(
            'grafica' => array($this, 'block_grafica'),
            'evento' => array($this, 'block_evento'),
            'superacion' => array($this, 'block_superacion'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_grafica($context, array $blocks = array())
    {
        // line 6
        echo "

            <script type=\"text/javascript\">
                \$(function () {
                    var chart;

                    \$(document).ready(function () {

                        // Build the chart
                        \$('#ciencia').highcharts({
                            chart: {
                                plotBackgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false
                            },
                            title: {
                                text: 'Categoría Científica'
                            },
                            tooltip: {
                                pointFormat: '{series.name}: <b>{point.y}</b>',
                                percentageDecimals: 1
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: false
                                    },
                                    showInLegend: true
                                }
                            },
                            series: [{
                                type: 'pie',
                                name: 'Cantidad',
                                data: [

                                    {
                                        name: 'Master',
                                        y: ";
        // line 45
        echo twig_escape_filter($this->env, (isset($context["master"]) ? $context["master"] : null), "html", null, true);
        echo ",
                                        sliced: true,
                                        selected: true
                                    },
                                    ['Doctores',   ";
        // line 49
        echo twig_escape_filter($this->env, (isset($context["doctor"]) ? $context["doctor"] : null), "html", null, true);
        echo "]

                                ]
                            }]
                        });
                    });

                });
            </script>




            <script type=\"text/javascript\">
                \$(function () {
                    var chart;

                    \$(document).ready(function () {

                        // Build the chart
                        \$('#docente').highcharts({
                            chart: {
                                plotBackgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false
                            },
                            title: {
                                text: 'Categoría Docente'
                            },
                            tooltip: {
                                pointFormat: '{series.name}: <b>{point.y}</b>',
                                percentageDecimals: 1
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: false
                                    },
                                    showInLegend: true
                                }
                            },
                            series: [{
                                type: 'pie',
                                name: 'Cantidad',
                                data: [
                                    ['Instructor',   ";
        // line 96
        echo twig_escape_filter($this->env, (isset($context["instructor"]) ? $context["instructor"] : null), "html", null, true);
        echo "],
                                    ['Auxiliar',       ";
        // line 97
        echo twig_escape_filter($this->env, (isset($context["auxiliar"]) ? $context["auxiliar"] : null), "html", null, true);
        echo "],
                                    {
                                        name: 'Titular',
                                        y: ";
        // line 100
        echo twig_escape_filter($this->env, (isset($context["titular"]) ? $context["titular"] : null), "html", null, true);
        echo ",
                                        sliced: true,
                                        selected: true
                                    },
                                     ['Asistente',       ";
        // line 104
        echo twig_escape_filter($this->env, (isset($context["asistente"]) ? $context["asistente"] : null), "html", null, true);
        echo "]

                                ]
                            }]
                        });
                    });

                });
            </script>




        ";
    }

    // line 121
    public function block_evento($context, array $blocks = array())
    {
        // line 122
        echo "
                ";
        // line 123
        if (((isset($context["cant_eventos"]) ? $context["cant_eventos"] : null) > 0)) {
            // line 124
            echo "




                <li><a href=\"#\" >Evento</a>

                 <ul class=\"dropdown\">
                 ";
            // line 132
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["eventos"]) ? $context["eventos"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 133
                echo "
                
                 
               
                  <li><a href=\"";
                // line 137
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("eventos", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "id"))), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "nombre"), "html", null, true);
                echo "</a>
                  </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 140
            echo "
                </ul>
                </li>
               

                ";
        }
        // line 146
        echo "

                  ";
    }

    // line 155
    public function block_superacion($context, array $blocks = array())
    {
        // line 156
        echo " 
 
 
 <div class=\"container\">
    <!-- Start Home Page Slider -->
    
    <section id=\"home\">
      <!-- Carousel -->
      <div id=\"main-slide\" class=\"carousel slide\" data-ride=\"carousel\">

        <!-- Indicators -->
        <ol class=\"carousel-indicators\">
          <li data-target=\"#main-slide\" data-slide-to=\"0\" class=\"active\"></li>
          <li data-target=\"#main-slide\" data-slide-to=\"1\"></li>
          <li data-target=\"#main-slide\" data-slide-to=\"2\"></li>
        </ol>
        <!--/ Indicators end-->

        <!-- Carousel inner -->
        <div class=\"carousel-inner\">
          <div class=\"item active\">
            <img class=\"img-responsive\" src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/images/slider/bg1.jpg"), "html", null, true);
        echo "\" alt=\"slider\">
            <div class=\"slider-content\">
              <div class=\"col-md-12 text-center\">
                <!-- <h2 class=\"animated2\">
                             <span>Bienbenidos a <strong>GEDELTUR</strong></span>
                              </h2>
                <h3 class=\"animated3\">
                                <span>Centro de </span>
                              </h3>-->
                <!-- <p class=\"animated4\"><a href=\"#\" class=\"slider btn btn-system btn-large\">Check Now</a>
                </p>-->
              </div>
            </div>
          </div>
          <!--/ Carousel item end -->
          <div class=\"item\">
            <img class=\"img-responsive\" src=\"";
        // line 193
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/images/slider/bg2.jpg"), "html", null, true);
        echo "\" alt=\"slider\">
            <div class=\"slider-content\">
              <div class=\"col-md-12 text-center\">
                <h2 class=\"animated4\">
                              <!--  <span><strong>GEDELTUR</strong> por el futuro</span>
                            </h2>
                <h3 class=\"animated5\">
                              <span>La llave del éxito</span>
                            </h3>
                <p class=\"animated6\"><a href=\"#\" class=\"slider btn btn-system btn-large\">Buy Now</a>
                </p>-->
              </div>
            </div>
          </div>
          <!--/ Carousel item end -->
          <div class=\"item\">
            <img class=\"img-responsive\" src=\"";
        // line 209
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/images/slider/bg3.jpg"), "html", null, true);
        echo "\" alt=\"slider\">
            <div class=\"slider-content\">
              <div class=\"col-md-12 text-center\">
                <h2 class=\"animated7 white\">
                           <!--     <span>El camino para <strong>triunfar</strong></span>
                            </h2>
                <h3 class=\"animated8 white\">
                              <span></span>
                            </h3>-->
               <!-- <div class=\"\">
                  <a class=\"animated4 slider btn btn-system btn-large btn-min-block\" href=\"#\">Get Now</a><a class=\"animated4 slider btn btn-default btn-min-block\" href=\"#\">Live Demo</a>
                </div>-->
              </div>
            </div>
          </div>

          
          <!--/ Carousel item end -->
        </div>
        <!-- Carousel inner end-->

        <!-- Controls -->
        <a class=\"left carousel-control\" href=\"#main-slide\" data-slide=\"prev\">
          <span><i class=\"fa fa-angle-left\"></i></span>
        </a>
        <a class=\"right carousel-control\" href=\"#main-slide\" data-slide=\"next\">
          <span><i class=\"fa fa-angle-right\"></i></span>
        </a>
      </div>
      <!-- /carousel -->
    
    
    
    
    
    
    <!-- End Home Page Slider -->
    <div id=\"content\">
    
      <div class=\"container\">
      
      
        <div class=\"page-content\">
    <div class=\"row\">
    
    <div class=\"big-title text-center animated fadeInDown delay-01\" data-animation=\"fadeInDown\" data-animation-delay=\"01\">
                        <h1>¿Quiénes Somos? <strong></strong></h1>
                    </div>

      <!-- Start Image Service Box 1 -->
      <div class=\"col-md-4 image-service-box\">
        <img class=\"img-thumbnail\" src=\"";
        // line 260
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/images/iconos/GEDELTUR.jpg"), "html", null, true);
        echo "\" alt=\"\">
        <h4>GEDELTUR</h4>
        <p>El Centro de Estudios de Gerencia, Desarrollo Local y Turismo (GEDELTUR) adscrito a la Facultad de Ciencias Económicas y Empresariales de la Universidad de Pinar del Río, surgió en el año 2003 .Participa activamente en redes internacionales .Se dirigen como promedio unos 15 proyectos de investigación e innovación de carácter nacional, institucional y empresarial.</p>

      </div>
      <!-- End Image Service Box 1 -->

      <!-- Start Image Service Box 2 -->
      <div class=\"col-md-4 image-service-box\">
        <img class=\"img-thumbnail\" src=\"";
        // line 269
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/images/iconos/Mision.jpg"), "html", null, true);
        echo "\" alt=\"\">
        <h4>Misión </h4>
        <p>Contribuir a la eficacia y eficiencia en la gestión empresarial y la administración pública, particularizando en el turismo, mediante la coordinación de las tareas relativas a las Funciones Estatales de “Dirección y control de la preparación y superación de dirigentes” e “Introducción de técnicas avanzadas de dirección” y la realización de actividades directas de capacitación, consultorías e investigaciones en las Ciencias Administrativas y en todas las esferas que contribuyan al desarrollo local, bajo los principios de la sustentabilidad con énfasis en el  Turismo.</p>
      </div>
      <!-- End Image Service Box 2 -->

      <!-- Start Image Service Box 3 -->
      <div class=\"col-md-4 image-service-box\">
        <img class=\"img-thumbnail\" src=\"";
        // line 277
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/principal/images/iconos/VISION.jpg"), "html", null, true);
        echo "\" alt=\"\">
        <h4>Visión </h4>
        <p>Posee una plantilla de 15 profesores de los cuales el 67% son doctores, el 20% Master y el 80% posee categoría docente superior de Titular o Auxiliar.
                            Brinda servicios científico – técnicos en cantidad (62%) y en financiamiento (68%) captado por este concepto. Participa activamente en redes internacionales tales como: 1) Unión Iberoamericana de Municipalitas; 2) Red de Desarrollo Sostenible y Medio Ambiente; 3) Red Latino Americana y del Caribe de Monitoreo y Evaluación de Políticas Públicas;etc.</p>
      </div>
      <!-- End Image Service Box 3 -->

    </div>


<!-- Graficas -->

    <div class=\"col-md-12  right-sidebar\">


                <div class=\"row\">
                </br>
                </br>
                </br>
                    <div class=\"big-title text-center animated fadeInDown delay-01\" data-animation=\"fadeInDown\" data-animation-delay=\"01\">
                        <h1> <strong>Nuestros Profesionales</strong></h1>
                    </div>
                    </br>
                    <!-- Start Service Icon 1 -->
                    <div class=\"col-md-6 col-sm-6 service-box service-center\" data-animation=\"fadeIn\" data-animation-delay=\"01\">
                       <div id=\"ciencia\" style=\"min-width: 400px; height: 300px; margin: 0 auto\"></div>
                    </div>
                    <!-- End Service Icon 1 -->

                    <!-- Start Service Icon 2 -->
                    <div class=\"col-md-6 col-sm-6 service-box service-center\" data-animation=\"fadeIn\" data-animation-delay=\"02\">
                        
                        
                       <div id=\"docente\" style=\"min-width: 400px; height: 300px; margin: 0 auto\"></div>
                    

                    </div>

                
            </div>
           
        </div>


<!--  End Graficas -->

        </div>
      </div>
      
      </section>














          <div id=\"superacion\">
        <div class=\"section service\">
            <div class=\"container\">

            <div class=\"col-md-12  right-sidebar\">


                <div class=\"row\">
                    <div class=\"big-title text-center animated fadeInDown delay-01\" data-animation=\"fadeInDown\" data-animation-delay=\"01\">
                        <h1>Ofrecemos <strong></strong></h1>
                    </div>
                    </br>
                    <!-- Start Service Icon 1 -->
                    <div class=\"col-md-4 col-sm-6 service-box service-center\" data-animation=\"fadeIn\" data-animation-delay=\"01\">
                        <div class=\"service-icon\">
                            <a href=\"";
        // line 356
        echo $this->env->getExtension('routing')->getPath("diplomados");
        echo "\" >
                             <img  class=\"img-responsive\" src=\"";
        // line 357
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/iconos/diplomados.png"), "html", null, true);
        echo "\" alt=\"slider\"></a>

                             <figcaption>Diplomados</figcaption>

                        </div>
                        <div class=\"service-content\">
                           
                           

                        </div>
                    </div>
                    <!-- End Service Icon 1 -->

                    <!-- Start Service Icon 2 -->
                    <div class=\"col-md-4 col-sm-6 service-box service-center\" data-animation=\"fadeIn\" data-animation-delay=\"02\">
                        

                        <div class=\"service-icon\">
                         <a href=\"";
        // line 375
        echo $this->env->getExtension('routing')->getPath("maestrias");
        echo "\" >
                            <img  class=\"img-responsive\" src=\"";
        // line 376
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/iconos/maestria.png"), "html", null, true);
        echo "\" alt=\"slider\">
                            </a>

                            <figcaption>Maestrías</figcaption>
                        </div>
                        <div class=\"service-content\">
                            
                           
                        </div>
                    </div>
                    <!-- End Service Icon 2 -->

                    <!-- Start Service Icon 3 -->
                    <div class=\"col-md-4 col-sm-6 service-box service-center\" data-animation=\"fadeIn\" data-animation-delay=\"03\">
                        <div class=\"service-icon\">
                         <a href=\"";
        // line 391
        echo $this->env->getExtension('routing')->getPath("especialidades");
        echo "\" >
                          <img class=\"img-responsive\" src=\"";
        // line 392
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/iconos/especialidad.png"), "html", null, true);
        echo "\" alt=\"slider\">
                          </a>
                        <figcaption>Especialidades</figcaption>
                        </div>
                       
                        <div class=\"service-content\">
                           
                           
                        </div>
                    </div>

                    </br>











                    <!-- End Service Icon 3 -->

                    <!-- Start Service Icon 4 -->

                    <!-- End Service Icon 4 -->

                    <!-- Start Service Icon 5 -->

                    <!-- End Service Icon 5 -->

                    <!-- Start Service Icon 6 -->

                    <!-- End Service Icon 6 -->

                    <!-- Start Service Icon 7 -->

                    <!-- End Service Icon 7 -->

                    <!-- Start Service Icon 8 -->

                    <!-- End Service Icon 8 -->

                </div>




                <div class=\"row\">

                    </br>
                    <!-- Start Service Icon 1 -->

                    <!-- End Service Icon 1 -->

                    <!-- Start Service Icon 2 -->

                    <!-- End Service Icon 2 -->

                    <!-- Start Service Icon 3 -->


                    </br>



                    <div class=\"col-md-4 col-sm-6 service-box service-center\" data-animation=\"fadeIn\" data-animation-delay=\"01\">
            <div class=\"service-icon\">
              <a href=\"";
        // line 462
        echo $this->env->getExtension('routing')->getPath("sct_filtro", array("tipo" => "capacitacion"));
        echo "\" >
             <img class=\"img-responsive\" src=\"";
        // line 463
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/iconos/capacitacion.png"), "html", null, true);
        echo "\" alt=\"slider\">
             </a>

             <figcaption>Capacitaciones</figcaption>
            </div>
            <div class=\"service-content\">



            </div>
          </div>

           <div class=\"col-md-4 col-sm-6 service-box service-center\" data-animation=\"fadeIn\" data-animation-delay=\"02\">
            <div class=\"service-icon\">
             <a href=\"";
        // line 477
        echo $this->env->getExtension('routing')->getPath("sct_filtro", array("tipo" => "asesoría"));
        echo "\" >
             <img class=\"img-responsive\" src=\"";
        // line 478
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/iconos/asesoria.png"), "html", null, true);
        echo "\" alt=\"slider\">
              </a>

              <figcaption>Asesorías</figcaption>
            </div>
            <div class=\"service-content\">


            </div>
          </div>


          <div class=\"col-md-4 col-sm-6 service-box service-center\" data-animation=\"fadeIn\" data-animation-delay=\"03\">
            <div class=\"service-icon\">
              <a href=\"";
        // line 492
        echo $this->env->getExtension('routing')->getPath("sct_filtro", array("tipo" => "consultoría"));
        echo "\" >
             <img  class=\"img-responsive\" src=\"";
        // line 493
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/iconos/consultoria.jpg"), "html", null, true);
        echo "\" alt=\"slider\">
               </a>
                 <figcaption>Consultorías</figcaption>
            </div>
            <div class=\"service-content\">


            </div>
          </div>



                    <!-- End Service Icon 3 -->

                    <!-- Start Service Icon 4 -->

                    <!-- End Service Icon 4 -->

                    <!-- Start Service Icon 5 -->

                    <!-- End Service Icon 5 -->

                    <!-- Start Service Icon 6 -->

                    <!-- End Service Icon 6 -->

                    <!-- Start Service Icon 7 -->

                    <!-- End Service Icon 7 -->

                    <!-- Start Service Icon 8 -->

                    <!-- End Service Icon 8 -->

                </div>








                <div class=\"row\">
                   </br>
                    </br>
                    <!-- Start Service Icon 1 -->
                    <div class=\"col-md-4 col-sm-6 service-box service-center\" data-animation=\"fadeIn\" data-animation-delay=\"01\">
                        <div class=\"service-icon\">
                            <a href=\"";
        // line 542
        echo $this->env->getExtension('routing')->getPath("cursos");
        echo "\" >
                             <img  class=\"img-responsive\" src=\"";
        // line 543
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/iconos/cursos.jpg"), "html", null, true);
        echo "\" alt=\"slider\"></a>

                             <figcaption>Cursos</figcaption>

                        </div>
                        <div class=\"service-content\">



                        </div>
                    </div>
                    <!-- End Service Icon 1 -->

                    <!-- Start Service Icon 2 -->

                    <!-- End Service Icon 2 -->

                    <!-- Start Service Icon 3 -->


                    </br>











                    <!-- End Service Icon 3 -->

                    <!-- Start Service Icon 4 -->

                    <!-- End Service Icon 4 -->

                    <!-- Start Service Icon 5 -->

                    <!-- End Service Icon 5 -->

                    <!-- Start Service Icon 6 -->

                    <!-- End Service Icon 6 -->

                    <!-- Start Service Icon 7 -->

                    <!-- End Service Icon 7 -->

                    <!-- Start Service Icon 8 -->

                    <!-- End Service Icon 8 -->

                </div>








                
            </div>
            <!-- .container -->
        </div>
        </div>
        </div>
        
        
        
        






         
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        


















      </div>
    
     </div>

\t   



\t  

 
 
  ";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Default:principal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  668 => 543,  664 => 542,  612 => 493,  608 => 492,  591 => 478,  587 => 477,  570 => 463,  566 => 462,  493 => 392,  489 => 391,  471 => 376,  467 => 375,  446 => 357,  442 => 356,  360 => 277,  349 => 269,  337 => 260,  283 => 209,  264 => 193,  245 => 177,  222 => 156,  219 => 155,  213 => 146,  205 => 140,  194 => 137,  188 => 133,  184 => 132,  174 => 124,  172 => 123,  169 => 122,  166 => 121,  148 => 104,  141 => 100,  135 => 97,  131 => 96,  81 => 49,  74 => 45,  33 => 6,  30 => 5,);
    }
}

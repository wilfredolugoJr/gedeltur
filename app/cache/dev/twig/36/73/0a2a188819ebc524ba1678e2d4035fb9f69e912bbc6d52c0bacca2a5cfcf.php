<?php

/* GEDELTURBundle:Curso:new.html.twig */
class __TwigTemplate_36730a2a188819ebc524ba1678e2d4035fb9f69e912bbc6d52c0bacca2a5cfcf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:plantilla.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:plantilla.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "    Nuevo Curso
";
    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"bs-example4\" >

    
 ";
        // line 11
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
    
<div class=\"input-group\">
         <div class=\"col-sm-2 control-label\">";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nombre"), 'label');
        echo "</div>

        <div class=\"col-sm-8\"><div class=\"input-group\">
                                    <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
            ";
        // line 23
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nombre"), 'widget', array("attr" => array("class" => "form-control1")));
        echo "
        </div>
        </div>
    <div style=\"float: right\" class=\"col-sm-6\">

    <p class=\"help-block\">";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nombre"), 'errors');
        echo "</p>
        </div>
    </div>


     <div class=\"input-group\">
         <div class=\"col-sm-2 control-label\"> ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "descripcion"), 'label', array("label" => "Objetivos"));
        echo "</div>
         <div class=\"col-sm-8\"><div class=\"input-group\">
                                    <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
             ";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "objetivos"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
         </div>
         </div>
         <div style=\"float: right\" class=\"col-sm-6\">

         <p class=\"help-block\">";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "objetivos"), 'errors');
        echo "</p>
         </div>

     </div>



    <div class=\"input-group\">
       <div class=\"col-sm-2 control-label\"> ";
        // line 54
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "descripcion"), 'label', array("label" => "Descripción"));
        echo "</div>
        <div class=\"col-sm-8\"><div class=\"input-group\">
                                    <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
        ";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "descripcion"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
            </div>
            </div>
        <div style=\"float: right\" class=\"col-sm-6\">

        <p class=\"help-block\">";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "descripcion"), 'errors');
        echo "</p>
        </div>

    </div>




     <div class=\"input-group\">
         <div class=\"col-sm-2 control-label\"> ";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plantilla"), 'label', array("label" => "Docente"));
        echo "</div>
         <div class=\"col-sm-3\"><div class=\"input-group\">
                                    <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
             ";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plantilla"), 'widget', array("attr" => array("class" => "form-control js-example-basic-single")));
        echo "
         </div>
         </div>
         <div style=\"float: right\" class=\"col-sm-6\">

         <p class=\"help-block\">";
        // line 87
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plantilla"), 'errors');
        echo "</p>
         </div>

     </div>






   

    


    <div class=\"panel-footer\">
    <div class=\"row\">
    <div class=\"col-sm-8 col-sm-offset-2\">
        ";
        // line 105
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "submit"), 'widget', array("attr" => array("class" => "btn btn-primary")));
        echo "
       
        <a href=\"";
        // line 107
        echo $this->env->getExtension('routing')->getPath("curso");
        echo "\">
            <button type=\"button\" class=\"btn btn-warning warning_22\">Cancelar</button>
        </a>
    </div>
    </div>
    </div>



    ";
        // line 116
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "


     <script type=\"text/javascript\">
         \$(document).ready(function() {
             \$(\".js-example-basic-single\").select2();
         });
     </script>

       <script type=\"text/javascript\">
         \$(function () {
        \$('#gedeltur_bundle_curso_nombre').keydown(letras);
        \$('#gedeltur_bundle_curso_descripcion').keydown(let);
             \$('#gedeltur_bundle_curso_objetivos').keydown(let);

        
       

})
</script> 
";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Curso:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 116,  186 => 107,  181 => 105,  160 => 87,  152 => 82,  142 => 75,  130 => 66,  122 => 61,  112 => 54,  101 => 46,  93 => 41,  83 => 34,  74 => 28,  66 => 23,  55 => 15,  49 => 12,  45 => 11,  40 => 8,  37 => 7,  32 => 4,  29 => 3,);
    }
}

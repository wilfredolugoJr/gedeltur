<?php

/* @WebProfiler/Profiler/base_js.html.twig */
class __TwigTemplate_3ade83818bbe39dcd5ad1fc739a9ae677c0aad26bab1992532975b2891e2d55e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script>/*<![CDATA[*/
    Sfjs = (function() {
        \"use strict\";

        var noop = function() {},

            profilerStorageKey = 'sf2/profiler/',

            request = function(url, onSuccess, onError, payload, options) {
                var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
                options = options || {};
                options.maxTries = options.maxTries || 0;
                xhr.open(options.method || 'GET', url, true);
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.onreadystatechange = function(state) {
                    if (4 !== xhr.readyState) {
                        return null;
                    }

                    if (xhr.status == 404 && options.maxTries > 1) {
                        setTimeout(function(){
                            options.maxTries--;
                            request(url, onSuccess, onError, payload, options);
                        }, 500);

                        return null;
                    }

                    if (200 === xhr.status) {
                        (onSuccess || noop)(xhr);
                    } else {
                        (onError || noop)(xhr);
                    }
                };
                xhr.send(payload || '');
            },

            hasClass = function(el, klass) {
                return el.className && el.className.match(new RegExp('\\\\b' + klass + '\\\\b'));
            },

            removeClass = function(el, klass) {
                if (el.className) {
                    el.className = el.className.replace(new RegExp('\\\\b' + klass + '\\\\b'), ' ');
                }
            },

            addClass = function(el, klass) {
                if (!hasClass(el, klass)) {
                    el.className += \" \" + klass;
                }
            },

            getPreference = function(name) {
                if (!window.localStorage) {
                    return null;
                }

                return localStorage.getItem(profilerStorageKey + name);
            },

            setPreference = function(name, value) {
                if (!window.localStorage) {
                    return null;
                }

                localStorage.setItem(profilerStorageKey + name, value);
            };

        return {
            hasClass: hasClass,

            removeClass: removeClass,

            addClass: addClass,

            getPreference: getPreference,

            setPreference: setPreference,

            request: request,

            load: function(selector, url, onSuccess, onError, options) {
                var el = document.getElementById(selector);

                if (el && el.getAttribute('data-sfurl') !== url) {
                    request(
                        url,
                        function(xhr) {
                            el.innerHTML = xhr.responseText;
                            el.setAttribute('data-sfurl', url);
                            removeClass(el, 'loading');
                            (onSuccess || noop)(xhr, el);
                        },
                        function(xhr) { (onError || noop)(xhr, el); },
                        '',
                        options
                    );
                }

                return this;
            },

            toggle: function(selector, elOn, elOff) {
                var i,
                    style,
                    tmp = elOn.style.display,
                    el = document.getElementById(selector);

                elOn.style.display = elOff.style.display;
                elOff.style.display = tmp;

                if (el) {
                    el.style.display = 'none' === tmp ? 'none' : 'block';
                }

                return this;
            }
        }
    })();
/*]]>*/</script>
";
    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/base_js.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  91 => 35,  83 => 30,  75 => 28,  70 => 26,  50 => 15,  42 => 12,  26 => 3,  24 => 2,  19 => 1,  606 => 349,  601 => 307,  596 => 306,  591 => 303,  586 => 157,  581 => 144,  575 => 69,  572 => 68,  567 => 27,  560 => 350,  558 => 349,  551 => 345,  547 => 344,  539 => 339,  506 => 308,  503 => 307,  501 => 306,  495 => 303,  472 => 283,  467 => 281,  458 => 275,  452 => 272,  446 => 269,  436 => 262,  430 => 259,  424 => 256,  418 => 253,  406 => 244,  399 => 240,  386 => 230,  380 => 227,  374 => 224,  360 => 213,  350 => 206,  342 => 201,  334 => 196,  326 => 191,  317 => 185,  311 => 182,  305 => 179,  283 => 160,  280 => 159,  277 => 158,  275 => 157,  266 => 151,  256 => 144,  237 => 128,  231 => 125,  225 => 122,  219 => 119,  206 => 109,  197 => 103,  188 => 97,  161 => 72,  159 => 68,  150 => 62,  141 => 56,  136 => 54,  132 => 53,  128 => 52,  123 => 50,  119 => 49,  115 => 48,  111 => 47,  107 => 46,  102 => 44,  81 => 28,  79 => 29,  73 => 24,  66 => 25,  62 => 24,  58 => 18,  49 => 15,  45 => 14,  40 => 12,  27 => 1,  51 => 23,  38 => 17,  32 => 6,  29 => 14,  205 => 107,  196 => 104,  193 => 103,  189 => 102,  183 => 98,  169 => 90,  160 => 86,  149 => 78,  142 => 74,  134 => 69,  122 => 60,  117 => 58,  113 => 57,  108 => 55,  104 => 54,  98 => 51,  94 => 39,  90 => 49,  86 => 47,  82 => 46,  57 => 25,  54 => 17,  46 => 14,  36 => 6,  33 => 5,  30 => 5,);
    }
}

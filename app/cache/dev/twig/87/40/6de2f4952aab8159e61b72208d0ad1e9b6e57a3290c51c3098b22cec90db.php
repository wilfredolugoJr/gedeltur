<?php

/* base.html.twig */
class __TwigTemplate_87406de2f4952aab8159e61b72208d0ad1e9b6e57a3290c51c3098b22cec90db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascript' => array($this, 'block_javascript'),
            'user' => array($this, 'block_user'),
            'usuario' => array($this, 'block_usuario'),
            'title' => array($this, 'block_title'),
            'boton' => array($this, 'block_boton'),
            'body' => array($this, 'block_body'),
            'js' => array($this, 'block_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!DOCTYPE HTML>
<html>
<head>
    <title>GEDELTUR</title>
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    <meta name=\"keywords\" content=\"Modern Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design\" />
    <script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Bootstrap Core CSS -->
    <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/css/style.css"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
    <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/css/font-awesome.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/css/dataTables.bootstrap.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/css/dataTables.responsive.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/css/font-awesome.min.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/css/bootstrap-datetimepicker.css"), "html", null, true);
        echo "\" />



    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/dist/css/select2.min.css"), "html", null, true);
        echo "\" />

    
    ";
        // line 27
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 28
        echo "



    





    <!-- jQuery -->
    <script src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <!----webfonts--->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
    <!---//webfonts--->
    <!-- Bootstrap Core JavaScript -->
    <script src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <!----Calender -------->
    <link rel=\"stylesheet\" href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/css/clndr.css"), "html", null, true);
        echo "\" type=\"text/css\" />
    <script src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/underscore-min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
    <script src= \"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/moment-2.2.1.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
    <script src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/clndr.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
    <script src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/site.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

    <script src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/dataTables.bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/moment-with-locales.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/bootstrap-datetimepicker.js"), "html", null, true);
        echo "\"></script>





    <script src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/dist/js/select2.min.js"), "html", null, true);
        echo "\"></script>




 
";
        // line 68
        $this->displayBlock('javascript', $context, $blocks);
        // line 72
        echo "









     
            <!----End Calender -------->
</head>
<body>
<div id=\"wrapper\">
    <!-- Navigation -->
    <nav class=\"top1 navbar navbar-default navbar-static-top\" role=\"navigation\" style=\"margin-bottom: 0\">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>

            <a title=\"Centro de Estudios de Gerencia, Desarrollo Local y Turismo\" class=\"navbar-brand\" href=\"";
        // line 97
        echo $this->env->getExtension('routing')->getPath("gedeltur_homepage");
        echo "\">GEDELTUR</a>

        </div>
        <!-- /.navbar-header -->
        <ul class=\"nav navbar-top-links navbar-right\">

        <li><a href=\"";
        // line 103
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\"><span class=\"glyphicon glyphicon-home\"></span>
                    Inicio</a>
            </li>

 <li class=\"dropdown\">

                      <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"";
        // line 109
        echo $this->env->getExtension('routing')->getPath("gedeltur_homepage");
        echo "\">Reportes<span class=\"caret\"></span></a>



                    
                    <ul class=\"dropdown-menu\">



             <li>
                            <a href=\"";
        // line 119
        echo $this->env->getExtension('routing')->getPath("sct_entidad");
        echo "\">Servicios Científico Técnicos brindados por Institución</a>
                        </li>
                        <li>
                            <a href=\"";
        // line 122
        echo $this->env->getExtension('routing')->getPath("ws_sct");
        echo "\">Trabajadores por Servicio Científico Técnico </a>
                        </li>
                        <li>
                            <a href=\"";
        // line 125
        echo $this->env->getExtension('routing')->getPath("ws_li");
        echo "\">Trabajadores por Líneas de investigación</a>
                        </li>
                        <li>
                            <a href=\"";
        // line 128
        echo $this->env->getExtension('routing')->getPath("ws_pro");
        echo "\">Trabajadores por Proyectos</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>

            


               
                <!-- /.dropdown -->
                
                <!-- /.dropdown -->
                <li class=\"dropdown\">

                    <a aria-expanded=\"false\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                         ";
        // line 144
        $this->displayBlock('user', $context, $blocks);
        echo "<i class=\"fa fa-user fa-fw\"></i> <i class=\"fa fa-caret-down\"></i>
                    </a>
                    <ul class=\"dropdown-menu dropdown-user\">



                        
                        ";
        // line 151
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_FULLY")) {
            echo " 
 
 
 

                       
                        ";
            // line 157
            $this->displayBlock('usuario', $context, $blocks);
            // line 158
            echo "                         ";
        }
        // line 159
        echo "
                          <li><a href=\"";
        // line 160
        echo $this->env->getExtension('routing')->getPath("logout");
        echo "\"><i class=\"fa fa-sign-out fa-fw\"></i> Salir</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
         <!--<form class=\"navbar-form navbar-right\">
            <input type=\"text\" class=\"form-control\" value=\"Search...\" onfocus=\"this.value = '';\" onblur=\"if (this.value == '') {this.value = 'Search...';}\">
        </form> -->
        <div class=\"navbar-default sidebar\" role=\"navigation\">
            <div class=\"sidebar-nav navbar-collapse\">
                <ul class=\"nav\" id=\"side-menu\">
                    
                    <li>
                        <a href=\"#\">Recursos Humanos<span class=\"fa arrow\"></span></a>
                        <ul class=\"nav nav-second-level\">
                            
                             <li>
                                <a href=\"";
        // line 179
        echo $this->env->getExtension('routing')->getPath("plantilla");
        echo "\" >Trabajadores</a>
                            </li>
                            <li>
                                <a href=\"";
        // line 182
        echo $this->env->getExtension('routing')->getPath("gce");
        echo "\">Estudiantes</a>
                            </li>
                            <li>
                                <a href=\"";
        // line 185
        echo $this->env->getExtension('routing')->getPath("colaborador");
        echo "\">Colaboradores</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href=\"";
        // line 191
        echo $this->env->getExtension('routing')->getPath("contacto");
        echo "\">Contactos</a>
                        
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href=\"";
        // line 196
        echo $this->env->getExtension('routing')->getPath("lineainvestigacion");
        echo "\">Líneas de Investigación</a>

                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a class=\"active\" href=\"";
        // line 201
        echo $this->env->getExtension('routing')->getPath("proyecto");
        echo "\">Proyectos</a>
                        
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href=\"";
        // line 206
        echo $this->env->getExtension('routing')->getPath("curso");
        echo "\">Cursos</a>
                    </li>
                    
                    <li>
                        <a href=\"#\">Pregrado<span class=\"fa arrow\"></span></a>
                        <ul class=\"nav nav-second-level\">
                            <li>
                                <a href=\"";
        // line 213
        echo $this->env->getExtension('routing')->getPath("asignatura");
        echo "\">Asignaturas</a>
                            </li>
                            
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <li>
                        <a href=\"#\">Postgrado<span class=\"fa arrow\"></span></a>
                        <ul class=\"nav nav-second-level\">
                            <li>
                                <a href=\"";
        // line 224
        echo $this->env->getExtension('routing')->getPath("especialidad");
        echo "\">Especialidades</a>
                            </li>
                            <li>
                                <a href=\"";
        // line 227
        echo $this->env->getExtension('routing')->getPath("diplomado");
        echo "\">Diplomados</a>
                            </li>
                             <li>
                                <a href=\"";
        // line 230
        echo $this->env->getExtension('routing')->getPath("maestria");
        echo "\">Maestrías</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <li>
                        <a href=\"#\">Servicios Científico Técnicos<span class=\"fa arrow\"></span></a>
                        <ul class=\"nav nav-second-level\">
                             <li>
                                <a href=\"";
        // line 240
        echo $this->env->getExtension('routing')->getPath("servicioscienticotecnicos");
        echo "\">Servicios</a>
                            </li>
                           
                            <li>
                                <a href=\"";
        // line 244
        echo $this->env->getExtension('routing')->getPath("tematicasct");
        echo "\">Temáticas</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href=\"#\">Eventos<span class=\"fa arrow\"></span></a>
                        <ul class=\"nav nav-second-level\">
                            <li>
                                <a href=\"";
        // line 253
        echo $this->env->getExtension('routing')->getPath("norma");
        echo "\">Normas</a>
                            </li>
                            <li>
                                <a href=\"";
        // line 256
        echo $this->env->getExtension('routing')->getPath("tematica");
        echo "\">Temáticas</a>
                            </li>
                            <li>
                                <a href=\"";
        // line 259
        echo $this->env->getExtension('routing')->getPath("taller");
        echo "\">Taller</a>
                            </li>
                            <li>
                                <a href=\"";
        // line 262
        echo $this->env->getExtension('routing')->getPath("evento");
        echo "\">Convocatoria</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href=\"";
        // line 269
        echo $this->env->getExtension('routing')->getPath("institucion");
        echo "\">Relaciones Institucionales<span class=\"fa arrow\"></span></a>
                        <ul class=\"nav nav-second-level\">
                            <li>
                                <a href=\"";
        // line 272
        echo $this->env->getExtension('routing')->getPath("institucion");
        echo "\">Instituciones</a>
                            </li>
                             <li>
                                <a href=\"";
        // line 275
        echo $this->env->getExtension('routing')->getPath("otrasrelaciones");
        echo "\">Otras Relaciones</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <li><a href=\"";
        // line 281
        echo $this->env->getExtension('routing')->getPath("usuarios");
        echo "\">Usuarios</a>
                        </li>
                    <li><a href=\"";
        // line 283
        echo $this->env->getExtension('routing')->getPath("correo");
        echo "\"> Foro</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>


    <div id=\"page-wrapper\" >
        
        <div class=\"col-md-12 graphs\">
            <div class=\"xs\">


                   
                   
                <div class=\"panel panel-default\">
                    <!-- Default panel contents -->
                    <div class=\"panel-heading\"> ";
        // line 303
        $this->displayBlock('title', $context, $blocks);
        echo "     </div>
                        <div class=\"bs-example4\" >

                         ";
        // line 306
        $this->displayBlock('boton', $context, $blocks);
        // line 307
        echo "                        ";
        $this->displayBlock('body', $context, $blocks);
        // line 308
        echo "                        


                        </div>
                    </div>
                </div>

            </div>

           
           <div class=\"copy_layout\">
            <p>UPR © 2016 GEDELTUR |  Contáctenos en <a href=\"mailto:wilfredo.lugo@nauta.cu\" target=\"_blank\">wilfredo.lugo@nauta.cu</a> </p>
           </div>


        </div>



 
    </div>

          
           
    
</div>


<!-- /#wrapper -->
<!-- Nav CSS -->

<link href=\"";
        // line 339
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/css/custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">


<!-- Metis Menu Plugin JavaScript -->

<script src=\"";
        // line 344
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/metisMenu.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 345
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/js/custom.js"), "html", null, true);
        echo "\"></script>



";
        // line 349
        $this->displayBlock('js', $context, $blocks);
        // line 350
        echo "</body>
</html>

";
    }

    // line 27
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 68
    public function block_javascript($context, array $blocks = array())
    {
        // line 69
        echo "

 ";
    }

    // line 144
    public function block_user($context, array $blocks = array())
    {
    }

    // line 157
    public function block_usuario($context, array $blocks = array())
    {
    }

    // line 303
    public function block_title($context, array $blocks = array())
    {
    }

    // line 306
    public function block_boton($context, array $blocks = array())
    {
    }

    // line 307
    public function block_body($context, array $blocks = array())
    {
    }

    // line 349
    public function block_js($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  606 => 349,  601 => 307,  596 => 306,  591 => 303,  586 => 157,  581 => 144,  575 => 69,  572 => 68,  567 => 27,  560 => 350,  558 => 349,  551 => 345,  547 => 344,  539 => 339,  506 => 308,  503 => 307,  501 => 306,  495 => 303,  472 => 283,  467 => 281,  458 => 275,  452 => 272,  446 => 269,  436 => 262,  430 => 259,  424 => 256,  418 => 253,  406 => 244,  399 => 240,  386 => 230,  380 => 227,  374 => 224,  360 => 213,  350 => 206,  342 => 201,  334 => 196,  326 => 191,  317 => 185,  311 => 182,  305 => 179,  283 => 160,  280 => 159,  277 => 158,  275 => 157,  266 => 151,  256 => 144,  237 => 128,  231 => 125,  225 => 122,  219 => 119,  206 => 109,  197 => 103,  188 => 97,  161 => 72,  159 => 68,  150 => 62,  141 => 56,  136 => 54,  132 => 53,  128 => 52,  123 => 50,  119 => 49,  115 => 48,  111 => 47,  107 => 46,  102 => 44,  94 => 39,  81 => 28,  79 => 27,  73 => 24,  66 => 20,  62 => 19,  58 => 18,  54 => 17,  49 => 15,  45 => 14,  40 => 12,  27 => 1,);
    }
}

<?php

/* GEDELTURBundle:Colaborador:new.html.twig */
class __TwigTemplate_1bdae07997cac66dfe09ff64f411c9c69ac1dd9d246467697b19a3019d51e4fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:plantilla.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:plantilla.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "    Nuevo Colaborador 
";
    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        // line 8
        echo "<div class=\"bs-example4\" >
  
    
";
        // line 11
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
    


    
 <div class=\"input-group\">
         <div class=\"col-sm-2 control-label\">";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "ci"), 'label', array("label" => "Carné de Identidad"));
        echo "</div>

        <div class=\"col-sm-8\">
            <div class=\"input-group\">
                                    <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
            ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "ci"), 'widget', array("attr" => array("class" => "form-control1")));
        echo "
        </div>
        </div>
        <div style=\"float: right;\"  class=\"col-sm-6\">
            <p class=\"help-block\">";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "ci"), 'errors');
        echo "</p>
        </div>
    </div>



    <div class=\"input-group\">
       <div class=\"col-sm-2 control-label\"> ";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "nombre"), 'label');
        echo "</div>
        <div class=\"col-sm-8\">
            <div class=\"input-group\">
                                    <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
        ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "nombre"), 'widget', array("attr" => array("class" => "form-control1")));
        echo "
            </div>
            </div>
        <div style=\"float: right\" class=\"col-sm-6\">

        <p class=\"help-block\">";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "nombre"), 'errors');
        echo "</p>
        </div>

    </div>



    <div class=\"input-group\">
        <div class=\"col-sm-2 control-label\">";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "apellidos"), 'label', array("label" => "Apellidos"));
        echo "</div>

        <div class=\"col-sm-8\">
            <div class=\"input-group\">
                                    <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
            ";
        // line 68
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "apellidos"), 'widget', array("attr" => array("class" => "form-control1")));
        echo "
        </div>
        </div>

        <div style=\"float: right\" class=\"col-sm-6\">

        <p class=\"help-block\"> ";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "apellidos"), 'errors');
        echo "</p>
        </div>
    </div>


    <div class=\"input-group\">
        <div class=\"col-sm-2 control-label\"> ";
        // line 80
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "direccion"), 'label', array("label" => "Dirección"));
        echo "</div>
        <div class=\"col-sm-8\">
            <div class=\"input-group\">
                                    <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
            ";
        // line 88
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "direccion"), 'widget', array("attr" => array("class" => "form-control1")));
        echo "
        </div>
        </div>
        <div style=\"float: right\" class=\"col-sm-6\">

        <p class=\"help-block\">  ";
        // line 93
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "direccion"), 'errors');
        echo "</p>
        </div>
    </div>


    <div class=\"input-group\">
       <div class=\"col-sm-2 control-label\"> ";
        // line 99
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "image"), 'label', array("label" => "Imagen"));
        echo "</div>


        <div class=\"col-sm-8\">
            ";
        // line 103
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "image"), 'widget');
        echo "
            <p class=\"help-block\">No debe exceder de los 500 kb.</p>
        </div>
        <div style=\"float: right\" class=\"col-sm-6\">

        <p class=\"help-block\">   ";
        // line 108
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "image"), 'errors');
        echo "</p>
        </div>

    </div>





    <div class=\"input-group\">
       <div class=\"col-sm-2 control-label\"> ";
        // line 118
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "centro"), 'label');
        echo "</div>
        <div class=\"col-sm-8\">
            <div class=\"input-group\">
                                    <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
        ";
        // line 126
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "centro"), 'widget', array("attr" => array("class" => "form-control1")));
        echo "
            </div>
            </div>
        <div style=\"float: right\" class=\"col-sm-6\">

        <p class=\"help-block\">";
        // line 131
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "centro"), 'errors');
        echo "</p>
        </div>

    </div>

     <div class=\"input-group\">
         <div class=\"col-sm-2 control-label\"> ";
        // line 137
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cat_doc"), 'label', array("label" => "Categoría Docente"));
        echo "</div>
         <div class=\"col-sm-3\">
             <div class=\"input-group\">
                                    <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
             ";
        // line 145
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cat_doc"), 'widget', array("attr" => array("class" => "form-control1 js-example-basic-single")));
        echo "
         </div>
         </div>
         <div class=\"col-sm-2 control-label\"> ";
        // line 148
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cat_cien"), 'label', array("label" => "Categoría Científica"));
        echo "</div>
         <div class=\"col-sm-3\">
             <div class=\"input-group\">
                                    <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
             ";
        // line 156
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cat_cien"), 'widget', array("attr" => array("class" => "form-control1 js-example-basic-singles")));
        echo "
         </div>
         </div>

     </div>



    

    <div class=\"panel-footer\">
    <div class=\"row\">
    <div class=\"col-sm-8 col-sm-offset-2\">

        ";
        // line 170
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "submit"), 'widget', array("attr" => array("class" => "btn btn-primary")));
        echo "
       
        <a href=\"";
        // line 172
        echo $this->env->getExtension('routing')->getPath("colaborador");
        echo "\">
            <button type=\"button\" class=\"btn btn-warning warning_22\">Cancelar</button>
        </a>
    </div>
    </div>
    </div>


<script type=\"text/javascript\">
         \$(function () {
        \$('#gedeltur_bundle_colaborador_centro').keydown(letras);

        \$('#gedeltur_bundle_colaborador_id_ci').keydown(ci);
         \$('#gedeltur_bundle_colaborador_id_nombre').keydown(letras);
           \$('#gedeltur_bundle_colaborador_id_apellidos').keydown(letras);

           \$('#gedeltur_bundle_colaborador_id_direccion').keydown(adress);
       

})
</script>

<div class=\"alert alert-success\">
        ";
        // line 195
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msg"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 196
            echo "            <div class=\"msg\">
                ";
            // line 197
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 200
        echo "    </div>
    ";
        // line 201
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "



     <script type=\"text/javascript\">
         \$(document).ready(function() {
             \$(\".js-example-basic-single\").select2();
         });
     </script>

     <script type=\"text/javascript\">
         \$(document).ready(function() {
             \$(\".js-example-basic-singles\").select2();
         });
     </script>

     <script type=\"text/javascript\">
         \$(document).ready(function() {
             \$(\".js-example-basic-singless\").select2();
         });
     </script>
       
";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Colaborador:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  326 => 201,  323 => 200,  314 => 197,  311 => 196,  307 => 195,  281 => 172,  276 => 170,  259 => 156,  248 => 148,  242 => 145,  231 => 137,  222 => 131,  214 => 126,  203 => 118,  190 => 108,  182 => 103,  175 => 99,  166 => 93,  158 => 88,  147 => 80,  138 => 74,  129 => 68,  117 => 59,  106 => 51,  98 => 46,  87 => 38,  77 => 31,  70 => 27,  58 => 18,  49 => 12,  45 => 11,  40 => 8,  37 => 7,  32 => 4,  29 => 3,);
    }
}

<?php

/* GEDELTURBundle:Default:menu.html.twig */
class __TwigTemplate_2125b108476a08ec15aea7aa1915b92d51a8f89e16a600d12b0372b01af29e50 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout.html.twig");

        $this->blocks = array(
            'superacion' => array($this, 'block_superacion'),
            'nombre' => array($this, 'block_nombre'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["pagina"] = "curso";
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_superacion($context, array $blocks = array())
    {
        // line 7
        echo "

 

<div id=\"superacion\" style=\"padding-bottom: 100px; padding-top: 10px\">
       
        <div class=\"big-title text-center\" data-animation=\"fadeInDown\" data-animation-delay=\"01\">


         ";
        // line 16
        $this->displayBlock('nombre', $context, $blocks);
        // line 19
        echo " 


        </div>
        
          

        <div class=\"row sidebar-page\">


                    <!-- Page Content -->

                    <div class=\"col-md-7 page-content\">


                     ";
        // line 34
        $this->displayBlock('contenido', $context, $blocks);
        // line 41
        echo "                    
                    </div>
                    
                    
                    
                    
                    
                    
                    
                   


                    <!--Sidebar-->
                    <div class=\"col-md-3 sidebar right-sidebar\" style=\"float: right\">

                        <!--
                        <div class=\"widget widget-search\">
                            <form action=\"#\">
                                <input type=\"search\" placeholder=\"Enter Keywords...\" />
                                <button class=\"search-btn\" type=\"submit\"><i class=\"fa fa-search\"></i></button>
                            </form>
                        </div>

                        <!-- Categories Widget -->

                        <div class=\"widget widget-categories\">

                        <h4> Pregrado <span class=\"head-line\"></span></h4>
                            <ul>
                                <li>
                                    <a href=\"";
        // line 71
        echo $this->env->getExtension('routing')->getPath("asignaturas");
        echo "\">Asignaturas</a>
                                </li>
                                
                            </ul>

                            </br>
                            <h4>Postgrado <span class=\"head-line\"></span></h4>
                            <ul>
                                <li>
                                    <a href=\"";
        // line 80
        echo $this->env->getExtension('routing')->getPath("cursos");
        echo "\">Cursos</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 83
        echo $this->env->getExtension('routing')->getPath("diplomados");
        echo "\">Diplomados </a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 86
        echo $this->env->getExtension('routing')->getPath("maestrias");
        echo "\">Maestrías</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 89
        echo $this->env->getExtension('routing')->getPath("especialidad");
        echo "\">Especialidades</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                        


                   
        </div>
</div>
              


 ";
    }

    // line 16
    public function block_nombre($context, array $blocks = array())
    {
        // line 17
        echo "           <h1><strong>Asignaturas  </strong></h1>

          ";
    }

    // line 34
    public function block_contenido($context, array $blocks = array())
    {
        echo "                     

                    
                    
                    
                    
                      ";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Default:menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 34,  152 => 17,  149 => 16,  129 => 89,  117 => 83,  99 => 71,  67 => 41,  65 => 34,  46 => 16,  35 => 7,  32 => 6,  27 => 1,  198 => 97,  184 => 88,  177 => 86,  171 => 83,  167 => 82,  162 => 81,  160 => 80,  157 => 79,  153 => 78,  140 => 72,  133 => 68,  123 => 86,  118 => 61,  111 => 80,  107 => 55,  91 => 43,  86 => 33,  83 => 32,  77 => 27,  69 => 21,  58 => 18,  52 => 14,  48 => 19,  38 => 5,  36 => 4,  33 => 3,  30 => 2,);
    }
}

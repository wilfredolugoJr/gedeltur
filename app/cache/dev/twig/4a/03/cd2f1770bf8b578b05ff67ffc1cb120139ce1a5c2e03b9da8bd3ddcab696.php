<?php

/* GEDELTURBundle:Default:superacion.html.twig */
class __TwigTemplate_4a03cd2f1770bf8b578b05ff67ffc1cb120139ce1a5c2e03b9da8bd3ddcab696 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:menu.html.twig");

        $this->blocks = array(
            'evento' => array($this, 'block_evento'),
            'superacion' => array($this, 'block_superacion'),
            'titulo' => array($this, 'block_titulo'),
            'especifica' => array($this, 'block_especifica'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_evento($context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        if (((isset($context["cant_eventos"]) ? $context["cant_eventos"] : $this->getContext($context, "cant_eventos")) > 0)) {
            // line 6
            echo "

    <li xmlns=\"http://www.w3.org/1999/html\"><a href=\"#\" >Evento</a>

        <ul class=\"dropdown\">
            ";
            // line 11
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["eventos"]) ? $context["eventos"] : $this->getContext($context, "eventos")));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 12
                echo "



                <li><a href=\"";
                // line 16
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("eventos", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nombre"), "html", null, true);
                echo "</a>
                </li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "
        </ul>
    </li>


";
        }
        // line 25
        echo "

                  ";
    }

    // line 29
    public function block_superacion($context, array $blocks = array())
    {
        // line 30
        echo "




 <div id=\"superacion\" style=\"padding-bottom: 50px; padding-top: 10px\">
        
        <div class=\"big-title text-center\" data-animation=\"fadeInDown\" data-animation-delay=\"01\">
          ";
        // line 38
        $this->displayBlock('titulo', $context, $blocks);
        // line 42
        echo "        </div>
        
          
                <div class=\"row sidebar-page\">


                    <!-- Page Content -->
                    <div class=\"col-md-7 page-content\">


                        <!-- Divider -->


                        <!-- Accordion -->
                        <div class=\"panel-group\" id=\"accordion\">

                           

                            <!-- Start Accordion 2 -->

                              ";
        // line 62
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 63
            echo "
                            <div class=\"panel panel-default\">
                                <!-- Toggle Heading -->
                                <div class=\"panel-heading\">
                                    <h4 class=\"panel-title\">
                                        <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#";
            // line 68
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "id"), "html", null, true);
            echo "\" class=\"collapsed\">
                                            <i class=\"fa fa-angle-up control-icon\"></i>
                                            ";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "titulo"), "html", null, true);
            echo "


                                        </a>
                                    </h4>
                                </div>
                                <!-- Toggle Content -->
                                <div id=\"";
            // line 77
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "id"), "html", null, true);
            echo "\" class=\"panel-collapse collapse\">



                                    <div class=\"panel-body\">

                                        <h6><span class=\"glyphicon glyphicon-calendar\"></span><strong>Fecha de inicio:</strong>";
            // line 83
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "inicio"), "d/m/Y"), "html", null, true);
            echo "   <strong>Fecha de Fin:</strong>  ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "fin"), "d/m/Y"), "html", null, true);
            echo "</h6>

                                        ";
            // line 85
            if (($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "tipo") == "Maestria")) {
                // line 86
                echo "
                                         <div class=\"alert alert-info\">

        <h6><strong></strong>";
                // line 89
                if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "acreditada") == 0)) {
                    // line 90
                    echo "            <strong> No Acreditada!</strong>
            ";
                }
                // line 91
                if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "acreditada") == 1)) {
                    // line 92
                    echo "                <strong>Acreditada!</strong>
            ";
                }
                // line 93
                echo "</h6>

    </div>

                                        ";
            }
            // line 98
            echo "

                                        <a  href=\"";
            // line 100
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("trabajadores_show", array("id" => $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "encargado"), "id"), "id"))), "html", null, true);
            echo "\"><h6> <strong>Responsable:</strong> ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "encargado"), "id"), "nombre"))), "html", null, true);
            echo "  ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "encargado"), "id"), "apellidos"))), "html", null, true);
            echo "</h6></a>

                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Título del Egresado</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                <p> ";
            // line 107
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "tituloEgresado"), "html", null, true);
            echo "</p>
                                            </div>
                                        </div>


                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Perfil del Egresado</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                <p>";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "perfilEgresado"), "html", null, true);
            echo "</p>
                                            </div>
                                        </div>


                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Objetivos</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                <p>";
            // line 127
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "objetivos"), "html", null, true);
            echo "</p>
                                            </div>
                                        </div>




                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Requisitos</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                <p>";
            // line 139
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "requisitos"), "html", null, true);
            echo "</p>
                                            </div>
                                        </div>










                                         <!-- Accordion<h6><strong>Tarifa:</strong> ";
            // line 152
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "tarifa"), "html", null, true);
            echo "</h6> -->


                                        ";
            // line 155
            $this->displayBlock('especifica', $context, $blocks);
            // line 160
            echo "


                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Contactos</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                ";
            // line 168
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["contactos"]) ? $context["contactos"] : $this->getContext($context, "contactos")));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 169
                echo "
                                                    ";
                // line 170
                if (($this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "rrhh") == $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "encargado"), "id"))) {
                    // line 171
                    echo "                                                        
                                                        <h6>Correo :<strong>";
                    // line 172
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "correo"), "html", null, true);
                    echo "</strong></h6>
                                                        <h6>Fijo: <strong>";
                    // line 173
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "fijo"), "html", null, true);
                    echo "</strong></h6>
                                                        <h6>Móvil :<strong>";
                    // line 174
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "movil"), "html", null, true);
                    echo "</strong></h6>

                                                    ";
                }
                // line 177
                echo "
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 179
            echo "                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            
                            
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 189
        echo "                            
                            
                            
                            
                            
                            
                            <!-- End Accordion 3 -->

                            <!-- Start Accordion 3 -->

                            <!-- End Accordion 3 -->

                        </div>
                        <!-- End Accordion -->

                    </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    <!-- End Page Content-->


                    <!--Sidebar-->
                    <div class=\"col-md-3 sidebar right-sidebar\" style=\"float: right\">

                        <!--
                        <div class=\"widget widget-search\">
                            <form action=\"#\">
                                <input type=\"search\" placeholder=\"Enter Keywords...\" />
                                <button class=\"search-btn\" type=\"submit\"><i class=\"fa fa-search\"></i></button>
                            </form>
                        </div>

                        <!-- Categories Widget -->

                        <div class=\"widget widget-categories\">

                            <h4> Pregrado <span class=\"head-line\"></span></h4>
                            <ul>
                                <li>
                                    <a href=\"";
        // line 239
        echo $this->env->getExtension('routing')->getPath("asignaturas");
        echo "\">Asignaturas</a>
                                </li>

                            </ul>

                            </br>
                            <h4>Postgrado <span class=\"head-line\"></span></h4>
                            <ul>
                                <li>
                                    <a href=\"";
        // line 248
        echo $this->env->getExtension('routing')->getPath("cursos");
        echo "\">Cursos</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 251
        echo $this->env->getExtension('routing')->getPath("diplomados");
        echo "\">Diplomados </a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 254
        echo $this->env->getExtension('routing')->getPath("maestrias");
        echo "\">Maestrías</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 257
        echo $this->env->getExtension('routing')->getPath("especialidades");
        echo "\">Especialidades</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>


 ";
    }

    // line 38
    public function block_titulo($context, array $blocks = array())
    {
        // line 39
        echo "

          ";
    }

    // line 155
    public function block_especifica($context, array $blocks = array())
    {
        // line 156
        echo "


                                        ";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Default:superacion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  457 => 156,  454 => 155,  448 => 39,  445 => 38,  430 => 257,  424 => 254,  418 => 251,  412 => 248,  400 => 239,  348 => 189,  325 => 179,  318 => 177,  312 => 174,  308 => 173,  304 => 172,  301 => 171,  299 => 170,  296 => 169,  292 => 168,  282 => 160,  280 => 155,  274 => 152,  258 => 139,  243 => 127,  230 => 117,  217 => 107,  203 => 100,  199 => 98,  192 => 93,  188 => 92,  186 => 91,  182 => 90,  180 => 89,  175 => 86,  173 => 85,  166 => 83,  157 => 77,  147 => 70,  142 => 68,  135 => 63,  118 => 62,  96 => 42,  94 => 38,  84 => 30,  75 => 25,  67 => 19,  56 => 16,  50 => 12,  46 => 11,  39 => 6,  37 => 5,  34 => 4,  31 => 3,  107 => 48,  103 => 47,  101 => 46,  97 => 45,  95 => 44,  81 => 29,  70 => 24,  58 => 20,  55 => 19,  51 => 18,  41 => 10,  38 => 9,  32 => 4,  29 => 3,);
    }
}

<?php

/* GEDELTURBundle:Default:cursos.html.twig */
class __TwigTemplate_bd54ffb9539727c1b3fa96d17b3f13afb4be821f52d7e12ce4834481b592164c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout.html.twig");

        $this->blocks = array(
            'evento' => array($this, 'block_evento'),
            'superacion' => array($this, 'block_superacion'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_evento($context, array $blocks = array())
    {
        // line 4
        echo "
                ";
        // line 5
        if (((isset($context["cant_eventos"]) ? $context["cant_eventos"] : $this->getContext($context, "cant_eventos")) > 0)) {
            // line 6
            echo "




                <li><a href=\"#\" >Evento</a>

                 <ul class=\"dropdown\">
                 ";
            // line 14
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["eventos"]) ? $context["eventos"] : $this->getContext($context, "eventos")));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 15
                echo "
                
                 
               
                  <li><a href=\"";
                // line 19
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("eventos", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nombre"), "html", null, true);
                echo "</a>
                  </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "
                </ul>
                </li>
               

                ";
        }
        // line 28
        echo "

                  ";
    }

    // line 32
    public function block_superacion($context, array $blocks = array())
    {
        // line 33
        echo "

 <div id=\"superacion\" style=\"padding-bottom: 50px; padding-top: 10px\">
        </br>
        </br>
        
        <div class=\"big-title text-center\" data-animation=\"fadeInDown\" data-animation-delay=\"01\">
          <h1><strong>Cursos</strong></h1>
        </div>
        
          
                <div class=\"row sidebar-page\">


                    <!-- Page Content -->
                    <div class=\"col-md-7 page-content\">


                        <!-- Divider -->


                        <!-- Accordion -->
                        <div class=\"panel-group\" id=\"accordion\">

                           

                            <!-- Start Accordion 2 -->

                              ";
        // line 61
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 62
            echo "
                            <div class=\"panel panel-default\">
                                <!-- Toggle Heading -->
                                <div class=\"panel-heading\">
                                    <h4 class=\"panel-title\">
                                        <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#";
            // line 67
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
            echo "\" class=\"collapsed\">
                                            <i class=\"fa fa-angle-up control-icon\"></i>
                                         ";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nombre"), "html", null, true);
            echo "
                                        </a>
                                    </h4>
                                </div>
                                <!-- Toggle Content -->
                                <div id=\"";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
            echo "\" class=\"panel-collapse collapse\">
                                    <div class=\"panel-body\">

                                        <a  href=\"";
            // line 77
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("trabajadores_show", array("id" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "plantilla"), "id"), "id"))), "html", null, true);
            echo "\"><h6> <strong>Docente:</strong> ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "plantilla"), "id"), "nombre"))), "html", null, true);
            echo "  ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "plantilla"), "id"), "apellidos"))), "html", null, true);
            echo "</h6></a>
                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Objetivos</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                <p>";
            // line 83
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "objetivos"), "html", null, true);
            echo "</p>
                                            </div>
                                        </div>

                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Descripción</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                <p>";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "descripcion"), "html", null, true);
            echo "</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            
                            
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 102
        echo "                            
                            
                            
                            
                            
                            
                            <!-- End Accordion 3 -->

                            <!-- Start Accordion 3 -->

                            <!-- End Accordion 3 -->

                        </div>
                        <!-- End Accordion -->

                    </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    <!-- End Page Content-->


                    <!--Sidebar-->
                    <div class=\"col-md-3 sidebar right-sidebar\" style=\"float: right\">

                        <!--
                        <div class=\"widget widget-search\">
                            <form action=\"#\">
                                <input type=\"search\" placeholder=\"Enter Keywords...\" />
                                <button class=\"search-btn\" type=\"submit\"><i class=\"fa fa-search\"></i></button>
                            </form>
                        </div>

                        <!-- Categories Widget -->

                        <div class=\"widget widget-categories\">

                            <h4> Pregrado <span class=\"head-line\"></span></h4>
                            <ul>
                                <li>
                                    <a href=\"";
        // line 152
        echo $this->env->getExtension('routing')->getPath("asignaturas");
        echo "\">Asignaturas</a>
                                </li>

                            </ul>

                            </br>
                            <h4>Postgrado <span class=\"head-line\"></span></h4>
                            <ul>
                                <li>
                                    <a href=\"";
        // line 161
        echo $this->env->getExtension('routing')->getPath("cursos");
        echo "\">Cursos</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 164
        echo $this->env->getExtension('routing')->getPath("diplomados");
        echo "\">Diplomados </a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 167
        echo $this->env->getExtension('routing')->getPath("maestrias");
        echo "\">Maestrías</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 170
        echo $this->env->getExtension('routing')->getPath("especialidades");
        echo "\">Especialidades</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>


 ";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Default:cursos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  268 => 170,  262 => 167,  256 => 164,  250 => 161,  238 => 152,  186 => 102,  170 => 92,  158 => 83,  145 => 77,  139 => 74,  131 => 69,  126 => 67,  119 => 62,  115 => 61,  85 => 33,  82 => 32,  76 => 28,  68 => 22,  57 => 19,  51 => 15,  47 => 14,  37 => 6,  35 => 5,  32 => 4,  29 => 3,);
    }
}

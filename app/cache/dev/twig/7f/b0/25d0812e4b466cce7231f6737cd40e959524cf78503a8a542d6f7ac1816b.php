<?php

/* @WebProfiler/Profiler/toolbar_js.html.twig */
class __TwigTemplate_7fb025d0812e4b466cce7231f6737cd40e959524cf78503a8a542d6f7ac1816b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"sfwdt";
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "\" class=\"sf-toolbar\" style=\"display: none\"></div>
";
        // line 2
        $this->env->loadTemplate("@WebProfiler/Profiler/base_js.html.twig")->display($context);
        // line 3
        echo "<script>/*<![CDATA[*/
    (function () {
        ";
        // line 5
        if (("top" == (isset($context["position"]) ? $context["position"] : $this->getContext($context, "position")))) {
            // line 6
            echo "            var sfwdt = document.getElementById('sfwdt";
            echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
            echo "');
            document.body.insertBefore(
                document.body.removeChild(sfwdt),
                document.body.firstChild
            );
        ";
        }
        // line 12
        echo "
        Sfjs.load(
            'sfwdt";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "',
            '";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_wdt", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))), "html", null, true);
        echo "',
            function(xhr, el) {
                el.style.display = -1 !== xhr.responseText.indexOf('sf-toolbarreset') ? 'block' : 'none';

                if (el.style.display == 'none') {
                    return;
                }

                if (Sfjs.getPreference('toolbar/displayState') == 'none') {
                    document.getElementById('sfToolbarMainContent-";
        // line 24
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "').style.display = 'none';
                    document.getElementById('sfToolbarClearer-";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "').style.display = 'none';
                    document.getElementById('sfMiniToolbar-";
        // line 26
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "').style.display = 'block';
                } else {
                    document.getElementById('sfToolbarMainContent-";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "').style.display = 'block';
                    document.getElementById('sfToolbarClearer-";
        // line 29
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "').style.display = 'block';
                    document.getElementById('sfMiniToolbar-";
        // line 30
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "').style.display = 'none';
                }
            },
            function(xhr) {
                if (xhr.status !== 0) {
                    confirm('An error occurred while loading the web debug toolbar (' + xhr.status + ': ' + xhr.statusText + ').\\n\\nDo you want to open the profiler?') && (window.location = '";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_profiler", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))), "html", null, true);
        echo "');
                }
            },
            {'maxTries': 5}
        );
    })();
/*]]>*/</script>
";
    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/toolbar_js.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 35,  83 => 30,  75 => 28,  70 => 26,  50 => 15,  42 => 12,  26 => 3,  24 => 2,  19 => 1,  606 => 349,  601 => 307,  596 => 306,  591 => 303,  586 => 157,  581 => 144,  575 => 69,  572 => 68,  567 => 27,  560 => 350,  558 => 349,  551 => 345,  547 => 344,  539 => 339,  506 => 308,  503 => 307,  501 => 306,  495 => 303,  472 => 283,  467 => 281,  458 => 275,  452 => 272,  446 => 269,  436 => 262,  430 => 259,  424 => 256,  418 => 253,  406 => 244,  399 => 240,  386 => 230,  380 => 227,  374 => 224,  360 => 213,  350 => 206,  342 => 201,  334 => 196,  326 => 191,  317 => 185,  311 => 182,  305 => 179,  283 => 160,  280 => 159,  277 => 158,  275 => 157,  266 => 151,  256 => 144,  237 => 128,  231 => 125,  225 => 122,  219 => 119,  206 => 109,  197 => 103,  188 => 97,  161 => 72,  159 => 68,  150 => 62,  141 => 56,  136 => 54,  132 => 53,  128 => 52,  123 => 50,  119 => 49,  115 => 48,  111 => 47,  107 => 46,  102 => 44,  81 => 28,  79 => 29,  73 => 24,  66 => 25,  62 => 24,  58 => 18,  49 => 15,  45 => 14,  40 => 12,  27 => 1,  51 => 23,  38 => 17,  32 => 6,  29 => 14,  205 => 107,  196 => 104,  193 => 103,  189 => 102,  183 => 98,  169 => 90,  160 => 86,  149 => 78,  142 => 74,  134 => 69,  122 => 60,  117 => 58,  113 => 57,  108 => 55,  104 => 54,  98 => 51,  94 => 39,  90 => 49,  86 => 47,  82 => 46,  57 => 25,  54 => 17,  46 => 14,  36 => 6,  33 => 5,  30 => 5,);
    }
}

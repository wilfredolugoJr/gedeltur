<?php

/* GEDELTURBundle:Default:especialidad.html.twig */
class __TwigTemplate_6bd9696613ee884b93d19465cf0369e3c9d73e7df63c76f601100dc9a8c1da81 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:menu.html.twig");

        $this->blocks = array(
            'evento' => array($this, 'block_evento'),
            'superacion' => array($this, 'block_superacion'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_evento($context, array $blocks = array())
    {
        // line 5
        echo "
     ";
        // line 6
        if (((isset($context["cant_eventos"]) ? $context["cant_eventos"] : $this->getContext($context, "cant_eventos")) > 0)) {
            // line 7
            echo "




         <li><a href=\"#\" >Evento</a>

             <ul class=\"dropdown\">
                 ";
            // line 15
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["eventos"]) ? $context["eventos"] : $this->getContext($context, "eventos")));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 16
                echo "



                     <li><a href=\"";
                // line 20
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("eventos", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nombre"), "html", null, true);
                echo "</a>
                     </li>
                 ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "
             </ul>
         </li>


     ";
        }
        // line 29
        echo "

 ";
    }

    // line 33
    public function block_superacion($context, array $blocks = array())
    {
        // line 34
        echo "




 <div id=\"superacion\" style=\"padding-bottom: 100px; padding-top: 10px\">
        
        <div class=\"big-title text-center\" data-animation=\"fadeInDown\" data-animation-delay=\"01\">
          <h1>Especialidades</h1>
        </div>
        
          
                <div class=\"row sidebar-page\">


                    <!-- Page Content -->
                    <div class=\"col-md-7 page-content\">


                        <!-- Divider -->


                        <!-- Accordion -->
                        <div class=\"panel-group\" id=\"accordion\">

                           

                            <!-- Start Accordion 2 -->

                              ";
        // line 63
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 64
            echo "
                            <div class=\"panel panel-default\">
                                <!-- Toggle Heading -->
                                <div class=\"panel-heading\">
                                    <h4 class=\"panel-title\">
                                        <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "id"), "html", null, true);
            echo "\" class=\"collapsed\">
                                            <i class=\"fa fa-angle-up control-icon\"></i>
                                         ";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "titulo"), "html", null, true);
            echo "
                                        </a>
                                    </h4>
                                </div>
                                <!-- Toggle Content -->
                                <div id=\"";
            // line 76
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "id"), "html", null, true);
            echo "\" class=\"panel-collapse collapse\">
                                    <div class=\"panel-body\">

                                        <h6><span class=\"glyphicon glyphicon-calendar\"></span><strong>Inicia:</strong>";
            // line 79
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "inicio"), "d/m/Y"), "html", null, true);
            echo "  <strong>Termina:</strong> ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "fin"), "d/m/Y"), "html", null, true);
            echo "</h6>

                                        <a  href=\"";
            // line 81
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("trabajadores_show", array("id" => $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "encargado"), "id"), "id"))), "html", null, true);
            echo "\"><h6> Responsable:<strong> ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "encargado"), "id"), "nombre"))), "html", null, true);
            echo "  ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "encargado"), "id"), "apellidos"))), "html", null, true);
            echo "</strong></h6></a>

                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Título del Egresado</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                <p> ";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "tituloEgresado"), "html", null, true);
            echo "</p>
                                            </div>
                                        </div>


                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Perfil del Egresado</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                <p>";
            // line 98
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "perfilEgresado"), "html", null, true);
            echo "</p>
                                            </div>
                                        </div>

                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Objetivos</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                <p>";
            // line 107
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "objetivos"), "html", null, true);
            echo "</p>
                                            </div>
                                        </div>


                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Requisitos</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                <p>";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "requisitos"), "html", null, true);
            echo "</p>
                                            </div>
                                        </div>


                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Cursos</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                ";
            // line 127
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "curso"));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 128
                echo "                                                    <a  href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cursos_show", array("id" => $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "id"))), "html", null, true);
                echo "\"><h6><strong> ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "nombre"), "html", null, true);
                echo "  </strong></h6></a>

                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 131
            echo "                                            </div>
                                        </div>
                                        </br>


                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Costo(CUP)</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                ";
            // line 141
            if (($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "tarifa") != "")) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "tarifa"), "html", null, true);
                echo "
                                                ";
            } else {
                // line 142
                echo "-
                                                ";
            }
            // line 144
            echo "
                                            </div>
                                        </div>



                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Contactos</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                ";
            // line 155
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["contactos"]) ? $context["contactos"] : $this->getContext($context, "contactos")));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 156
                echo "
                                                    ";
                // line 157
                if (($this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "rrhh") == $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "encargado"), "id"))) {
                    // line 158
                    echo "                                                        <h6><strong>Contactos</strong></h6>
                                                        <h6>Correo :<strong>";
                    // line 159
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "correo"), "html", null, true);
                    echo "</strong></h6>
                                                        <h6>Fijo: <strong>";
                    // line 160
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "fijo"), "html", null, true);
                    echo "</strong></h6>
                                                        <h6>Móvil :<strong>";
                    // line 161
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "movil"), "html", null, true);
                    echo "</strong></h6>

                                                    ";
                }
                // line 164
                echo "
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 166
            echo "                                            </div>
                                        </div>








                                    

                                    </div>
                                </div>
                            </div>
                            
                            
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 184
        echo "                            
                            
                            
                            
                            
                            
                            <!-- End Accordion 3 -->

                            <!-- Start Accordion 3 -->

                            <!-- End Accordion 3 -->

                        </div>
                        <!-- End Accordion -->

                    </div>




                    
                    
                    
                    
                    
                    
                    
                    
                    <!-- End Page Content-->


                    <!--Sidebar-->
                    <div class=\"col-md-3 sidebar right-sidebar\" style=\"float: right\">

                        <!--
                        <div class=\"widget widget-search\">
                            <form action=\"#\">
                                <input type=\"search\" placeholder=\"Enter Keywords...\" />
                                <button class=\"search-btn\" type=\"submit\"><i class=\"fa fa-search\"></i></button>
                            </form>
                        </div>

                        <!-- Categories Widget -->

                        <div class=\"widget widget-categories\">

                            <h4> Pregrado <span class=\"head-line\"></span></h4>
                            <ul>
                                <li>
                                    <a href=\"";
        // line 233
        echo $this->env->getExtension('routing')->getPath("asignaturas");
        echo "\">Asignaturas</a>
                                </li>

                            </ul>

                            </br>
                            <h4>Postgrado <span class=\"head-line\"></span></h4>
                            <ul>
                                <li>
                                    <a href=\"";
        // line 242
        echo $this->env->getExtension('routing')->getPath("cursos");
        echo "\">Cursos</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 245
        echo $this->env->getExtension('routing')->getPath("diplomados");
        echo "\">Diplomados </a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 248
        echo $this->env->getExtension('routing')->getPath("maestrias");
        echo "\">Maestrías</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 251
        echo $this->env->getExtension('routing')->getPath("especialidad");
        echo "\">Especialidades</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>


 ";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Default:especialidad.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  407 => 251,  401 => 248,  395 => 245,  389 => 242,  377 => 233,  326 => 184,  303 => 166,  296 => 164,  290 => 161,  286 => 160,  282 => 159,  279 => 158,  277 => 157,  274 => 156,  270 => 155,  257 => 144,  253 => 142,  246 => 141,  234 => 131,  222 => 128,  218 => 127,  205 => 117,  192 => 107,  180 => 98,  167 => 88,  153 => 81,  146 => 79,  140 => 76,  132 => 71,  127 => 69,  120 => 64,  116 => 63,  85 => 34,  82 => 33,  76 => 29,  68 => 23,  57 => 20,  51 => 16,  47 => 15,  37 => 7,  35 => 6,  32 => 5,  29 => 4,);
    }
}

<?php

/* GEDELTURBundle:Asignatura:index.html.twig */
class __TwigTemplate_4f223f23bbaa910da419081e67697b4379930b707dec0da13d719121c9d56991 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:plantilla.html.twig");

        $this->blocks = array(
            'boton' => array($this, 'block_boton'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:plantilla.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_boton($context, array $blocks = array())
    {
        // line 4
        echo "     
        <a  href=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("asignatura_new");
        echo "\"><button type=\"button\" class=\"btn btn-warning warning_22\"><span class=\"glyphicon glyphicon-plus\"  id=\"pencil\"></span>   Nuevo</button></a>
         
    
</br>
        </br>
    ";
    }

    // line 12
    public function block_title($context, array $blocks = array())
    {
        echo "Asignaturas";
    }

    // line 15
    public function block_body($context, array $blocks = array())
    {
        // line 24
        echo "<div class=\"table-responsive\">

      <table class=\"table table-striped table-bordered table-hover dataTable no-footer\"  id=\"dataTables-example\">
   
        <thead>
            <tr >
                
                <th>Nombre</th>
                <th>Carrera</th>
                <th>Docente</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 39
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 40
            echo "            <tr>
             
                <td>";
            // line 42
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nombre"))), "html", null, true);
            echo "</td>
                <td>";
            // line 43
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "carrera"))), "html", null, true);
            echo "</td>
                <td>";
            // line 44
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "plantilla"), "id"), "nombre"))), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "plantilla"), "id"), "apellidos"))), "html", null, true);
            echo "</td>
                <td>
                
                        
                    
                        <a title=\"Editar\" href=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("asignatura_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\"><span class=\"glyphicon glyphicon-edit\"></span></a>
                        </td>


                         <td>
                        <a title=\"Eliminar\" data-toggle=\"modal\" href=\"#";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
            echo "\"><span class=\"glyphicon glyphicon-trash\"></span></a>
                </td>
            </tr>
            <div class=\"modal fade\" id=\"";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\" style=\"display: none;\">
                    <div class=\"modal-dialog\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                                <h2 class=\"modal-title\">Confirmación de Eliminar</h2>
                            </div>
                            <div class=\"modal-body\">
                                <h5 class=\"modal-title\">¿Estás seguro que deseas eliminar  la asignatura: ";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nombre"), "html", null, true);
            echo "? </h5>
                            </div>
                            <div class=\"modal-footer\">

                                <a  href=\"";
            // line 69
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("asignatura_delete", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary\">Sí</button></a>
                                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">No</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                
           </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "        </tbody>
    </table>


    <div class=\"alert alert-success\">
        ";
        // line 82
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msg"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 83
            echo "            <div class=\"msg\">
                ";
            // line 84
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "    </div>




        <script type=\"text/javascript\">
            jQuery(document).ready(function() {
                jQuery('#dataTables-example').DataTable({
                    responsive: true,
                });
            });
        </script>
         
    ";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Asignatura:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 87,  158 => 84,  155 => 83,  151 => 82,  144 => 77,  130 => 69,  123 => 65,  112 => 57,  106 => 54,  98 => 49,  88 => 44,  84 => 43,  80 => 42,  76 => 40,  72 => 39,  55 => 24,  52 => 15,  46 => 12,  36 => 5,  33 => 4,  30 => 3,);
    }
}

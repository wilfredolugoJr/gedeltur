<?php

/* GEDELTURBundle:Usuarios:registrarse.html.twig */
class __TwigTemplate_261c6f490896ee6f9d913d3ef71ad4a20774975c51584395ef7cf0046cfd6430 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout.html.twig");

        $this->blocks = array(
            'evento' => array($this, 'block_evento'),
            'superacion' => array($this, 'block_superacion'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_evento($context, array $blocks = array())
    {
        // line 4
        echo "
    ";
        // line 5
        if (((isset($context["cant_eventos"]) ? $context["cant_eventos"] : $this->getContext($context, "cant_eventos")) > 0)) {
            // line 6
            echo "

        <li xmlns=\"http://www.w3.org/1999/html\"><a href=\"#\" >Evento</a>

            <ul class=\"dropdown\">
                ";
            // line 11
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["eventos"]) ? $context["eventos"] : $this->getContext($context, "eventos")));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 12
                echo "



                    <li><a href=\"";
                // line 16
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("eventos", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nombre"), "html", null, true);
                echo "</a>
                    </li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "
            </ul>
        </li>


    ";
        }
        // line 25
        echo "

";
    }

    // line 29
    public function block_superacion($context, array $blocks = array())
    {
        // line 30
        echo "
  ";
        // line 31
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "

<div class=\"section service\">
            <div class=\"main\">
      <div class=\"container\">
        
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class=\"row margin-bottom-40\">

            <div class=\"alert alert-success\" style=\"width: 55%\">
            ";
        // line 42
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msg"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 43
            echo "

            <div class=\"msg\">
                ";
            // line 46
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "            </div>

                </div>
          <div class=\"col-md-12 col-sm-9\">



            <h1>Crear una cuenta</h1>
            <div class=\"content-form-page\">
              <div class=\"row\">
                <div class=\"col-md-7 col-sm-7\">



                  
                    <fieldset>

                      <legend>Detalles personales</legend>
                      

                      <div class=\"form-group\">
                       <div class=\"col-lg-4 control-label\">";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nombre"), 'label');
        echo "<span class=\"require\">*</span></div>
                       
                        
                        <div class=\"col-lg-8\">
                          ";
        // line 73
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nombre"), 'widget', array("attr" => array("class" => "form-control", "id" => "firstname")));
        echo "
                          <div class=\"col-sm-2\">
                          <p class=\"help-block\">";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nombre"), 'errors');
        echo "</p>
                          </div>
                          </div>
                          </div>


                      <div class=\"form-group\">

                      <div class=\"col-lg-4 control-label\">";
        // line 83
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "apellido"), 'label');
        echo " <span class=\"require\">*</span></div>
                        
                        <div class=\"col-lg-8\">

                        ";
        // line 87
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "apellido"), 'widget', array("attr" => array("class" => "form-control", "id" => "lastname")));
        echo "
                          <div class=\"col-sm-2\">
                          <p class=\"help-block\">";
        // line 89
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "apellido"), 'errors');
        echo "</p>
                          </div>
                        </div>
                      </div>


                      <div class=\"form-group\">

                      <div class=\"col-lg-4 control-label\">";
        // line 97
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "usuario"), 'label');
        echo "<span class=\"require\">*</span></div>
                         
                        <div class=\"col-lg-8\">

                        ";
        // line 101
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "usuario"), 'widget', array("attr" => array("class" => "form-control", "id" => "lastname")));
        echo "
                          <div class=\"col-sm-2\">
                          <p class=\"help-block\">";
        // line 103
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "usuario"), 'errors');
        echo "</p>
                          </div>
                        </div>
                      </div>


                    </fieldset>



                    <fieldset>
                      <legend>Tu contraseña</legend>
                      
                      <div class=\"form-group\">

                      <div class=\"col-lg-4 control-label\">";
        // line 118
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "contrasenna"), "first"), 'label');
        echo "<span class=\"require\">*</span></div>
                         
                        <div class=\"col-lg-8\">

                        ";
        // line 122
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "contrasenna"), "first"), 'widget', array("attr" => array("class" => "form-control", "id" => "lastname")));
        echo "
                          <div class=\"col-sm-2\">
                          <p >";
        // line 124
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "contrasenna"), "first"), 'errors');
        echo "</p>
                          </div>
                        </div>
                      </div>


                      
                       <div class=\"form-group\">

                      <div class=\"col-lg-4 control-label\">";
        // line 133
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "contrasenna"), "second"), 'label');
        echo "<span class=\"require\">*</span></div>
                         
                        <div class=\"col-lg-8\">

                        ";
        // line 137
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "contrasenna"), "second"), 'widget', array("attr" => array("class" => "form-control", "id" => "lastname")));
        echo "

                            <div class=\"col-sm-2\">
                          <p class=\"help-block\">";
        // line 140
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "contrasenna"), "second"), 'errors');
        echo "</p>
                          </div>


                        </div>
                      </div>

                    </fieldset>
                    
                    <div class=\"row\">
                      <div class=\"col-lg-8 col-md-offset-4 padding-left-0 padding-top-20\"> 
                     <button id=\"a\" data-toggle=\"tooltip\" title=\"Registrar\" type=\"submit\" class=\"btn btn-default\">
                    <span class=\"glyphicon glyphicon-save\"></span>
                </button>                      
                        
                        <a href=\"";
        // line 155
        echo $this->env->getExtension('routing')->getPath("gedeltur_homepage");
        echo "\">
            <button type=\"button\" class=\"btn btn-success\">Cancelar</button>
        </a>
                      </div>
                    </div>


               
                </div>
                  <!-- <div class=\"col-md-4 col-sm-4 pull-right\">
                    <div class=\"form-info\">
                      <h2><em>Información</em> Importante</h2>



                    </div>
                  </div>-->
                </div>
              </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END SIDEBAR & CONTENT -->
      </div>
    </div>
            <!-- .container -->
        </div>
\t\t
\t\t
\t\t


   




\t\t";
        // line 192
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "


\t\t
";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Usuarios:registrarse.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  313 => 192,  273 => 155,  255 => 140,  249 => 137,  242 => 133,  230 => 124,  225 => 122,  218 => 118,  200 => 103,  195 => 101,  188 => 97,  177 => 89,  172 => 87,  165 => 83,  154 => 75,  149 => 73,  142 => 69,  119 => 48,  111 => 46,  106 => 43,  102 => 42,  89 => 32,  85 => 31,  82 => 30,  79 => 29,  73 => 25,  65 => 19,  54 => 16,  48 => 12,  44 => 11,  37 => 6,  35 => 5,  32 => 4,  29 => 3,);
    }
}

<?php

/* GEDELTURBundle:GCE:index.html.twig */
class __TwigTemplate_2ed0d9720b0566c49c65d77e71d433aa0a4a2aa430bdb2723026c8f11ee0f551 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:plantilla.html.twig");

        $this->blocks = array(
            'boton' => array($this, 'block_boton'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:plantilla.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_boton($context, array $blocks = array())
    {
        // line 3
        echo "     
        <a  href=\"";
        // line 4
        echo $this->env->getExtension('routing')->getPath("gce_new");
        echo "\"><button type=\"button\" class=\"btn btn-warning warning_22\"><span class=\"glyphicon glyphicon-plus\"  id=\"pencil\"></span>   Nuevo</button></a>
         
   
</br>
        </br>
    ";
    }

    // line 11
    public function block_title($context, array $blocks = array())
    {
        echo "Estudiantes  <span class=\"badge\">";
        echo twig_escape_filter($this->env, (isset($context["contador"]) ? $context["contador"] : $this->getContext($context, "contador")), "html", null, true);
        echo "</span>";
    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        // line 21
        echo "<div class=\"table-responsive\">
        
    <table class=\"table table-striped table-bordered table-hover dataTable no-footer\"  id=\"dataTables-example\">
        <thead>
            <tr>
                
                <th>Imagen</th>
                <th>Carné de Identidad</th>
                
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Dirección</th>
                <th>Facultad</th>
                <th>Año Académico</th>
                <th>Carrera</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 41
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 42
            echo "            <tr>

               <td><img style=\"width: 80px; height: 60px\" src=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("uploads/gedeltur/fotos/" . $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "image"))), "html", null, true);
            echo "\"></td>
                <td>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "ci"), "html", null, true);
            echo "</td>
             
                <td>";
            // line 47
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "nombre"))), "html", null, true);
            echo "</td>
                <td>";
            // line 48
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "apellidos"))), "html", null, true);
            echo "</td>
                   <td>";
            // line 49
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "direccion"))), "html", null, true);
            echo "</td>
                <td>";
            // line 50
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "facultad"))), "html", null, true);
            echo "</td>
                <td>";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "anyo"), "html", null, true);
            echo "</td>
                <td>";
            // line 52
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "carrera"))), "html", null, true);
            echo "</td>
                <td>

                    
                        <a title=\"Editar\" href=\"";
            // line 56
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("gce_edit", array("id" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "id"))), "html", null, true);
            echo "\"><span class=\"glyphicon glyphicon-edit\"></span></a>
                          </td>


                            <td>
                        <a title=\"Eliminar\" data-toggle=\"modal\" href=\"#";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "id"), "html", null, true);
            echo "\"><span class=\"glyphicon glyphicon-trash\"></span></a>

                 
                  
                </td>
            </tr>
            <div class=\"modal fade\" id=\"";
            // line 67
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "id"), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\" style=\"display: none;\">
                    <div class=\"modal-dialog\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                                <h2 class=\"modal-title\">Confirmación de Eliminar</h2>
                            </div>
                            <div class=\"modal-body\">
                                <h5 class=\"modal-title\">¿Estás seguro que deseas eliminar al estudiante: ";
            // line 75
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "nombre"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "apellidos"), "html", null, true);
            echo " ?</h5>
                            </div>
                            <div class=\"modal-footer\">

                                <a  href=\"";
            // line 79
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("gce_delete", array("id" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "id"))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary\">Sí</button></a>
                                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">No</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                
           </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "        </tbody>
    </table>


    <div class=\"alert alert-success\">
        ";
        // line 92
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msg"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 93
            echo "            <div class=\"msg\">
                ";
            // line 94
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 97
        echo "   



      
 

      <script type=\"text/javascript\">
            jQuery(document).ready(function() {
                jQuery('#dataTables-example').DataTable({
                    responsive: true
                });
            });
        </script>
    ";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:GCE:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 97,  188 => 94,  185 => 93,  181 => 92,  174 => 87,  160 => 79,  151 => 75,  140 => 67,  131 => 61,  123 => 56,  116 => 52,  112 => 51,  108 => 50,  104 => 49,  100 => 48,  96 => 47,  91 => 45,  87 => 44,  83 => 42,  79 => 41,  57 => 21,  54 => 14,  46 => 11,  36 => 4,  33 => 3,  30 => 2,);
    }
}

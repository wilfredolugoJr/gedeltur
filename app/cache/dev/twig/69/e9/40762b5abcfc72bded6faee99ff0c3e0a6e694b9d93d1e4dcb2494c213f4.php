<?php

/* GEDELTURBundle:Reportes:ws_li.html.twig */
class __TwigTemplate_69e940762b5abcfc72bded6faee99ff0c3e0a6e694b9d93d1e4dcb2494c213f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:plantilla.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:plantilla.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        // line 6
        echo "   Trabajadores por Linea de Investigación 
";
    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "



             <div>

      <table   class=\"table table-hover\" id=\"dataTables-example\">
        <thead>
          <tr>

              <th>Linea de Investigación</th>

            <th>Nombre  Completo  de los Trabajadores</th>




          </tr>
        </thead>
        <tbody>
          <tr>
           
          
            
           
            ";
        // line 35
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["linea"]) ? $context["linea"] : $this->getContext($context, "linea")));
        foreach ($context['_seq'] as $context["_key"] => $context["li"]) {
            // line 36
            echo "
            <td >

               <strong>";
            // line 39
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["li"]) ? $context["li"] : $this->getContext($context, "li")), "titulo"))), "html", null, true);
            echo " </strong>

            </td>




            <td>



                ";
            // line 50
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["li"]) ? $context["li"] : $this->getContext($context, "li")), "proyecto"));
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                // line 51
                echo "                    ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["p"]) ? $context["p"] : $this->getContext($context, "p")), "rrhh"));
                foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                    // line 52
                    echo "

                ";
                    // line 54
                    echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["r"]) ? $context["r"] : $this->getContext($context, "r")), "nombre"))), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["r"]) ? $context["r"] : $this->getContext($context, "r")), "apellidos"))), "html", null, true);
                    echo ".  </br>





                     ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 61
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 62
            echo "




            </td>



          </tr>
 ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['li'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "          
        </tbody>
</table>

 
    
 <script type=\"text/javascript\">
            jQuery(document).ready(function() {
                jQuery('#dataTables-example').DataTable({
                    responsive: true
                });
            });
        </script>


 ";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Reportes:ws_li.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 73,  124 => 62,  118 => 61,  103 => 54,  99 => 52,  94 => 51,  90 => 50,  76 => 39,  71 => 36,  67 => 35,  40 => 10,  37 => 9,  32 => 6,  29 => 5,);
    }
}

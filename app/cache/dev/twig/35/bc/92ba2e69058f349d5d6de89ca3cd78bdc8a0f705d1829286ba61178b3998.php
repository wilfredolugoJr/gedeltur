<?php

/* GEDELTURBundle:Reportes:ws_pro.html.twig */
class __TwigTemplate_35bc92ba2e69058f349d5d6de89ca3cd78bdc8a0f705d1829286ba61178b3998 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:plantilla.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:plantilla.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        // line 7
        echo "  Trabajadores por Proyecto 
";
    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        // line 11
        echo "


 
             <div>

      <table   class=\"table table-hover\" id=\"dataTables-example\">
        <thead>
          <tr>

              <th>Proyecto</th>

            <th>Nombre  Completo  de los Trabajadores</th>




          </tr>
        </thead>
        <tbody>
          <tr>
           
          
            
           
            ";
        // line 36
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["proyecto"]) ? $context["proyecto"] : $this->getContext($context, "proyecto")));
        foreach ($context['_seq'] as $context["_key"] => $context["pro"]) {
            // line 37
            echo "
            <td >

               <strong>";
            // line 40
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["pro"]) ? $context["pro"] : $this->getContext($context, "pro")), "titulo"))), "html", null, true);
            echo " </strong>

            </td>




            <td>

         ";
            // line 49
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["pro"]) ? $context["pro"] : $this->getContext($context, "pro")), "rrhh"));
            foreach ($context['_seq'] as $context["_key"] => $context["w"]) {
                // line 50
                echo "

                    <strong>";
                // line 52
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["w"]) ? $context["w"] : $this->getContext($context, "w")), "nombre"), "html", null, true);
                echo "  ";
                echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["w"]) ? $context["w"] : $this->getContext($context, "w")), "apellidos"))), "html", null, true);
                echo " </strong> </br>


         ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['w'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 56
            echo "




            </td>



          </tr>
 ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pro'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "          
        </tbody>
</table>

   
    <script type=\"text/javascript\">
            jQuery(document).ready(function() {
                jQuery('#dataTables-example').DataTable({
                    responsive: true
                });
            });
        </script>
    



 ";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Reportes:ws_pro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 67,  108 => 56,  96 => 52,  92 => 50,  88 => 49,  76 => 40,  71 => 37,  67 => 36,  40 => 11,  37 => 10,  32 => 7,  29 => 6,);
    }
}

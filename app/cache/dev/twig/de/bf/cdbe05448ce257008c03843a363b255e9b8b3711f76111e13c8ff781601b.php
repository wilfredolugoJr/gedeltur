<?php

/* GEDELTURBundle:Default:contacto.html.twig */
class __TwigTemplate_debfcdbe05448ce257008c03843a363b255e9b8b3711f76111e13c8ff781601b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout.html.twig");

        $this->blocks = array(
            'evento' => array($this, 'block_evento'),
            'superacion' => array($this, 'block_superacion'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_evento($context, array $blocks = array())
    {
        // line 4
        echo "
                ";
        // line 5
        if (((isset($context["cant_eventos"]) ? $context["cant_eventos"] : $this->getContext($context, "cant_eventos")) > 0)) {
            // line 6
            echo "




                <li><a href=\"#\" >Evento</a>

                 <ul class=\"dropdown\">
                 ";
            // line 14
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["eventos"]) ? $context["eventos"] : $this->getContext($context, "eventos")));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 15
                echo "
                
                 
               
                  <li><a href=\"";
                // line 19
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("eventos", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nombre"), "html", null, true);
                echo "</a>
                  </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "
                </ul>
                </li>
               

                ";
        }
        // line 28
        echo "

                  ";
    }

    // line 32
    public function block_superacion($context, array $blocks = array())
    {
        // line 33
        echo "

    <div class=\" servicio\" xmlns=\"http://www.w3.org/1999/html\">
            <div class=\"main\">
                <div class=\"container\">

                    <div class=\"row\">


                        <h2 class=\"comments-title\">(";
        // line 42
        echo twig_escape_filter($this->env, (isset($context["contador"]) ? $context["contador"] : $this->getContext($context, "contador")), "html", null, true);
        echo ") Comentarios</h2>




                        <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                        <a href=\"";
        // line 48
        echo $this->env->getExtension('routing')->getPath("correo_superacion");
        echo "\" ><button type=\"button\" style=\"background: #0B5111 none repeat scroll 0%\" class=\"btn btn-primary\">Superación  (";
        echo twig_escape_filter($this->env, (isset($context["superacion"]) ? $context["superacion"] : $this->getContext($context, "superacion")), "html", null, true);
        echo ")</button></a>

                        <!-- Indicates a successful or positive action -->
                        <a href=\"";
        // line 51
        echo $this->env->getExtension('routing')->getPath("correo_sct");
        echo "\" ><button style=\"background: rgb(19, 42, 141) none repeat scroll 0% 0%;\" type=\"button\" class=\"btn btn-success\">Servicios Científico Técnicos  (";
        echo twig_escape_filter($this->env, (isset($context["servicio"]) ? $context["servicio"] : $this->getContext($context, "servicio")), "html", null, true);
        echo ")</button></a>

                        <!-- Contextual button for informational alert messages -->
                            <a href=\"";
        // line 54
        echo $this->env->getExtension('routing')->getPath("correo_inv");
        echo "\" > <button type=\"button\" class=\"btn btn-info\">Investigación  (";
        echo twig_escape_filter($this->env, (isset($context["investigacion"]) ? $context["investigacion"] : $this->getContext($context, "investigacion")), "html", null, true);
        echo ")</button></a>

                        <!-- Indicates caution should be taken with this action -->

                        <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                                <a href=\"";
        // line 59
        echo $this->env->getExtension('routing')->getPath("correo_otro");
        echo "\" ><button type=\"button\" class=\"btn btn-danger\">Otro  (";
        echo twig_escape_filter($this->env, (isset($context["otro"]) ? $context["otro"] : $this->getContext($context, "otro")), "html", null, true);
        echo ") </button></a>



                        <div class=\"col-md-8\">



                            <div id=\"comments\" class=\"scrollbar\">


                                ";
        // line 70
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["correo"]) ? $context["correo"] : $this->getContext($context, "correo")));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 71
            echo "
                            <ol class=\"comments-list\">
                                <li>
                                    <div class=\"comment-box clearfix\">

                                        <div class=\"avatar\"><img src=\"";
            // line 76
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/iconos/pregunta.jpg"), "html", null, true);
            echo "\" alt=\"\"></div>

                                        <div class=\"comment-content\">
                                            <div class=\"comment-meta\">
                                                <span class=\"comment-by\">";
            // line 80
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "nombre"))), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "apellidos"))), "html", null, true);
            echo "   </span>
                                                <span class=\"comment-date\">";
            // line 81
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "fecha"), "d/m/Y"), "html", null, true);
            echo "</span>
                                                ";
            // line 82
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 83
                echo "                                                <span class=\"reply-link\"><a  href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("correo_edit", array("id" => $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "id"))), "html", null, true);
                echo "\">Atender</a></span>

                                                <span class=\"reply-link\"><a  href=\"";
                // line 85
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("correo_eliminar", array("id" => $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "id"))), "html", null, true);
                echo "\">Eliminar</a></span>

                                                ";
            }
            // line 88
            echo "                                            </div>

                                            <p>Sujeto:<strong>  ";
            // line 90
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "sujeto"))), "html", null, true);
            echo "</strong></p>

                                            <p>Mensaje: <strong> ";
            // line 92
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "mensaje"))), "html", null, true);
            echo "</strong></p>
                                        </div>
                                    </div>
                                    ";
            // line 95
            if (($this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "respuesta") != "")) {
                // line 96
                echo "                                    <ul>
                                        <li>
                                            <div class=\"comment-box clearfix\">
                                                <div class=\"avatar\"><img src=\"";
                // line 99
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/gedeltur/iconos/índice.jpg"), "html", null, true);
                echo "\" alt=\"\"></div>
                                                <div class=\"comment-content\">
                                                    <div class=\"comment-meta\">
                                                        <span class=\"comment-by\">Responde:";
                // line 102
                echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "nombreresponde"))), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "apellidosresponde"))), "html", null, true);
                echo " </span>
                                                        <span class=\"comment-date\"></span>

                                                    </div>
                                                    <p>";
                // line 106
                echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "respuesta"))), "html", null, true);
                echo "</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    ";
            }
            // line 112
            echo "
                                </li>

                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 116
        echo "
                            </ol>
                                </div>



















                            <!-- Classic Heading -->
                            <h4 style=\"background-color: rgb(255, 255, 255);text-align: justify;\" class=\"classic-title\"><span>Contáctenos</span></h4>

                            <!-- Start Contact Form -->


                            ";
        // line 144
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
                            ";
        // line 145
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "



                            <div class=\"contact-form\" id=\"contact-form\">



                                <div class=\"form-group\">

                                    <div class=\"col-sm-2 control-label\">";
        // line 155
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "tematica"), 'label');
        echo "</div>

                                    <div class=\"col-sm-8\">
                                        ";
        // line 158
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "tematica"), 'widget', array("attr" => array("class" => "controls", "placeholder" => "Temática")));
        echo "
                                    </div>

                                    <div class=\"col-sm-2\">
                                        <p class=\"help-block\"> ";
        // line 162
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "tematica"), 'errors');
        echo "</p>
                                    </div>
                                </div>


                                </br>
                                </br>




                            <div class=\"form-group\">


                                <div class=\"col-sm-8\">
                                    ";
        // line 177
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "sujeto"), 'widget', array("attr" => array("class" => "controls", "placeholder" => "Sujeto")));
        echo "
                                </div>

                                <div class=\"col-sm-2\">
                                    <p class=\"help-block\"> ";
        // line 181
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "sujeto"), 'errors');
        echo "</p>
                                </div>
                            </div>



                            <div class=\"form-group\">


                                <div class=\"col-sm-8\">
                                    ";
        // line 191
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mensaje"), 'widget', array("attr" => array("class" => "controls", "placeholder" => "Mensaje")));
        echo "
                                </div>

                                <div class=\"col-sm-2\">
                                    <p class=\"help-block\"> ";
        // line 195
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mensaje"), 'errors');
        echo "</p>
                                </div>
                            </div>



                                <div class=\"form-group\">
                                    <div class=\"col-sm-8\">

                                            ";
        // line 204
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "submit"), 'widget', array("attr" => array("class" => "btn btn-info ", "float" => "left")));
        echo "

                                        <a href=\"";
        // line 206
        echo $this->env->getExtension('routing')->getPath("correo_nuevo");
        echo "\">
                                            <button type=\"button\" class=\"btn btn-success\">Cancelar</button>
                                        </a>

                                    </div>
                                </div>



                            </div>

                            ";
        // line 217
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "



                            <!-- End Contact Form -->


                            </div>

                        <div class=\"col-md-4\">

                            <!-- Classic Heading -->
                            <h4 class=\"classic-title\"><span style=\"color: rgb(251, 251, 251);\">Información</span></h4>

                            <!-- Some Info -->
                            <p>Por motivo de seguridad almacenamos su nombre y contacto en cada uno de sus mensajes para garantizar una atención personal y  la integridad de nuestro sitio.</p>

                            <!-- Divider -->
                            <div class=\"hr1\" style=\"margin-bottom:10px;\"></div>

                            <!-- Info - Icons List -->
                            <ul class=\"icons-list\">

                                <li><i class=\"fa fa-envelope-o\"></i> <strong>Correo:</strong> admin@gedeltur.com</li>
                                <li><i class=\"fa fa-mobile\"></i> <strong>Teléfono:</strong> 48781669</li>
                            </ul>

                            <!-- Divider -->
                            <div class=\"hr1\" style=\"margin-bottom:15px;\"></div>

                            <!-- Classic Heading -->


                            <!-- Info - List -->


                        </div>

                    </div>

                </div>
    </div>
            <!-- .container -->
        </div>





";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Default:contacto.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  380 => 217,  366 => 206,  361 => 204,  349 => 195,  342 => 191,  329 => 181,  322 => 177,  304 => 162,  297 => 158,  291 => 155,  278 => 145,  274 => 144,  244 => 116,  235 => 112,  226 => 106,  217 => 102,  211 => 99,  206 => 96,  204 => 95,  198 => 92,  193 => 90,  189 => 88,  183 => 85,  177 => 83,  175 => 82,  171 => 81,  165 => 80,  158 => 76,  151 => 71,  147 => 70,  131 => 59,  121 => 54,  113 => 51,  105 => 48,  96 => 42,  85 => 33,  82 => 32,  76 => 28,  68 => 22,  57 => 19,  51 => 15,  47 => 14,  37 => 6,  35 => 5,  32 => 4,  29 => 3,);
    }
}

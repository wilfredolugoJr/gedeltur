<?php

/* GEDELTURBundle:Plantilla:index.html.twig */
class __TwigTemplate_de9c70afd3396b6dae182e05355412f14649257f7e6a0b7e1dda3b097449ca70 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:plantilla.html.twig");

        $this->blocks = array(
            'boton' => array($this, 'block_boton'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:plantilla.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_boton($context, array $blocks = array())
    {
        // line 4
        echo "     
        <a  href=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("plantilla_new");
        echo "\"><button type=\"button\" class=\"btn btn-warning warning_22\"><span class=\"glyphicon glyphicon-plus\"  id=\"pencil\"></span>   Nuevo</button></a>
         


    
</br>
        </br>
    ";
    }

    // line 14
    public function block_title($context, array $blocks = array())
    {
        echo "Trabajadores <span class=\"badge\">";
        echo twig_escape_filter($this->env, (isset($context["contador"]) ? $context["contador"] : $this->getContext($context, "contador")), "html", null, true);
        echo "</span>";
    }

    // line 17
    public function block_body($context, array $blocks = array())
    {
        // line 22
        echo "<div class=\"table-responsive\">

        
    <table class=\"table table-striped table-bordered table-hover dataTable no-footer\"  id=\"dataTables-example\">
    

    
        <thead>
            <tr>
           
                <th>Imagen</th>
                <th>Carné de Identidad</th>
                
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Dirección</th>
                <th>Responsabilidad</th>
                <th>Categoría Docente</th>
                <th>Categoría Científica</th>
                <th>Editar</th>
                 <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 46
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 47
            echo "            <tr>
            
                <td><img style=\"width: 80px; height: 60px\" src=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("uploads/gedeltur/fotos/" . $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "image"))), "html", null, true);
            echo "\"></td>
                <td>";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "ci"), "html", null, true);
            echo "</td>
               
                <td>";
            // line 52
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "nombre"))), "html", null, true);
            echo "</td>
                <td>";
            // line 53
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "apellidos"))), "html", null, true);
            echo "</td>
                 <td>";
            // line 54
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "direccion"))), "html", null, true);
            echo "</td>
                <td>";
            // line 55
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "cargo"))), "html", null, true);
            echo "</td>
                <td>";
            // line 56
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "catDoc"))), "html", null, true);
            echo "</td>
                <td>";
            // line 57
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "catCien"))), "html", null, true);
            echo "</td>
                <td>
                
                       



                        
                    
                        <a title=\"Editar\" href=\"";
            // line 66
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("plantilla_edit", array("id" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "id"))), "html", null, true);
            echo "\"><span class=\"glyphicon glyphicon-edit\"></span></a>
                        
                       
                </td>

                <td>
                
                       



                        
                    
                      
                        
                        <a title=\"Eliminar\" data-toggle=\"modal\" href=\"#";
            // line 81
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "id"), "html", null, true);
            echo "\"><span class=\"glyphicon glyphicon-trash\"></span></a>
                   
                </td>
            </tr>

            <div class=\"modal fade\" id=\"";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "id"), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\" style=\"display: none;\">
                    <div class=\"modal-dialog\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                                <h2 class=\"modal-title\">Confirmación de Eliminar</h2>
                            </div>
                            <div class=\"modal-body\">
                                <h5 class=\"modal-title\">¿Estás seguro que deseas eliminar el Trabajador: ";
            // line 94
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "nombre"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "apellidos"), "html", null, true);
            echo " ?</h5>
                            </div>
                            <div class=\"modal-footer\">

                                <a title=\"Editar\" href=\"";
            // line 98
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("plantilla_delete", array("id" => $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "id"))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary\">Sí</button></a>
                                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">No</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                
           </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 106
        echo "        </tbody>
    </table>

    
    <div class=\"alert alert-success\">
        ";
        // line 111
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msg"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 112
            echo "            <div class=\"msg\">
                ";
            // line 113
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 116
        echo "    </div>

    
     
       
    
  


     <script type=\"text/javascript\">
            jQuery(document).ready(function() {
                jQuery('#dataTables-example').DataTable({
                    responsive: true
                });
            });
        </script>
    ";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Plantilla:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 116,  208 => 113,  205 => 112,  201 => 111,  194 => 106,  180 => 98,  171 => 94,  160 => 86,  152 => 81,  134 => 66,  122 => 57,  118 => 56,  114 => 55,  110 => 54,  106 => 53,  102 => 52,  97 => 50,  93 => 49,  89 => 47,  85 => 46,  59 => 22,  56 => 17,  48 => 14,  36 => 5,  33 => 4,  30 => 3,);
    }
}

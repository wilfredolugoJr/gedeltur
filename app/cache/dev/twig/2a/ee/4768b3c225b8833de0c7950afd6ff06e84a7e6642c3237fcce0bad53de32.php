<?php

/* GEDELTURBundle:LineaInvestigacion:index.html.twig */
class __TwigTemplate_2aee4768b3c225b8833de0c7950afd6ff06e84a7e6642c3237fcce0bad53de32 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:plantilla.html.twig");

        $this->blocks = array(
            'boton' => array($this, 'block_boton'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:plantilla.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_boton($context, array $blocks = array())
    {
        // line 3
        echo "     
        <a  href=\"";
        // line 4
        echo $this->env->getExtension('routing')->getPath("lineainvestigacion_new");
        echo "\"><button type=\"button\" class=\"btn btn-warning warning_22\"><span class=\"glyphicon glyphicon-plus\"  id=\"pencil\"></span>   Nuevo</button></a>
        <a target=\"_blank\" href=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("generarpdf_linea");
        echo "\"><button type=\"button\" class=\"btn btn-warning warning_22\"><span class=\"glyphicon glyphicon-export\"  id=\"pencil\"></span>Exportar</button></a>

   
</br>
        </br>
    ";
    }

    // line 12
    public function block_title($context, array $blocks = array())
    {
        echo " Líneas de Investigación ";
    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        // line 17
        echo "<div class=\"table-responsive\">
   <table class=\"table table-striped table-bordered table-hover dataTable no-footer\" id=\"dataTables-example\">


        
    
        <thead>
            <tr>
                
                <th>Título</th>
                <th>Resumen</th>
                <th>Objetivos</th>
                <th>Impacto</th>
                <th>Proyectos</th>

                <th>Editar</th>
                <th>Eliminar</th>

            </tr>
        </thead>
        <tbody>
        ";
        // line 38
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 39
            echo "            <tr>
                 <td>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "titulo"), "html", null, true);
            echo "</td>
                <td><p>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "resumen"), "html", null, true);
            echo "</p></td>
                <td><p>";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "objetivo"), "html", null, true);
            echo "</p></td>
                <td><p>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "impacto"), "html", null, true);
            echo "</p></td>

                <td>


                    ";
            // line 48
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "proyecto"));
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                // line 49
                echo "

                ";
                // line 51
                echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["p"]) ? $context["p"] : $this->getContext($context, "p")), "titulo"))), "html", null, true);
                echo " .  </br>





                     ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 58
            echo "                </td>
               
             <!--   <td>


               ";
            // line 63
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "proyecto"));
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                // line 64
                echo "                    ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["p"]) ? $context["p"] : $this->getContext($context, "p")), "rrhh"));
                foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                    // line 65
                    echo "

                ";
                    // line 67
                    echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["r"]) ? $context["r"] : $this->getContext($context, "r")), "nombre"))), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["r"]) ? $context["r"] : $this->getContext($context, "r")), "apellidos"))), "html", null, true);
                    echo ".  </br>





                     ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 74
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "               </td>-->
                <td>
                
                        


                       

                        <a title=\"Editar\" href=\"";
            // line 83
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("lineainvestigacion_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            // line 84
            echo "\"><span class=\"glyphicon glyphicon-edit\"></span></a>
                        </td>


                        <td>
                        <a title=\"Eliminar\" data-toggle=\"modal\" href=\"#";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
            echo "\"><span class=\"glyphicon glyphicon-trash\"></span></a>
                   
                </td>
            </tr>

            <div class=\"modal fade\" id=\"";
            // line 94
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\" style=\"display: none;\">
                    <div class=\"modal-dialog\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                                <h2 class=\"modal-title\">Confirmación de Eliminar</h2>
                            </div>
                            <div class=\"modal-body\">
                                <h5 class=\"modal-title\">¿Estás seguro que deseas eliminar la Línea de Investigación: ";
            // line 102
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "titulo"))), "html", null, true);
            echo " ?</h5>
                            </div>
                            <div class=\"modal-footer\">

                                <a title=\"Editar\" href=\"";
            // line 106
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("lineainvestigacion_delete", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary\">Sí</button></a>
                                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">No</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                
           </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 114
        echo "        </tbody>
    </table>

    <div class=\"alert alert-success\">
        ";
        // line 118
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msg"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 119
            echo "            <div class=\"msg\">
                ";
            // line 120
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 122
        echo "            </div>

        
    

     <script type=\"text/javascript\">
            jQuery(document).ready(function() {
                jQuery('#dataTables-example').DataTable({
                    responsive: true
                });
            });
        </script>
    ";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:LineaInvestigacion:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  251 => 122,  243 => 120,  240 => 119,  236 => 118,  230 => 114,  216 => 106,  209 => 102,  198 => 94,  190 => 89,  183 => 84,  181 => 83,  171 => 75,  165 => 74,  150 => 67,  146 => 65,  141 => 64,  137 => 63,  130 => 58,  117 => 51,  113 => 49,  109 => 48,  101 => 43,  97 => 42,  93 => 41,  89 => 40,  86 => 39,  82 => 38,  59 => 17,  56 => 14,  50 => 12,  40 => 5,  36 => 4,  33 => 3,  30 => 2,);
    }
}

<?php

/* GEDELTURBundle:Proyecto:index.html.twig */
class __TwigTemplate_0196797fdaf4558eb80c9a8f8bf4bb9c299c5f4f0b10cd38c37cb4ff584ab013 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:plantilla.html.twig");

        $this->blocks = array(
            'boton' => array($this, 'block_boton'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:plantilla.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["pagina"] = "proyecto";
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_boton($context, array $blocks = array())
    {
        // line 5
        echo "    
         
   <a  href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("proyecto_new");
        echo "\"><button type=\"button\" class=\"btn btn-warning warning_22\"><span class=\"glyphicon glyphicon-plus\"  id=\"pencil\"></span>   Nuevo</button></a>
   <a target=\"_blank\" href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("generarpdf_proyecto");
        echo "\"><button type=\"button\" class=\"btn btn-warning warning_22\"><span class=\"glyphicon glyphicon-export\"  id=\"pencil\"></span>   Exportar</button></a>



</br>
</br>



    ";
    }

    // line 19
    public function block_title($context, array $blocks = array())
    {
        echo "Proyectos";
    }

    // line 22
    public function block_body($context, array $blocks = array())
    {
        // line 36
        echo "<table class=\"table table-striped \"  id=\"dataTables-example\">
        <thead>
            <tr>
              
                <th>Título</th>
                <th>Impacto</th>
                <th>Objetivos</th>
                <th>Líneas de Investigación</th>
                 <th>Integrantes</th>
                <th>Jefe de Proyecto</th>

                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 52
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 53
            echo "            <tr>
               
                <td>";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "titulo"), "html", null, true);
            echo "</td>
                <td>";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "impacto"), "html", null, true);
            echo "</td>
                <td>";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "objetivo"), "html", null, true);
            echo "</td>
                <td>


                    ";
            // line 61
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "lineas"));
            foreach ($context['_seq'] as $context["_key"] => $context["l"]) {
                // line 62
                echo "

                ";
                // line 64
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["l"]) ? $context["l"] : $this->getContext($context, "l")), "titulo"), "html", null, true);
                echo "  . </br>





                     ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['l'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 71
            echo "                </td>
                <td>

           
               ";
            // line 75
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "rrhh"));
            foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                // line 76
                echo "               
            
                ";
                // line 78
                echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["r"]) ? $context["r"] : $this->getContext($context, "r")), "nombre"))), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["r"]) ? $context["r"] : $this->getContext($context, "r")), "apellidos"))), "html", null, true);
                echo " . </br>
            
            
                
            
               
                     ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 85
            echo "               </td>

                <td>";
            // line 87
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "plantilla"), "id"), "nombre"))), "html", null, true);
            echo "  ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "plantilla"), "id"), "apellidos"))), "html", null, true);
            echo "</td>
                <td>
                
                        


                        

                        <a title=\"Editar\" href=\"";
            // line 95
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("proyecto_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\"><span class=\"glyphicon glyphicon-edit\"></span></a>

                        </td>
                        <td>
                        
                        <a title=\"Eliminar\" data-toggle=\"modal\" href=\"#";
            // line 100
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
            echo "\"><span class=\"glyphicon glyphicon-trash\"></span></a>
                    
                </td>
            </tr>

             <div class=\"modal fade\" id=\"";
            // line 105
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\" style=\"display: none;\">
                    <div class=\"modal-dialog\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
                                <h2 class=\"modal-title\">Confirmación de Eliminar</h2>
                            </div>
                            <div class=\"modal-body\">
                                <h5 class=\"modal-title\">¿Estás seguro que desea eliminar el Proyecto: ";
            // line 113
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "titulo"), "html", null, true);
            echo "? </h5>
                            </div>
                            <div class=\"modal-footer\">

                                <a title=\"Eliminar\" href=\"";
            // line 117
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("proyecto_delete", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
            echo "\"><button type=\"button\" class=\"btn btn-primary\">Sí</button></a>
                                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">No</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                
           </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 125
        echo "        </tbody>
    </table>
<div class=\"alert alert-success\">
        ";
        // line 128
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msg"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 129
            echo "            <div class=\"msg\">
                ";
            // line 130
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 132
        echo "           
            
          
           
      
    

     <script type=\"text/javascript\">
            jQuery(document).ready(function() {
                jQuery('#dataTables-example').DataTable({
                    responsive: true
                });
            });
        </script>
    ";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Proyecto:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  242 => 132,  234 => 130,  231 => 129,  227 => 128,  222 => 125,  208 => 117,  201 => 113,  190 => 105,  182 => 100,  174 => 95,  161 => 87,  157 => 85,  142 => 78,  138 => 76,  134 => 75,  128 => 71,  115 => 64,  111 => 62,  107 => 61,  100 => 57,  96 => 56,  92 => 55,  88 => 53,  84 => 52,  66 => 36,  63 => 22,  57 => 19,  43 => 8,  39 => 7,  35 => 5,  32 => 4,  27 => 1,);
    }
}

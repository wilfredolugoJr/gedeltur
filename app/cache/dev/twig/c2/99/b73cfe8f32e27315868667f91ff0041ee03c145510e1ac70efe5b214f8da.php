<?php

/* GEDELTURBundle:Default:rrhhtabla.html.twig */
class __TwigTemplate_c299b73cfe8f32e27315868667f91ff0041ee03c145510e1ac70efe5b214f8da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout.html.twig");

        $this->blocks = array(
            'evento' => array($this, 'block_evento'),
            'superacion' => array($this, 'block_superacion'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_evento($context, array $blocks = array())
    {
        // line 4
        echo "
                ";
        // line 5
        if (((isset($context["cant_eventos"]) ? $context["cant_eventos"] : $this->getContext($context, "cant_eventos")) > 0)) {
            // line 6
            echo "




                <li><a href=\"#\" >Evento</a>

                 <ul class=\"dropdown\">
                 ";
            // line 14
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["eventos"]) ? $context["eventos"] : $this->getContext($context, "eventos")));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 15
                echo "
                
                 
               
                  <li><a href=\"";
                // line 19
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("eventos", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nombre"), "html", null, true);
                echo "</a>
                  </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "
                </ul>
                </li>
               

                ";
        }
        // line 28
        echo "

                  ";
    }

    // line 32
    public function block_superacion($context, array $blocks = array())
    {
        // line 33
        echo "

 <div id=\"trabajadores\">
    <div class=\"section\" style=\"background:#fff;\">
      <div class=\"container\">

        <!-- Start Big Heading -->
        <div class=\"big-title text-center\" data-animation=\"fadeInDown\" data-animation-delay=\"01\">

           <h1>";
        // line 42
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["rrhh"]) ? $context["rrhh"] : $this->getContext($context, "rrhh")), "nombre"))), "html", null, true);
        echo "  ";
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["rrhh"]) ? $context["rrhh"] : $this->getContext($context, "rrhh")), "apellidos"))), "html", null, true);
        echo "</h1>

           <img style=\"width: 250px; height: 200px \" src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("uploads/gedeltur/fotos/" . $this->getAttribute((isset($context["rrhh"]) ? $context["rrhh"] : $this->getContext($context, "rrhh")), "image"))), "html", null, true);
        echo "\">

           

        </div>
        <!-- End Big Heading -->

        <!-- Some Text -->
        

       <div class=\"row\">
        <!-- Start Team Members -->
        

          <!-- Start Memebr 1 -->
 
           <div class=\"bs-example\">
           <div class=\"table-responsive\">
      <table   class=\"table table-hover\" id=\"dataTables-example\">
        <thead>
          <tr>

              <th>Contactos</th>
            <th>Asignaturas</th>
            <th>Programas Académicos</th>
            <th>Líneas de Investigación</th>
            <th>Proyectos</th>
          </tr>
        </thead>
        <tbody>
          <tr>



              <td>
                  ";
        // line 79
        if (((isset($context["con"]) ? $context["con"] : $this->getContext($context, "con")) > 0)) {
            // line 80
            echo "
                      <h5>Correo: <strong>";
            // line 81
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contactos"]) ? $context["contactos"] : $this->getContext($context, "contactos")), "correo"), "html", null, true);
            echo "</strong></h5>
                      <h5>Celular: <strong>";
            // line 82
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contactos"]) ? $context["contactos"] : $this->getContext($context, "contactos")), "movil"), "html", null, true);
            echo "</strong></h5>
                      <h5>Facebook: <strong>";
            // line 83
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contactos"]) ? $context["contactos"] : $this->getContext($context, "contactos")), "facebook"), "html", null, true);
            echo "</strong></h5>
                      <h5>Twiter: <strong>";
            // line 84
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contactos"]) ? $context["contactos"] : $this->getContext($context, "contactos")), "twiter"), "html", null, true);
            echo "</strong></h5>
                  ";
        }
        // line 86
        echo "
              </td>


          
            
           
            

            <td>";
        // line 95
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["asignaturas"]) ? $context["asignaturas"] : $this->getContext($context, "asignaturas")));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 96
            echo "
              <a  href=\"";
            // line 97
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("asignaturas_show", array("id" => $this->getAttribute((isset($context["a"]) ? $context["a"] : $this->getContext($context, "a")), "id"))), "html", null, true);
            echo "\"> <h5><strong>";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["a"]) ? $context["a"] : $this->getContext($context, "a")), "nombre"))), "html", null, true);
            echo "      Carrera: ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["a"]) ? $context["a"] : $this->getContext($context, "a")), "carrera"))), "html", null, true);
            echo "</strong> </h5></a>


                 

                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 103
        echo "            </td>

            <td>
            ";
        // line 106
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["aca"]) ? $context["aca"] : $this->getContext($context, "aca")));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 107
            echo "

            

                  <a style=\"padding-top: 28px; padding-bottom: 28px;\"  href=\"";
            // line 111
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("diplomados_show", array("id" => $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "id"))), "html", null, true);
            echo "\"> <h5><strong>";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "titulo"))), "html", null, true);
            echo "</strong> </h5>
                  </a>

                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 115
        echo "            </td>

             <td>



            ";
        // line 121
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["li"]) ? $context["li"] : $this->getContext($context, "li")));
        foreach ($context['_seq'] as $context["_key"] => $context["l"]) {
            // line 122
            echo "
                ";
            // line 123
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["l"]) ? $context["l"] : $this->getContext($context, "l")), "proyecto"));
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                // line 124
                echo "
                    ";
                // line 125
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["p"]) ? $context["p"] : $this->getContext($context, "p")), "rrhh"));
                foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                    // line 126
                    echo "
                 ";
                    // line 127
                    if (($this->getAttribute((isset($context["r"]) ? $context["r"] : $this->getContext($context, "r")), "id") == $this->getAttribute((isset($context["rrhh"]) ? $context["rrhh"] : $this->getContext($context, "rrhh")), "id"))) {
                        // line 128
                        echo "
                 <a  href=\"";
                        // line 129
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("li_show", array("id" => $this->getAttribute((isset($context["l"]) ? $context["l"] : $this->getContext($context, "l")), "id"))), "html", null, true);
                        echo "\">  <h5><strong>";
                        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["l"]) ? $context["l"] : $this->getContext($context, "l")), "titulo"))), "html", null, true);
                        echo "</strong> </h5></a>

                 ";
                    }
                    // line 132
                    echo "
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 134
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 135
            echo "

                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['l'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 138
        echo "            </td>

            <td>

            ";
        // line 142
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pro"]) ? $context["pro"] : $this->getContext($context, "pro")));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 143
            echo "

                ";
            // line 145
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["p"]) ? $context["p"] : $this->getContext($context, "p")), "rrhh"));
            foreach ($context['_seq'] as $context["_key"] => $context["r"]) {
                // line 146
                echo "
                ";
                // line 147
                if (($this->getAttribute((isset($context["r"]) ? $context["r"] : $this->getContext($context, "r")), "id") == $this->getAttribute((isset($context["rrhh"]) ? $context["rrhh"] : $this->getContext($context, "rrhh")), "id"))) {
                    // line 148
                    echo "
                <a  href=\"";
                    // line 149
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("proyectos_show", array("id" => $this->getAttribute((isset($context["p"]) ? $context["p"] : $this->getContext($context, "p")), "id"))), "html", null, true);
                    echo "\">  <h5><strong>";
                    echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["p"]) ? $context["p"] : $this->getContext($context, "p")), "titulo"))), "html", null, true);
                    echo "  </strong> </h5></a>
                ";
                }
                // line 151
                echo "
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['r'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 153
            echo "
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 155
        echo "            </td>

          </tr>
          
        </tbody>
</table>

    </div>
    </div>
          <!-- End Memebr 1 -->

          

        </div>
        <!-- End Team Members -->

      </div>
      <!-- .container -->
    </div>
    
    </div>




 ";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Default:rrhhtabla.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  334 => 155,  327 => 153,  320 => 151,  313 => 149,  310 => 148,  308 => 147,  305 => 146,  301 => 145,  297 => 143,  293 => 142,  287 => 138,  279 => 135,  273 => 134,  266 => 132,  258 => 129,  255 => 128,  253 => 127,  250 => 126,  246 => 125,  243 => 124,  239 => 123,  236 => 122,  232 => 121,  224 => 115,  212 => 111,  206 => 107,  202 => 106,  197 => 103,  181 => 97,  178 => 96,  174 => 95,  163 => 86,  158 => 84,  154 => 83,  150 => 82,  146 => 81,  143 => 80,  141 => 79,  103 => 44,  96 => 42,  85 => 33,  82 => 32,  76 => 28,  68 => 22,  57 => 19,  51 => 15,  47 => 14,  37 => 6,  35 => 5,  32 => 4,  29 => 3,);
    }
}

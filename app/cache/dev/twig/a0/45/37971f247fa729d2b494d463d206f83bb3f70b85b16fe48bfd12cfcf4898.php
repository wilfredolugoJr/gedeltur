<?php

/* GEDELTURBundle:Default:index.html.twig */
class __TwigTemplate_a04537971f247fa729d2b494d463d206f83bb3f70b85b16fe48bfd12cfcf4898 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:plantilla.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:plantilla.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Bienvenidos a GEDELTUR

 ";
    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "




















\t\t  <div class=\"but_list\">
       <div class=\"alert alert-success\" role=\"alert\">
       
       <h1>GEDELTUR</h1>
        <p>El Centro de Estudios de Gerencia, Desarrollo Local y Turismo (GEDELTUR) adscrito a la Facultad de Ciencias Económicas y Empresariales de la Universidad de Pinar del Río, surgió en el año 2003 .Participa activamente en redes internacionales .Se dirigen como promedio unos 15 proyectos de investigación e innovación de carácter nacional, institucional y empresarial.
                        </p>
       </div>
       
      </div>
      

       <div class=\"but_list\">
       <div class=\"alert alert-success\" role=\"alert\">
       <h1>Misión</h1>
        <p>  Contribuir a la eficacia y eficiencia en la gestión empresarial y la administración pública, particularizando en el turismo, mediante la coordinación de las tareas relativas a las Funciones Estatales de “Dirección y control de la preparación y superación de dirigentes” e “Introducción de técnicas avanzadas de dirección” y la realización de actividades directas de capacitación, consultorías e investigaciones en las Ciencias Administrativas y en todas las esferas que contribuyan al desarrollo local, bajo los principios de la sustentabilidad con énfasis en el  Turismo. </p>
       </div>
       
      </div>

       <div class=\"but_list\">
       <div class=\"alert alert-success\" role=\"alert\">
       <h1>De Interés</h1>
       <p>Posee una plantilla de 15 profesores de los cuales el 67% son doctores, el 20% Master y el 80% posee categoría docente superior de Titular o Auxiliar.
                            Brinda servicios científico – técnicos en cantidad (62%) y en financiamiento (68%) captado por este concepto. Participa activamente en redes internacionales tales como: 1) Unión Iberoamericana de Municipalitas; 2) Red de Desarrollo Sostenible y Medio Ambiente; 3) Red Latino Americana y del Caribe de Monitoreo y Evaluación de Políticas Públicas;etc</p>
       </div>
       
     
     
";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 10,  37 => 9,  29 => 5,);
    }
}

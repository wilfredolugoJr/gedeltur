<?php

/* GEDELTURBundle:Default:diplomado.html.twig */
class __TwigTemplate_65090a14b7011f5a8b158745a583026e3eed673d3de81457306173c63b6b6da8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:superacion.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'especifica' => array($this, 'block_especifica'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:superacion.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "\t\t<h1>Diplomados</h1>

";
    }

    // line 9
    public function block_especifica($context, array $blocks = array())
    {
        // line 10
        echo "
    <div class=\"panel panel-primary\">
        <div class=\"panel-heading\">
            <h3 class=\"panel-title\">Cursos</h3>
        </div>
        <div class=\"panel-body\">
            ";
        // line 16
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "curso"));
        foreach ($context['_seq'] as $context["_key"] => $context["cur"]) {
            // line 17
            echo "

                <a  href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("cursos_show", array("id" => $this->getAttribute((isset($context["cur"]) ? $context["cur"] : $this->getContext($context, "cur")), "id"))), "html", null, true);
            echo "\"> <h6> <strong>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["cur"]) ? $context["cur"] : $this->getContext($context, "cur")), "nombre"), "html", null, true);
            echo "</strong></h6></a>


            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cur'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "        </div>
    </div>



";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Default:diplomado.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 23,  57 => 19,  53 => 17,  49 => 16,  41 => 10,  38 => 9,  32 => 4,  29 => 3,);
    }
}

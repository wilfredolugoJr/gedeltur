<?php

/* GEDELTURBundle:Default:maestria.html.twig */
class __TwigTemplate_32f3d59903e5c7bb72d1be44f8059e2985d2a39ed5769cfde456d74ceb4f1074 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:superacion.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'especifica' => array($this, 'block_especifica'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:superacion.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "\t\t<h1>Maestrías</h1>

";
    }

    // line 9
    public function block_especifica($context, array $blocks = array())
    {
        // line 10
        echo "

<div class=\"panel panel-primary\">
        <div class=\"panel-heading\">
            <h3 class=\"panel-title\">Diplomados</h3>
        </div>
        <div class=\"panel-body\">

            ";
        // line 18
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "diplomado"));
        foreach ($context['_seq'] as $context["_key"] => $context["d"]) {
            // line 19
            echo "
    <a  href=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("diplomados_show", array("id" => $this->getAttribute($this->getAttribute((isset($context["d"]) ? $context["d"] : $this->getContext($context, "d")), "id"), "id"))), "html", null, true);
            echo "\"> <h5><strong>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["d"]) ? $context["d"] : $this->getContext($context, "d")), "id"), "titulo"), "html", null, true);
            echo "</strong> </h5></a>


            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['d'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "        </div>
    </div>
    </br>

    <div class=\"panel panel-primary\">
        <div class=\"panel-heading\">
            <h3 class=\"panel-title\">Programa</h3>
        </div>
        <div class=\"panel-body\">
            <p>";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "programa"), "html", null, true);
        echo "</p>
        </div>
    </div>
    </br>





   <!-- <div class=\"alert alert-info\">

        <h6><strong></strong>";
        // line 44
        if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "acreditada") == 0)) {
            // line 45
            echo "            <strong> No Acreditada!</strong>
            ";
        }
        // line 46
        if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "acreditada") == 1)) {
            // line 47
            echo "                <strong>Acreditada!</strong>
            ";
        }
        // line 48
        echo "</h6>

    </div>-->



";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Default:maestria.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 48,  103 => 47,  101 => 46,  97 => 45,  95 => 44,  81 => 33,  70 => 24,  58 => 20,  55 => 19,  51 => 18,  41 => 10,  38 => 9,  32 => 4,  29 => 3,);
    }
}

<?php

/* GEDELTURBundle:Default:servicios.html.twig */
class __TwigTemplate_5710ac20b85b68ea9e4f3089b2ec02e54c46ec7a92f320bb29d0baad8c07e384 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:menu.html.twig");

        $this->blocks = array(
            'evento' => array($this, 'block_evento'),
            'superacion' => array($this, 'block_superacion'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_evento($context, array $blocks = array())
    {
        // line 5
        echo "
    ";
        // line 6
        if (((isset($context["cant_eventos"]) ? $context["cant_eventos"] : $this->getContext($context, "cant_eventos")) > 0)) {
            // line 7
            echo "




        <li><a href=\"#\" >Evento</a>

            <ul class=\"dropdown\">
                ";
            // line 15
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["eventos"]) ? $context["eventos"] : $this->getContext($context, "eventos")));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 16
                echo "



                    <li><a href=\"";
                // line 20
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("eventos", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nombre"), "html", null, true);
                echo "</a>
                    </li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "
            </ul>
        </li>


    ";
        }
        // line 29
        echo "

";
    }

    // line 33
    public function block_superacion($context, array $blocks = array())
    {
        // line 34
        echo "




 <div id=\"superacion\" style=\"padding-bottom: 100px; padding-top: 10px\">
       
        <div class=\"big-title text-center\" data-animation=\"fadeInDown\" data-animation-delay=\"01\">
          <h1>Servicios Científico Técnicos</h1>
        </div>
        
          
                <div class=\"row sidebar-page\">


                    <!-- Page Content -->
                    <div class=\"col-md-7 page-content\">


                        <!-- Divider -->


                        <!-- Accordion -->
                        <div class=\"panel-group\" id=\"accordion\">

                           

                            <!-- Start Accordion 2 -->

            ";
        // line 63
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 64
            echo "
                            <div class=\"panel panel-default\">
                                <!-- Toggle Heading -->
                                <div class=\"panel-heading\">
                                    <h4 class=\"panel-title\">
                                        <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
            echo "\" class=\"collapsed\">
                                            <i class=\"fa fa-angle-up control-icon\"></i>
                                           ";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nombre"), "html", null, true);
            echo " ,  Tipo:";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "tipo"))), "html", null, true);
            echo "
                                        </a>
                                    </h4>
                                </div>
                                <!-- Toggle Content -->
                                <div id=\"";
            // line 76
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
            echo "\" class=\"panel-collapse collapse\">
                                    <div class=\"panel-body\">


                                        <a  href=\"";
            // line 80
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("trabajadores_show", array("id" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "rrhh"), "id"), "id"))), "html", null, true);
            echo "\"><h6> <strong>Responsable:</strong> ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "rrhh"), "id"), "nombre"))), "html", null, true);
            echo "  ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "rrhh"), "id"), "apellidos"))), "html", null, true);
            echo "</h6></a>


                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Descripción</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                <p>";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "descripcion"), "html", null, true);
            echo "</p>
                                            </div>
                                        </div>

                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Temáticas</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                ";
            // line 97
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "tematica"));
            foreach ($context['_seq'] as $context["_key"] => $context["tema"]) {
                // line 98
                echo "


                                                    <h6><strong>";
                // line 101
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["tema"]) ? $context["tema"] : $this->getContext($context, "tema")), "nombre"), "html", null, true);
                echo ".</strong></h6>

                                                    <div class=\"panel panel-info\">
                                                        <div class=\"panel-heading\">
                                                            <h3 class=\"panel-title\">Integrantes</h3>
                                                        </div>
                                                        <div class=\"panel-body\">
                                                            ";
                // line 108
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["tema"]) ? $context["tema"] : $this->getContext($context, "tema")), "plantilla"));
                foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                    // line 109
                    echo "
                                                                <h6><strong>";
                    // line 110
                    echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute((isset($context["p"]) ? $context["p"] : $this->getContext($context, "p")), "id"), "nombre"))), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute((isset($context["p"]) ? $context["p"] : $this->getContext($context, "p")), "id"), "apellidos"))), "html", null, true);
                    echo ".</strong></h6>
                                                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 112
                echo "                                                        </div>
                                                    </div></br>



                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tema'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 118
            echo "                                            </div>
                                        </div>



                                        ";
            // line 123
            if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "tipo") == "capacitación")) {
                // line 124
                echo "
                                            <div class=\"panel panel-primary\">
                                                <div class=\"panel-heading\">
                                                    <h3 class=\"panel-title\">Contenido</h3>
                                                </div>
                                                <div class=\"panel-body\">
                                                    ";
                // line 130
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["contenidos"]) ? $context["contenidos"] : $this->getContext($context, "contenidos")));
                foreach ($context['_seq'] as $context["_key"] => $context["con"]) {
                    // line 131
                    echo "
                                                        ";
                    // line 132
                    if (($this->getAttribute($this->getAttribute((isset($context["con"]) ? $context["con"] : $this->getContext($context, "con")), "id"), "id") == $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))) {
                        // line 133
                        echo "

                                                             <p>";
                        // line 135
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["con"]) ? $context["con"] : $this->getContext($context, "con")), "contenido"), "html", null, true);
                        echo "</p>

                                                        ";
                    }
                    // line 138
                    echo "
                                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['con'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 140
                echo "                                                </div>
                                            </div>



                                        ";
            }
            // line 146
            echo "

                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Contactos</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                ";
            // line 153
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["contactos"]) ? $context["contactos"] : $this->getContext($context, "contactos")));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 154
                echo "

                                                    ";
                // line 156
                if (($this->getAttribute($this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "rrhh"), "id") == $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "rrhh"), "id"), "id"))) {
                    // line 157
                    echo "
                                                        <h5><strong>Contactos</strong></h5>
                                                        <h6><strong>Correo :</strong>";
                    // line 159
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "correo"), "html", null, true);
                    echo "</h6>
                                                        <h6><strong>Facebook:</strong> ";
                    // line 160
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "facebook"), "html", null, true);
                    echo "</h6>
                                                        <h6><strong>Móvil :</strong>";
                    // line 161
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "movil"), "html", null, true);
                    echo "</h6>


                                                    ";
                }
                // line 165
                echo "

                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 168
            echo "                                            </div>
                                        </div>


                                         
                                    </div>
                                </div>
                            </div>
                            
                            
             ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 179
        echo "                            
                            
                            
                            
                            
                            
                            <!-- End Accordion 3 -->

                            <!-- Start Accordion 3 -->

                            <!-- End Accordion 3 -->

                        </div>
                        <!-- End Accordion -->

                    </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    <!-- End Page Content-->


                    <!--Sidebar-->
                    <div class=\"col-md-3 sidebar right-sidebar\" style=\"float: right\">

                        <!-- Search Widget
                        <div class=\"widget widget-search\">
                            <form action=\"#\">
                                <input type=\"search\" placeholder=\"Enter Keywords...\" />
                                <button class=\"search-btn\" type=\"submit\"><i class=\"fa fa-search\"></i></button>
                            </form>
                        </div>

                        <!-- Categories Widget -->
                        

                            <div class=\"widget widget-categories\">
                            <h4>Servicios Científico Técnicos <span class=\"head-line\"></span></h4>
                            <ul>
                            <li>
                                    <a href=";
        // line 229
        echo $this->env->getExtension('routing')->getPath("sct_filtro", array("tipo" => "capacitacion"));
        echo " >Capacitaciones</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 232
        echo $this->env->getExtension('routing')->getPath("sct_filtro", array("tipo" => "consultoría"));
        echo "\">Consultorías</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 235
        echo $this->env->getExtension('routing')->getPath("sct_filtro", array("tipo" => "asesoría"));
        echo "\">Asesorías </a>
                                </li>


                            </ul>
                        </div>
                       
                        <!-- Popular Posts widget -->

                        <!--End sidebar-->


                    </div>
                </div>
            </div>


 ";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Default:servicios.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  400 => 235,  394 => 232,  388 => 229,  336 => 179,  320 => 168,  312 => 165,  305 => 161,  301 => 160,  297 => 159,  293 => 157,  291 => 156,  287 => 154,  283 => 153,  274 => 146,  266 => 140,  259 => 138,  253 => 135,  249 => 133,  247 => 132,  244 => 131,  240 => 130,  232 => 124,  230 => 123,  223 => 118,  212 => 112,  202 => 110,  199 => 109,  195 => 108,  185 => 101,  180 => 98,  176 => 97,  164 => 88,  149 => 80,  142 => 76,  132 => 71,  127 => 69,  120 => 64,  116 => 63,  85 => 34,  82 => 33,  76 => 29,  68 => 23,  57 => 20,  51 => 16,  47 => 15,  37 => 7,  35 => 6,  32 => 5,  29 => 4,);
    }
}

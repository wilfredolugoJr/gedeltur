<?php

/* GEDELTURBundle:Plantilla:new.html.twig */
class __TwigTemplate_e59833d34973f6629c1903be61f4d0171e109372314ffcfe04e7f45878eb4709 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:plantilla.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:plantilla.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        // line 5
        echo "    Nuevo Trabajador 
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "<div class=\"bs-example4\" >
    
";
        // line 11
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 12
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
    
<div class=\"input-group\">
         <div class=\"col-sm-2 control-label\">";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "ci"), 'label', array("label" => "Carné de Identidad"));
        echo "</div>

        <div class=\"col-sm-8\">
            <div class=\"input-group\">
                                    <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
            ";
        // line 24
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "ci"), 'widget', array("attr" => array("class" => "form-control1")));
        echo "
        </div>
        </div>
        <div class=\"col-sm-2\">
            <p class=\"help-block\">";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "ci"), 'errors');
        echo "</p>
        </div>
    </div>



    <div class=\"input-group\">
       <div class=\"col-sm-2 control-label\"> ";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "nombre"), 'label');
        echo "</div>
        <div class=\"col-sm-8\">
            <div class=\"input-group\">
                                    <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
        ";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "nombre"), 'widget', array("attr" => array("class" => "form-control1")));
        echo "
            </div>
            </div>
        <div class=\"col-sm-2\">
            <p class=\"help-block\">";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "nombre"), 'errors');
        echo "</p>
        </div>

    </div>



    <div class=\"input-group\">
        <div class=\"col-sm-2 control-label\">";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "apellidos"), 'label', array("label" => "Apellidos"));
        echo "</div>

        <div class=\"col-sm-8\">
            <div class=\"input-group\">
                                    <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
            ";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "apellidos"), 'widget', array("attr" => array("class" => "form-control1")));
        echo "
        </div>
            </div>

        <div class=\"col-sm-2\">
            <p class=\"help-block\"> ";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "apellidos"), 'errors');
        echo "</p>
        </div>
    </div>


    <div class=\"input-group\">
        <div class=\"col-sm-2 control-label\"> ";
        // line 75
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "direccion"), 'label', array("label" => "Dirección"));
        echo "</div>
        <div class=\"col-sm-8\">
            <div class=\"input-group\">
                                    <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
            ";
        // line 83
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "direccion"), 'widget', array("attr" => array("class" => "form-control1")));
        echo "
        </div>
            </div>
        <div class=\"col-sm-2\">
            <p class=\"help-block\">  ";
        // line 87
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "direccion"), 'errors');
        echo "</p>
        </div>
    </div>


    <div class=\"input-group\">
       <div class=\"col-sm-2 control-label \"> ";
        // line 93
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "image"), 'label', array("label" => "Imagen "));
        echo "</div>


        <div class=\"col-sm-8\">


            ";
        // line 99
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "image"), 'widget', array("attr" => array("id" => "exampleInputFile")));
        echo "

        </div>


        <div class=\"col-sm-2\">
            <p class=\"help-block\">   ";
        // line 105
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "id"), "image"), 'errors');
        echo "</p>
        </div>

    </div>


<div class=\"input-group\">

       <div class=\"col-sm-2 control-label\"> ";
        // line 113
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cat_doc"), 'label', array("label" => "Categoría Docente"));
        echo "</div>
        <div class=\"col-sm-3\">
            <div class=\"input-group\">
         <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
        ";
        // line 121
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cat_doc"), 'widget', array("attr" => array("class" => "form-control1 js-example-basic-singless")));
        echo "
            </div>
            </div>
       <div class=\"col-sm-2 control-label\"> ";
        // line 124
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cat_cien"), 'label', array("label" => "Categoría Científica"));
        echo "</div>
        <div class=\"col-sm-3\">
            <div class=\"input-group\">
         <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
        ";
        // line 132
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cat_cien"), 'widget', array("attr" => array("class" => "form-control1 js-example-basic-singles")));
        echo "
            </div>
            </div>

    </div>


    

    <div class=\"input-group\">
         <div class=\"col-sm-2 control-label\">";
        // line 142
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cargo"), 'label', array("label" => "Responsabilidad"));
        echo "</div>

        <div class=\"col-sm-3\">
            <div class=\"input-group\">
         <span class=\"input-group-addon\">
                                       <span  class=\"glyphicon glyphicon-asterisk\">

                                    </span>
                                    </span>
            ";
        // line 151
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cargo"), 'widget', array("attr" => array("class" => "form-control1 js-example-basic-single")));
        echo "
        </div>
            </div>
        <div class=\"col-sm-2\">
            <p class=\"help-block\">";
        // line 155
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cargo"), 'errors');
        echo "</p>
        </div>
    </div>



    

   



   

    


    <div class=\"panel-footer\">
    <div class=\"row\">
    <div class=\"col-sm-8 col-sm-offset-2\">
        ";
        // line 175
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "submit"), 'widget', array("attr" => array("class" => "btn btn-primary")));
        echo "
       
        <a href=\"";
        // line 177
        echo $this->env->getExtension('routing')->getPath("plantilla");
        echo "\">
            <button type=\"button\" class=\"btn btn-warning warning_22\">Cancelar</button>
        </a>
    </div>
    </div>
    </div>

<script type=\"text/javascript\">
        \$(function () {
            //validaciones js
            \$('#gedeltur_bundle_plantilla_id_ci').keydown(ci);
              \$('#gedeltur_bundle_plantilla_cargo').keydown(letras);
            
        
            \$('#gedeltur_bundle_plantilla_id_nombre').keydown(letras);
           \$('#gedeltur_bundle_plantilla_id_apellidos').keydown(letras);
           \$('#gedeltur_bundle_plantilla_id_direccion').keydown(adress);
        })
    </script>

    <div class=\"alert alert-success\">
        ";
        // line 198
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session"), "flashbag"), "get", array(0 => "msg"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 199
            echo "            <div class=\"msg\">
                ";
            // line 200
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : $this->getContext($context, "flashMessage")), "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 203
        echo "    </div>

    ";
        // line 205
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "



     <script type=\"text/javascript\">
         \$(document).ready(function() {
             \$(\".js-example-basic-single\").select2();
         });
     </script>

     <script type=\"text/javascript\">
         \$(document).ready(function() {
             \$(\".js-example-basic-singles\").select2();
         });
     </script>

     <script type=\"text/javascript\">
         \$(document).ready(function() {
             \$(\".js-example-basic-singless\").select2();
         });
     </script>
        
";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Plantilla:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  329 => 205,  325 => 203,  316 => 200,  313 => 199,  309 => 198,  285 => 177,  280 => 175,  257 => 155,  250 => 151,  238 => 142,  225 => 132,  214 => 124,  208 => 121,  197 => 113,  186 => 105,  177 => 99,  168 => 93,  159 => 87,  152 => 83,  141 => 75,  132 => 69,  124 => 64,  112 => 55,  101 => 47,  94 => 43,  83 => 35,  73 => 28,  66 => 24,  54 => 15,  48 => 12,  44 => 11,  40 => 9,  37 => 8,  32 => 5,  29 => 4,);
    }
}

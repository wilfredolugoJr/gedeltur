<?php

/* GEDELTURBundle:Default:asignaturas.html.twig */
class __TwigTemplate_e248eb63a7e85a67a39eee82be3ecce066d4180a22678b08019a1a4e2bec59ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("GEDELTURBundle:Default:menu.html.twig");

        $this->blocks = array(
            'evento' => array($this, 'block_evento'),
            'nombre' => array($this, 'block_nombre'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "GEDELTURBundle:Default:menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_evento($context, array $blocks = array())
    {
        // line 3
        echo "
                ";
        // line 4
        if (((isset($context["cant_eventos"]) ? $context["cant_eventos"] : $this->getContext($context, "cant_eventos")) > 0)) {
            // line 5
            echo "




                <li><a href=\"#\" >Evento</a>

                 <ul class=\"dropdown\">
                 ";
            // line 13
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["eventos"]) ? $context["eventos"] : $this->getContext($context, "eventos")));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 14
                echo "
                
                 
               
                  <li><a href=\"";
                // line 18
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("eventos", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"))), "html", null, true);
                echo "\" >";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nombre"), "html", null, true);
                echo "</a>
                  </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "
                </ul>
                </li>
               

                ";
        }
        // line 27
        echo "

                  ";
    }

    // line 32
    public function block_nombre($context, array $blocks = array())
    {
        // line 33
        echo "           <h1><strong>Asignaturas  </strong></h1>
 ";
    }

    // line 43
    public function block_contenido($context, array $blocks = array())
    {
        echo "                     
                 
                    
                    
                    
                                     
                        <div class=\"panel-group\" id=\"accordion\">

                           

                            <!-- Start Accordion 2 -->

                              ";
        // line 55
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 56
            echo "
                            <div class=\"panel panel-default\">
                                <!-- Toggle Heading -->
                                <div class=\"panel-heading\">
                                    <h4 class=\"panel-title\">
                                        <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
            echo "\" class=\"collapsed\">
                                            <i class=\"fa fa-angle-up control-icon\"></i>
                                          Nombre:";
            // line 63
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nombre"))), "html", null, true);
            echo " ,      Carrera: ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "carrera"))), "html", null, true);
            echo "
                                        </a>
                                    </h4>
                                </div>
                                <!-- Toggle Content -->
                                <div id=\"";
            // line 68
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id"), "html", null, true);
            echo "\" class=\"panel-collapse collapse\">
                                    <div class=\"panel-body\">


                                        <a  href=\"";
            // line 72
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("trabajadores_show", array("id" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "plantilla"), "id"), "id"))), "html", null, true);
            echo "\"><h6> Docente:<strong> ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "plantilla"), "id"), "nombre"))), "html", null, true);
            echo "  ";
            echo twig_escape_filter($this->env, twig_title_string_filter($this->env, strip_tags($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "plantilla"), "id"), "apellidos"))), "html", null, true);
            echo "</strong></h6></a>
                                        <div class=\"panel panel-primary\">
                                            <div class=\"panel-heading\">
                                                <h3 class=\"panel-title\">Contactos</h3>
                                            </div>
                                            <div class=\"panel-body\">
                                                ";
            // line 78
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["contactos"]) ? $context["contactos"] : $this->getContext($context, "contactos")));
            foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                // line 79
                echo "
                                                    ";
                // line 80
                if (($this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "rrhh") == $this->getAttribute($this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "plantilla"), "id"))) {
                    // line 81
                    echo "                                                        <h6>Correo: <strong>";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "correo"), "html", null, true);
                    echo "</strong></h6>
                                                        <h6>Facebook: <strong>";
                    // line 82
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "facebook"), "html", null, true);
                    echo "</strong></h6>
                                                        <h6>Móvil: <strong>";
                    // line 83
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["c"]) ? $context["c"] : $this->getContext($context, "c")), "movil"), "html", null, true);
                    echo "</strong></h6>

                                                    ";
                }
                // line 86
                echo "
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 88
            echo "                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            
                            
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 97
        echo "                            
                            
                            
                            
                            
                            
                            <!-- End Accordion 3 -->

                            <!-- Start Accordion 3 -->

                            <!-- End Accordion 3 -->

                        </div>

                         ";
    }

    public function getTemplateName()
    {
        return "GEDELTURBundle:Default:asignaturas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 97,  184 => 88,  177 => 86,  171 => 83,  167 => 82,  162 => 81,  160 => 80,  157 => 79,  153 => 78,  140 => 72,  133 => 68,  123 => 63,  118 => 61,  111 => 56,  107 => 55,  91 => 43,  86 => 33,  83 => 32,  77 => 27,  69 => 21,  58 => 18,  52 => 14,  48 => 13,  38 => 5,  36 => 4,  33 => 3,  30 => 2,);
    }
}

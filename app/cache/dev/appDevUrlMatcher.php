<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                if (0 === strpos($pathinfo, '/_profiler/i')) {
                    // _profiler_info
                    if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                    }

                    // _profiler_import
                    if ($pathinfo === '/_profiler/import') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:importAction',  '_route' => '_profiler_import',);
                    }

                }

                // _profiler_export
                if (0 === strpos($pathinfo, '/_profiler/export') && preg_match('#^/_profiler/export/(?P<token>[^/\\.]++)\\.txt$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_export')), array (  '_controller' => 'web_profiler.controller.profiler:exportAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/admin')) {
            if (0 === strpos($pathinfo, '/admin/recursoshumanos')) {
                // recursoshumanos
                if (rtrim($pathinfo, '/') === '/admin/recursoshumanos') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'recursoshumanos');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\RecursosHumanosController::indexAction',  '_route' => 'recursoshumanos',);
                }

                // recursoshumanos_show
                if (preg_match('#^/admin/recursoshumanos/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'recursoshumanos_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\RecursosHumanosController::showAction',));
                }

                // recursoshumanos_new
                if ($pathinfo === '/admin/recursoshumanos/new') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\RecursosHumanosController::newAction',  '_route' => 'recursoshumanos_new',);
                }

                // recursoshumanos_create
                if ($pathinfo === '/admin/recursoshumanos/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_recursoshumanos_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\RecursosHumanosController::createAction',  '_route' => 'recursoshumanos_create',);
                }
                not_recursoshumanos_create:

                // recursoshumanos_edit
                if (preg_match('#^/admin/recursoshumanos/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'recursoshumanos_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\RecursosHumanosController::editAction',));
                }

                // recursoshumanos_update
                if (preg_match('#^/admin/recursoshumanos/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_recursoshumanos_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'recursoshumanos_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\RecursosHumanosController::updateAction',));
                }
                not_recursoshumanos_update:

                // recursoshumanos_delete
                if (preg_match('#^/admin/recursoshumanos/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'recursoshumanos_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\RecursosHumanosController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/plantilla')) {
                // plantilla
                if (rtrim($pathinfo, '/') === '/admin/plantilla') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'plantilla');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaController::indexAction',  '_route' => 'plantilla',);
                }

                // plantilla_show
                if (preg_match('#^/admin/plantilla/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'plantilla_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaController::showAction',));
                }

                // plantilla_new
                if ($pathinfo === '/admin/plantilla/new') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaController::newAction',  '_route' => 'plantilla_new',);
                }

                // plantilla_create
                if ($pathinfo === '/admin/plantilla/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_plantilla_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaController::createAction',  '_route' => 'plantilla_create',);
                }
                not_plantilla_create:

                // plantilla_edit
                if (preg_match('#^/admin/plantilla/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'plantilla_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaController::editAction',));
                }

                // plantilla_update
                if (preg_match('#^/admin/plantilla/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_plantilla_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'plantilla_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaController::updateAction',));
                }
                not_plantilla_update:

                // plantilla_delete
                if (preg_match('#^/admin/plantilla/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'plantilla_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/telefono')) {
                // telefono
                if (rtrim($pathinfo, '/') === '/admin/telefono') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'telefono');
                    }

                    return array (  '_controller' => 'GEDELTURBundle:Telefono:index',  '_route' => 'telefono',);
                }

                // telefono_show
                if (preg_match('#^/admin/telefono/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'telefono_show')), array (  '_controller' => 'GEDELTURBundle:Telefono:show',));
                }

                // telefono_new
                if ($pathinfo === '/admin/telefono/new') {
                    return array (  '_controller' => 'GEDELTURBundle:Telefono:new',  '_route' => 'telefono_new',);
                }

                // telefono_create
                if ($pathinfo === '/admin/telefono/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_telefono_create;
                    }

                    return array (  '_controller' => 'GEDELTURBundle:Telefono:create',  '_route' => 'telefono_create',);
                }
                not_telefono_create:

                // telefono_edit
                if (preg_match('#^/admin/telefono/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'telefono_edit')), array (  '_controller' => 'GEDELTURBundle:Telefono:edit',));
                }

                // telefono_update
                if (preg_match('#^/admin/telefono/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_telefono_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'telefono_update')), array (  '_controller' => 'GEDELTURBundle:Telefono:update',));
                }
                not_telefono_update:

                // telefono_delete
                if (preg_match('#^/admin/telefono/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'telefono_delete')), array (  '_controller' => 'GEDELTURBundle:Telefono:delete',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/co')) {
                if (0 === strpos($pathinfo, '/admin/contacto')) {
                    // contacto
                    if (rtrim($pathinfo, '/') === '/admin/contacto') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'contacto');
                        }

                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ContactoController::indexAction',  '_route' => 'contacto',);
                    }

                    // contacto_show
                    if (preg_match('#^/admin/contacto/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'contacto_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ContactoController::showAction',));
                    }

                    // contacto_new
                    if ($pathinfo === '/admin/contacto/nuevo') {
                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ContactoController::newAction',  '_route' => 'contacto_new',);
                    }

                    // contacto_create
                    if ($pathinfo === '/admin/contacto/crear') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_contacto_create;
                        }

                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ContactoController::createAction',  '_route' => 'contacto_create',);
                    }
                    not_contacto_create:

                    // contacto_edit
                    if (preg_match('#^/admin/contacto/(?P<id>[^/]++)/editat$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'contacto_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ContactoController::editAction',));
                    }

                    // contacto_update
                    if (preg_match('#^/admin/contacto/(?P<id>[^/]++)/editor$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_contacto_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'contacto_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ContactoController::updateAction',));
                    }
                    not_contacto_update:

                    // contacto_delete
                    if (preg_match('#^/admin/contacto/(?P<id>[^/]++)/eliminar$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'contacto_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ContactoController::deleteAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/admin/correo')) {
                    // correo
                    if (rtrim($pathinfo, '/') === '/admin/correo') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'correo');
                        }

                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CorreoController::indexAction',  '_route' => 'correo',);
                    }

                    // correo_show
                    if (preg_match('#^/admin/correo/(?P<id>[^/]++)/ver$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'correo_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CorreoController::showAction',));
                    }

                    // correo_new
                    if ($pathinfo === '/admin/correo/nuevo') {
                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CorreoController::newAction',  '_route' => 'correo_new',);
                    }

                    // correo_create
                    if ($pathinfo === '/admin/correo/crear') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_correo_create;
                        }

                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CorreoController::createAction',  '_route' => 'correo_create',);
                    }
                    not_correo_create:

                    // correo_edit
                    if (preg_match('#^/admin/correo/(?P<id>[^/]++)/editar$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'correo_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CorreoController::editAction',));
                    }

                    // correo_update
                    if (preg_match('#^/admin/correo/(?P<id>[^/]++)/actualizar$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_correo_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'correo_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CorreoController::updateAction',));
                    }
                    not_correo_update:

                    // correo_delete
                    if (preg_match('#^/admin/correo/(?P<id>[^/]++)/eliminar$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'correo_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CorreoController::deleteAction',));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin/gce')) {
                // gce
                if (rtrim($pathinfo, '/') === '/admin/gce') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'gce');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\GCEController::indexAction',  '_route' => 'gce',);
                }

                // gce_show
                if (preg_match('#^/admin/gce/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'gce_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\GCEController::showAction',));
                }

                // gce_new
                if ($pathinfo === '/admin/gce/nuevo') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\GCEController::newAction',  '_route' => 'gce_new',);
                }

                // gce_create
                if ($pathinfo === '/admin/gce/crear') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_gce_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\GCEController::createAction',  '_route' => 'gce_create',);
                }
                not_gce_create:

                // gce_edit
                if (preg_match('#^/admin/gce/(?P<id>[^/]++)/editar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'gce_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\GCEController::editAction',));
                }

                // gce_update
                if (preg_match('#^/admin/gce/(?P<id>[^/]++)/editor$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_gce_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'gce_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\GCEController::updateAction',));
                }
                not_gce_update:

                // gce_delete
                if (preg_match('#^/admin/gce/(?P<id>[^/]++)/eliminar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'gce_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\GCEController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/colaborador')) {
                // colaborador
                if (rtrim($pathinfo, '/') === '/admin/colaborador') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'colaborador');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ColaboradorController::indexAction',  '_route' => 'colaborador',);
                }

                // colaborador_show
                if (preg_match('#^/admin/colaborador/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'colaborador_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ColaboradorController::showAction',));
                }

                // colaborador_new
                if ($pathinfo === '/admin/colaborador/nuevo') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ColaboradorController::newAction',  '_route' => 'colaborador_new',);
                }

                // colaborador_create
                if ($pathinfo === '/admin/colaborador/crear') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_colaborador_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ColaboradorController::createAction',  '_route' => 'colaborador_create',);
                }
                not_colaborador_create:

                // colaborador_edit
                if (preg_match('#^/admin/colaborador/(?P<id>[^/]++)/editar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'colaborador_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ColaboradorController::editAction',));
                }

                // colaborador_update
                if (preg_match('#^/admin/colaborador/(?P<id>[^/]++)/editor$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_colaborador_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'colaborador_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ColaboradorController::updateAction',));
                }
                not_colaborador_update:

                // colaborador_delete
                if (preg_match('#^/admin/colaborador/(?P<id>[^/]++)/eliminar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'colaborador_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ColaboradorController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/asignatura')) {
                // asignatura
                if (rtrim($pathinfo, '/') === '/admin/asignatura') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'asignatura');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\AsignaturaController::indexAction',  '_route' => 'asignatura',);
                }

                // asignatura_show
                if (preg_match('#^/admin/asignatura/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'asignatura_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\AsignaturaController::showAction',));
                }

                // asignatura_new
                if ($pathinfo === '/admin/asignatura/nuevo') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\AsignaturaController::newAction',  '_route' => 'asignatura_new',);
                }

                // asignatura_create
                if ($pathinfo === '/admin/asignatura/crear') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_asignatura_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\AsignaturaController::createAction',  '_route' => 'asignatura_create',);
                }
                not_asignatura_create:

                // asignatura_edit
                if (preg_match('#^/admin/asignatura/(?P<id>[^/]++)/editar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'asignatura_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\AsignaturaController::editAction',));
                }

                // asignatura_update
                if (preg_match('#^/admin/asignatura/(?P<id>[^/]++)/editor$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_asignatura_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'asignatura_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\AsignaturaController::updateAction',));
                }
                not_asignatura_update:

                // asignatura_delete
                if (preg_match('#^/admin/asignatura/(?P<id>[^/]++)/eliminar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'asignatura_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\AsignaturaController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/c')) {
                if (0 === strpos($pathinfo, '/admin/contenido')) {
                    // contenido
                    if (rtrim($pathinfo, '/') === '/admin/contenido') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'contenido');
                        }

                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ContenidoController::indexAction',  '_route' => 'contenido',);
                    }

                    // contenido_show
                    if (preg_match('#^/admin/contenido/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'contenido_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ContenidoController::showAction',));
                    }

                    // contenido_new
                    if ($pathinfo === '/admin/contenido/nuevo') {
                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ContenidoController::newAction',  '_route' => 'contenido_new',);
                    }

                    // contenido_create
                    if ($pathinfo === '/admin/contenido/crear') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_contenido_create;
                        }

                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ContenidoController::createAction',  '_route' => 'contenido_create',);
                    }
                    not_contenido_create:

                    // contenido_edit
                    if (preg_match('#^/admin/contenido/(?P<id>[^/]++)/editat$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'contenido_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ContenidoController::editAction',));
                    }

                    // contenido_update
                    if (preg_match('#^/admin/contenido/(?P<id>[^/]++)/editor$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_contenido_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'contenido_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ContenidoController::updateAction',));
                    }
                    not_contenido_update:

                    // contenido_delete
                    if (preg_match('#^/admin/contenido/(?P<id>[^/]++)/eliminar$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'contenido_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ContenidoController::deleteAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/admin/curso')) {
                    // curso
                    if (rtrim($pathinfo, '/') === '/admin/curso') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'curso');
                        }

                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CursoController::indexAction',  '_route' => 'curso',);
                    }

                    // curso_show
                    if (preg_match('#^/admin/curso/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'curso_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CursoController::showAction',));
                    }

                    // curso_new
                    if ($pathinfo === '/admin/curso/nuevo') {
                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CursoController::newAction',  '_route' => 'curso_new',);
                    }

                    // curso_create
                    if ($pathinfo === '/admin/curso/crear') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_curso_create;
                        }

                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CursoController::createAction',  '_route' => 'curso_create',);
                    }
                    not_curso_create:

                    // curso_edit
                    if (preg_match('#^/admin/curso/(?P<id>[^/]++)/editar$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'curso_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CursoController::editAction',));
                    }

                    // curso_update
                    if (preg_match('#^/admin/curso/(?P<id>[^/]++)/editor$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_curso_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'curso_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CursoController::updateAction',));
                    }
                    not_curso_update:

                    // curso_delete
                    if (preg_match('#^/admin/curso/(?P<id>[^/]++)/eliminar$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'curso_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CursoController::deleteAction',));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin/diplomado')) {
                // diplomado
                if (rtrim($pathinfo, '/') === '/admin/diplomado') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'diplomado');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DiplomadoController::indexAction',  '_route' => 'diplomado',);
                }

                // diplomado_show
                if (preg_match('#^/admin/diplomado/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'diplomado_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DiplomadoController::showAction',));
                }

                // diplomado_new
                if ($pathinfo === '/admin/diplomado/nuevo') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DiplomadoController::newAction',  '_route' => 'diplomado_new',);
                }

                // diplomado_create
                if ($pathinfo === '/admin/diplomado/crear') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_diplomado_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DiplomadoController::createAction',  '_route' => 'diplomado_create',);
                }
                not_diplomado_create:

                // diplomado_edit
                if (preg_match('#^/admin/diplomado/(?P<id>[^/]++)/editar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'diplomado_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DiplomadoController::editAction',));
                }

                // diplomado_update
                if (preg_match('#^/admin/diplomado/(?P<id>[^/]++)/editor$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_diplomado_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'diplomado_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DiplomadoController::updateAction',));
                }
                not_diplomado_update:

                // diplomado_delete
                if (preg_match('#^/admin/diplomado/(?P<id>[^/]++)/eliminar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'diplomado_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DiplomadoController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/evento')) {
                // evento
                if (rtrim($pathinfo, '/') === '/admin/evento') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'evento');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\EventoController::indexAction',  '_route' => 'evento',);
                }

                // evento_show
                if (preg_match('#^/admin/evento/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'evento_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\EventoController::showAction',));
                }

                // evento_new
                if ($pathinfo === '/admin/evento/nuevo') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\EventoController::newAction',  '_route' => 'evento_new',);
                }

                // evento_create
                if ($pathinfo === '/admin/evento/crear') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_evento_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\EventoController::createAction',  '_route' => 'evento_create',);
                }
                not_evento_create:

                // evento_edit
                if (preg_match('#^/admin/evento/(?P<id>[^/]++)/editar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'evento_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\EventoController::editAction',));
                }

                // evento_update
                if (preg_match('#^/admin/evento/(?P<id>[^/]++)/editor$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_evento_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'evento_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\EventoController::updateAction',));
                }
                not_evento_update:

                // evento_delete
                if (preg_match('#^/admin/evento/(?P<id>[^/]++)/eliminar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'evento_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\EventoController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/institucion')) {
                // institucion
                if (rtrim($pathinfo, '/') === '/admin/institucion') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'institucion');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\InstitucionController::indexAction',  '_route' => 'institucion',);
                }

                // institucion_show
                if (preg_match('#^/admin/institucion/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'institucion_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\InstitucionController::showAction',));
                }

                // institucion_new
                if ($pathinfo === '/admin/institucion/nuevo') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\InstitucionController::newAction',  '_route' => 'institucion_new',);
                }

                // institucion_create
                if ($pathinfo === '/admin/institucion/crear') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_institucion_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\InstitucionController::createAction',  '_route' => 'institucion_create',);
                }
                not_institucion_create:

                // institucion_edit
                if (preg_match('#^/admin/institucion/(?P<id>[^/]++)/editar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'institucion_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\InstitucionController::editAction',));
                }

                // institucion_update
                if (preg_match('#^/admin/institucion/(?P<id>[^/]++)/editor$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_institucion_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'institucion_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\InstitucionController::updateAction',));
                }
                not_institucion_update:

                // institucion_delete
                if (preg_match('#^/admin/institucion/(?P<id>[^/]++)/eliminando$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'institucion_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\InstitucionController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/lineainvestigacion')) {
                // lineainvestigacion
                if (rtrim($pathinfo, '/') === '/admin/lineainvestigacion') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'lineainvestigacion');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\LineaInvestigacionController::indexAction',  '_route' => 'lineainvestigacion',);
                }

                // lineainvestigacion_show
                if (preg_match('#^/admin/lineainvestigacion/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'lineainvestigacion_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\LineaInvestigacionController::showAction',));
                }

                // lineainvestigacion_new
                if ($pathinfo === '/admin/lineainvestigacion/nuevo') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\LineaInvestigacionController::newAction',  '_route' => 'lineainvestigacion_new',);
                }

                // lineainvestigacion_create
                if ($pathinfo === '/admin/lineainvestigacion/crear') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_lineainvestigacion_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\LineaInvestigacionController::createAction',  '_route' => 'lineainvestigacion_create',);
                }
                not_lineainvestigacion_create:

                // lineainvestigacion_edit
                if (preg_match('#^/admin/lineainvestigacion/(?P<id>[^/]++)/editar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'lineainvestigacion_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\LineaInvestigacionController::editAction',));
                }

                // lineainvestigacion_update
                if (preg_match('#^/admin/lineainvestigacion/(?P<id>[^/]++)/editor$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_lineainvestigacion_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'lineainvestigacion_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\LineaInvestigacionController::updateAction',));
                }
                not_lineainvestigacion_update:

                // lineainvestigacion_delete
                if (preg_match('#^/admin/lineainvestigacion/(?P<id>[^/]++)/eliminar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'lineainvestigacion_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\LineaInvestigacionController::deleteAction',));
                }

                // generarpdf_linea
                if ($pathinfo === '/admin/lineainvestigacion/lineas-pdf') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\LineaInvestigacionController::lineapdfAction',  '_route' => 'generarpdf_linea',);
                }

            }

            if (0 === strpos($pathinfo, '/admin/maestria')) {
                // maestria
                if (rtrim($pathinfo, '/') === '/admin/maestria') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'maestria');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\MaestriaController::indexAction',  '_route' => 'maestria',);
                }

                // maestria_show
                if (preg_match('#^/admin/maestria/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'maestria_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\MaestriaController::showAction',));
                }

                // maestria_new
                if ($pathinfo === '/admin/maestria/nueva') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\MaestriaController::newAction',  '_route' => 'maestria_new',);
                }

                // maestria_create
                if ($pathinfo === '/admin/maestria/crear') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_maestria_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\MaestriaController::createAction',  '_route' => 'maestria_create',);
                }
                not_maestria_create:

                // maestria_edit
                if (preg_match('#^/admin/maestria/(?P<id>[^/]++)/editar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'maestria_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\MaestriaController::editAction',));
                }

                // maestria_update
                if (preg_match('#^/admin/maestria/(?P<id>[^/]++)/editor$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_maestria_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'maestria_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\MaestriaController::updateAction',));
                }
                not_maestria_update:

                // maestria_delete
                if (preg_match('#^/admin/maestria/(?P<id>[^/]++)/eliminar$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'maestria_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\MaestriaController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/norma')) {
                // norma
                if (rtrim($pathinfo, '/') === '/admin/norma') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'norma');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\NormaController::indexAction',  '_route' => 'norma',);
                }

                // norma_show
                if (preg_match('#^/admin/norma/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'norma_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\NormaController::showAction',));
                }

                // norma_new
                if ($pathinfo === '/admin/norma/new') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\NormaController::newAction',  '_route' => 'norma_new',);
                }

                // norma_create
                if ($pathinfo === '/admin/norma/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_norma_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\NormaController::createAction',  '_route' => 'norma_create',);
                }
                not_norma_create:

                // norma_edit
                if (preg_match('#^/admin/norma/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'norma_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\NormaController::editAction',));
                }

                // norma_update
                if (preg_match('#^/admin/norma/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_norma_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'norma_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\NormaController::updateAction',));
                }
                not_norma_update:

                // norma_delete
                if (preg_match('#^/admin/norma/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'norma_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\NormaController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/otrasrelaciones')) {
                // otrasrelaciones
                if (rtrim($pathinfo, '/') === '/admin/otrasrelaciones') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'otrasrelaciones');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\OtrasRelacionesController::indexAction',  '_route' => 'otrasrelaciones',);
                }

                // otrasrelaciones_show
                if (preg_match('#^/admin/otrasrelaciones/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'otrasrelaciones_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\OtrasRelacionesController::showAction',));
                }

                // otrasrelaciones_new
                if ($pathinfo === '/admin/otrasrelaciones/new') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\OtrasRelacionesController::newAction',  '_route' => 'otrasrelaciones_new',);
                }

                // otrasrelaciones_create
                if ($pathinfo === '/admin/otrasrelaciones/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_otrasrelaciones_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\OtrasRelacionesController::createAction',  '_route' => 'otrasrelaciones_create',);
                }
                not_otrasrelaciones_create:

                // otrasrelaciones_edit
                if (preg_match('#^/admin/otrasrelaciones/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'otrasrelaciones_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\OtrasRelacionesController::editAction',));
                }

                // otrasrelaciones_update
                if (preg_match('#^/admin/otrasrelaciones/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_otrasrelaciones_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'otrasrelaciones_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\OtrasRelacionesController::updateAction',));
                }
                not_otrasrelaciones_update:

                // otrasrelaciones_delete
                if (preg_match('#^/admin/otrasrelaciones/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'otrasrelaciones_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\OtrasRelacionesController::deleteAction',));
                }

                // intitucion
                if ($pathinfo === '/admin/otrasrelaciones/institucion') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\OtrasRelacionesController::institucionAction',  '_route' => 'intitucion',);
                }

                // institucion_eliminar
                if (preg_match('#^/admin/otrasrelaciones/(?P<id>[^/]++)/eliminar/institución$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'institucion_eliminar')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\OtrasRelacionesController::eliminarInsAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/pro')) {
                if (0 === strpos($pathinfo, '/admin/programaacademico')) {
                    // programaacademico
                    if (rtrim($pathinfo, '/') === '/admin/programaacademico') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'programaacademico');
                        }

                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ProgramaAcademicoController::indexAction',  '_route' => 'programaacademico',);
                    }

                    // programaacademico_show
                    if (preg_match('#^/admin/programaacademico/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'programaacademico_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ProgramaAcademicoController::showAction',));
                    }

                    // programaacademico_new
                    if ($pathinfo === '/admin/programaacademico/new') {
                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ProgramaAcademicoController::newAction',  '_route' => 'programaacademico_new',);
                    }

                    // programaacademico_create
                    if ($pathinfo === '/admin/programaacademico/create') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_programaacademico_create;
                        }

                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ProgramaAcademicoController::createAction',  '_route' => 'programaacademico_create',);
                    }
                    not_programaacademico_create:

                    // programaacademico_edit
                    if (preg_match('#^/admin/programaacademico/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'programaacademico_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ProgramaAcademicoController::editAction',));
                    }

                    // programaacademico_update
                    if (preg_match('#^/admin/programaacademico/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_programaacademico_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'programaacademico_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ProgramaAcademicoController::updateAction',));
                    }
                    not_programaacademico_update:

                    // programaacademico_delete
                    if (preg_match('#^/admin/programaacademico/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'programaacademico_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ProgramaAcademicoController::deleteAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/admin/proyecto')) {
                    // proyecto
                    if (rtrim($pathinfo, '/') === '/admin/proyecto') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'proyecto');
                        }

                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ProyectoController::indexAction',  '_route' => 'proyecto',);
                    }

                    // proyecto_show
                    if (preg_match('#^/admin/proyecto/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'proyecto_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ProyectoController::showAction',));
                    }

                    // proyecto_new
                    if ($pathinfo === '/admin/proyecto/new') {
                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ProyectoController::newAction',  '_route' => 'proyecto_new',);
                    }

                    // proyecto_create
                    if ($pathinfo === '/admin/proyecto/create') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_proyecto_create;
                        }

                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ProyectoController::createAction',  '_route' => 'proyecto_create',);
                    }
                    not_proyecto_create:

                    // proyecto_edit
                    if (preg_match('#^/admin/proyecto/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'proyecto_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ProyectoController::editAction',));
                    }

                    // proyecto_update
                    if (preg_match('#^/admin/proyecto/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_proyecto_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'proyecto_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ProyectoController::updateAction',));
                    }
                    not_proyecto_update:

                    // proyecto_delete
                    if (preg_match('#^/admin/proyecto/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'proyecto_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ProyectoController::deleteAction',));
                    }

                    // generarpdf_proyecto
                    if ($pathinfo === '/admin/proyecto/proyectos-pdf') {
                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ProyectoController::proyectopdfAction',  '_route' => 'generarpdf_proyecto',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin/servicioscienticotecnicos')) {
                // servicioscienticotecnicos
                if (rtrim($pathinfo, '/') === '/admin/servicioscienticotecnicos') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'servicioscienticotecnicos');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ServiciosCienticoTecnicosController::indexAction',  '_route' => 'servicioscienticotecnicos',);
                }

                // servicioscienticotecnicos_show
                if (preg_match('#^/admin/servicioscienticotecnicos/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'servicioscienticotecnicos_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ServiciosCienticoTecnicosController::showAction',));
                }

                // servicioscienticotecnicos_new
                if ($pathinfo === '/admin/servicioscienticotecnicos/new') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ServiciosCienticoTecnicosController::newAction',  '_route' => 'servicioscienticotecnicos_new',);
                }

                // servicioscienticotecnicos_create
                if ($pathinfo === '/admin/servicioscienticotecnicos/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_servicioscienticotecnicos_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ServiciosCienticoTecnicosController::createAction',  '_route' => 'servicioscienticotecnicos_create',);
                }
                not_servicioscienticotecnicos_create:

                // servicioscienticotecnicos_edit
                if (preg_match('#^/admin/servicioscienticotecnicos/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'servicioscienticotecnicos_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ServiciosCienticoTecnicosController::editAction',));
                }

                // servicioscienticotecnicos_update
                if (preg_match('#^/admin/servicioscienticotecnicos/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_servicioscienticotecnicos_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'servicioscienticotecnicos_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ServiciosCienticoTecnicosController::updateAction',));
                }
                not_servicioscienticotecnicos_update:

                // servicioscienticotecnicos_delete
                if (preg_match('#^/admin/servicioscienticotecnicos/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'servicioscienticotecnicos_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ServiciosCienticoTecnicosController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/t')) {
                if (0 === strpos($pathinfo, '/admin/taller')) {
                    // taller
                    if (rtrim($pathinfo, '/') === '/admin/taller') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'taller');
                        }

                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TallerController::indexAction',  '_route' => 'taller',);
                    }

                    // taller_show
                    if (preg_match('#^/admin/taller/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'taller_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TallerController::showAction',));
                    }

                    // taller_new
                    if ($pathinfo === '/admin/taller/new') {
                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TallerController::newAction',  '_route' => 'taller_new',);
                    }

                    // taller_create
                    if ($pathinfo === '/admin/taller/create') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_taller_create;
                        }

                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TallerController::createAction',  '_route' => 'taller_create',);
                    }
                    not_taller_create:

                    // taller_edit
                    if (preg_match('#^/admin/taller/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'taller_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TallerController::editAction',));
                    }

                    // taller_update
                    if (preg_match('#^/admin/taller/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_taller_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'taller_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TallerController::updateAction',));
                    }
                    not_taller_update:

                    // taller_delete
                    if (preg_match('#^/admin/taller/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'taller_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TallerController::deleteAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/admin/tematica')) {
                    // tematica
                    if (rtrim($pathinfo, '/') === '/admin/tematica') {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'tematica');
                        }

                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TematicaController::indexAction',  '_route' => 'tematica',);
                    }

                    // tematica_show
                    if (preg_match('#^/admin/tematica/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'tematica_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TematicaController::showAction',));
                    }

                    // tematica_new
                    if ($pathinfo === '/admin/tematica/new') {
                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TematicaController::newAction',  '_route' => 'tematica_new',);
                    }

                    // tematica_create
                    if ($pathinfo === '/admin/tematica/create') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_tematica_create;
                        }

                        return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TematicaController::createAction',  '_route' => 'tematica_create',);
                    }
                    not_tematica_create:

                    // tematica_edit
                    if (preg_match('#^/admin/tematica/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'tematica_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TematicaController::editAction',));
                    }

                    // tematica_update
                    if (preg_match('#^/admin/tematica/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                        if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                            $allow = array_merge($allow, array('POST', 'PUT'));
                            goto not_tematica_update;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'tematica_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TematicaController::updateAction',));
                    }
                    not_tematica_update:

                    // tematica_delete
                    if (preg_match('#^/admin/tematica/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'tematica_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TematicaController::deleteAction',));
                    }

                    if (0 === strpos($pathinfo, '/admin/tematicasct')) {
                        // tematicasct
                        if (rtrim($pathinfo, '/') === '/admin/tematicasct') {
                            if (substr($pathinfo, -1) !== '/') {
                                return $this->redirect($pathinfo.'/', 'tematicasct');
                            }

                            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TematicaSctController::indexAction',  '_route' => 'tematicasct',);
                        }

                        // tematicasct_show
                        if (preg_match('#^/admin/tematicasct/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tematicasct_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TematicaSctController::showAction',));
                        }

                        // tematicasct_new
                        if ($pathinfo === '/admin/tematicasct/new') {
                            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TematicaSctController::newAction',  '_route' => 'tematicasct_new',);
                        }

                        // tematicasct_create
                        if ($pathinfo === '/admin/tematicasct/create') {
                            if ($this->context->getMethod() != 'POST') {
                                $allow[] = 'POST';
                                goto not_tematicasct_create;
                            }

                            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TematicaSctController::createAction',  '_route' => 'tematicasct_create',);
                        }
                        not_tematicasct_create:

                        // tematicasct_edit
                        if (preg_match('#^/admin/tematicasct/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tematicasct_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TematicaSctController::editAction',));
                        }

                        // tematicasct_update
                        if (preg_match('#^/admin/tematicasct/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                                $allow = array_merge($allow, array('POST', 'PUT'));
                                goto not_tematicasct_update;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tematicasct_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TematicaSctController::updateAction',));
                        }
                        not_tematicasct_update:

                        // tematicasct_delete
                        if (preg_match('#^/admin/tematicasct/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'tematicasct_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\TematicaSctController::deleteAction',));
                        }

                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin/usuarios')) {
                // usuarios
                if (rtrim($pathinfo, '/') === '/admin/usuarios') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'usuarios');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\UsuariosController::indexAction',  '_route' => 'usuarios',);
                }

                // usuarios_show
                if (preg_match('#^/admin/usuarios/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'usuarios_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\UsuariosController::showAction',));
                }

                // usuarios_new
                if ($pathinfo === '/admin/usuarios/new') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\UsuariosController::newAction',  '_route' => 'usuarios_new',);
                }

                // usuarios_create
                if ($pathinfo === '/admin/usuarios/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_usuarios_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\UsuariosController::createAction',  '_route' => 'usuarios_create',);
                }
                not_usuarios_create:

                // usuarios_edit
                if (preg_match('#^/admin/usuarios/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'usuarios_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\UsuariosController::editAction',));
                }

                // usuarios_update
                if (preg_match('#^/admin/usuarios/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_usuarios_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'usuarios_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\UsuariosController::updateAction',));
                }
                not_usuarios_update:

                // usuarios_delete
                if (preg_match('#^/admin/usuarios/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'usuarios_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\UsuariosController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/plantillalineainvestigacion')) {
                // plantillalineainvestigacion
                if (rtrim($pathinfo, '/') === '/admin/plantillalineainvestigacion') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'plantillalineainvestigacion');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaLineaInvestigacionController::indexAction',  '_route' => 'plantillalineainvestigacion',);
                }

                // plantillalineainvestigacion_show
                if (preg_match('#^/admin/plantillalineainvestigacion/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'plantillalineainvestigacion_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaLineaInvestigacionController::showAction',));
                }

                // plantillalineainvestigacion_new
                if ($pathinfo === '/admin/plantillalineainvestigacion/new') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaLineaInvestigacionController::newAction',  '_route' => 'plantillalineainvestigacion_new',);
                }

                // plantillalineainvestigacion_create
                if ($pathinfo === '/admin/plantillalineainvestigacion/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_plantillalineainvestigacion_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaLineaInvestigacionController::createAction',  '_route' => 'plantillalineainvestigacion_create',);
                }
                not_plantillalineainvestigacion_create:

                // plantillalineainvestigacion_edit
                if (preg_match('#^/admin/plantillalineainvestigacion/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'plantillalineainvestigacion_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaLineaInvestigacionController::editAction',));
                }

                // plantillalineainvestigacion_update
                if (preg_match('#^/admin/plantillalineainvestigacion/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_plantillalineainvestigacion_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'plantillalineainvestigacion_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaLineaInvestigacionController::updateAction',));
                }
                not_plantillalineainvestigacion_update:

                // plantillalineainvestigacion_delete
                if (preg_match('#^/admin/plantillalineainvestigacion/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'plantillalineainvestigacion_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaLineaInvestigacionController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/especialidad')) {
                // especialidad
                if (rtrim($pathinfo, '/') === '/admin/especialidad') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'especialidad');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\EspecialidadController::indexAction',  '_route' => 'especialidad',);
                }

                // especialidad_show
                if (preg_match('#^/admin/especialidad/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'especialidad_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\EspecialidadController::showAction',));
                }

                // especialidad_new
                if ($pathinfo === '/admin/especialidad/new') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\EspecialidadController::newAction',  '_route' => 'especialidad_new',);
                }

                // especialidad_create
                if ($pathinfo === '/admin/especialidad/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_especialidad_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\EspecialidadController::createAction',  '_route' => 'especialidad_create',);
                }
                not_especialidad_create:

                // especialidad_edit
                if (preg_match('#^/admin/especialidad/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'especialidad_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\EspecialidadController::editAction',));
                }

                // especialidad_update
                if (preg_match('#^/admin/especialidad/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_especialidad_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'especialidad_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\EspecialidadController::updateAction',));
                }
                not_especialidad_update:

                // especialidad_delete
                if (preg_match('#^/admin/especialidad/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'especialidad_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\EspecialidadController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin/plantillaproyecto')) {
                // plantillaproyecto
                if (rtrim($pathinfo, '/') === '/admin/plantillaproyecto') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'plantillaproyecto');
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaProyectoController::indexAction',  '_route' => 'plantillaproyecto',);
                }

                // plantillaproyecto_show
                if (preg_match('#^/admin/plantillaproyecto/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'plantillaproyecto_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaProyectoController::showAction',));
                }

                // plantillaproyecto_new
                if ($pathinfo === '/admin/plantillaproyecto/new') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaProyectoController::newAction',  '_route' => 'plantillaproyecto_new',);
                }

                // plantillaproyecto_create
                if ($pathinfo === '/admin/plantillaproyecto/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_plantillaproyecto_create;
                    }

                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaProyectoController::createAction',  '_route' => 'plantillaproyecto_create',);
                }
                not_plantillaproyecto_create:

                // plantillaproyecto_edit
                if (preg_match('#^/admin/plantillaproyecto/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'plantillaproyecto_edit')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaProyectoController::editAction',));
                }

                // plantillaproyecto_update
                if (preg_match('#^/admin/plantillaproyecto/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_plantillaproyecto_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'plantillaproyecto_update')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaProyectoController::updateAction',));
                }
                not_plantillaproyecto_update:

                // plantillaproyecto_delete
                if (preg_match('#^/admin/plantillaproyecto/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'plantillaproyecto_delete')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\PlantillaProyectoController::deleteAction',));
                }

            }

        }

        // _bienvenid
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::loginAction',  '_route' => '_bienvenid',);
        }

        // sobre
        if ($pathinfo === '/sobre') {
            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::sobreAction',  '_route' => 'sobre',);
        }

        // login_check
        if ($pathinfo === '/login_check') {
            return array('_route' => 'login_check');
        }

        // gedeltur_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'gedeltur_homepage');
            }

            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::inicioAction',  '_route' => 'gedeltur_homepage',);
        }

        // asignaturas
        if ($pathinfo === '/asignaturas') {
            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::asignaturasAction',  '_route' => 'asignaturas',);
        }

        if (0 === strpos($pathinfo, '/registrar')) {
            // usuarios_registrar
            if ($pathinfo === '/registrar') {
                return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\UsuariosController::registrarAction',  '_route' => 'usuarios_registrar',);
            }

            // usuarios_registro
            if ($pathinfo === '/registrarse') {
                return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\UsuariosController::registroAction',  '_route' => 'usuarios_registro',);
            }

        }

        if (0 === strpos($pathinfo, '/cambiar-')) {
            // cambiar_password_user
            if ($pathinfo === '/cambiar-contrasenna') {
                return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\UsuariosController::cambiarcontraAction',  '_route' => 'cambiar_password_user',);
            }

            // cambiar_password
            if ($pathinfo === '/cambiar-password') {
                return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\UsuariosController::cambiarAction',  '_route' => 'cambiar_password',);
            }

        }

        if (0 === strpos($pathinfo, '/foro')) {
            // correo_nuevo
            if ($pathinfo === '/foro') {
                return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CorreoController::newAction',  '_route' => 'correo_nuevo',);
            }

            if (0 === strpos($pathinfo, '/foro/s')) {
                // correo_superacion
                if ($pathinfo === '/foro/superacion') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CorreoController::superacionAction',  '_route' => 'correo_superacion',);
                }

                // correo_sct
                if ($pathinfo === '/foro/servicio') {
                    return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CorreoController::sctAction',  '_route' => 'correo_sct',);
                }

            }

            // correo_inv
            if ($pathinfo === '/foro/investigacion') {
                return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CorreoController::investigacionAction',  '_route' => 'correo_inv',);
            }

            // correo_otro
            if ($pathinfo === '/foro/otro') {
                return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CorreoController::otroAction',  '_route' => 'correo_otro',);
            }

        }

        // correo_creado
        if ($pathinfo === '/correo/creado') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_correo_creado;
            }

            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CorreoController::createAction',  '_route' => 'correo_creado',);
        }
        not_correo_creado:

        // trabajadores
        if ($pathinfo === '/trabajadores') {
            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::trabajadoresAction',  '_route' => 'trabajadores',);
        }

        // pro
        if ($pathinfo === '/proyectos') {
            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::proyectosAction',  '_route' => 'pro',);
        }

        // li
        if ($pathinfo === '/lineas') {
            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::liAction',  '_route' => 'li',);
        }

        // servicios
        if ($pathinfo === '/servicios') {
            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::serviciosAction',  '_route' => 'servicios',);
        }

        // eventos
        if (preg_match('#^/(?P<id>[^/]++)/evento$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'eventos')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::eventoAction',));
        }

        // trabajadores_show
        if (preg_match('#^/(?P<id>[^/]++)/trabajadores$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'trabajadores_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::trabajadores_showAction',));
        }

        // asignaturas_show
        if (preg_match('#^/(?P<id>[^/]++)/asignaturas$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'asignaturas_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::asignaturas_showAction',));
        }

        // diplomados_show
        if (preg_match('#^/(?P<id>[^/]++)/programa$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'diplomados_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::diplomados_showAction',));
        }

        // proyectos_show
        if (preg_match('#^/(?P<id>[^/]++)/proyectos$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'proyectos_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::proyectos_showAction',));
        }

        // li_show
        if (preg_match('#^/(?P<id>[^/]++)/lineas$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'li_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::li_showAction',));
        }

        // estudiantes
        if ($pathinfo === '/estudiantes') {
            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::estudiantesAction',  '_route' => 'estudiantes',);
        }

        // colaboradores
        if ($pathinfo === '/colaboradores') {
            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::colaboradoresAction',  '_route' => 'colaboradores',);
        }

        // home
        if ($pathinfo === '/inicio') {
            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::indexAction',  '_route' => 'home',);
        }

        // maestrias
        if ($pathinfo === '/maestrias') {
            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::maestriasAction',  '_route' => 'maestrias',);
        }

        // diplomados
        if ($pathinfo === '/diplomados') {
            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::diplomadosAction',  '_route' => 'diplomados',);
        }

        // especialidades
        if ($pathinfo === '/especialidades') {
            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::especialidadAction',  '_route' => 'especialidades',);
        }

        // cursos
        if ($pathinfo === '/cursos') {
            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::cursosAction',  '_route' => 'cursos',);
        }

        // cursos_show
        if (preg_match('#^/(?P<id>[^/]++)/curso$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'cursos_show')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::cursos_showAction',));
        }

        // logout
        if ($pathinfo === '/logout') {
            return array('_route' => 'logout');
        }

        // sct_entidad
        if ($pathinfo === '/entidades/servicio') {
            return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ReportesController::sct_entidadAction',  '_route' => 'sct_entidad',);
        }

        if (0 === strpos($pathinfo, '/trabajadores')) {
            // ws_sct
            if ($pathinfo === '/trabajadores/servicio') {
                return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ReportesController::ws_sctAction',  '_route' => 'ws_sct',);
            }

            // ws_li
            if ($pathinfo === '/trabajadores/investigación') {
                return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ReportesController::ws_liAction',  '_route' => 'ws_li',);
            }

            // ws_pro
            if ($pathinfo === '/trabajadores/proyecto') {
                return array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\ReportesController::ws_proAction',  '_route' => 'ws_pro',);
            }

        }

        // sct_filtro
        if (preg_match('#^/(?P<tipo>[^/]++)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'sct_filtro');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sct_filtro')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\DefaultController::sct_filtroAction',));
        }

        // correo_eliminar
        if (preg_match('#^/(?P<id>[^/]++)/eliminar$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'correo_eliminar')), array (  '_controller' => 'GEDELTUR\\Bundle\\Controller\\CorreoController::eliminarAction',));
        }

        // _welcome
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_welcome');
            }

            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\WelcomeController::indexAction',  '_route' => '_welcome',);
        }

        if (0 === strpos($pathinfo, '/demo')) {
            if (0 === strpos($pathinfo, '/demo/secured')) {
                if (0 === strpos($pathinfo, '/demo/secured/log')) {
                    if (0 === strpos($pathinfo, '/demo/secured/login')) {
                        // _demo_login
                        if ($pathinfo === '/demo/secured/login') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::loginAction',  '_route' => '_demo_login',);
                        }

                        // _demo_security_check
                        if ($pathinfo === '/demo/secured/login_check') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::securityCheckAction',  '_route' => '_demo_security_check',);
                        }

                    }

                    // _demo_logout
                    if ($pathinfo === '/demo/secured/logout') {
                        return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::logoutAction',  '_route' => '_demo_logout',);
                    }

                }

                if (0 === strpos($pathinfo, '/demo/secured/hello')) {
                    // acme_demo_secured_hello
                    if ($pathinfo === '/demo/secured/hello') {
                        return array (  'name' => 'World',  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',  '_route' => 'acme_demo_secured_hello',);
                    }

                    // _demo_secured_hello
                    if (preg_match('#^/demo/secured/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',));
                    }

                    // _demo_secured_hello_admin
                    if (0 === strpos($pathinfo, '/demo/secured/hello/admin') && preg_match('#^/demo/secured/hello/admin/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello_admin')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloadminAction',));
                    }

                }

            }

            // _demo
            if (rtrim($pathinfo, '/') === '/demo') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_demo');
                }

                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::indexAction',  '_route' => '_demo',);
            }

            // _demo_hello
            if (0 === strpos($pathinfo, '/demo/hello') && preg_match('#^/demo/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::helloAction',));
            }

            // _demo_contact
            if ($pathinfo === '/demo/contact') {
                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::contactAction',  '_route' => '_demo_contact',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}

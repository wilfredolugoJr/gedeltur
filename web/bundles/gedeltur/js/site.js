// call this from the developer console and you can control both instances
var calendars = {};

$(document).ready( function() {

  // assuming you've got the appropriate language files,
  // clndr will respect whatever moment's language is set to.
  // moment.lang('ru');

  // here's some magic to make sure the dates are happening this month.
  var thisMonth = moment().format('YYYY-MM');

  var eventArray = [
    { startDate: thisMonth + '-10', endDate: thisMonth + '-14', title: 'Multi-Day Event' },
    { startDate: thisMonth + '-21', endDate: thisMonth + '-23', title: 'Another Multi-Day Event' }
  ];

  // the order of the click handlers is predictable.
  // direct click action callbacks come first: click, nextMonth, previousMonth, nextYear, previousYear, or today.
  // then onMonthChange (if the month changed).
  // finally onYearChange (if the year changed).

  calendars.clndr1 = $('.cal1').clndr({
    events: eventArray,
    // constraints: {
    //   startDate: '2013-11-01',
    //   endDate: '2013-11-15'
    // },
    clickEvents: {
      click: function(target) {
        console.log(target);
        if($(target.element).hasClass('inactive')) {
          console.log('not a valid datepicker date.');
        } else {
          console.log('VALID datepicker date.');
        }
      },
      nextMonth: function() {
        console.log('next month.');
      },
      previousMonth: function() {
        console.log('previous month.');
      },
      onMonthChange: function() {
        console.log('month changed.');
      },
      nextYear: function() {
        console.log('next year.');
      },
      previousYear: function() {
        console.log('previous year.');
      },
      onYearChange: function() {
        console.log('year changed.');
      }
    },
    multiDayEvents: {
      startDate: 'startDate',
      endDate: 'endDate'
    },
    showAdjacentMonths: true,
    adjacentDaysChangeMonth: false
  });

  // calendars.clndr2 = $('.cal2').clndr({
  //   template: $('#template-calendar').html(),
  //   events: eventArray,
  //   startWithMonth: moment().add('month', 1),
  //   clickEvents: {
  //     click: function(target) {
  //       console.log(target);
  //     }
  //   }
  // });

  // bind both clndrs to the left and right arrow keys
  $(document).keydown( function(e) {
    if(e.keyCode == 37) {
      // left arrow
      calendars.clndr1.back();
      calendars.clndr2.back();
    }
    if(e.keyCode == 39) {
      // right arrow
      calendars.clndr1.forward();
      calendars.clndr2.forward();
    }
  });

});


function floats(evt)
{
    evt = window.event ? window.event : evt;

    if(evt.keyCode == 96 || evt.keyCode == 48)
    {
        if(evt.target.value.length == 1 && evt.target.value.charAt(0) == '0')
            return false;
    }



    if(evt.target.value.indexOf('.')!=-1 && (evt.target.value.length - evt.target.value.indexOf('.'))>2)
    {
        if( evt.keyCode == 110 || evt.keyCode == 8 || (evt.keyCode == 190 && evt.target.value.indexOf('.') == -1 && evt.target.value != '') || evt.keyCode == 39 || evt.keyCode == 37 || evt.keyCode == 9 || evt.keyCode == 46)
            return true;
        return false;
    }

    //alert(evt.target.value.indexOf('.'))



        if((evt.keyCode >= 48 && evt.keyCode <= 57) || (evt.keyCode >= 96 && evt.keyCode <= 105) || evt.keyCode == 110 || evt.keyCode == 8 || (evt.keyCode == 190 && evt.target.value.indexOf('.') == -1 && evt.target.value != '') || evt.keyCode == 39 || evt.keyCode == 37 || evt.keyCode == 9 || evt.keyCode == 46)
        return true;
    return false;
}

function letras(evt)
{
    evt = window.event ? window.event : evt;
    if(evt.keyCode == 32 && (evt.target.value == '' || evt.target.value.charAt(evt.target.value.length - 1) == ' '))
        return false;

    if((evt.keyCode >= 65 && evt.keyCode <= 90) || evt.keyCode == 8 || evt.keyCode == 32 || evt.keyCode == 39 || evt.keyCode == 37 || evt.keyCode == 9 || evt.keyCode == 46)
        return true;
    return false;
}

function let(evt)
{
    evt = window.event ? window.event : evt;
    //alert(evt.keyCode)
    //alert(evt.target.value.charAt(evt.target.value.length - 1))

if(evt.keyCode ==192)
  return false;
if(evt.keyCode == 222 && (evt.target.value != '' || evt.target.value.charAt(evt.target.value.length - 1) != ' '))
        return true;
    if(evt.keyCode == 32 && (evt.target.value == '' || evt.target.value.charAt(evt.target.value.length - 1) == ' '))
        return false;

      if(evt.keyCode == 32 && (evt.target.value == ',' || evt.target.value.charAt(evt.target.value.length - 1) == ','))
        return false;

       



     if(evt.keyCode == 188 && (evt.target.value == ',' || evt.target.value.charAt(evt.target.value.length - 1) == ','))
        return false;

       if(evt.keyCode == 188 && (evt.target.value == '' || evt.target.value.charAt(evt.target.value.length - 1) == ' '))
        return false;

      if(evt.keyCode == 188 && (evt.target.value == '.' || evt.target.value.charAt(evt.target.value.length - 1) == '.'))
        return false;

if(evt.keyCode == 190 && (evt.target.value == '' || evt.target.value.charAt(evt.target.value.length - 1) == ' '))
        return false;
if(evt.keyCode == 190 && (evt.target.value == ',' || evt.target.value.charAt(evt.target.value.length - 1) == ','))
        return false;
 if(evt.keyCode == 190 && (evt.target.value == '.' || evt.target.value.charAt(evt.target.value.length - 1) == '.'))
        return false;

    if((evt.keyCode >= 65 && evt.keyCode <= 90) || evt.keyCode == 8 || evt.keyCode == 32 || evt.keyCode == 39 || evt.keyCode == 37 || evt.keyCode == 9 || evt.keyCode == 46 || (evt.keyCode == 188 && evt.target.value.length !=0) || (evt.keyCode == 190 && evt.target.value.length !=0))
        return true;
    return false;
}

function adress(evt)
{
    evt = window.event ? window.event : evt;
    //alert(evt.keyCode)
    //alert(evt.target.value.charAt(evt.target.value.length - 1))

    if(evt.keyCode == 32 && (evt.target.value == '' || evt.target.value.charAt(evt.target.value.length - 1) == ' '))
        return false;

      if(evt.keyCode == 32 && (evt.target.value == ',' || evt.target.value.charAt(evt.target.value.length - 1) == ','))
        return false;

       



     if(evt.keyCode == 188 && (evt.target.value == ',' || evt.target.value.charAt(evt.target.value.length - 1) == ','))
        return false;

       if(evt.keyCode == 188 && (evt.target.value == '' || evt.target.value.charAt(evt.target.value.length - 1) == ' '))
        return false;

      if(evt.keyCode == 188 && (evt.target.value == '.' || evt.target.value.charAt(evt.target.value.length - 1) == '.'))
        return false;

if(evt.keyCode == 190 && (evt.target.value == '' || evt.target.value.charAt(evt.target.value.length - 1) == ' '))
        return false;
if(evt.keyCode == 190 && (evt.target.value == ',' || evt.target.value.charAt(evt.target.value.length - 1) == ','))
        return false;
 if(evt.keyCode == 190 && (evt.target.value == '.' || evt.target.value.charAt(evt.target.value.length - 1) == '.'))
        return false;

    if((evt.keyCode >= 65 && evt.keyCode <= 90) || (evt.keyCode >= 48 && evt.keyCode <= 57) || (evt.keyCode >= 96 && evt.keyCode <= 105) || evt.keyCode == 8 || evt.keyCode == 32 || evt.keyCode == 39 || evt.keyCode == 37 || evt.keyCode == 9 || evt.keyCode == 46 || (evt.keyCode == 188 && evt.target.value.length !=0) || (evt.keyCode == 190 && evt.target.value.length !=0))
        return true;
    return false;
}

function numeros(evt)
{
    evt = window.event ? window.event : evt;
    //alert(evt.keyCode)
    if((evt.keyCode >= 48 && evt.keyCode <= 57) || (evt.keyCode >= 96 && 
      evt.keyCode <= 105) || evt.keyCode == 39 || evt.keyCode == 37 || evt.keyCode == 9 
       || evt.keyCode == 8 || evt.keyCode == 46)
        return true;
    return false;
}

function ci(evt)
{
    evt = window.event ? window.event : evt;

    var num = 0;
    if(evt.keyCode >= 48 && evt.keyCode <= 57)
        num = evt.keyCode - 48;
    else if(evt.keyCode >= 96 && evt.keyCode <= 105)
        num = evt.keyCode - 96;

    //alert(num);
    if(evt.target.value.length == 2 && (num != 0 && num != 1))
        return false;

    if(evt.target.value.length == 3 && evt.target.value.charAt(2) == 1 && (num != 0 && num != 1 && num != 2))
        return false;

    if(evt.target.value.length == 4 && (num != 0 && num != 1 && num != 2 && num != 3))
        return false;

    if(evt.target.value.length == 5 && evt.target.value.charAt(4) == 3 && (num != 0 && num != 1))
        return false;
   

    if((evt.keyCode >= 48 && evt.keyCode <= 57) || (evt.keyCode >= 96 && evt.keyCode <= 105) || evt.keyCode == 39 
      || evt.keyCode == 37 || evt.keyCode == 9 || evt.keyCode == 8 || evt.keyCode == 46)
        return true;
    return false;
}
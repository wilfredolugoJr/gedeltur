<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\Usuarios;
use GEDELTUR\Bundle\Form\UsuariosType;

/**
 * Usuarios controller.
 *
 */
class UsuariosController extends Controller
{

    /**
     * Lists all Usuarios entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Usuarios')->findAll();

        return $this->render('GEDELTURBundle:Usuarios:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Usuarios entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Usuarios();
        $form = $this->createCreateForm($entity);
        $form->remove('contrasennaantigua');
        $form->handleRequest($request);

        if ($form->isValid()) {



            if($form['contrasenna']['first']->getData()==null)
            {
               $this->get('session')->getFlashBag()->add('msg','Lo siento! Por motivos de seguridad el usuario debe contener una contraseña');
            return $this->render('GEDELTURBundle:Usuarios:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            ));
            }

            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($entity);
            $contrasenna = $encoder->encodePassword($entity->getContrasenna(), $entity->getSalt());
            $entity->setContrasenna($contrasenna);
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Usuario adicionado correctamente');
            return $this->redirect($this->generateUrl('usuarios'));
        }

        return $this->render('GEDELTURBundle:Usuarios:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Usuarios entity.
    *
    * @param Usuarios $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Usuarios $entity)
    {
        $form = $this->createForm(new UsuariosType(), $entity, array(
            'action' => $this->generateUrl('usuarios_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new Usuarios entity.
     *
     */
    public function newAction()
    {
        $entity = new Usuarios();
        $form   = $this->createCreateForm($entity);
        $form->remove('contrasennaantigua');

        return $this->render('GEDELTURBundle:Usuarios:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }



    public function registroAction()
    {
        $entity = new Usuarios();
        $form   = $this->createRegistroForm($entity);
        $form->remove('contrasennaantigua');
        $form->remove('role');

        $em = $this->getDoctrine()->getManager();

        $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();


        return $this->render('GEDELTURBundle:Usuarios:registrarse.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'eventos' => $eventos,
            'cant_eventos' => count($eventos),
        ));
    }


     private function createRegistroForm(Usuarios $entity)
    {
        $form = $this->createForm(new UsuariosType(), $entity, array(
            'action' => $this->generateUrl('usuarios_registrar'),
            'method' => 'POST',
        ));




        //$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    public function registrarAction(Request $request)//dice que el formulario no es valido
    {

        $entity = new Usuarios();
        $form = $this->createRegistroForm($entity);
        $form->remove('contrasennaantigua');
        $form->remove('role');
        $form->handleRequest($request);
         

        if ($form->isValid()) {



                $em = $this->getDoctrine()->getManager();
            $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();


            if($form['contrasenna']['first']->getData()==null)
            {
               $this->get('session')->getFlashBag()->add('msg','Lo siento! Por motivos de seguridad el usuario debe contener una contraseña.');
            return $this->render('GEDELTURBundle:Usuarios:registrarse.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
                'eventos' => $eventos,
                'cant_eventos' => count($eventos),
            ));
            }

            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($entity);
            $contrasenna = $encoder->encodePassword($entity->getContrasenna(), $entity->getSalt());
            $entity->setContrasenna($contrasenna);
            $entity->setRole('ROLE_USER');
           //return new Response("aaaaaaaaaaaaaa");
            $em->persist($entity);
            $em->flush();



            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Ya se ha registrado, Felicidades.');
            return $this->redirect($this->generateUrl('usuarios_registro'));
        }
        $em = $this->getDoctrine()->getManager();
        $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();


        return $this->render('GEDELTURBundle:Usuarios:registrarse.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'eventos' => $eventos,
            'cant_eventos' => count($eventos),
        ));
    }



    /**
     * Finds and displays a Usuarios entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Usuarios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuarios entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Usuarios:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Usuarios entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Usuarios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuarios entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
          $editForm->remove('contrasennaantigua');

        return $this->render('GEDELTURBundle:Usuarios:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Usuarios entity.
    *
    * @param Usuarios $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Usuarios $entity)
    {
        $form = $this->createForm(new UsuariosType(), $entity, array(
            'action' => $this->generateUrl('usuarios_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Usuarios entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Usuarios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuarios entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
         $editForm->remove('contrasennaantigua');
        
          $passwordOriginal = $entity->getPassword();

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {



            if($editForm['contrasenna']['first']->getData()==null) {

                $entity->setContrasenna($passwordOriginal);

               
              
            }
            else{

                 $factory = $this->get('security.encoder_factory');
                $encoder = $factory->getEncoder($entity);
                $contrasenna = $encoder->encodePassword($entity->getContrasenna(), $entity->getSalt());
                $entity->setContrasenna($contrasenna);
                }
                $em->flush();
                

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Usuario modificado correctamente');
            return $this->redirect($this->generateUrl('usuarios'));
        }

        return $this->render('GEDELTURBundle:Usuarios:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Usuarios entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        

        
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:Usuarios')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Usuarios entity.');
            }

            $em->remove($entity);
            $em->flush();
        
        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Usuario eliminado correctamente');
        return $this->redirect($this->generateUrl('usuarios'));
    }

    /**
     * Creates a form to delete a Usuarios entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usuarios_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }



    public function cambiarAction()
    {

        //return new Response("werfwerwe");
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Usuarios')->find($this->getUser()->getId());

        if(!$entity)
        {
            throw $this->createNotFoundException('No se encontro el usuario.');
        }
        $editForm   = $this->createCambiarPasswordForm($entity);

        $em = $this->getDoctrine()->getManager();

        $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();

        //$editForm->remove('ci');
        $editForm->remove('nombre');
        $editForm->remove('apellido');
        $editForm->remove('role');
        $editForm->remove('usuario');
        $editForm->remove('apellidos');
        //$editForm->add('contrasennaantigua', 'password', array('mapped'=>false, 'label'=> 'Contraseña Antigua'));

        return $this->render('GEDELTURBundle:Usuarios:cambiarpassword.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'eventos' => $eventos,
            'cant_eventos' => count($eventos),
        ));
    }

    private function createCambiarPasswordForm(Usuarios $entity)
    {
        $form = $this->createForm(new UsuariosType(), $entity, array(
            'action' => $this->generateUrl('cambiar_password_user', array('id' => $entity->getId())),
            'method' => 'POST',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    public function cambiarcontraAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->get('security.context')->getToken()->getUser();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuarios entity.');
        }

        $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();


        $editForm = $this->createCambiarPasswordForm($entity);
        //$editForm->remove('ci');
        $editForm->remove('nombre');
        $editForm->remove('apellido');
        $editForm->remove('role');
        $editForm->remove('usuario');
        $passwordOriginal = $entity->getPassword();

        $editForm->handleRequest($request);







        $encoder = $this->get('security.encoder_factory')->getEncoder($entity);
        $passwordComprobar = $encoder->encodePassword(
            $editForm['contrasennaantigua']->getData(),
            $entity->getSalt()
        );

        //return new Response("dfdfs");
        $passwordNuevo = $encoder->encodePassword(
            $editForm['contrasenna']->getData(),
            $entity->getSalt()
        );


        //return new Response($passwordComprobar.' ---- '. $passwordOriginal);
        /*if($editForm['contrasennaantigua']->getData()== null)
        {
            $editForm->get('contrasennaantigua')->addError(new FormError('Por favor la debe introducir la contraseña antigua'));
        }*/
        if ($passwordComprobar != $passwordOriginal) {
            $this->get('session')->getFlashBag()->add('error',
                'La contraseña debe cohincidir con la antigua. '
            );
            //$editForm->get('contrasennaantigua')->addError(new FormError('La contraseña debe cohincidir con la antigua'));
            return $this->redirect($this->generateUrl('cambiar_password', array('id' => $entity->getId())));
            //throw $this->createNotFoundException('La contraseña debe cohincidir.');

        }

        if($editForm->isValid())
        {
            $aux=$entity->getPassword();
            //return new Response("$aux");

            if (  $entity->getPassword()== null) {

                $entity->setContrasenna($passwordOriginal);


            }


        else {


            $entity->setContrasenna($passwordNuevo);
            //return new Response("dfdfs");


            $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('msg',
                    'Su contraseña se ha cambiado correctamente. '
                );
            }

            return $this->redirect($this->generateUrl('cambiar_password', array('id' => $entity->getId())));
        }


        return $this->render('GEDELTURBundle:Usuarios:cambiarpassword.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'eventos' => $eventos,
            'cant_eventos' => count($eventos),
        ));
    }
}

<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\PlantillaLineaInvestigacion;
use GEDELTUR\Bundle\Form\PlantillaLineaInvestigacionType;

/**
 * PlantillaLineaInvestigacion controller.
 *
 */
class PlantillaLineaInvestigacionController extends Controller
{

    /**
     * Lists all PlantillaLineaInvestigacion entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:PlantillaLineaInvestigacion')->findAll();

        return $this->render('GEDELTURBundle:PlantillaLineaInvestigacion:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new PlantillaLineaInvestigacion entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new PlantillaLineaInvestigacion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        $consulta = $em->getRepository('GEDELTURBundle:PlantillaLineaInvestigacion')->findBy(array(
            'responsable' => '1'
        ));

        $c=count($consulta);
        $res= $form['responsable']->getData();


        $linea= $form['linea']->getData();
        $plantilla= $form['plantilla']->getData();
        


        $em = $this->getDoctrine()->getManager();

        $con = $em->getRepository('GEDELTURBundle:PlantillaLineaInvestigacion')->findBy(array(
            'linea' => $linea,
            'plantilla' => $plantilla,

        ));

        $k=count($con);


        //return new Response("$plantilla");


        if ($form->isValid()) {


            if($c>0 && $res==1 )
            {
            $this->get('session')->getFlashBag()->add('msg','Lo siento! Esta Investigación ya cuenta con un responsable');
            return $this->render('GEDELTURBundle:PlantillaLineaInvestigacion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
            }

            if($k>0)
            {
            $this->get('session')->getFlashBag()->add('msg','Lo siento! Estos datos fueron ingresados con anterioridad');
            return $this->render('GEDELTURBundle:PlantillaLineaInvestigacion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
            }


            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Investigación adicionada correctamente');
            return $this->redirect($this->generateUrl('plantillalineainvestigacion'));
        }

        return $this->render('GEDELTURBundle:PlantillaLineaInvestigacion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a PlantillaLineaInvestigacion entity.
    *
    * @param PlantillaLineaInvestigacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(PlantillaLineaInvestigacion $entity)
    {
        $form = $this->createForm(new PlantillaLineaInvestigacionType(), $entity, array(
            'action' => $this->generateUrl('plantillalineainvestigacion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new PlantillaLineaInvestigacion entity.
     *
     */
    public function newAction()
    {
        $entity = new PlantillaLineaInvestigacion();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:PlantillaLineaInvestigacion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PlantillaLineaInvestigacion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:PlantillaLineaInvestigacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PlantillaLineaInvestigacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:PlantillaLineaInvestigacion:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing PlantillaLineaInvestigacion entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:PlantillaLineaInvestigacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PlantillaLineaInvestigacion entity.');
        }

        
        
       

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        
        

        return $this->render('GEDELTURBundle:PlantillaLineaInvestigacion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a PlantillaLineaInvestigacion entity.
    *
    * @param PlantillaLineaInvestigacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(PlantillaLineaInvestigacion $entity)
    {
        $form = $this->createForm(new PlantillaLineaInvestigacionType(), $entity, array(
            'action' => $this->generateUrl('plantillalineainvestigacion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing PlantillaLineaInvestigacion entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:PlantillaLineaInvestigacion')->find($id);
        $responsable=$entity->getResponsable();
        $p=$entity->getPlantilla();
        $l=$entity->getLinea();


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PlantillaLineaInvestigacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $consulta = $em->getRepository('GEDELTURBundle:PlantillaLineaInvestigacion')->findBy(array(
            'responsable' => '1'
            ));
        $res= $editForm['responsable']->getData();
        $c=count($consulta);



        $linea= $editForm['linea']->getData();
        $plantilla= $editForm['plantilla']->getData();
        


        $em = $this->getDoctrine()->getManager();

        $con = $em->getRepository('GEDELTURBundle:PlantillaLineaInvestigacion')->findBy(array(
            'linea' => $linea,
            'plantilla' => $plantilla,

        ));

        $k=count($con);
        


        if ($editForm->isValid()) {

            if($c>0 && $res==1 && $responsable!=1 )
            {

                $this->get('session')->getFlashBag()->add('msg','Lo siento! Esta Investigación ya cuenta con un responsable'); 
               return $this->render('GEDELTURBundle:PlantillaLineaInvestigacion:edit.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
        ));
           }


           if($k>0 && ($plantilla!=$p || $linea!=$l) )
            {

                $this->get('session')->getFlashBag()->add('msg','Lo siento! Esta Investigación ya ha sido registrada'); 
               return $this->render('GEDELTURBundle:PlantillaLineaInvestigacion:edit.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
        ));
           }

            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Investigación modificada correctamente');
            return $this->redirect($this->generateUrl('plantillalineainvestigacion'));
        }

        return $this->render('GEDELTURBundle:PlantillaLineaInvestigacion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a PlantillaLineaInvestigacion entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:PlantillaLineaInvestigacion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PlantillaLineaInvestigacion entity.');
            }

            $em->remove($entity);
            $em->flush();
        

        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Investigación eliminada correctamente');
        return $this->redirect($this->generateUrl('plantillalineainvestigacion'));
    }

    /**
     * Creates a form to delete a PlantillaLineaInvestigacion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('plantillalineainvestigacion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\OtrasRelaciones;
use GEDELTUR\Bundle\Form\OtrasRelacionesType;

/**
 * OtrasRelaciones controller.
 *
 */
class OtrasRelacionesController extends Controller
{

    /**
     * Lists all OtrasRelaciones entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:OtrasRelaciones')->findAll();





        $query = $em->createQuery('SELECT te FROM GEDELTURBundle:OtrasRelaciones te
                                      GROUP BY te.institucion ');
        $res = $query->getResult();

            $aux=count($entities);
            //return new Response("$aux");

        return $this->render('GEDELTURBundle:OtrasRelaciones:index.html.twig', array(
            'entities' => $entities,
            'res' => $res,

        ));
    }


     /**
     * Lists all OtrasRelaciones entities.
     *
     */
    public function institucionAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:OtrasRelaciones')->findAll();





        $query = $em->createQuery('SELECT te FROM GEDELTURBundle:OtrasRelaciones te
                                      GROUP BY te.institucion ');
        $res = $query->getResult();

            $aux=count($entities);
            //return new Response("$aux");

        return $this->render('GEDELTURBundle:OtrasRelaciones:indexd.html.twig', array(
            'entities' => $entities,
            'res' => $res,

        ));
    }



    /**
     * Creates a new OtrasRelaciones entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new OtrasRelaciones();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);


        $concepto= $form['concepto']->getData();
        $institucion= $form['institucion']->getData();
        $em = $this->getDoctrine()->getManager();

        $consulta = $em->getRepository('GEDELTURBundle:OtrasRelaciones')->findBy(array(
            'concepto' => $concepto,
            'institucion' => $institucion,

        ));




        $contador=count($consulta);

        if ($form->isValid()) {

            if($contador>0)
            {
                $this->get('session')->getFlashBag()->add('msg','Lo siento! Esta Relación  ya ha sido adicionada ');
                return $this->render('GEDELTURBundle:OtrasRelaciones:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
            }


            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Relación adicionada correctamente');
            return $this->redirect($this->generateUrl('otrasrelaciones'));
        }

        return $this->render('GEDELTURBundle:OtrasRelaciones:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a OtrasRelaciones entity.
    *
    * @param OtrasRelaciones $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(OtrasRelaciones $entity)
    {
        $form = $this->createForm(new OtrasRelacionesType(), $entity, array(
            'action' => $this->generateUrl('otrasrelaciones_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new OtrasRelaciones entity.
     *
     */
    public function newAction()
    {
        $entity = new OtrasRelaciones();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:OtrasRelaciones:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a OtrasRelaciones entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:OtrasRelaciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find OtrasRelaciones entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:OtrasRelaciones:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing OtrasRelaciones entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:OtrasRelaciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find OtrasRelaciones entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:OtrasRelaciones:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a OtrasRelaciones entity.
    *
    * @param OtrasRelaciones $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(OtrasRelaciones $entity)
    {
        $form = $this->createForm(new OtrasRelacionesType(), $entity, array(
            'action' => $this->generateUrl('otrasrelaciones_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing OtrasRelaciones entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:OtrasRelaciones')->find($id);
        $c=$entity->getConcepto();
        $i=$entity->getInstitucion();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find OtrasRelaciones entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);


        $concepto= $editForm['concepto']->getData();
        $institucion= $editForm['institucion']->getData();
        $em = $this->getDoctrine()->getManager();

        $consulta = $em->getRepository('GEDELTURBundle:OtrasRelaciones')->findBy(array(
            'concepto' => $concepto,
            'institucion' => $institucion,

        ));




        $contador=count($consulta);
        //return new Response("$contador");

        if ($editForm->isValid()) {

            if($contador>0 && ($c!=$concepto || $i!=$institucion))
            {
                 $this->get('session')->getFlashBag()->add('msg','Lo siento! Esta Relación  ya existe ');
                  return $this->render('GEDELTURBundle:OtrasRelaciones:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
            }
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Relación actualizada correctamente');
            return $this->redirect($this->generateUrl('otrasrelaciones'));
        }

        return $this->render('GEDELTURBundle:OtrasRelaciones:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a OtrasRelaciones entity.
     *
     */
    public function eliminarInsAction(Request $request, $id)
    {
        
            $em = $this->getDoctrine()->getManager();
            //$entity = $em->getRepository('GEDELTURBundle:OtrasRelaciones')->find($id);

            

            $entidades = $em->getRepository('GEDELTURBundle:OtrasRelaciones')->findBy(array(
            'institucion' => $id,

        ));

            $aux=count($entidades);
            //return new Response("$id");

            if (!$entidades) {
                throw $this->createNotFoundException('Unable to find OtrasRelaciones entity.');
            }

            foreach ($entidades as $e ) 
            {
                
                $entity = $em->getRepository('GEDELTURBundle:OtrasRelaciones')->find($e->getId()); 

                $em->remove($entity);
                $em->flush();   
            }



            /*if (!$entity) {
                throw $this->createNotFoundException('Unable to find OtrasRelaciones entity.');
            }*/

            
        

        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Relación eliminada correctamente');
        return $this->redirect($this->generateUrl('intitucion'));
    }






     public function deleteAction(Request $request, $id)
    {
        
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:OtrasRelaciones')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find OtrasRelaciones entity.');
            }

            $em->remove($entity);
            $em->flush();
        

        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Relación eliminada correctamente');
        return $this->redirect($this->generateUrl('otrasrelaciones'));
    }








    /**
     * Creates a form to delete a OtrasRelaciones entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('otrasrelaciones_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\Asignatura;
use GEDELTUR\Bundle\Form\AsignaturaType;

/**
 * Asignatura controller.
 *
 */
class AsignaturaController extends Controller
{

    /**
     * Lists all Asignatura entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Asignatura')->findAll();

        return $this->render('GEDELTURBundle:Asignatura:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Asignatura entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Asignatura();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);


        $nombre= $form['nombre']->getData();
        $carrera= $form['carrera']->getData();
        $em = $this->getDoctrine()->getManager();

        $consulta = $em->getRepository('GEDELTURBundle:Asignatura')->findOneBy(array(
            'nombre' => $nombre,
            'carrera' => $carrera,

        ));




        $contador=count($consulta);
        //return new Response("$contador");

        if ($form->isValid()) {


            if($contador>0)
            {
                 $this->get('session')->getFlashBag()->add('msg','Lo siento! Ya esta asignatura se encuentra registrada');
                 return $this->render('GEDELTURBundle:Asignatura:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            ));
            }
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Asignatura adicionada correctamente');

           return $this->redirect($this->generateUrl('asignatura'));
        }

        return $this->render('GEDELTURBundle:Asignatura:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Asignatura entity.
    *
    * @param Asignatura $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Asignatura $entity)
    {
        $form = $this->createForm(new AsignaturaType(), $entity, array(
            'action' => $this->generateUrl('asignatura_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new Asignatura entity.
     *
     */
    public function newAction()
    {
        $entity = new Asignatura();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:Asignatura:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Asignatura entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Asignatura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Asignatura entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Asignatura:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Asignatura entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Asignatura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Asignatura entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Asignatura:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Asignatura entity.
    *
    * @param Asignatura $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Asignatura $entity)
    {
        $form = $this->createForm(new AsignaturaType(), $entity, array(
            'action' => $this->generateUrl('asignatura_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Asignatura entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Asignatura')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la entidad Asignatura.');
        }

        $n= $entity->getNombre();
        $c= $entity->getCarrera();

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);


        





        $nombre= $editForm['nombre']->getData();
        $carrera= $editForm['carrera']->getData();

        $em = $this->getDoctrine()->getManager();

        $consulta = $em->getRepository('GEDELTURBundle:Asignatura')->findOneBy(array(
            'nombre' => $nombre,
            'carrera' => $carrera,

        ));




        $contador=count($consulta);
       // return new Response("$c");

        if ($editForm->isValid()) {
            //return new Response("$contador");

            if($contador>0 && ($nombre!=$n || $carrera!=$c)  )
            {
                 //return new Response("$contador");
                 $this->get('session')->getFlashBag()->add('msg','Lo siento! Ya esta asignatura se encuentra registrada');
                 return $this->render('GEDELTURBundle:Asignatura:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
            }
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Asignatura modificada correctamente');

            return $this->redirect($this->generateUrl('asignatura'));
        }

        return $this->render('GEDELTURBundle:Asignatura:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Asignatura entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
       
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:Asignatura')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Asignatura entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Asignatura eliminada correctamente');




        

        return $this->redirect($this->generateUrl('asignatura'));
    }

    /**
     * Creates a form to delete a Asignatura entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('asignatura_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use GEDELTUR\Bundle\Entity\Colaborador;
use GEDELTUR\Bundle\Form\ColaboradorType;

/**
 * Colaborador controller.
 *
 */
class ColaboradorController extends Controller
{


    private function ci_valido($ci)
    {
        $mes = substr($ci, 2, 2);
        $dia = substr($ci, 4, 2);
        $dias = array('01' => 31,'02' => 28,'03' => 31,'04' => 30,'05' => 31,'06' => 30,'07' => 31,'08' => 31,'09' => 30,'10' => 31,'11' => 30,'12' => 31);

        if($mes <= 12 && $dia <= $dias[$mes])
        {
            return true;
        }
        return false;
    }

    /**
     * Lists all Colaborador entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Colaborador')->findAll();
        $contador=count($entities);

        return $this->render('GEDELTURBundle:Colaborador:index.html.twig', array(
            'entities' => $entities,
            'contador' => $contador,
        ));
    }
    /**
     * Creates a new Colaborador entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Colaborador();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);



        $ci= $form['id']['ci']->getData();
        $em = $this->getDoctrine()->getManager();

        $consulta = $em->getRepository('GEDELTURBundle:RecursosHumanos')->findOneBy(array(
            'ci' => $ci
        ));


        if ($form->isValid() && count($consulta)>0)
        {

            $this->get('session')->getFlashBag()->add('msg','Lo siento! El carnet de identidad corresponde a otra Persona.');
            return $this->render('GEDELTURBundle:Colaborador:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
        }

        if($form->isValid() && !$this->ci_valido($form['id']['ci']->getData()))
        {

             $this->get('session')->getFlashBag()->add('msg','Lo siento! El carnet de identidad debe tener un valor válido de mes y día.');
            return $this->render('GEDELTURBundle:Colaborador:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
            
        }

        if ($form->isValid() && $form['id']['image']->getData()==null)
        {
             $this->get('session')->getFlashBag()->add('msg','Lo siento! Debe incluir una imagen. ');
            return $this->render('GEDELTURBundle:Colaborador:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
        }



        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->getId()->subirFoto();
             $var=$entity->getId();
             
           $em->persist($entity->getId());
            $em->flush();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Colaborador adicionado correctamente.');

            return $this->redirect($this->generateUrl('colaborador'));
        }

        return $this->render('GEDELTURBundle:Colaborador:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Colaborador entity.
    *
    * @param Colaborador $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Colaborador $entity)
    {
        $form = $this->createForm(new ColaboradorType(), $entity, array(
            'action' => $this->generateUrl('colaborador_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new Colaborador entity.
     *
     */
    public function newAction()
    {
        $entity = new Colaborador();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:Colaborador:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Colaborador entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Colaborador')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Colaborador entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Colaborador:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Colaborador entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Colaborador')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Colaborador entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Colaborador:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Colaborador entity.
    *
    * @param Colaborador $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Colaborador $entity)
    {
        $form = $this->createForm(new ColaboradorType(), $entity, array(
            'action' => $this->generateUrl('colaborador_update', array('id' => $entity->getId()->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Colaborador entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Colaborador')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Colaborador entity.');
        }




        $foto = $entity->getId()->getImage();
        $c= $entity->getId()->getCi();

         $rutaFoto='uploads/gedeltur/fotos/'.$entity->getId()->getImage();

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        

         $ci= $editForm['id']['ci']->getData();
         
        
           
        $em = $this->getDoctrine()->getManager();

        $consulta = $em->getRepository('GEDELTURBundle:RecursosHumanos')->findOneBy(array(
            'ci' => $ci
        ));

        $a=count($consulta);
          
        //return new Response("$a");

         if ($editForm->isValid() && $ci!=$c && count($consulta)>0)
        {
             
            $this->get('session')->getFlashBag()->add('msg','Lo siento! El carnet de identidad corresponde a otra Persona.');
            return $this->render('GEDELTURBundle:Colaborador:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
       
        }

        if($editForm->isValid() && !$this->ci_valido($editForm['id']['ci']->getData()))
        {

             $this->get('session')->getFlashBag()->add('msg','Lo siento! El carnet de identidad debe tener un valor válido de mes y día.');
            return $this->render('GEDELTURBundle:Colaborador:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
        
            
        }

       if ($editForm->isValid()) {
            if ($editForm['id']['image']->getData()!=null)
            {
                $entity->getId()->removeUpload($rutaFoto);
                $entity->getId()->subirFoto();
            }
            

            if($editForm['id']['image']->getData()==null)
            {
                $entity->getId()->setImage($foto);

            }
            $em->flush();
             
             $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Colaborador editado correctamente.');
            return $this->redirect($this->generateUrl('colaborador'));
        }

           
        

        return $this->render('GEDELTURBundle:Colaborador:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Colaborador entity.
     *
     */
    public function deleteAction(Request $request, $id)//Eliminar
    {
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('GEDELTURBundle:Colaborador')->find($id);
        $rrhh = $em->getRepository('GEDELTURBundle:RecursosHumanos')->find($id);
        //$rrhhp=$rrhh->get;

        $rutaFoto='uploads/gedeltur/fotos/'.$entity->getId()->getImage();
        if (!$entity) 
        {
            throw $this->createNotFoundException('Unable to find Colaborador entity.');
        }
        //$padre =$entity->getId()->getId();
        
        return new Response("$entity");

        $entity->getId()->removeUpload($rutaFoto);


        $em->remove($entity);
        $em->flush();

        
        $em->remove($padre);
        $em->flush();
        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Colaborador eliminado correctamente.');
        

        return $this->redirect($this->generateUrl('colaborador'));
    }

    /**
     * Creates a form to delete a Colaborador entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('colaborador_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

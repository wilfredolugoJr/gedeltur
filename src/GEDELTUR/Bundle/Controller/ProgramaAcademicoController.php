<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\ProgramaAcademico;
use GEDELTUR\Bundle\Form\ProgramaAcademicoType;

/**
 * ProgramaAcademico controller.
 *
 */
class ProgramaAcademicoController extends Controller
{

    /**
     * Lists all ProgramaAcademico entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:ProgramaAcademico')->findBy(array(
          
            'tipo' => "Especialidad",
            
        ));

        return $this->render('GEDELTURBundle:ProgramaAcademico:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new ProgramaAcademico entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new ProgramaAcademico();
        $form = $this->createCreateForm($entity);
         //return new Response("dfgdfg");
        $form->handleRequest($request);

       

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setTipo("Especialidad");
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Especialidad adicionada correctamente');
            return $this->redirect($this->generateUrl('programaacademico'));
        }

        return $this->render('GEDELTURBundle:ProgramaAcademico:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a ProgramaAcademico entity.
    *
    * @param ProgramaAcademico $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(ProgramaAcademico $entity)
    {

        $plantilla='';
        $em = $this->getDoctrine()->getManager();

        $plantillas = $em->getRepository('GEDELTURBundle:Plantilla')->findall();

        $i=0;

        foreach($plantillas as $p)
        {
            if($i+1<count($plantillas))
            {
                $plantilla.=$p->getId()->getId().',';
            }
            else
                $plantilla.=$p->getId()->getId();
            $i++;
        }
        $form = $this->createForm(new ProgramaAcademicoType(), $entity, array(
            'action' => $this->generateUrl('programaacademico_create'),
            'method' => 'POST',
            'attr'   => array('plantilla'=>$plantilla)
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new ProgramaAcademico entity.
     *
     */
    public function newAction()
    {
        $entity = new ProgramaAcademico();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:ProgramaAcademico:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ProgramaAcademico entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:ProgramaAcademico')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProgramaAcademico entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:ProgramaAcademico:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing ProgramaAcademico entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:ProgramaAcademico')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProgramaAcademico entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:ProgramaAcademico:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ProgramaAcademico entity.
    *
    * @param ProgramaAcademico $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ProgramaAcademico $entity)
    {

        $plantilla='';
        $em = $this->getDoctrine()->getManager();

        $plantillas = $em->getRepository('GEDELTURBundle:Plantilla')->findall();

        $i=0;

        foreach($plantillas as $p)
        {
            if($i+1<count($plantillas))
            {
                $plantilla.=$p->getId()->getId().',';
            }
            else
                $plantilla.=$p->getId()->getId();
            $i++;
        }

        $form = $this->createForm(new ProgramaAcademicoType(), $entity, array(
            'action' => $this->generateUrl('programaacademico_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'attr'   => array('plantilla'=>$plantilla),
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }


    private function createExtraForm(ProgramaAcademico $entity)
    {



        $form = $this->createForm(new ProgramaAcademicoaType(), $entity, array(
            'action' => $this->generateUrl('programaacademico_update', array('id' => $entity->getId())),
            'method' => 'PUT',

        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }




    /**
     * Edits an existing ProgramaAcademico entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:ProgramaAcademico')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProgramaAcademico entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        //print_r($editForm);
        //return new Response('');
        $editForm->handleRequest($request);
        //$var=$request->request->get('gedeltur_bundle_programaacademico');
        //$entity=new ProgramaAcademico();
        /*$entity->setTitulo($var[titulo]);
        $entity->setEncargado($var[encargado]);
        $entity->setInicio($var[inicio]);
        $entity->setObjetivos($var[objetivos]);
        $entity->setPerfilEgresado($var[perfilEgresado]);
        $entity->setRequisitos($var[requisitos]);
        $entity->setTiempo($var[tiempo]);
        $entity->setTarifa($var[tarifa]);*/


        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Especialidad modificada correctamente');
            return $this->redirect($this->generateUrl('programaacademico'));
        }

        return $this->render('GEDELTURBundle:ProgramaAcademico:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ProgramaAcademico entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
       
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:ProgramaAcademico')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ProgramaAcademico entity.');
            }

            $em->remove($entity);
            $em->flush();
        

        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Especialidad eliminada correctamente');
        return $this->redirect($this->generateUrl('programaacademico'));
    }

    /**
     * Creates a form to delete a ProgramaAcademico entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('programaacademico_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

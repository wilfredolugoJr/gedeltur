<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\Proyecto;
use GEDELTUR\Bundle\Form\ProyectoType;
use Symfony\Component\ClassLoader\ClassMapGenerator;
use Symfony\Component\ClassLoader\MapClassLoader;
use Symfony\Component\HttpFoundation\Response;

/**
 * Proyecto controller.
 *
 */
class ProyectoController extends Controller
{

    /**
     * Lists all Proyecto entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Proyecto')->findAll();

        return $this->render('GEDELTURBundle:Proyecto:index.html.twig', array(
            'entities' => $entities,
        ));
    }





    public function proyectopdfAction()
    {
        ClassMapGenerator::createMap(__DIR__ . '/fpdf17');
        ClassMapGenerator::dump(__DIR__ . '/fpdf17', __DIR__ . '/class_map.php');

        $mapping = include __DIR__ . '/class_map.php';
        $loader = new MapClassLoader($mapping);
        $loader->register();



        $pdf = new \FPDF('P','mm','Legal');
        $pdf->SetFont('times', 'B', 16);
        $pdf->AddPage();

        //$pdf->SetAutoPageBreak();
        $fechaactual = new \DateTime('now');

        $pdf->Image('bundles/gedeltur/iconos/logo.jpg',10,10,-300);


        $pdf->Cell(190, 10, utf8_decode("Centro de Estudios de Gerencia,Desarrollo Local y Turismo."), 0, 2, "C");
        $pdf->SetFont('times', 'B', 16);
        $pdf->Cell(190, 10, utf8_decode("Proyectos"), 0, 2, "C");
        $pdf->SetFont('times', 'B', 8);

        $pdf->Cell(190, 10,$fechaactual ->format('d-m-Y'), 10,10,-300);
        //$pdf->Cell(50, 10, utf8_decode($fechaactual), 1);

        $em = $this->getDoctrine()->getManager();
        $pdf->SetFont('times', 'B', 14);
        $lineas = $em->getRepository('GEDELTURBundle:Proyecto')->findAll();
        //$afiliados = array();


        $pdf->Cell(40, 10, "Nombre", 1);
        //$pdf->Cell(40, 10, "Resumen", 1);
        $pdf->Cell(60, 10, "Objetivos", 1);
        $pdf->Cell(60, 10, "Jefe del Proyecto", 1);
        $pdf->Cell(40, 10,utf8_decode( "Líneas Asociadas"), 1);

        $pdf->Ln(10);
        foreach($lineas as $linea )
        {
            $pdf->SetFont('arial', '', 11);

            $titulos=$this->divide($linea->getTitulo(),40);
            $objs=$this->divide($linea->getObjetivo(),60);
            $imps=$this->divide($linea->getPlantilla(),60);
            //array_push($afiliados,$prest);

            // $pdf->Cell(40, 10, $linea->getResumen(), 1);

            $p_tis=array();
            foreach($linea->getLineas()  as  $p  )
            {
                $p_tis=array_merge($p_tis,$this->divide($p->getTitulo(),40));
                //$pdf->MultiCell(40, 10 ,  $p->getTitulo(),1);
            }
            $cant=$this->mayor( $titulos,$objs,$imps,$p_tis);
//return new Response("rfgsdf");

            for($i =0;$i<$cant;$i++)
            {
                if($i<$cant-1)
                    $fondo="R";
                else
                    $fondo="RB";
                if($i<count($titulos))
                    $pdf->Cell(40, 6, $titulos[$i], $fondo."L");
                else
                    $pdf->Cell(40, 6, "", $fondo."L");
                if($i<count($objs))
                    $pdf->Cell(60, 6, $objs[$i], $fondo);
                else
                    $pdf->Cell(60, 6, "", $fondo);
                if($i<count($imps))
                    $pdf->Cell(60, 6, $imps[$i], $fondo);
                else
                    $pdf->Cell(60, 6, "", $fondo);
                if($i<count($p_tis))
                    $pdf->Cell(40, 6, $p_tis[$i],$fondo,1);
                else
                    $pdf->Cell(40, 6, "", $fondo, 1);
            }
        }

        $response = new Response(
            $pdf->Output(), 200, array('content-type' => 'application/pdf')
        );


        return $response;
    }

    private function mayor($pos,$pos2,$pos3,$pos4)
    {
        $cant1=count($pos);
        $cant2=count($pos2);
        $cant3=count($pos3);
        $cant4=count($pos4);

        $mayor = $cant1;

        if($cant2>$mayor)
            $mayor = $cant2;
        if($cant3>$mayor)
            $mayor = $cant3;
        if($cant4>$mayor)
            $mayor = $cant4;
        return $mayor;
    }


    private function divide($cadena,$ancho)
    {
        ClassMapGenerator::createMap(__DIR__ . '/fpdf17');
        ClassMapGenerator::dump(__DIR__ . '/fpdf17', __DIR__ . '/class_map.php');
        $mapping = include __DIR__ . '/class_map.php';
        $loader = new MapClassLoader($mapping);
        $loader->register();

        $pdf = new \FPDF('P','mm','Legal');
        $pdf->SetFont('arial', '', 12);

        $res=array();
        $partes=explode(' ',$cadena);

        $pos=0;
        $longit=count($partes);
        while($pos< $longit) {
            $linea=$partes[$pos++];
            while ($pos< $longit && $pdf->GetStringWidth($linea . ' ' . $partes[$pos]) < $ancho) {
                $linea .= ' ' .$partes[$pos];
                $pos++;
            }
            $res[]=$linea;
        }
        return $res;
    }




    /**
     * Creates a new Proyecto entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Proyecto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $p=$form['rrhh']->getData();
            $j=$form['plantilla']->getData();

            $flag=false;


            foreach( $p  as $plantilla )
            {
                if($plantilla->getId()==$j->getId()->getId())
                {
                    $flag=true;
                    //return new Response("wdfsdf");
                }
            }

            if($flag==false)
            {
                $this->get('session')->getFlashBag()->add('msg','Lo siento! El jefe de Proyecto debe ser un integrante del mismo.');
                return $this->render('GEDELTURBundle:Proyecto:new.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
                ));
            }



            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Proyecto adicionado correctamente');
            return $this->redirect($this->generateUrl('proyecto'));
        }

        return $this->render('GEDELTURBundle:Proyecto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Proyecto entity.
    *
    * @param Proyecto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Proyecto $entity)
    {
        $form = $this->createForm(new ProyectoType(), $entity, array(
            'action' => $this->generateUrl('proyecto_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new Proyecto entity.
     *
     */
    public function newAction()
    {
        $entity = new Proyecto();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:Proyecto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Proyecto entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Proyecto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Proyecto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Proyecto:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Proyecto entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Proyecto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Proyecto entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Proyecto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Proyecto entity.
    *
    * @param Proyecto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Proyecto $entity)
    {
        $form = $this->createForm(new ProyectoType(), $entity, array(
            'action' => $this->generateUrl('proyecto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Proyecto entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Proyecto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Proyecto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $p=$editForm['rrhh']->getData();
            $j=$editForm['plantilla']->getData();

            $flag=false;


            foreach( $p  as $plantilla )
            {
                if($plantilla->getId()==$j->getId()->getId())
                {
                    $flag=true;
                }
            }

            if($flag==false)
            {
                $this->get('session')->getFlashBag()->add('msg','Lo siento! El jefe de Proyecto debe ser un integrante del mismo.');
                return $this->render('GEDELTURBundle:Proyecto:edit.html.twig', array(
                    'entity'      => $entity,
                    'edit_form'   => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                ));
            }



            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Proyecto modificado correctamente');
            return $this->redirect($this->generateUrl('proyecto'));
        }

        return $this->render('GEDELTURBundle:Proyecto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Proyecto entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
       
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:Proyecto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Proyecto entity.');
            }

            $em->remove($entity);
            $em->flush();
       

        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Proyecto eliminado correctamente');
        return $this->redirect($this->generateUrl('proyecto'));
    }

    /**
     * Creates a form to delete a Proyecto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('proyecto_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

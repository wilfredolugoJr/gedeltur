<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\TematicaSct;
use GEDELTUR\Bundle\Form\TematicaSctType;

/**
 * TematicaSct controller.
 *
 */
class TematicaSctController extends Controller
{

    /**
     * Lists all TematicaSct entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:TematicaSct')->findAll();

        return $this->render('GEDELTURBundle:TematicaSct:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TematicaSct entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TematicaSct();
        $form = $this->createCreateForm($entity);
        $idpla=$request->request->get('res');

        $em = $this->getDoctrine()->getManager();

        $plantilla = $em->getRepository('GEDELTURBundle:Plantilla')->findBy(array(
            'id' => $idpla,

        ));





        $form->handleRequest($request);

        $nombre= $form['nombre']->getData();

        $em = $this->getDoctrine()->getManager();

        $consulta = $em->getRepository('GEDELTURBundle:TematicaSct')->findBy(array(
            'nombre' => $nombre,


        ));







        $contador=count($consulta);
        //return new Response("$contador");

        if ($form->isValid()) {
            if( $contador>0)
            {
               $this->get('session')->getFlashBag()->add('msg','Lo siento! Esta Tématica ya ha sido registrada para este servicio'); 
                return $this->render('GEDELTURBundle:TematicaSct:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        )); 
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Temática adicionada correctamente');
            return $this->redirect($this->generateUrl('tematicasct'));
        }

        return $this->render('GEDELTURBundle:TematicaSct:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a TematicaSct entity.
    *
    * @param TematicaSct $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(TematicaSct $entity)
    {

        $plantilla='';
        $em = $this->getDoctrine()->getManager();

        $plantillas = $em->getRepository('GEDELTURBundle:Plantilla')->findall();

        $i=0;

        foreach($plantillas as $p)
        {
            if($i+1<count($plantillas))
            {
                $plantilla.=$p->getId()->getId().',';
            }
            else
                $plantilla.=$p->getId()->getId();
            $i++;
        }


        $form = $this->createForm(new TematicaSctType(), $entity, array(
            'action' => $this->generateUrl('tematicasct_create'),
            'method' => 'POST',
            'attr'   => array('plantilla'=>$plantilla)
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new TematicaSct entity.
     *
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();

        $plantillas = $em->getRepository('GEDELTURBundle:Plantilla')->findall();
        $entity = new TematicaSct();
        $form   = $this->createCreateForm($entity);


        return $this->render('GEDELTURBundle:TematicaSct:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'plantillas'=>$plantillas,
        ));
    }

    /**
     * Finds and displays a TematicaSct entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:TematicaSct')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TematicaSct entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:TematicaSct:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing TematicaSct entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:TematicaSct')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TematicaSct entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:TematicaSct:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TematicaSct entity.
    *
    * @param TematicaSct $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TematicaSct $entity)
    {
        $form = $this->createForm(new TematicaSctType(), $entity, array(
            'action' => $this->generateUrl('tematicasct_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing TematicaSct entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:TematicaSct')->find($id);

        $n=$entity->getNombre();



        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TematicaSct entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);


        $nombre= $editForm['nombre']->getData();

        $em = $this->getDoctrine()->getManager();

        $consulta = $em->getRepository('GEDELTURBundle:TematicaSct')->findBy(array(
            'nombre' => $nombre,


        ));




        $contador=count($consulta);

        if ($editForm->isValid()) {

            if($contador>0 && ($nombre!=$n ))
            {
                 $this->get('session')->getFlashBag()->add('msg','Lo siento! Esta Tématica ya ha sido registrada para este servicio'); 
                 return $this->render('GEDELTURBundle:TematicaSct:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));           
            }
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Temática modificada correctamente');
            return $this->redirect($this->generateUrl('tematicasct'));
        }

        return $this->render('GEDELTURBundle:TematicaSct:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TematicaSct entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        

        
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:TematicaSct')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TematicaSct entity.');
            }

            $em->remove($entity);
            $em->flush();
        

        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Temática eliminada correctamente');
        return $this->redirect($this->generateUrl('tematicasct'));
    }

    /**
     * Creates a form to delete a TematicaSct entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tematicasct_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

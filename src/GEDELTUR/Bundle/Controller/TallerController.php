<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\Taller;
use GEDELTUR\Bundle\Form\TallerType;

/**
 * Taller controller.
 *
 */
class TallerController extends Controller
{

    /**
     * Lists all Taller entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Taller')->findAll();

        return $this->render('GEDELTURBundle:Taller:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Taller entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Taller();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Taller adicionado correctamente');
            return $this->redirect($this->generateUrl('taller'));
        }

        return $this->render('GEDELTURBundle:Taller:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Taller entity.
    *
    * @param Taller $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Taller $entity)
    {
        $form = $this->createForm(new TallerType(), $entity, array(
            'action' => $this->generateUrl('taller_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new Taller entity.
     *
     */
    public function newAction()
    {
        $entity = new Taller();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:Taller:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Taller entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Taller')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Taller entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Taller:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Taller entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Taller')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Taller entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Taller:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Taller entity.
    *
    * @param Taller $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Taller $entity)
    {
        $form = $this->createForm(new TallerType(), $entity, array(
            'action' => $this->generateUrl('taller_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Taller entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Taller')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Taller entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Taller modificado correctamente');
            return $this->redirect($this->generateUrl('taller'));
        }

        return $this->render('GEDELTURBundle:Taller:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Taller entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:Taller')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Taller entity.');
            }

            $em->remove($entity);
            $em->flush();
        

        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Taller eliminado correctamente');
        return $this->redirect($this->generateUrl('taller'));
    }

    /**
     * Creates a form to delete a Taller entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('taller_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContext;
use GEDELTUR\Bundle\Entity\Usuarios;
use GEDELTUR\Bundle\Entity\Proyecto;
use GEDELTUR\Bundle\Entity\RecursosHumanos;
use GEDELTUR\Bundle\Form\RecursosHumanosType;
use GEDELTUR\Bundle\Form\ProyectoType;
use GEDELTUR\Bundle\Entity\Plantilla;
use GEDELTUR\Bundle\Form\PlantillaType;

use GEDELTUR\Bundle\Entity\PlantillaProyecto;
use GEDELTUR\Bundle\Form\PlantillaProyectoType;

class DefaultController extends Controller
{
    public function indexAction()
    {
       
        
        $user = $this->get('security.context')->getToken()->getUser();
        $pagina="";
        
        return $this->render('GEDELTURBundle:Default:index.html.twig', array(
            'user' => $user,
            'pagina' => $pagina,

        ));
    }


    public function sobreAction()
    {

        $em = $this->getDoctrine()->getManager();


        $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();


        return $this->render('GEDELTURBundle:Default:sobre.html.twig', array(


            'eventos' => $eventos,
            'cant_eventos' => count($eventos),
        ));
    }

    public function asignaturasAction()
    {
       
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Asignatura')->findAll();
        $contactos = $em->getRepository('GEDELTURBundle:Contacto')->findAll();
        $var=0;
          $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();


        return $this->render('GEDELTURBundle:Default:asignaturas.html.twig', array(
             'entities' => $entities,
             'contactos' => $contactos,
             'var' => $var,
             'eventos' => $eventos,
             'cant_eventos' => count($eventos),
        ));
    }


    public function asignaturas_showAction($id)
    {


       
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Asignatura')->findBy(array(
            'id' => $id,
            
        ));

       $contactos = $em->getRepository('GEDELTURBundle:Contacto')->findAll();

        $var=count($contactos);
        //return new Response("$var");

         $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();

        return $this->render('GEDELTURBundle:Default:asignaturas.html.twig', array(
             'entities' => $entities,
             'contactos' => $contactos,
             'eventos' => $eventos,
           'cant_eventos' => count($eventos),

        ));
    }

    public function diplomados_showAction($id)
    {
       
        $em = $this->getDoctrine()->getManager();

        $contactos = $em->getRepository('GEDELTURBundle:Contacto')->findAll();

        $entities = $em->getRepository('GEDELTURBundle:Maestria')->findBy(array(
            'id' => $id,
            
        ));
              $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();


        if(count($entities)>0)
        {
            return $this->render('GEDELTURBundle:Default:maestria.html.twig', array(
             'entities' => $entities,
             'contactos' => $contactos,
             'eventos' => $eventos,
           'cant_eventos' => count($eventos),
              ));
        }

         $entities = $em->getRepository('GEDELTURBundle:Diplomado')->findBy(array(
            'id' => $id,
            
        ));


         if(count($entities)>0)
        {
           return $this->render('GEDELTURBundle:Default:diplomado.html.twig', array(
             'entities' => $entities,
             'contactos' => $contactos, 
             'eventos' => $eventos,
           'cant_eventos' => count($eventos),
              ));
        }


          $entities = $em->getRepository('GEDELTURBundle:ProgramaAcademico')->findBy(array(
            'id' => $id,
            'tipo' => "Especialidad",
            
        ));
       

          //$var=count($entities);

        //return new Response("$var");

       return $this->render('GEDELTURBundle:Default:especialidad.html.twig', array(
             'entities' => $entities,
             'contactos' => $contactos,
             'eventos' => $eventos,
           'cant_eventos' => count($eventos),
             

        ));
    }





    



    public function maestriasAction()
    {
       
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Maestria')->findAll();
        $contactos = $em->getRepository('GEDELTURBundle:Contacto')->findAll();
         $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();
           
        
        return $this->render('GEDELTURBundle:Default:maestria.html.twig', array(
             'entities' => $entities,
             'contactos' => $contactos,
              'eventos' => $eventos,
              'cant_eventos' => count($eventos),
            

        ));
    }


    public function especialidadAction()
    {
       
        $em = $this->getDoctrine()->getManager();

       


 $entities = $em->getRepository('GEDELTURBundle:Especialidad')->findAll();

        $contactos = $em->getRepository('GEDELTURBundle:Contacto')->findAll();
         $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();
           
        
        return $this->render('GEDELTURBundle:Default:especialidad.html.twig', array(
             'entities' => $entities,
             'contactos' => $contactos,
            'eventos' => $eventos,
           'cant_eventos' => count($eventos),

        ));
    }



    public function diplomadosAction()
    {
       
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Diplomado')->findAll();
        $contactos = $em->getRepository('GEDELTURBundle:Contacto')->findAll();
         $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();
          
        return $this->render('GEDELTURBundle:Default:diplomado.html.twig', array(
             'entities' => $entities,
             'contactos' => $contactos,
              'eventos' => $eventos,
           'cant_eventos' => count($eventos),

        ));
    }

    public function cursosAction()
    {
       
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Curso')->findAll();
         $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();

        
        return $this->render('GEDELTURBundle:Default:cursos.html.twig', array(
             'entities' => $entities,
              'eventos' => $eventos,
           'cant_eventos' => count($eventos),
            

        ));
    }

    public function cursos_showAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Curso')->findBy(array(
            'id' => $id,

        ));
         $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();
           


        return $this->render('GEDELTURBundle:Default:cursos.html.twig', array(
            'entities' => $entities,
            'eventos' => $eventos,
           'cant_eventos' => count($eventos),


        ));
    }


    public function serviciosAction()
    {

        $em = $this->getDoctrine()->getManager();

        $contenidos = $em->getRepository('GEDELTURBundle:Contenido')->findAll();
        $entities = $em->getRepository('GEDELTURBundle:ServiciosCienticoTecnicos')->findAll();
        $tematicas = $em->getRepository('GEDELTURBundle:TematicaSct')->findAll();
        $contactos = $em->getRepository('GEDELTURBundle:Contacto')->findAll();
        $var=0;
         $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();


        return $this->render('GEDELTURBundle:Default:servicios.html.twig', array(
             'entities' => $entities,
             'tematicas' => $tematicas,
             'contactos' => $contactos,
             'contenidos' => $contenidos,
              'eventos' => $eventos,
           'cant_eventos' => count($eventos),

             'var' => $var,

        ));
    }



    public function proyectosAction()
    {

        $em = $this->getDoctrine()->getManager();

        $proyectos = $em->getRepository('GEDELTURBundle:Proyecto')->findAll();


        $var=0;
        $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();


        return $this->render('GEDELTURBundle:Default:proyecto.html.twig', array(
            'proyectos' => $proyectos,

            'eventos' => $eventos,
            'cant_eventos' => count($eventos),

            'var' => $var,

        ));
    }


    public function proyectos_showAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $proyectos = $em->getRepository('GEDELTURBundle:Proyecto')->findBy(array(
            'id' => $id,

        ));




        $var=0;
        $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();


        return $this->render('GEDELTURBundle:Default:proyecto.html.twig', array(
            'proyectos' => $proyectos,

            'eventos' => $eventos,
            'cant_eventos' => count($eventos),

            'var' => $var,

        ));
    }


    public function liAction()
    {

        $em = $this->getDoctrine()->getManager();

        $lineas = $em->getRepository('GEDELTURBundle:LineaInvestigacion')->findAll();


        $var=0;
        $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();

        $query = $em->createQuery('SELECT tm.nombre,tm.apellidos,tm.id FROM GEDELTURBundle:LineaInvestigacion li

                                        JOIN li.proyecto p
                                        JOIN p.rrhh tm


                                        GROUP BY tm.id');
        $res = $query->getResult();


        return $this->render('GEDELTURBundle:Default:lineas.html.twig', array(
            'lineas' => $lineas,

            'eventos' => $eventos,
            'cant_eventos' => count($eventos),

            'var' => $var,
            'res' => $res,

        ));
    }



    public function li_showAction($id)
    {

        $em = $this->getDoctrine()->getManager();


        $lineas = $em->getRepository('GEDELTURBundle:LineaInvestigacion')->findBy(array(
            'id' => $id,

        ));


        $query = $em->createQuery('SELECT tm.nombre,tm.apellidos,tm.id FROM GEDELTURBundle:LineaInvestigacion li

                                        JOIN li.proyecto p
                                        JOIN p.rrhh tm


                                        GROUP BY tm.id');
        $res = $query->getResult();




        $var=0;
        $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();


        return $this->render('GEDELTURBundle:Default:lineas.html.twig', array(
            'lineas' => $lineas,

            'eventos' => $eventos,
            'cant_eventos' => count($eventos),

            'var' => $var,
            'res' => $res,

        ));
    }




 public function eventoAction($id)
    {
       
        $em = $this->getDoctrine()->getManager();

        $eventos = $em->getRepository('GEDELTURBundle:Evento')->findBy(array(
            'id' => $id,

        ));
        $e = $em->getRepository('GEDELTURBundle:Evento')->findOneBy(array(
            'id' => $id,

        ));
        $var=$e->getCurso();

        $query = $em->createQuery('SELECT te.nombre,te.descrpcion FROM GEDELTURBundle:Evento e
                                        JOIN e.taller t
                                        JOIN t.tematica te

                                        GROUP BY te.id');
        $res = $query->getResult();


$evento = $em->getRepository('GEDELTURBundle:Evento')->findAll();
           

        return $this->render('GEDELTURBundle:Default:evento.html.twig', array(
             'eventos' => $eventos,
            'res' => $res,
            'cant' => count($var),
     'evento' => $evento,
           'cant_eventos' => count($evento),
        ));
    }



    public function registrarseAction()
    {
       
        
        
        
        return $this->render('GEDELTURBundle:Default:registrarse.html.twig', array(
            

        ));
    }

    public function trabajadoresAction()
    {
       
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Plantilla')->findAll();
        $contactos = $em->getRepository('GEDELTURBundle:Contacto')->findAll();
        $asignaturas=$em->getRepository('GEDELTURBundle:Asignatura')->findAll();
        $pa=$em->getRepository('GEDELTURBundle:ProgramaAcademico')->findAll();
         $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();
 
       
        return $this->render('GEDELTURBundle:Default:trabajadores.html.twig', array(
            'entities' => $entities,
            'contactos' => $contactos,
            'asignaturas' => $asignaturas,
            'pa' => $pa,
            'eventos' => $eventos,
           'cant_eventos' => count($eventos),

            

        ));
    }
 
   

    public function trabajadores_showAction($id)
    {
       
        $em = $this->getDoctrine()->getManager();

          $rrhh = $em->getRepository('GEDELTURBundle:RecursosHumanos')->findOneBy(array(
            'id' => $id,
            
        ));


        $contactos = $em->getRepository('GEDELTURBundle:Contacto')->findOneBy(array(
            'rrhh' => $id,

        ));


        $con=count( $contactos);




           

           

        $asignaturas = $em->getRepository('GEDELTURBundle:Asignatura')->findBy(array(
            'plantilla' => $id,
            
        ));
                //$c=count( $rrhh);

                //return new Response("$id");



        $pro = $em->getRepository('GEDELTURBundle:Proyecto')->findAll();



        $li = $em->getRepository('GEDELTURBundle:LineaInvestigacion')->findAll();
                //$c=count( $li);

                //return new Response("$c");
        $aca = $em->getRepository('GEDELTURBundle:ProgramaAcademico')->findBy(array(
            'encargado' => $id,
            
        ));
         $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();

        $query = $em->createQuery('SELECT tm.nombre,tm.apellidos,tm.id FROM GEDELTURBundle:LineaInvestigacion li

                                        JOIN li.proyecto p
                                        JOIN p.rrhh tm


                                        GROUP BY tm.id');
        $res = $query->getResult();

       

       
        return $this->render('GEDELTURBundle:Default:rrhhtabla.html.twig', array(
            'rrhh' => $rrhh,
            'asignaturas' => $asignaturas,
            'li' => $li,
            'pro' => $pro,
            'aca' => $aca,
            'res' => $res,
            'cant_eventos' => count($eventos),

 
           'eventos' => $eventos,
            'contactos' => $contactos,
            'con' => $con,
           
            

        ));
    }



     public function estudiantesAction()
    {
       
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:GCE')->findAll();
        $contactos = $em->getRepository('GEDELTURBundle:Contacto')->findAll();
        $lineas=$em->getRepository('GEDELTURBundle:LineaInvestigacion')->findAll();

         $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();




       //$c=count($entities);
        //return new Response("$c");
       
        return $this->render('GEDELTURBundle:Default:estudiantes.html.twig', array(
            'entities' => $entities,
            'contactos' => $contactos,
            'lineas' => $lineas,
           'cant_eventos' => count($eventos),

 
           'eventos' => $eventos,
            
           

            

        ));
    }


     public function colaboradoresAction()
    {
       
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Colaborador')->findAll();
        $contactos = $em->getRepository('GEDELTURBundle:Contacto')->findAll();

        $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();


        
       //$c=count($entities);
        //return new Response("$c");
       
        return $this->render('GEDELTURBundle:Default:colaborador.html.twig', array(
            'entities' => $entities,
            'contactos' => $contactos,
            'cant_eventos' => count($eventos),


            'eventos' => $eventos,
           
           
            
           

            

        ));
    }


    public function inicioAction()
    {
        $em = $this->getDoctrine()->getManager();
        $colaborador = $em->getRepository('GEDELTURBundle:Colaborador')->findBy(array(
            'catDoc' => 'instructor',
        ));
        $plantilla = $em->getRepository('GEDELTURBundle:Plantilla')->findBy(array(
            'catDoc' => 'instructor',
        ));

        $instructor=count($colaborador)+count($plantilla);





        $colaborador_uno = $em->getRepository('GEDELTURBundle:Colaborador')->findBy(array(
            'catDoc' => 'auxiliar',
        ));
        $plantilla_uno = $em->getRepository('GEDELTURBundle:Plantilla')->findBy(array(
            'catDoc' => 'auxiliar',
        ));

        $auxiliar=count($colaborador_uno)+count($plantilla_uno);


        $colaborador_dos = $em->getRepository('GEDELTURBundle:Colaborador')->findBy(array(
        'catDoc' => 'titular',
    ));

        $plantilla_dos = $em->getRepository('GEDELTURBundle:Plantilla')->findBy(array(
            'catDoc' => 'titular',
        ));

        $titular=count($colaborador_dos)+count($plantilla_dos);


        $colaborador_asistente = $em->getRepository('GEDELTURBundle:Colaborador')->findBy(array(
        'catDoc' => 'asistente',
    ));

        $plantilla_asistente = $em->getRepository('GEDELTURBundle:Plantilla')->findBy(array(
            'catDoc' => 'asistente',
        ));

        //$titular=count($colaborador_dos)+count($plantilla_dos);
        $asistente=count($colaborador_asistente)+count($plantilla_asistente);








        $colaborador_tres = $em->getRepository('GEDELTURBundle:Colaborador')->findBy(array(
            'catCien' => 'licenciado',
        ));

        $plantilla_tres = $em->getRepository('GEDELTURBundle:Plantilla')->findBy(array(
            'catCien' => 'licenciado',
        ));

        $licenciado=count($colaborador_tres)+count($plantilla_tres);




        $colaborador_cuatro = $em->getRepository('GEDELTURBundle:Colaborador')->findBy(array(
            'catCien' => 'ingeniero',
        ));

        $plantilla_cuatro = $em->getRepository('GEDELTURBundle:Plantilla')->findBy(array(
            'catCien' => 'ingeniero',
        ));

        $ingeniero=count($colaborador_cuatro)+count($plantilla_cuatro);


        $colaborador_cinco = $em->getRepository('GEDELTURBundle:Colaborador')->findBy(array(
            'catCien' => 'master',
        ));

        $plantilla_cinco = $em->getRepository('GEDELTURBundle:Plantilla')->findBy(array(
            'catCien' => 'master',
        ));

        $master=count($colaborador_cinco)+count($plantilla_cinco);



        $colaborador_seis = $em->getRepository('GEDELTURBundle:Colaborador')->findBy(array(
            'catCien' => 'doctor',
        ));

        $plantilla_seis = $em->getRepository('GEDELTURBundle:Plantilla')->findBy(array(
            'catCien' => 'doctor',
        ));

        $doctor=count($colaborador_seis)+count($plantilla_seis);








        $curso = $em->getRepository('GEDELTURBundle:Curso')->findAll();
        $diplomado = $em->getRepository('GEDELTURBundle:Diplomado')->findAll();
        $maestria = $em->getRepository('GEDELTURBundle:Maestria')->findAll();
        $proyecto = $em->getRepository('GEDELTURBundle:Proyecto')->findAll();
        $linea = $em->getRepository('GEDELTURBundle:LineaInvestigacion')->findAll();

        $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();







        
        
        return $this->render('GEDELTURBundle:Default:principal.html.twig', array(


            'instructor' => $instructor,
            'auxiliar' => $auxiliar,
            'titular' => $titular,
            'asistente' => $asistente,
            'licenciado' => $licenciado,
            'ingeniero' => $ingeniero,
            'master' => $master,
            'doctor' => $doctor,

            'curso' => count($curso),
            'diplomado' => count($diplomado),
            'proyecto' => count(  $proyecto ),
            'maestria' => count($maestria),
            'linea' => count($linea),
            'eventos' => $eventos,
             'cant_eventos' => count($eventos),
            

        ));
    }


    
    public function loginAction(Request $request)
    {
       // $entity = new Usuarios();
        //$factory = $this->get('security.encoder_factory');
        //$encoder = $factory->getEncoder($entity);
        //$contrasenna = $encoder->encodePassword("123", $entity->getSalt());
        //return new Response($contrasenna);
        //$entity->setContrasenna($contrasenna);
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render(
            'GEDELTURBundle:Default:login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                'error'         => $error,
            )
        );
    }



    public function sct_filtroAction($tipo)
    {

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:ServiciosCienticoTecnicos')->findBy(array(
            'tipo' => $tipo,

        ));
        $contenidos = $em->getRepository('GEDELTURBundle:Contenido')->findAll();

        $tematicas = $em->getRepository('GEDELTURBundle:TematicaSct')->findAll();
        $contactos = $em->getRepository('GEDELTURBundle:Contacto')->findAll();
        $var=0;
        $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();



        return $this->render('GEDELTURBundle:Default:servicios.html.twig', array(
            'entities' => $entities,
            'tematicas' => $tematicas,
            'contactos' => $contactos,
            'contenidos' => $contenidos,
            'eventos' => $eventos,
            'cant_eventos' => count($eventos),

            'var' => $var,

        ));
    }






}


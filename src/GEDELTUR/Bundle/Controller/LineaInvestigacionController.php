<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\LineaInvestigacion;
use GEDELTUR\Bundle\Form\LineaInvestigacionType;
use Symfony\Component\HttpFoundation\Response;




use Symfony\Component\ClassLoader\ClassMapGenerator;
use Symfony\Component\ClassLoader\MapClassLoader;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;



use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * LineaInvestigacion controller.
 *
 */
class LineaInvestigacionController extends Controller
{

    /**
     * Lists all LineaInvestigacion entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:LineaInvestigacion')->findAll();


        $query = $em->createQuery('SELECT tm.nombre,tm.apellidos FROM GEDELTURBundle:LineaInvestigacion li

                                        JOIN li.proyecto p
                                        JOIN p.rrhh tm


                                        GROUP BY tm.id');
        $res = $query->getResult();


        $int=count($res);
        //return new Response("$int");
           

       

        return $this->render('GEDELTURBundle:LineaInvestigacion:index.html.twig', array(
            'entities' => $entities,
             'contador' => count($entities),
            'res' => $res,

        ));

    }







    public function lineapdfAction()
    {
        ClassMapGenerator::createMap(__DIR__ . '/fpdf17');
        ClassMapGenerator::dump(__DIR__ . '/fpdf17', __DIR__ . '/class_map.php');

        $mapping = include __DIR__ . '/class_map.php';
        $loader = new MapClassLoader($mapping);
        $loader->register();



        $pdf = new \FPDF('P','mm','Legal');
        $pdf->SetFont('times', 'B', 14);
        $pdf->AddPage();

        //$pdf->SetAutoPageBreak();
        $fechaactual = new \DateTime('now');

        $pdf->Image('bundles/gedeltur/iconos/logo.jpg',10,10,-300);



        $pdf->Cell(190, 10, utf8_decode("Centro de Estudios de Gerencia,Desarrollo local y turismo."), 0, 2, "C");
        $pdf->SetFont('times', 'B', 14);
        $pdf->Cell(190, 10, utf8_decode("Líneas de Investigación"), 0, 2, "C");
        $pdf->SetFont('times', 'B', 8);
        $pdf->Cell(190, 10,$fechaactual ->format('d-m-Y'), 10,10,-300);
        //$pdf->Cell(50, 10, utf8_decode($fechaactual), 1);

        $em = $this->getDoctrine()->getManager();
        $pdf->SetFont('times', 'B', 12);
        $lineas = $em->getRepository('GEDELTURBundle:LineaInvestigacion')->findAll();
        //$afiliados = array();


        $pdf->Cell(40, 10, "Nombre", 1);
        //$pdf->Cell(40, 10, "Resumen", 1);
        $pdf->Cell(60, 10, "Objetivos", 1);
        $pdf->Cell(60, 10, "Impacto", 1);
        $pdf->Cell(40, 10, "Proyectos Asociados", 1);

        $pdf->Ln(10);
        foreach($lineas as $linea )
        {
            $pdf->SetFont('arial', '', 12);

            $titulos=$this->divide($linea->getTitulo(),40);
            $objs=$this->divide($linea->getObjetivo(),60);
            $imps=$this->divide($linea->getImpacto(),60);
                //array_push($afiliados,$prest);


               // $pdf->Cell(40, 10, $linea->getResumen(), 1);

            $p_tis=array();
            foreach($linea->getProyecto()  as  $p  )
                {
                    $p_tis=array_merge($p_tis,$this->divide($p->getTitulo(),40));
                    //$pdf->MultiCell(40, 10 ,  $p->getTitulo(),1);
                }
                    $cant=$this->mayor( $titulos,$objs,$imps,$p_tis);

                for($i =0;$i<$cant;$i++)
                {
                    if($i<$cant-1)
                        $fondo="R";
                    else
                        $fondo="RB";
                    if($i<count($titulos))
                        $pdf->Cell(40, 6, $titulos[$i], $fondo."L");
                    else
                        $pdf->Cell(40, 6, "", $fondo."L");
                    if($i<count($objs))
                        $pdf->Cell(60, 6, $objs[$i], $fondo);
                    else
                        $pdf->Cell(60, 6, "", $fondo);
                    if($i<count($imps))
                        $pdf->Cell(60, 6, $imps[$i], $fondo);
                    else
                        $pdf->Cell(60, 6, "", $fondo);
                    if($i<count($p_tis))
                        $pdf->Cell(40, 6, $p_tis[$i],$fondo,1);
                    else
                        $pdf->Cell(40, 6, "", $fondo, 1);
                }
            }

        $response = new Response(
            $pdf->Output(), 200, array('content-type' => 'application/pdf')
        );


        return $response;
    }

    private function mayor($pos,$pos2,$pos3,$pos4)
    {
        $cant1=count($pos);
        $cant2=count($pos2);
        $cant3=count($pos3);
        $cant4=count($pos4);

        $mayor = $cant1;

        if($cant2>$mayor)
            $mayor = $cant2;
        if($cant3>$mayor)
            $mayor = $cant3;
        if($cant4>$mayor)
            $mayor = $cant4;
        return $mayor;
    }


    private function divide($cadena,$ancho)
    {
        ClassMapGenerator::createMap(__DIR__ . '/fpdf17');
        ClassMapGenerator::dump(__DIR__ . '/fpdf17', __DIR__ . '/class_map.php');
        $mapping = include __DIR__ . '/class_map.php';
        $loader = new MapClassLoader($mapping);
        $loader->register();

        $pdf = new \FPDF('P','mm','Legal');
        $pdf->SetFont('arial', '', 12);

        $res=array();
        $partes=explode(' ',$cadena);

        $pos=0;
        $longit=count($partes);
        while($pos< $longit) {
            $linea=$partes[$pos++];
            while ($pos< $longit && $pdf->GetStringWidth($linea . ' ' . $partes[$pos]) < $ancho) {
                $linea .= ' ' .$partes[$pos];
                $pos++;
            }
            $res[]=$linea;
        }
        return $res;
    }










    /**
     * Creates a new LineaInvestigacion entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new LineaInvestigacion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho!  Linea de Investigación  adicionada correctamente');
            return $this->redirect($this->generateUrl('lineainvestigacion'));
        }

        return $this->render('GEDELTURBundle:LineaInvestigacion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a LineaInvestigacion entity.
    *
    * @param LineaInvestigacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(LineaInvestigacion $entity)
    {
        $form = $this->createForm(new LineaInvestigacionType(), $entity, array(
            'action' => $this->generateUrl('lineainvestigacion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new LineaInvestigacion entity.
     *
     */
    public function newAction()
    {
        $entity = new LineaInvestigacion();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:LineaInvestigacion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a LineaInvestigacion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:LineaInvestigacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LineaInvestigacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:LineaInvestigacion:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing LineaInvestigacion entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:LineaInvestigacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LineaInvestigacion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:LineaInvestigacion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a LineaInvestigacion entity.
    *
    * @param LineaInvestigacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(LineaInvestigacion $entity)
    {
        $form = $this->createForm(new LineaInvestigacionType(), $entity, array(
            'action' => $this->generateUrl('lineainvestigacion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing LineaInvestigacion entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:LineaInvestigacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find LineaInvestigacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho!  Linea de Investigación  editada correctamente');
            return $this->redirect($this->generateUrl('lineainvestigacion'));
        }

        return $this->render('GEDELTURBundle:LineaInvestigacion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a LineaInvestigacion entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
       
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:LineaInvestigacion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find LineaInvestigacion entity.');
            }

            $em->remove($entity);
            $em->flush();
       

        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Linea de Investigación eliminada correctamente');
        return $this->redirect($this->generateUrl('lineainvestigacion'));
    }

    /**
     * Creates a form to delete a LineaInvestigacion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('lineainvestigacion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\Evento;
use GEDELTUR\Bundle\Form\EventoType;

/**
 * Evento controller.
 *
 */
class EventoController extends Controller
{

    /**
     * Lists all Evento entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Evento')->findAll();

        return $this->render('GEDELTURBundle:Evento:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Evento entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Evento();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $fechaAct = new \DateTime();




        if ($form->isValid() && $form['image']->getData()==null)
        {
            $this->get('session')->getFlashBag()->add('msg','Lo siento! Debe incluir una imagen ');
            return $this->render('GEDELTURBundle:Evento:new.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));
        }

        if($form['fechaResumen']->getData() < $fechaAct ||$form['fechaAceptacion']->getData() < $fechaAct ||$form['fecha']->getData() < $fechaAct ||$form['fin']->getData() < $fechaAct)
        {
            $this->get('session')->getFlashBag()->add('msg','Lo siento! Las fechas no deben ser menor que la actual');
            return $this->render('GEDELTURBundle:Evento:new.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));
        }


        if($form['fechaResumen']->getData() > $form['fechaAceptacion']->getData() )
        {
            $this->get('session')->getFlashBag()->add('msg','Lo siento! La fecha de Aceptación del Trabajo para el evento no puede ser antes de la del envio del mismo');
            return $this->render('GEDELTURBundle:Evento:new.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));
        }

        if($form['fin']->getData()< $form['fecha']->getData() )
        {
            $this->get('session')->getFlashBag()->add('msg','Lo siento! La fechas de inicio y fin del eventos no son correctas.');
            return $this->render('GEDELTURBundle:Evento:new.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));
        }


        if($form['fechaResumen']->getData() > $form['fecha']->getData() || $form['fechaAceptacion']->getData() > $form['fecha']->getData())
        {
            $this->get('session')->getFlashBag()->add('msg','Lo siento! La fecha del evento no debe ser antes que el evio del trabajo o  de la aceptación del mismo');
            return $this->render('GEDELTURBundle:Evento:new.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));
        }

        if ($form->isValid()) {


            $em = $this->getDoctrine()->getManager();
            $entity->subirFoto();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Evento adicionado correctamente');
            return $this->redirect($this->generateUrl('evento'));

        }

        return $this->render('GEDELTURBundle:Evento:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Evento entity.
    *
    * @param Evento $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Evento $entity)
    {
        $form = $this->createForm(new EventoType(), $entity, array(
            'action' => $this->generateUrl('evento_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new Evento entity.
     *
     */
    public function newAction()
    {
        $entity = new Evento();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:Evento:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Evento entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Evento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Evento entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Evento:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Evento entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Evento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Evento entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Evento:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Evento entity.
    *
    * @param Evento $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Evento $entity)
    {
        $form = $this->createForm(new EventoType(), $entity, array(
            'action' => $this->generateUrl('evento_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Evento entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Evento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Evento entity.');
        }

        $foto = $entity->getImage();
        $rutaFoto='uploads/gedeltur/fotos/'.$entity->getImage();

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);


        $fechaAct = new \DateTime();



        if($editForm['fechaResumen']->getData() < $fechaAct ||$editForm['fechaAceptacion']->getData() < $fechaAct ||$editForm['fecha']->getData() < $fechaAct ||$editForm['fin']->getData() < $fechaAct)
        {
            $this->get('session')->getFlashBag()->add('msg','Lo siento! Las fechas no deben ser menor que la actual');
            return $this->render('GEDELTURBundle:Evento:edit.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }


        if($editForm['fechaResumen']->getData() > $editForm['fechaAceptacion']->getData() )
        {
            $this->get('session')->getFlashBag()->add('msg','Lo siento! La fecha de Aceptación del Trabajo para el evento no puede ser antes de la del envio del mismo');
            return $this->render('GEDELTURBundle:Evento:edit.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }

        if($editForm['fin']->getData() < $editForm['fecha']->getData() )
        {
            $this->get('session')->getFlashBag()->add('msg','Lo siento! La fechas de inicio y fin del eventos no son correctas.');
            return $this->render('GEDELTURBundle:Evento:edit.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }


        if($editForm['fechaResumen']->getData() > $editForm['fecha']->getData() || $editForm['fechaAceptacion']->getData() > $editForm['fecha']->getData())
        {
            $this->get('session')->getFlashBag()->add('msg','Lo siento! La fechas de inicio y fin del eventos no son correctas.');
            return $this->render('GEDELTURBundle:Evento:edit.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }






        if ($editForm->isValid()) {

            if ($editForm['image']->getData()!=null)
            {
                $entity->removeUpload($rutaFoto);
                $entity->subirFoto();
            }

            if($editForm['image']->getData()==null)
            {
                $entity->setImage($foto);

            }
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Evento editado correctamente');
            return $this->redirect($this->generateUrl('evento'));
        }

        return $this->render('GEDELTURBundle:Evento:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Evento entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:Evento')->find($id);
             $rutaFoto='uploads/gedeltur/fotos/'.$entity->getImage();
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Evento entity.');
            }
            $entity->removeUpload($rutaFoto);
            $em->remove($entity);
            $em->flush();
        



        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Evento eliminado correctamente');
        return $this->redirect($this->generateUrl('evento'));
    }

    /**
     * Creates a form to delete a Evento entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('evento_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

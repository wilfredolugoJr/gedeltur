<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\Especialidad;
use GEDELTUR\Bundle\Form\EspecialidadType;

/**
 * Especialidad controller.
 *
 */
class EspecialidadController extends Controller
{

    /**
     * Lists all Especialidad entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
//return  new Response("ERtert");
        $entities = $em->getRepository('GEDELTURBundle:Especialidad')->findAll();

        return $this->render('GEDELTURBundle:Especialidad:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Especialidad entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Especialidad();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $fechaAct = new \DateTime();

        if ($form->isValid()) {

            if( $form['id']['inicio']->getData()> $form['id']['fin']->getData())
            {
                $this->get('session')->getFlashBag()->add('msg','Lo siento! Las fechas no son correctas');
                return $this->render('GEDELTURBundle:Especialidad:new.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
                ));
            }




            $em = $this->getDoctrine()->getManager();
            $entity->getId()->setTipo("Especialidad");

            $em->persist($entity->getId());
            $em->flush();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Especialidad adicionada correctamente.');
            return $this->redirect($this->generateUrl('especialidad'));
        }

        return $this->render('GEDELTURBundle:Especialidad:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Especialidad entity.
    *
    * @param Especialidad $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Especialidad $entity)
    {
        $form = $this->createForm(new EspecialidadType(), $entity, array(
            'action' => $this->generateUrl('especialidad_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new Especialidad entity.
     *
     */
    public function newAction()
    {
        $entity = new Especialidad();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:Especialidad:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Especialidad entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Especialidad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Especialidad entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Especialidad:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Especialidad entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Especialidad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Especialidad entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Especialidad:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Especialidad entity.
    *
    * @param Especialidad $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Especialidad $entity)
    {
        $form = $this->createForm(new EspecialidadType(), $entity, array(
            'action' => $this->generateUrl('especialidad_update', array('id' => $entity->getId()->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Especialidad entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Especialidad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Especialidad entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            if( $editForm['id']['inicio']->getData()> $editForm['id']['fin']->getData())
            {
                $this->get('session')->getFlashBag()->add('msg','Lo siento! Las fechas no son correctas.');
                return $this->render('GEDELTURBundle:Especialidad:edit.html.twig', array(
                    'entity'      => $entity,
                    'edit_form'   => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                ));}

            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Especialidad editada correctamente');
            return $this->redirect($this->generateUrl('especialidad'));
        }

        return $this->render('GEDELTURBundle:Especialidad:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Especialidad entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {



            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:Especialidad')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Especialidad entity.');
            }

            $padre =$entity->getId();
            $em->remove($entity);
            $em->flush();

            $em->remove($padre);
            $em->flush();

        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Especialidad eliminada correctamente');
        return $this->redirect($this->generateUrl('especialidad'));
    }

    /**
     * Creates a form to delete a Especialidad entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('especialidad_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

<?php

namespace GEDELTUR\Bundle\Controller;

use GEDELTUR\Bundle\Entity\Contenido;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\ServiciosCienticoTecnicos;
use GEDELTUR\Bundle\Form\ServiciosCienticoTecnicosType;

/**
 * ServiciosCienticoTecnicos controller.
 *
 */
class ServiciosCienticoTecnicosController extends Controller
{

    /**
     * Lists all ServiciosCienticoTecnicos entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:ServiciosCienticoTecnicos')->findAll();

        return $this->render('GEDELTURBundle:ServiciosCienticoTecnicos:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new ServiciosCienticoTecnicos entity.
     *
     */
    public function createAction(Request $request)
    {

        $entity = new ServiciosCienticoTecnicos();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);


        if ($form->isValid()) {

















            $em = $this->getDoctrine()->getManager();
            if($form->get('contenido')->getData()!="")
            {
                $em->persist($entity);
                $em->flush();
                $c=new Contenido();
                $c->setId($entity);
                $c->setContenido($form->get('contenido')->getData());
                $em->persist($c);
            }
           else
               $em->persist($entity);

            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Servicio Científico Técnico adicionado correctamente');
            return $this->redirect($this->generateUrl('servicioscienticotecnicos'));
        }

        return $this->render('GEDELTURBundle:ServiciosCienticoTecnicos:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a ServiciosCienticoTecnicos entity.
    *
    * @param ServiciosCienticoTecnicos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(ServiciosCienticoTecnicos $entity)
    {
        $form = $this->createForm(new ServiciosCienticoTecnicosType(), $entity, array(
            'action' => $this->generateUrl('servicioscienticotecnicos_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new ServiciosCienticoTecnicos entity.
     *
     */
    public function newAction()
    {
        $entity = new ServiciosCienticoTecnicos();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:ServiciosCienticoTecnicos:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ServiciosCienticoTecnicos entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:ServiciosCienticoTecnicos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ServiciosCienticoTecnicos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:ServiciosCienticoTecnicos:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing ServiciosCienticoTecnicos entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:ServiciosCienticoTecnicos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ServiciosCienticoTecnicos entity.');
        }
        $editForm = $this->createEditForm($entity);

        if($entity->getTipo()=="capacitación")
        {
            $contenido = $em->getRepository('GEDELTURBundle:Contenido')->find($id);
            $editForm->get("contenido")->setData($contenido->getContenido());
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:ServiciosCienticoTecnicos:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ServiciosCienticoTecnicos entity.
    *
    * @param ServiciosCienticoTecnicos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ServiciosCienticoTecnicos $entity)
    {
        $form = $this->createForm(new ServiciosCienticoTecnicosType(), $entity, array(
            'action' => $this->generateUrl('servicioscienticotecnicos_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing ServiciosCienticoTecnicos entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:ServiciosCienticoTecnicos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ServiciosCienticoTecnicos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if($entity->getTipo()=="capacitación")
            {

                $contenido = $em->getRepository('GEDELTURBundle:Contenido')->find($id);

                $contenido->setContenido($editForm->get("contenido")->getData());


               // $contenido->setTipo($editForm->get("tipo")->getData());

            }
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Servicio Científico Técnico modificado correctamente');
            return $this->redirect($this->generateUrl('servicioscienticotecnicos'));
        }

        return $this->render('GEDELTURBundle:ServiciosCienticoTecnicos:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ServiciosCienticoTecnicos entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:ServiciosCienticoTecnicos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ServiciosCienticoTecnicos entity.');
            }

            $em->remove($entity);
            $em->flush();
       
        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Servicio Científico Técnico eliminado correctamente');
        return $this->redirect($this->generateUrl('servicioscienticotecnicos'));
    }

    /**
     * Creates a form to delete a ServiciosCienticoTecnicos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('servicioscienticotecnicos_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

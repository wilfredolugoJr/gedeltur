<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\Correo;
use GEDELTUR\Bundle\Form\CorreoType;
use GEDELTUR\Bundle\Form\CorreoRespuestaType;

/**
 * Correo controller.
 *
 */
class CorreoController extends Controller
{

    /**
     * Lists all Correo entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

       /* $entities = $em->getRepository('GEDELTURBundle:Correo')->findByRespuesta("");

        return $this->render('GEDELTURBundle:Correo:index.html.twig', array(
            'entities' => $entities,
        ));*/



        $entities = $em->getRepository('GEDELTURBundle:Correo')->findall();

        return $this->render('GEDELTURBundle:Correo:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Correo entity.
     *
     */
    public function createAction(Request $request)
    {
          $em = $this->getDoctrine()->getManager();
         $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();
         $correo = $em->getRepository('GEDELTURBundle:Correo')->findAll();
       
$superacion = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("superacion");
        $investigacion = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("investigacion");
        $servicio = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("servicio");
        $otro = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("otro");
        $user = $this->get('security.context')->getToken()->getUser();

        $entity = new Correo();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);


        if ($form->isValid()) {
            $nombre=$user->getNombre();

            $entity->setNombre($nombre);
            $entity->setRespuesta("");
             $fechaAct = new \DateTime();
            $entity->setFecha($fechaAct);
            

            $us=$user->getUsername();

            $entity->setCorreo($us)  ;
            $entity->setApellidos($user->getApellido());

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            /* $message = \Swift_Message::newInstance()
            ->setSubject('Notificación')
        ->setFrom('send@example.com')
        ->setTo('wilfredo.lugo@estudiantes.upr.edu.cu')
        ->setBody(
            $this->renderView(
                'GEDELTURBundle:Default:email.html.twig',
                array('entity' => $entity)
            )
        )
    ;
    $this->get('mailer')->send($message);*/
 

            return $this->redirect($this->generateUrl('correo_nuevo'));
        }


        return $this->render('GEDELTURBundle:Default:contacto.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'eventos' => $eventos,
           'cant_eventos' => count($eventos),
           'contador'   => count($correo),
            'superacion'   => count($superacion),
            'investigacion'   => count($investigacion),
            'servicio'   => count($servicio),
            'otro'   => count($otro),
            'correo'   => $correo,
        ));
    }

    /**
    * Creates a form to create a Correo entity.
    *
    * @param Correo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Correo $entity)
    {
        $form = $this->createForm(new CorreoType(), $entity, array(
            'action' => $this->generateUrl('correo_creado'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Enviar'));

        return $form;
    }

    /**
     * Displays a form to create a new Correo entity.
     *
     */
    public function newAction()
    {

        $em = $this->getDoctrine()->getManager();
        $correo = $em->getRepository('GEDELTURBundle:Correo')->findAll();

        $superacion = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("superacion");
        $investigacion = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("investigacion");
        $servicio = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("servicio");
        $otro = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("otro");

        $entity = new Correo();
        $form   = $this->createCreateForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();

          $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();
 

        return $this->render('GEDELTURBundle:Default:contacto.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'user'   => $user,
            'correo'   => $correo,
            'contador'   => count($correo),
            'superacion'   => count($superacion),
            'investigacion'   => count($investigacion),
            'servicio'   => count($servicio),
            'otro'   => count($otro),
'eventos' => $eventos,
           'cant_eventos' => count($eventos),
        ));
    }


    public function superacionAction()
    {

        $em = $this->getDoctrine()->getManager();
        $correo = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("superacion");

        $superacion = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("superacion");
        $investigacion = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("investigacion");
        $servicio = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("servicio");
        $otro = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("otro");


        $entity = new Correo();
        $form   = $this->createCreateForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
           $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();
 
        return $this->render('GEDELTURBundle:Default:contacto.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'user'   => $user,
            'correo'   => $correo,
            'contador'   => count($correo),
            'superacion'   => count($superacion),
            'investigacion'   => count($investigacion),
            'servicio'   => count($servicio),
            'otro'   => count($otro),
            'eventos' => $eventos,
           'cant_eventos' => count($eventos),

        ));
    }


    public function sctAction()
    {

        $em = $this->getDoctrine()->getManager();
        $correo = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("servicio");

        $superacion = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("superacion");
        $investigacion = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("investigacion");
        $servicio = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("servicio");
        $otro = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("otro");


        $entity = new Correo();
        $form   = $this->createCreateForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
         $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();


        return $this->render('GEDELTURBundle:Default:contacto.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'user'   => $user,
            'correo'   => $correo,
            'contador'   => count($correo),
            'superacion'   => count($superacion),
            'investigacion'   => count($investigacion),
            'servicio'   => count($servicio),
            'otro'   => count($otro),
             'eventos' => $eventos,
           'cant_eventos' => count($eventos),

        ));
    }



    public function investigacionAction()
    {

        $em = $this->getDoctrine()->getManager();
        $correo =  $em->getRepository('GEDELTURBundle:Correo')->findByTematica("investigacion");

        $superacion = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("superacion");
        $investigacion = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("investigacion");
        $servicio = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("servicio");
        $otro = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("otro");


        $entity = new Correo();
        $form   = $this->createCreateForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
             $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();
 


        return $this->render('GEDELTURBundle:Default:contacto.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'user'   => $user,
            'correo'   => $correo,
            'contador'   => count($correo),
            'superacion'   => count($superacion),
            'investigacion'   => count($investigacion),
            'servicio'   => count($servicio),
            'otro'   => count($otro),
            'eventos' => $eventos,
           'cant_eventos' => count($eventos),

        ));
    }



    public function otroAction()
    {

        $em = $this->getDoctrine()->getManager();
        $correo =  $em->getRepository('GEDELTURBundle:Correo')->findByTematica("otro");

        $superacion = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("superacion");
        $investigacion = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("investigacion");
        $servicio = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("servicio");
        $otro = $em->getRepository('GEDELTURBundle:Correo')->findByTematica("otro");


        $entity = new Correo();
        $form   = $this->createCreateForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
         $eventos = $em->getRepository('GEDELTURBundle:Evento')->findAll();
 

        return $this->render('GEDELTURBundle:Default:contacto.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'user'   => $user,
            'correo'   => $correo,
            'contador'   => count($correo),
            'superacion'   => count($superacion),
            'investigacion'   => count($investigacion),
            'servicio'   => count($servicio),
            'otro'   => count($otro),
'eventos' => $eventos,
           'cant_eventos' => count($eventos),
        ));
    }

    /**
     * Finds and displays a Correo entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Correo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Correo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Correo:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Correo entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Correo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Correo entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Correo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Correo entity.
    *
    * @param Correo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Correo $entity)
    {
        $form = $this->createForm(new CorreoRespuestaType(), $entity, array(
            'action' => $this->generateUrl('correo_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Correo entity.
     *
     */
    public function updateAction(Request $request, $id)
    {

        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Correo')->find($id);



        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Correo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {


            $nombre=$user->getNombre();

            $entity->setNombreResponde($nombre);


            $entity->setApellidosResponde($user->getApellido());
            $em->flush();

          /*  $message = \Swift_Message::newInstance()
            ->setSubject('Notificación')
        ->setFrom('wilfredo.lugo@estudiantes.upr.edu.cu')
        ->setTo($entity->getCorreo())
        ->setBody(
            $this->renderView(
                'GEDELTURBundle:Default:email.html.twig',
                array('entity' => $entity)
            )
        )
    ;
    $this->get('mailer')->send($message);*/

            return $this->redirect($this->generateUrl('correo_nuevo'));
        }

        return $this->render('GEDELTURBundle:Correo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Correo entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
       
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:Correo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Correo entity.');
            }

            $em->remove($entity);
            $em->flush();
       

        return $this->redirect($this->generateUrl('correo'));
    }


      public function eliminarAction(Request $request, $id)
    {
       
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:Correo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Correo entity.');
            }

            $em->remove($entity);
            $em->flush();
       

        return $this->redirect($this->generateUrl('correo_nuevo'));
    }

    /**
     * Creates a form to delete a Correo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('correo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Wiley
 * Date: 06/04/2016
 * Time: 18:33
 */

namespace GEDELTUR\Bundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GEDELTUR\Bundle\Entity\ServiciosCienticoTecnicos;
use GEDELTUR\Bundle\Form\ServiciosCienticoTecnicosType;

class ReportesController extends Controller


{
    public function sct_entidadAction()
    {

        $em = $this->getDoctrine()->getManager();
        $servicios = $em->getRepository('GEDELTURBundle:ServiciosCienticoTecnicos')->findAll();
        $institucion = $em->getRepository('GEDELTURBundle:Institucion')->findAll();
        $otra = $em->getRepository('GEDELTURBundle:OtrasRelaciones')->findAll();


        return $this->render('GEDELTURBundle:Reportes:servicios_entidad.html.twig', array(
            'servicios' => $servicios,
            'institucion' => $institucion,
            'otra' => $otra,
        ));
    }


    public function ws_sctAction()
    {

        $em = $this->getDoctrine()->getManager();
        $servicios = $em->getRepository('GEDELTURBundle:ServiciosCienticoTecnicos')->findAll();



        return $this->render('GEDELTURBundle:Reportes:ws_servicio.html.twig', array(
            'servicios' => $servicios,

        ));
    }

       public function ws_liAction()
    {

        $em = $this->getDoctrine()->getManager();
        $linea = $em->getRepository('GEDELTURBundle:LineaInvestigacion')->findAll();



        return $this->render('GEDELTURBundle:Reportes:ws_li.html.twig', array(
            'linea' => $linea,

        ));
    }


    public function ws_proAction()
    {

        $em = $this->getDoctrine()->getManager();
        $proyecto = $em->getRepository('GEDELTURBundle:Proyecto')->findAll();



        return $this->render('GEDELTURBundle:Reportes:ws_pro.html.twig', array(
            'proyecto' => $proyecto,

        ));
    }



}
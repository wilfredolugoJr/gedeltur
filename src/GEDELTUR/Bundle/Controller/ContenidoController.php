<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\Contenido;
use GEDELTUR\Bundle\Form\ContenidoType;

/**
 * Contenido controller.
 *
 */
class ContenidoController extends Controller
{

    /**
     * Lists all Contenido entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Contenido')->findAll();

        return $this->render('GEDELTURBundle:Contenido:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Contenido entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Contenido();
        $form = $this->createCreateForm($entity);

        
        
         

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $var=$entity->getId();
            
            $em->persist($entity->getId());

            $em->flush();
 
            $em->persist($entity);
            $em->flush();


            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Capacitación adicionada correctamente');

            return $this->redirect($this->generateUrl('contenido'));
        }

        return $this->render('GEDELTURBundle:Contenido:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Contenido entity.
    *
    * @param Contenido $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Contenido $entity)
    {
        $form = $this->createForm(new ContenidoType(), $entity, array(
            'action' => $this->generateUrl('contenido_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));
             $form['id']->remove('tipo');
        return $form;
    }

    /**
     * Displays a form to create a new Contenido entity.
     *
     */
    public function newAction()
    {
        $entity = new Contenido();
        $form   = $this->createCreateForm($entity);
        $form->remove('tipo');

        return $this->render('GEDELTURBundle:Contenido:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Contenido entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Contenido')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contenido entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Contenido:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Contenido entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Contenido')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contenido entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
         $editForm['id']->remove('tipo');

        return $this->render('GEDELTURBundle:Contenido:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Contenido entity.
    *
    * @param Contenido $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Contenido $entity)
    {
        $form = $this->createForm(new ContenidoType(), $entity, array(
            'action' => $this->generateUrl('contenido_update', array('id' => $entity->getId()->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Contenido entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Contenido')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contenido entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Capacitación editada correctamente');
            return $this->redirect($this->generateUrl('servicioscienticotecnicos'));
        }

        return $this->render('GEDELTURBundle:Contenido:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Contenido entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:Contenido')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Contenido entity.');
            }

            $em->remove($entity);
            $em->flush();
            $em->remove($entity->getId());
            $em->flush();
        
        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Capacitación eliminada correctamente');
        return $this->redirect($this->generateUrl('contenido'));
    }

    /**
     * Creates a form to delete a Contenido entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('contenido_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

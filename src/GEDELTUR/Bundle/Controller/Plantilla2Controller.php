<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\Plantilla;
use GEDELTUR\Bundle\Form\PlantillaType;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Length;

/**
 * Plantilla controller.
 *
 */
class PlantillaController extends Controller
{


     private function ci_valido($ci)
    {


        $mes = substr($ci, 2, 2);
        $dia = substr($ci, 4, 2);
        $dias = array('01' => 31,'02' => 28,'03' => 31,'04' => 30,'05' => 31,'06' => 30,'07' => 31,'08' => 31,'09' => 30,'10' => 31,'11' => 30,'12' => 31);

        if($mes <= 12 && $dia <= $dias[$mes])
        {
            return true;
        }
        return false;
    }

    /**
     * Lists all Plantilla entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Plantilla')->findAll();
        $contador=count($entities);

        return $this->render('GEDELTURBundle:Plantilla:index.html.twig', array(
            'entities' => $entities,
            'contador' => $contador,
        ));
    }
    /**
     * Creates a new Plantilla entity.
     *
     */
    public function createAction(Request $request)
    {

        return new Response("sadfasdasdf");
        $entity = new Plantilla();
        $valido=true;
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);


        $ci= $form['id']['ci']->getData();











        $em = $this->getDoctrine()->getManager();

        $consulta = $em->getRepository('GEDELTURBundle:RecursosHumanos')->findOneBy(array(
            'ci' => $ci
        ));

    if ($form->isValid())
    {


        

         if (count($consulta)>0)
        {

           $this->get('session')->getFlashBag()->add('msg','Lo siento! El carnet de identidad corresponde a otra Persona.');
            $valido=false;

        }
        else

         if( !$this->ci_valido($form['id']['ci']->getData()))
        {

             $this->get('session')->getFlashBag()->add('msg','Lo siento! El carnet de identidad debe tener un valor válido de mes y día.');
           $valido=false;
            
        }
        else


        if ($form['id']['image']->getData()==null)
        {
             $this->get('session')->getFlashBag()->add('msg','Lo siento! Debe incluir una imagen. ');
          $valido=false;
        }
            else

        {



       
            $em = $this->getDoctrine()->getManager();
            $entity->getId()->subirFoto();
            $em->persist($entity->getId());
            $em->flush();
            $em->persist($entity);
            $em->flush();


            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Trabajador adicionado correctamente.');

            return $this->redirect($this->generateUrl('plantilla'));
        }
    }


        return $this->render('GEDELTURBundle:Plantilla:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Plantilla entity.
    *
    * @param Plantilla $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Plantilla $entity)
    {
        $form = $this->createForm(new PlantillaType(), $entity, array(
            'action' => $this->generateUrl('plantilla_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new Plantilla entity.
     *
     */
    public function newAction()
    {
        $entity = new Plantilla();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:Plantilla:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Plantilla entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Plantilla')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Plantilla entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Plantilla:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Plantilla entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Plantilla')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Plantilla entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Plantilla:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Plantilla entity.
    *
    * @param Plantilla $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Plantilla $entity)
    {
        $form = $this->createForm(new PlantillaType(), $entity, array(
            'action' => $this->generateUrl('plantilla_update', array('id' => $entity->getId()->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Plantilla entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Plantilla')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Plantilla entity.');
        }

         $foto = $entity->getId()->getImage();
         $c= $entity->getId()->getCi();
         $rutaFoto='uploads/gedeltur/fotos/'.$entity->getId()->getImage();

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

         $ci= $editForm['id']['ci']->getData();
         
        
           
        $em = $this->getDoctrine()->getManager();

        $consulta = $em->getRepository('GEDELTURBundle:RecursosHumanos')->findOneBy(array(
            'ci' => $ci
        ));

        $a=count($consulta);


         if ($editForm->isValid() && $ci!=$c && count($consulta)>0)
        {
             
            $this->get('session')->getFlashBag()->add('msg','Lo siento! El carnet de identidad corresponde a otra Persona.');
            return $this->render('GEDELTURBundle:Plantilla:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
       
        }


        if($editForm->isValid() && !$this->ci_valido($editForm['id']['ci']->getData()))
        {

             $this->get('session')->getFlashBag()->add('msg','Lo siento! El carnet de identidad debe tener un valor válido de mes y día.');
            return $this->render('GEDELTURBundle:Plantilla:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
        
            
        }


        if ($editForm->isValid()) {


            if ($editForm['id']['image']->getData()!=null)
            {
                $entity->getId()->removeUpload($rutaFoto);
                $entity->getId()->subirFoto();

            }

            if($editForm['id']['image']->getData()==null)
            {
                $entity->getId()->setImage($foto);

            }

            $em->flush();
            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Trabajador actualizado correctamente.');

            return $this->redirect($this->generateUrl('plantilla'));
        }

        return $this->render('GEDELTURBundle:Plantilla:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Plantilla entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:Plantilla')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Plantilla entity.');
            }
            $padre =$entity->getId();
             $rutaFoto='uploads/gedeltur/fotos/'.$entity->getId()->getImage();
            $entity->getId()->removeUpload($rutaFoto);
            $em->remove($entity);
            $em->flush();
             $em->remove($padre);
            $em->flush();
            
       

        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Trabajador eliminado correctamente.');

        return $this->redirect($this->generateUrl('plantilla'));
    }

    /**
     * Creates a form to delete a Plantilla entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('plantilla_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\Norma;
use GEDELTUR\Bundle\Form\NormaType;

/**
 * Norma controller.
 *
 */
class NormaController extends Controller
{

    /**
     * Lists all Norma entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Norma')->findAll();

        return $this->render('GEDELTURBundle:Norma:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Norma entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Norma();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Norma guardada correctamente');
            return $this->redirect($this->generateUrl('norma'));
        }

        return $this->render('GEDELTURBundle:Norma:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Norma entity.
    *
    * @param Norma $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Norma $entity)
    {
        $form = $this->createForm(new NormaType(), $entity, array(
            'action' => $this->generateUrl('norma_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new Norma entity.
     *
     */
    public function newAction()
    {
        $entity = new Norma();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:Norma:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Norma entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Norma')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Norma entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Norma:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Norma entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Norma')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Norma entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Norma:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Norma entity.
    *
    * @param Norma $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Norma $entity)
    {
        $form = $this->createForm(new NormaType(), $entity, array(
            'action' => $this->generateUrl('norma_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Norma entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Norma')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Norma entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Norma actualizada correctamente');
            return $this->redirect($this->generateUrl('norma'));
        }

        return $this->render('GEDELTURBundle:Norma:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Norma entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:Norma')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Norma entity.');
            }

            $em->remove($entity);
            $em->flush();
        

        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Norma eliminada correctamente');
        return $this->redirect($this->generateUrl('norma'));
    }

    /**
     * Creates a form to delete a Norma entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('norma_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}

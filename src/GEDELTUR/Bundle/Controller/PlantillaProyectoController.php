<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\PlantillaProyecto;
use GEDELTUR\Bundle\Form\PlantillaProyectoType;

/**
 * PlantillaProyecto controller.
 *
 */
class PlantillaProyectoController extends Controller
{

    /**
     * Lists all PlantillaProyecto entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:PlantillaProyecto')->findAll();

        return $this->render('GEDELTURBundle:PlantillaProyecto:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new PlantillaProyecto entity.
     *
     */
    /*public function createAction(Request $request)
    {
        
        
        $form=$request->request->get('gedeltur_bundle_plantillaproyecto');
        $proyecto=$form['proyecto'];
        $plantillas=$form['plantilla'];
        print_r($plantillas);
        return new Response("retert");
        $em = $this->getDoctrine()->getManager();
        if(count($plantillas)>0)
            {
                foreach ($plantillas as $p) {
                   $entity = new PlantillaProyecto();
                   $entity->setPlantilla($p);
                   $entity->setProyecto($proyecto);

                   $em->persist($entity);
                   $em->flush();
                }

                return $this->redirect($this->generateUrl('plantillaproyecto_show', array('id' => $entity->getId())));
            }
        //$form = $this->createCreateForm($entity);

        //$form->handleRequest($request);


            

            

        return $this->render('GEDELTURBundle:PlantillaProyecto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }*/


     /**
     * Creates a new PlantillaProyecto entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new PlantillaProyecto();
        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);
        

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Proyecto adicionado correctamente');
            return $this->redirect($this->generateUrl('plantillaproyecto'));
        }

        return $this->render('GEDELTURBundle:PlantillaProyecto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a PlantillaProyecto entity.
    *
    * @param PlantillaProyecto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(PlantillaProyecto $entity)
    {
        $form = $this->createForm(new PlantillaProyectoType(), $entity, array(
            'action' => $this->generateUrl('plantillaproyecto_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new PlantillaProyecto entity.
     *
     */
    public function newAction()
    {
        $entity = new PlantillaProyecto();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:PlantillaProyecto:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PlantillaProyecto entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:PlantillaProyecto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PlantillaProyecto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:PlantillaProyecto:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing PlantillaProyecto entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:PlantillaProyecto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PlantillaProyecto entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:PlantillaProyecto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a PlantillaProyecto entity.
    *
    * @param PlantillaProyecto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(PlantillaProyecto $entity)
    {
        $form = $this->createForm(new PlantillaProyectoType(), $entity, array(
            'action' => $this->generateUrl('plantillaproyecto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing PlantillaProyecto entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:PlantillaProyecto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PlantillaProyecto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Proyecto modificado correctamente');
            return $this->redirect($this->generateUrl('plantillaproyecto'));
        }

        return $this->render('GEDELTURBundle:PlantillaProyecto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a PlantillaProyecto entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:PlantillaProyecto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PlantillaProyecto entity.');
            }

            $em->remove($entity);
            $em->flush();
        
        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Proyecto eliminado correctamente');
        return $this->redirect($this->generateUrl('plantillaproyecto'));
    }

    /**
     * Creates a form to delete a PlantillaProyecto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('plantillaproyecto_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

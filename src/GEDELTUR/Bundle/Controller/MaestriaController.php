<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\Maestria;
use GEDELTUR\Bundle\Form\MaestriaType;

/**
 * Maestria controller.
 *
 */
class MaestriaController extends Controller
{

    /**
     * Lists all Maestria entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Maestria')->findAll();
        $plantillas = $em->getRepository('GEDELTURBundle:Plantilla')->findAll();

        return $this->render('GEDELTURBundle:Maestria:index.html.twig', array(
            'entities' => $entities,
            'plantillas' => $plantillas,
        ));
    }
    /**
     * Creates a new Maestria entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Maestria();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            if( $form['id']['inicio']->getData()> $form['id']['fin']->getData())
            {
                $this->get('session')->getFlashBag()->add('msg','Lo siento! Las fechas no son correctas.');
                return $this->render('GEDELTURBundle:Maestria:new.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
                ));}

            $em = $this->getDoctrine()->getManager();
            $entity->getId()->setTipo("Maestria");
           
            $em->persist($entity->getId());
            $em->flush();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Maestría adicionada correctamente.');

            return $this->redirect($this->generateUrl('maestria'));
        }

        return $this->render('GEDELTURBundle:Maestria:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Maestria entity.
    *
    * @param Maestria $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Maestria $entity)
    {
        $form = $this->createForm(new MaestriaType(), $entity, array(
            'action' => $this->generateUrl('maestria_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new Maestria entity.
     *
     */
    public function newAction()
    {
        $entity = new Maestria();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:Maestria:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Maestria entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Maestria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Maestria entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Maestria:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Maestria entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Maestria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Maestria entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Maestria:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Maestria entity.
    *
    * @param Maestria $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Maestria $entity)
    {
        $form = $this->createForm(new MaestriaType(), $entity, array(
            'action' => $this->generateUrl('maestria_update', array('id' => $entity->getId()->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Maestria entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Maestria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Maestria entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {


            if( $editForm['id']['inicio']->getData()> $editForm['id']['fin']->getData())
            {
                $this->get('session')->getFlashBag()->add('msg','Lo siento! Las fechas no son correctas.');
                return $this->render('GEDELTURBundle:Maestria:edit.html.twig', array(
                    'entity'      => $entity,
                    'edit_form'   => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                ));}

            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Maestría editada correctamente');

            return $this->redirect($this->generateUrl('maestria'));
        }

        return $this->render('GEDELTURBundle:Maestria:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Maestria entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:Maestria')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Maestria entity.');
            }

            $em->remove($entity);
            $em->flush();
        
        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Maestría eliminada correctamente');

        return $this->redirect($this->generateUrl('maestria'));
    }

    /**
     * Creates a form to delete a Maestria entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('maestria_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

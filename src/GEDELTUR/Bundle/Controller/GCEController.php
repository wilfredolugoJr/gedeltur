<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\GCE;
use GEDELTUR\Bundle\Form\GCEType;
use Symfony\Component\Intl\ResourceBundle\Util\ArrayAccessibleResourceBundle;

/**
 * GCE controller.
 *
 */
class GCEController extends Controller
{


        private function ci_valido($ci)
    {
        $mes = substr($ci, 2, 2);
        $dia = substr($ci, 4, 2);
        $dias = array('01' => 31,'02' => 28,'03' => 31,'04' => 30,'05' => 31,'06' => 30,'07' => 31,'08' => 31,'09' => 30,'10' => 31,'11' => 30,'12' => 31);

        if($mes <= 12 && $dia <= $dias[$mes])
        {
            return true;
        }
        return false;
    }

    /**
     * Lists all GCE entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:GCE')->findAll();
        $contador=count($entities);

        return $this->render('GEDELTURBundle:GCE:index.html.twig', array(
            'entities' => $entities,
            'contador' => $contador,
        ));
    }
    /**
     * Creates a new GCE entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new GCE();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $fct = array(
            array('carrera' => 'Informática'),
            array('carrera' => 'Mecánica'),
            array('carrera' => 'Telecomunicaciones'),


        );

        $fce = array(
            array('carrera' => 'Economía'),
            array('carrera' => 'Contabilidad y Finanzas'),
            array('carrera' => 'Industrial'),


        );

        $fh = array(
            array('carrera' => 'Derecho'),
            array('carrera' => 'Periodismo'),
            array('carrera' => 'Comunicación Social'),


        );



        $ci= $form['id']['ci']->getData();
        $em = $this->getDoctrine()->getManager();

        $consulta = $em->getRepository('GEDELTURBundle:RecursosHumanos')->findOneBy(array(
            'ci' => $ci
        ));


        if ($form->isValid() && count($consulta)>0)
        {

             $this->get('session')->getFlashBag()->add('msg','Lo siento! El carnet de identidad corresponde a otra Persona.');
            return $this->render('GEDELTURBundle:GCE:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
                'fct' => $fct,
                'fce' => $fce,
                'fh' => $fh,
        ));
        }


         if($form->isValid() && !$this->ci_valido($form['id']['ci']->getData()))
        {

             $this->get('session')->getFlashBag()->add('msg','Lo siento! El carnet de identidad debe tener un valor válido de mes y día.');
            return $this->render('GEDELTURBundle:GCE:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
                'fct' => $fct,
                'fce' => $fce,
                'fh' => $fh,
        ));
            
        }



         if ($form->isValid() && $form['id']['image']->getData()==null)
        {
             $this->get('session')->getFlashBag()->add('msg','Lo siento! Debe incluir una imagen. ');
            return $this->render('GEDELTURBundle:GCE:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
                'fct' => $fct,
                'fce' => $fce,
                'fh' => $fh,
        ));
        }

        
        if ($form->isValid()) {


            
            $em = $this->getDoctrine()->getManager();
            
            $entity->getId()->subirFoto();
            $em->persist($entity->getId());
            $em->flush();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Estudiante adicionado correctamente.');

            return $this->redirect($this->generateUrl('gce'));
        }

        return $this->render('GEDELTURBundle:GCE:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'fct' => $fct,
            'fce' => $fce,
            'fh' => $fh,
        ));
    }

    /**
    * Creates a form to create a GCE entity.
    *
    * @param GCE $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(GCE $entity)
    {
        $form = $this->createForm(new GCEType(), $entity, array(
            'action' => $this->generateUrl('gce_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new GCE entity.
     *
     */
    public function newAction()
    {
        $fct = array(
            array( 'Informática'),
            array( 'Mecánica'),
            array('Telecomunicaciones'),


        );

        $fce = array(
            array('carrera' => 'Economía'),
            array('carrera' => 'Contabilidad y Finanzas'),
            array('carrera' => 'Industrial'),


        );

        $fh = array(
            array('carrera' => 'Derecho'),
            array('carrera' => 'Periodismo'),
            array('carrera' => 'Comunicación Social'),


        );


        $entity = new GCE();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:GCE:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'fct' => $fct,
            'fce' => $fce,
            'fh' => $fh,
        ));
    }

    /**
     * Finds and displays a GCE entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:GCE')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GCE entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:GCE:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing GCE entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:GCE')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GCE entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:GCE:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a GCE entity.
    *
    * @param GCE $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(GCE $entity)
    {
        $form = $this->createForm(new GCEType(), $entity, array(
            'action' => $this->generateUrl('gce_update', array('id' => $entity->getId()->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing GCE entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:GCE')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GCE entity.');
        }

         $foto = $entity->getId()->getImage();
         $c= $entity->getId()->getCi();
         $rutaFoto='uploads/gedeltur/fotos/'.$entity->getId()->getImage();

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
         
          $ci= $editForm['id']['ci']->getData();
         
        
           
          


         
        if($editForm->isValid() && !$this->ci_valido($editForm['id']['ci']->getData()))
        {

             $this->get('session')->getFlashBag()->add('msg','Lo siento! El carnet de identidad debe tener un valor válido de mes y día.');
            return $this->render('GEDELTURBundle:GCE:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
        
        }
        if ($editForm->isValid()) {
            if ($editForm['id']['image']->getData()!=null)
            {
                $entity->getId()->removeUpload($rutaFoto);
                $entity->getId()->subirFoto();
            }
            

            if($editForm['id']['image']->getData()==null)
            {
                $entity->getId()->setImage($foto);

            }
            $em->flush();
            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Estudiante actualizado correctamente.');

             return $this->redirect($this->generateUrl('gce'));
        }

        return $this->render('GEDELTURBundle:GCE:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a GCE entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
       
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:GCE')->find($id);
            $rutaFoto='uploads/gedeltur/fotos/'.$entity->getId()->getImage();
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find GCE entity.');
            }

            $padre =$entity->getId();
            $entity->getId()->removeUpload($rutaFoto);
            $em->remove($entity);
            $em->flush();
            $em->remove($padre);
            $em->flush();


       

        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Estudiante eliminado correctamente.');
        return $this->redirect($this->generateUrl('gce'));
    }

    /**
     * Creates a form to delete a GCE entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('gce_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Elimiar'))
            ->getForm()
        ;
    }
}

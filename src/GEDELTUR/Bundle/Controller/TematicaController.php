<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\Tematica;
use GEDELTUR\Bundle\Form\TematicaType;

/**
 * Tematica controller.
 *
 */
class TematicaController extends Controller
{

    /**
     * Lists all Tematica entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Tematica')->findAll();

        return $this->render('GEDELTURBundle:Tematica:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Tematica entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Tematica();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Temática adicionada correctamente');
            return $this->redirect($this->generateUrl('tematica'));
        }

        return $this->render('GEDELTURBundle:Tematica:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Tematica entity.
    *
    * @param Tematica $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Tematica $entity)
    {
        $form = $this->createForm(new TematicaType(), $entity, array(
            'action' => $this->generateUrl('tematica_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new Tematica entity.
     *
     */
    public function newAction()
    {
        $entity = new Tematica();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:Tematica:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Tematica entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Tematica')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tematica entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Tematica:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Tematica entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Tematica')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tematica entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Tematica:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Tematica entity.
    *
    * @param Tematica $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Tematica $entity)
    {
        $form = $this->createForm(new TematicaType(), $entity, array(
            'action' => $this->generateUrl('tematica_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Tematica entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Tematica')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tematica entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Temática modificada correctamente');
            return $this->redirect($this->generateUrl('tematica'));
        }

        return $this->render('GEDELTURBundle:Tematica:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Tematica entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
       

       
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:Tematica')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Tematica entity.');
            }

            $em->remove($entity);
            $em->flush();
        

        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Temática eliminada correctamente');
        return $this->redirect($this->generateUrl('tematica'));
    }

    /**
     * Creates a form to delete a Tematica entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tematica_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

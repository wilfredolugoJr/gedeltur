<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\RecursosHumanos;
use GEDELTUR\Bundle\Form\RecursosHumanosType;

/**
 * RecursosHumanos controller.
 *
 */
class RecursosHumanosController extends Controller
{

    /**
     * Lists all RecursosHumanos entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:RecursosHumanos')->findAll();

        return $this->render('GEDELTURBundle:RecursosHumanos:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new RecursosHumanos entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new RecursosHumanos();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('recursoshumanos_show', array('id' => $entity->getId())));
        }

        return $this->render('GEDELTURBundle:RecursosHumanos:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a RecursosHumanos entity.
    *
    * @param RecursosHumanos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(RecursosHumanos $entity)
    {
        $form = $this->createForm(new RecursosHumanosType(), $entity, array(
            'action' => $this->generateUrl('recursoshumanos_create'),
            'method' => 'POST',

        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new RecursosHumanos entity.
     *
     */
    public function newAction()
    {
        $entity = new RecursosHumanos();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:RecursosHumanos:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a RecursosHumanos entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:RecursosHumanos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RecursosHumanos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:RecursosHumanos:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing RecursosHumanos entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:RecursosHumanos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RecursosHumanos entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:RecursosHumanos:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a RecursosHumanos entity.
    *
    * @param RecursosHumanos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(RecursosHumanos $entity)
    {
        $form = $this->createForm(new RecursosHumanosType(), $entity, array(
            'action' => $this->generateUrl('recursoshumanos_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing RecursosHumanos entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:RecursosHumanos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RecursosHumanos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('recursoshumanos_edit', array('id' => $id)));
        }

        return $this->render('GEDELTURBundle:RecursosHumanos:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a RecursosHumanos entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:RecursosHumanos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find RecursosHumanos entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('recursoshumanos'));
    }

    /**
     * Creates a form to delete a RecursosHumanos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('recursoshumanos_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}

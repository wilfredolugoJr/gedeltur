<?php

namespace GEDELTUR\Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GEDELTUR\Bundle\Entity\Diplomado;
use GEDELTUR\Bundle\Form\DiplomadoType;

/**
 * Diplomado controller.
 *
 */
class DiplomadoController extends Controller
{

    /**
     * Lists all Diplomado entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GEDELTURBundle:Diplomado')->findAll();

        return $this->render('GEDELTURBundle:Diplomado:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Diplomado entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Diplomado();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            if( $form['id']['inicio']->getData()> $form['id']['fin']->getData())
            {
                $this->get('session')->getFlashBag()->add('msg','Lo siento! Las fechas no son correctas.');
                return $this->render('GEDELTURBundle:Diplomado:new.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
                ));}

            $em = $this->getDoctrine()->getManager();

            $entity->getId()->setTipo("Diplomado");
            $em->persist($entity->getId());
            $em->flush();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Diplomado adicionado correctamente');
            return $this->redirect($this->generateUrl('diplomado'));
        }

        return $this->render('GEDELTURBundle:Diplomado:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Diplomado entity.
    *
    * @param Diplomado $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Diplomado $entity)
    {
        $plantilla='';
        $em = $this->getDoctrine()->getManager();

        $plantillas = $em->getRepository('GEDELTURBundle:Plantilla')->findall();

        $i=0;

        foreach($plantillas as $p)
        {
            if($i+1<count($plantillas))
            {
                $plantilla.=$p->getId()->getId().',';
            }
            else
                $plantilla.=$p->getId()->getId();
            $i++;
        }

        $form = $this->createForm(new DiplomadoType(), $entity, array(
            'action' => $this->generateUrl('diplomado_create'),
            'method' => 'POST',
            'attr'   => array('plantilla'=>$plantilla)
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new Diplomado entity.
     *
     */
    public function newAction()
    {
        $entity = new Diplomado();
        $form   = $this->createCreateForm($entity);

        return $this->render('GEDELTURBundle:Diplomado:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Diplomado entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Diplomado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Diplomado entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Diplomado:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Diplomado entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Diplomado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Diplomado entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GEDELTURBundle:Diplomado:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Diplomado entity.
    *
    * @param Diplomado $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Diplomado $entity)
    {
        $form = $this->createForm(new DiplomadoType(), $entity, array(
            'action' => $this->generateUrl('diplomado_update', array('id' => $entity->getId()->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Diplomado entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GEDELTURBundle:Diplomado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Diplomado entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            if( $editForm['id']['inicio']->getData()> $editForm['id']['fin']->getData())
            {
                $this->get('session')->getFlashBag()->add('msg','Lo siento! Las fechas no son correctas.');

                return $this->render('GEDELTURBundle:Diplomado:edit.html.twig', array(
                    'entity'      => $entity,
                    'edit_form'   => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                ));}

            $em->flush();

            $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Diplomado editado correctamente');
            return $this->redirect($this->generateUrl('diplomado'));
        }

        return $this->render('GEDELTURBundle:Diplomado:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Diplomado entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GEDELTURBundle:Diplomado')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Diplomado entity.');
            }
            $padre =$entity->getId();
            $em->remove($entity);
            $em->flush();
           
            $em->remove($padre);
             $em->flush();
        $this->get('session')->getFlashBag()->add('msg','Bien Hecho! Diplomado eliminado correctamente');
        return $this->redirect($this->generateUrl('diplomado'));
    }

    /**
     * Creates a form to delete a Diplomado entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('diplomado_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}

<?php

namespace GEDELTUR\Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GCEType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('facultad')
            ->add('facultad','choice', array(
                'label' => 'Facultad',
                'choices' => array(
                    '' => 'Seleccione',
                    'Facultad de Ciencias Técnicas' => 'Facultad de Ciencias Técnicas',
                    'Facultad de Ciencias Económicas y Empresariales' => 'Facultad de Ciencias Económicas y Empresariales',
                    'Facultad de Ciencias Forestales y Agropecuarias' => 'Facultad de Ciencias Forestales y Agropecuarias',
                    'Facultad de Ciencias Sociales y Humanísticas' => 'Facultad de Ciencias Sociales y Humanísticas',

                )))
            ->add('anyo')
            ->add('anyo','choice', array(
                'label' => 'Año',
                'choices' => array(
                    '' => 'Seleccione',
                    '1' => '1ro',
                    '2' => '2do',
                    '3' => '3ro',
                    '4' => '4to',
                    '5' => '5to'
                )))
            ->add('carrera','hidden')
            ->add('id', new RecursosHumanosType())
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GEDELTUR\Bundle\Entity\GCE'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gedeltur_bundle_gce';
    }
}

<?php

namespace GEDELTUR\Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PlantillaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cargo')
            ->add('id', new RecursosHumanosType())
           
            
            ->add('cat_doc','choice', array(
                'label' => 'Categoria Docente',
                'choices' => array(
                    '' => 'Seleccione',
                    'instructor' => ' Instructor  ',
                    'auxiliar' => 'Auxiliar',
                    'asistente' => ' Asistente ',
                    'titular' => ' Titular ',
                    
                )))

            ->add('cargo','choice', array(
                'label' => 'Responsabilidad',
                'choices' => array(
                    '' => 'Seleccione',
                    'Director' => ' Director  ',
                    'Profesor' => 'Profesor',
                    'Adiestrado' => 'Adiestrado'
                )))

            ->add('cat_cien','choice', array(
                'label' => 'Grado Científico',
                'choices' => array(

                    '' => 'Seleccione',
                    'ninguna' => ' Ninguna  ',
                    'máster' => ' Máster  ',
                    'doctor' => 'Doctor',
                     
                    
                )))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GEDELTUR\Bundle\Entity\Plantilla'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gedeltur_bundle_plantilla';
    }
}

<?php

namespace GEDELTUR\Bundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;



class ProgramaAcademicoaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('perfilEgresado','textarea')
            ->add('objetivos','textarea')
            ->add('tiempo', null, array('max_length' => 2))
            ->add('requisitos','textarea')
            ->add('tituloEgresado')
            ->add('tarifa', null, array('max_length' => 5))
            ->add('encargado')
            ->add('inicio', null, array('widget' => 'single_text', 'format' => 'dd/MM/yyyy'))


        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GEDELTUR\Bundle\Entity\ProgramaAcademico'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gedeltur_bundle_programaacademico';
    }
}

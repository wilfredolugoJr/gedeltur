<?php

namespace GEDELTUR\Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ServiciosCienticoTecnicosType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('tipo','choice', array(
              'label' => 'Tipo',
               'choices' => array(
                    '' => 'Seleccione',
                    'consultoría' => 'Consultoría',
                   'asesoría' => 'Asesoría',
                   'capacitación' => 'Capacitación',

                )))
          
            ->add('descripcion','textarea')
            ->add('insitucion')
            ->add('rrhh', null, array('empty_value' => 'Seleccione'))
            ->add('tematica')

            ->add('tarifa', null, array('max_length' => 8))
            ->add('contenido','textarea', array('mapped' => false,'required' => false))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GEDELTUR\Bundle\Entity\ServiciosCienticoTecnicos'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gedeltur_bundle_servicioscienticotecnicos';
    }
}

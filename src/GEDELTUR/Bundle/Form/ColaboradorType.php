<?php

namespace GEDELTUR\Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ColaboradorType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        
         ->add('cat_doc','choice', array(
                'label' => 'Categoria Docente',
                'choices' => array(
                    '' => 'Seleccione',
                    'instructor' => 'Instructor',
                    'auxiliar' => 'Auxiliar',
                    
                    'asistente' => ' Asistente ',
                    'titular' => ' Titular '
                )))

            ->add('cat_cien','choice', array(
                'label' => 'Categoria Científica',
                'choices' => array(
                    '' => 'Seleccione',
                    'ninguna' => ' Ninguna  ',
                    'master' => ' Master  ',
                    'doctor' => 'Doctor',
                    
                    
                )))
            ->add('centro')
            ->add('id', new RecursosHumanosType())
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GEDELTUR\Bundle\Entity\Colaborador'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gedeltur_bundle_colaborador';
    }
}

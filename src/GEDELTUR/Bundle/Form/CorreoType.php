<?php

namespace GEDELTUR\Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CorreoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('nombre',null,array('disabled' => true))
            //->add('correo',null,array('disabled' => true))
            ->add('sujeto')
            ->add('mensaje','textarea')

            ->add('tematica','choice', array(
                'label' => 'Temática',
                'choices' => array(
                    'superacion' => ' Superación  ',
                    'servicio' => 'Servicios Científico Técnicos',
                    'investigacion' => 'Investigación ',


                    'otro' => ' Otro '
                )))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GEDELTUR\Bundle\Entity\Correo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gedeltur_bundle_correo';
    }
}

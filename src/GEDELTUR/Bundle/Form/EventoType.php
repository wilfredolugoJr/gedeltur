<?php

namespace GEDELTUR\Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EventoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('image','file',array('data_class' => null,'required'=>false))
            ->add('nombre',null, array('max_length' => 15))
            ->add('fechaResumen', null, array('widget' => 'single_text', 'format' => 'dd/MM/yyyy'))//para campos fecha
            ->add('fechaAceptacion', null, array('widget' => 'single_text', 'format' => 'dd/MM/yyyy'))
            ->add('convocatoria','textarea')
            ->add('cuotaInscripcion',null, array('max_length' => 8))
            ->add('fecha', null, array('widget' => 'single_text', 'format' => 'dd/MM/yyyy'))
            ->add('fin', null, array('widget' => 'single_text', 'format' => 'dd/MM/yyyy'))
            ->add('lugar')
            ->add('norma', null, array('empty_value' => 'Seleccione'))
            ->add('curso',null, array('required' => false))
             ->add('taller', null, array('required' => true))
            ->add('encargado')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GEDELTUR\Bundle\Entity\Evento'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gedeltur_bundle_evento';
    }
}

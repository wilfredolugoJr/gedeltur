<?php

namespace GEDELTUR\Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuariosType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('apellido')
            ->add('usuario')
            ->add('contrasenna','repeated',array(
                'required'=>false,
                'type'=>'password',
                'invalid_message' => 'Las dos contraseñas deben cohincidir',
                'first_options' => array('label' => 'Contraseña'),
                'second_options' => array('label' => 'Repite Contraseña'),
            ))
            
            ->add('role','choice', array(
                'label' => 'Rol',
                'choices' => array(
                    '' => 'Seleccione',
                    'ROLE_USER' => 'Usuario',
                    'ROLE_ADMIN' => 'Administrador'
                )))
            
            ->add('contrasennaantigua', 'password', array('mapped'=>false, 'label'=> 'Contraseña Antigua','required'=>false));
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GEDELTUR\Bundle\Entity\Usuarios'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gedeltur_bundle_usuarios';
    }
}

<?php

namespace GEDELTUR\Bundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;



class ProgramaAcademicoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('perfilEgresado','textarea')
            ->add('objetivos','textarea')

            ->add('requisitos','textarea')
            ->add('tituloEgresado')
            ->add('tarifa', null, array('max_length' => 7))
            ->add('encargado',null,array('empty_value' => 'Seleccione'))
            ->add('inicio', null, array('widget' => 'single_text', 'format' => 'dd/MM/yyyy'))
            ->add('fin', null, array('widget' => 'single_text', 'format' => 'dd/MM/yyyy'))

           /* ->add('encargado', null, array('empty_value' => 'Seleccione', 'attr' => array('class' => 'form-control'),
                'query_builder' => function(EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('e')
                        ->where('e.id in ('.$options['attr']['plantilla'].')');
                }))*/
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GEDELTUR\Bundle\Entity\ProgramaAcademico'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gedeltur_bundle_programaacademico';
    }
}

<?php

namespace GEDELTUR\Bundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * RecursosHumanos
 *
 * @ORM\Table(name="recursos_humanos")
 * @ORM\Entity
 * @UniqueEntity(fields={"ci"}, message="Este Trabajador ya está registrado.")
 */
class RecursosHumanos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=110, nullable=true)
     * @Assert\Image(mimeTypesMessage = "Por favor, suba una imagen.")
     */
    private $image;

    

    /**
     * @var string
     *
     * @ORM\Column(name="ci", type="string", length=11, nullable=false)
     * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
    *@Assert\Regex(pattern="/^[0-9]+$/", match=true, message="El carnet de indentidad solo puede tener números.")
     * @Assert\Length(
     *      min = "11",
     *      max = "11",
     *      exactMessage = "El número de carne de identidad debe ser de {{ limit }} dígitos.",
     *      exactMessage = "El número de carne de identidad debe ser de {{ limit }} dígitos."
     * )
     */
    private $ci;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=100, nullable=false)
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=false)
    * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=100, nullable=false)
     * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     */
    private $apellidos;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TematicaSct", mappedBy="plantilla")
     */
    private $tematica;


    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Proyecto", mappedBy="rrhh")
     */
    private $proyecto;


    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="LineaInvestigacion", mappedBy="rrhh")
     */
    private $linea;


    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Evento", mappedBy="encargado")
     */
    private $evento;



   

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

     public function __toString()
     {
        return $this->nombre.' '.$this->apellidos;
     }

    /**
     * Set image
     *
     * @param string $image
     * @return RecursosHumanos
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set ci
     *
     * @param string $ci
     * @return RecursosHumanos
     */
    public function setCi($ci)
    {
        $this->ci = $ci;

        return $this;
    }

    /**
     * Get ci
     *
     * @return string 
     */
    public function getCi()
    {
        return $this->ci;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return RecursosHumanos
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return RecursosHumanos
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     * @return RecursosHumanos
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string 
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }


    public function subirFoto()
    {
        if (null === $this->image) {
            return;
        }
        $directorioDestino = 'uploads/gedeltur/fotos/';
        $nombreArchivo = uniqid('w').'.jpg';

        $this->getImage()->move($directorioDestino, $nombreArchivo);
        $this->setImage($nombreArchivo);
    }

    public function removeUpload($file)
    {
        unlink($file);
    }




    /**
     * Add tematica
     *
     * @param \GEDELTUR\Bundle\Entity\TematicaSct $tematica
     * @return RecursosHumanos
     */
    public function addTematica(\GEDELTUR\Bundle\Entity\TematicaSct $tematica)
    {
        $this->tematica[] = $tematica;

        return $this;
    }

    /**
     * Remove tematica
     *
     * @param \GEDELTUR\Bundle\Entity\TematicaSct $tematica
     */
    public function removeTematica(\GEDELTUR\Bundle\Entity\TematicaSct $tematica)
    {
        $this->tematica->removeElement($tematica);
    }

    /**
     * Get tematica
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTematica()
    {
        return $this->tematica;
    }

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tematica = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Add proyecto
     *
     * @param \GEDELTUR\Bundle\Entity\Proyecto $proyecto
     * @return RecursosHumanos
     */
    public function addProyecto(\GEDELTUR\Bundle\Entity\Proyecto $proyecto)
    {
        $this->proyecto[] = $proyecto;

        return $this;
    }

    /**
     * Remove proyecto
     *
     * @param \GEDELTUR\Bundle\Entity\Proyecto $proyecto
     */
    public function removeProyecto(\GEDELTUR\Bundle\Entity\Proyecto $proyecto)
    {
        $this->proyecto->removeElement($proyecto);
    }

    /**
     * Get proyecto
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProyecto()
    {
        return $this->proyecto;
    }

    /**
     * Add linea
     *
     * @param \GEDELTUR\Bundle\Entity\LineaInvestigacion $linea
     * @return RecursosHumanos
     */
    public function addLinea(\GEDELTUR\Bundle\Entity\LineaInvestigacion $linea)
    {
        $this->linea[] = $linea;

        return $this;
    }

    /**
     * Remove linea
     *
     * @param \GEDELTUR\Bundle\Entity\LineaInvestigacion $linea
     */
    public function removeLinea(\GEDELTUR\Bundle\Entity\LineaInvestigacion $linea)
    {
        $this->linea->removeElement($linea);
    }

    /**
     * Get linea
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLinea()
    {
        return $this->linea;
    }

    /**
     * Add evento
     *
     * @param \GEDELTUR\Bundle\Entity\Evento $evento
     * @return RecursosHumanos
     */
    public function addEvento(\GEDELTUR\Bundle\Entity\Evento $evento)
    {
        $this->evento[] = $evento;

        return $this;
    }

    /**
     * Remove evento
     *
     * @param \GEDELTUR\Bundle\Entity\Evento $evento
     */
    public function removeEvento(\GEDELTUR\Bundle\Entity\Evento $evento)
    {
        $this->evento->removeElement($evento);
    }

    /**
     * Get evento
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvento()
    {
        return $this->evento;
    }
}

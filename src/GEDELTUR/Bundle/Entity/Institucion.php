<?php

namespace GEDELTUR\Bundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Institucion
 *
 * @ORM\Table(name="institucion")
 * @ORM\Entity
 * @UniqueEntity(fields = {"nombre"}, message="Ya este taller está registrado")
 */
class Institucion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=false)
      * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
    * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="organismo", type="string", length=80, nullable=false)
      * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
    * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $organismo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ServiciosCienticoTecnicos", mappedBy="insitucion")
     */
    private $servicio;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->servicio = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Institucion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    public function __toString()
    {
        return $this->nombre;
    }
    /**
     * Set organismo
     *
     * @param string $organismo
     * @return Institucion
     */
    public function setOrganismo($organismo)
    {
        $this->organismo = $organismo;

        return $this;
    }

    /**
     * Get organismo
     *
     * @return string 
     */
    public function getOrganismo()
    {
        return $this->organismo;
    }

    /**
     * Add servicio
     *
     * @param \GEDELTUR\Bundle\Entity\ServiciosCienticoTecnicos $servicio
     * @return Institucion
     */
    public function addServicio(\GEDELTUR\Bundle\Entity\ServiciosCienticoTecnicos $servicio)
    {
        $this->servicio[] = $servicio;

        return $this;
    }

    /**
     * Remove servicio
     *
     * @param \GEDELTUR\Bundle\Entity\ServiciosCienticoTecnicos $servicio
     */
    public function removeServicio(\GEDELTUR\Bundle\Entity\ServiciosCienticoTecnicos $servicio)
    {
        $this->servicio->removeElement($servicio);
    }

    /**
     * Get servicio
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getServicio()
    {
        return $this->servicio;
    }
}

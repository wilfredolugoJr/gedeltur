<?php

namespace GEDELTUR\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Correo
 *
 * @ORM\Table(name="correo")
 * @ORM\Entity
 *
 */
class Correo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=80, nullable=true)
     *
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="tematica", type="string", length=80, nullable=true)
     *
     */
    private $tematica;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=80, nullable=true)
     *
     */
    private $apellidos;



    /**
     * @var string
     *
     * @ORM\Column(name="nombre_responde", type="string", length=80, nullable=true)
     *
     */
    private $nombreresponde;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos_responde", type="string", length=80, nullable=true)
     *
     */
    private $apellidosresponde;

    /**
     * @var string
     *
     * @ORM\Column(name="correo", type="string", length=80, nullable=true)
     *
     *
     */
    private $correo;

    /**
     * @var string
     *
     * @ORM\Column(name="sujeto", type="string", length=80, nullable=false)
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $sujeto;

     /**
     * @var string
     *
     * @ORM\Column(name="mensaje", type="string", length=80, nullable=false)
      * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $mensaje;

     /**
     * @var string
     *
     * @ORM\Column(name="respuesta", type="string", length=2000, nullable=true)
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     * 
     */
    private $respuesta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=true)
     */
    private $fecha;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Correo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set correo
     *
     * @param string $correo
     * @return Correo
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string 
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Set sujeto
     *
     * @param string $sujeto
     * @return Correo
     */
    public function setSujeto($sujeto)
    {
        $this->sujeto = $sujeto;

        return $this;
    }

    /**
     * Get sujeto
     *
     * @return string 
     */
    public function getSujeto()
    {
        return $this->sujeto;
    }

    /**
     * Set mensaje
     *
     * @param string $mensaje
     * @return Correo
     */
    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;

        return $this;
    }

    /**
     * Get mensaje
     *
     * @return string 
     */
    public function getMensaje()
    {
        return $this->mensaje;
    }

    /**
     * Set respuesta
     *
     * @param string $respuesta
     * @return Correo
     */
    public function setRespuesta($respuesta)
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    /**
     * Get respuesta
     *
     * @return string 
     */
    public function getRespuesta()
    {
        return $this->respuesta;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     * @return Correo
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string 
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

   















    /**
     * Set nombreresponde
     *
     * @param string $nombreresponde
     * @return Correo
     */
    public function setNombreresponde($nombreresponde)
    {
        $this->nombreresponde = $nombreresponde;

        return $this;
    }

    /**
     * Get nombreresponde
     *
     * @return string 
     */
    public function getNombreresponde()
    {
        return $this->nombreresponde;
    }

    /**
     * Set apellidosresponde
     *
     * @param string $apellidosresponde
     * @return Correo
     */
    public function setApellidosresponde($apellidosresponde)
    {
        $this->apellidosresponde = $apellidosresponde;

        return $this;
    }

    /**
     * Get apellidosresponde
     *
     * @return string 
     */
    public function getApellidosresponde()
    {
        return $this->apellidosresponde;
    }

    /**
     * Set tematica
     *
     * @param string $tematica
     * @return Correo
     */
    public function setTematica($tematica)
    {
        $this->tematica = $tematica;

        return $this;
    }

    /**
     * Get tematica
     *
     * @return string 
     */
    public function getTematica()
    {
        return $this->tematica;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Correo
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }
}

<?php

namespace GEDELTUR\Bundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Proyecto
 *
 * @ORM\Table(name="proyecto")
 * @ORM\Entity
 * @UniqueEntity(fields = {"titulo"}, message="Ya existe este Proyecto en la Base de Datos")
 */
class Proyecto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=100, nullable=false)
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="impacto", type="string", length=100, nullable=false)
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $impacto;

    /**
     * @var string
     *
     * @ORM\Column(name="objetivo", type="string", length=100, nullable=false)
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $objetivo;


     /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="RecursosHumanos", inversedBy="proyecto")
     * @ORM\JoinTable(name="plantilla_proyecto",
     *   joinColumns={
     *     @ORM\JoinColumn(name="proyecto", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="rrhh", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $rrhh;



    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="LineaInvestigacion", inversedBy="proyecto")
     * @ORM\JoinTable(name="proyecto_linea",
     *   joinColumns={
     *     @ORM\JoinColumn(name="proyecto", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="lineas", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $lineas;


    /**
     * @var \Plantilla
     *
     * @ORM\ManyToOne(targetEntity="Plantilla")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="plantilla", referencedColumnName="id", onDelete="CASCADE")
     * })
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $plantilla;






    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Proyecto
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

     public function __toString()
    {
        return $this->titulo;
    }

    /**
     * Set impacto
     *
     * @param string $impacto
     * @return Proyecto
     */
    public function setImpacto($impacto)
    {
        $this->impacto = $impacto;

        return $this;
    }

    /**
     * Get impacto
     *
     * @return string 
     */
    public function getImpacto()
    {
        return $this->impacto;
    }

    /**
     * Set objetivo
     *
     * @param string $objetivo
     * @return Proyecto
     */
    public function setObjetivo($objetivo)
    {
        $this->objetivo = $objetivo;

        return $this;
    }

    /**
     * Get objetivo
     *
     * @return string 
     */
    public function getObjetivo()
    {
        return $this->objetivo;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rrhh = new \Doctrine\Common\Collections\ArrayCollection();
        $this->lineas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add rrhh
     *
     * @param \GEDELTUR\Bundle\Entity\RecursosHumanos $rrhh
     * @return Proyecto
     */
    public function addRrhh(\GEDELTUR\Bundle\Entity\RecursosHumanos $rrhh)
    {
        $this->rrhh[] = $rrhh;

        return $this;
    }

    /**
     * Remove rrhh
     *
     * @param \GEDELTUR\Bundle\Entity\RecursosHumanos $rrhh
     */
    public function removeRrhh(\GEDELTUR\Bundle\Entity\RecursosHumanos $rrhh)
    {
        $this->rrhh->removeElement($rrhh);
    }

    /**
     * Get rrhh
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRrhh()
    {
        return $this->rrhh;
    }

    /**
     * Set plantilla
     *
     * @param \GEDELTUR\Bundle\Entity\Plantilla $plantilla
     * @return Proyecto
     */
    public function setPlantilla(\GEDELTUR\Bundle\Entity\Plantilla $plantilla = null)
    {
        $this->plantilla = $plantilla;

        return $this;
    }

    /**
     * Get plantilla
     *
     * @return \GEDELTUR\Bundle\Entity\Plantilla 
     */
    public function getPlantilla()
    {
        return $this->plantilla;
    }

   

    /**
     * Add linea
     *
     * @param \GEDELTUR\Bundle\Entity\LineaInvestigacion $linea
     * @return Proyecto
     */
    public function addLinea(\GEDELTUR\Bundle\Entity\LineaInvestigacion $linea)
    {
        $this->linea[] = $linea;

        return $this;
    }

    /**
     * Remove linea
     *
     * @param \GEDELTUR\Bundle\Entity\LineaInvestigacion $linea
     */
    public function removeLinea(\GEDELTUR\Bundle\Entity\LineaInvestigacion $linea)
    {
        $this->linea->removeElement($linea);
    }

    /**
     * Get linea
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLinea()
    {
        return $this->linea;
    }

    /**
     * Get lineas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLineas()
    {
        return $this->lineas;
    }
}

<?php

namespace GEDELTUR\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Tematica
 *
 * @ORM\Table(name="tematica")
 * @ORM\Entity
 */
class Tematica
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=150, nullable=false)
     * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descrpcion", type="text", nullable=true)
     * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     */
    private $descrpcion;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Taller", mappedBy="tematica")
     * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
     */


    private $taller;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->taller = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Tematica
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    /**
     * Set descrpcion
     *
     * @param string $descrpcion
     * @return Tematica
     */
    public function setDescrpcion($descrpcion)
    {
        $this->descrpcion = $descrpcion;

        return $this;
    }

    /**
     * Get descrpcion
     *
     * @return string 
     */
    public function getDescrpcion()
    {
        return $this->descrpcion;
    }

    /**
     * Add taller
     *
     * @param \GEDELTUR\Bundle\Entity\Taller $taller
     * @return Tematica
     */
    public function addTaller(\GEDELTUR\Bundle\Entity\Taller $taller)
    {
        $this->taller[] = $taller;

        return $this;
    }

    /**
     * Remove taller
     *
     * @param \GEDELTUR\Bundle\Entity\Taller $taller
     */
    public function removeTaller(\GEDELTUR\Bundle\Entity\Taller $taller)
    {
        $this->taller->removeElement($taller);
    }

    /**
     * Get taller
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTaller()
    {
        return $this->taller;
    }
}

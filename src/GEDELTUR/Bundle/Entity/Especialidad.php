<?php


namespace GEDELTUR\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Especialidad
 *
 * @ORM\Table(name="especialidad")
 * @ORM\Entity
 */
class Especialidad {

    /**
     * @var \ProgramaAcademico
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="ProgramaAcademico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id", referencedColumnName="id", onDelete="CASCADE")
     * })
     * @Assert\Valid()
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Curso", inversedBy="especialidad")
     * @ORM\JoinTable(name="especialidades_curso",
     *   joinColumns={
     *     @ORM\JoinColumn(name="especialidad", referencedColumnName="id",onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="curso", referencedColumnName="id",onDelete="CASCADE")
     *   }
     * )
     */
    private $curso;









    /**
     * Constructor
     */
    public function __construct()
    {
        $this->curso = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set id
     *
     * @param \GEDELTUR\Bundle\Entity\ProgramaAcademico $id
     * @return Especialidad
     */
    public function setId(\GEDELTUR\Bundle\Entity\ProgramaAcademico $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return \GEDELTUR\Bundle\Entity\ProgramaAcademico 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add curso
     *
     * @param \GEDELTUR\Bundle\Entity\Curso $curso
     * @return Especialidad
     */
    public function addCurso(\GEDELTUR\Bundle\Entity\Curso $curso)
    {
        $this->curso[] = $curso;

        return $this;
    }

    /**
     * Remove curso
     *
     * @param \GEDELTUR\Bundle\Entity\Curso $curso
     */
    public function removeCurso(\GEDELTUR\Bundle\Entity\Curso $curso)
    {
        $this->curso->removeElement($curso);
    }

    /**
     * Get curso
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCurso()
    {
        return $this->curso;
    }
}

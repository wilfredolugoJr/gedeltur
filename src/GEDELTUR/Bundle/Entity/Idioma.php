<?php
namespace GEDELTUR\Bundle\Entity;


use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Idioma
 * @ORM\Table(name="idioma")
 * @ORM\Entity
 *  @UniqueEntity(fields = {"nombre"}, message="Por favor , ya ese idioma se encuentra en la base de datos")
 */

class Idioma {


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=false)
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $nombre;


    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Norma", mappedBy="idioma")
     */
    private $norma;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Idioma
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->norma = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add norma
     *
     * @param \GEDELTUR\Bundle\Entity\Norma $norma
     * @return Idioma
     */
    public function addNorma(\GEDELTUR\Bundle\Entity\Norma $norma)
    {
        $this->norma[] = $norma;

        return $this;
    }

    /**
     * Remove norma
     *
     * @param \GEDELTUR\Bundle\Entity\Norma $norma
     */
    public function removeNorma(\GEDELTUR\Bundle\Entity\Norma $norma)
    {
        $this->norma->removeElement($norma);
    }

    /**
     * Get norma
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNorma()
    {
        return $this->norma;
    }
}

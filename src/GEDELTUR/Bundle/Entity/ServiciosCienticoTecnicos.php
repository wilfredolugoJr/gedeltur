<?php

namespace GEDELTUR\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ServiciosCienticoTecnicos
 *
 * @ORM\Table(name="servicios_cientico_tecnicos")
 * @ORM\Entity
 * @UniqueEntity(fields={"nombre"}, message="Este Servicio Científico Técnico ya está registrado.")
 */
class ServiciosCienticoTecnicos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=false)
     * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     */
    private $nombre;

    /**
     * @var float
     *
     * @ORM\Column(name="tarifa", type="float", precision=18, scale=2, nullable=false)
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
      * @Assert\Type(type="float", message=" El campo debe tener valor de tipo número flotante.")
      * @Assert\Length(
     *     
     *      max = "8",
     *       
     *       exactMessage = "La Tarifa no debe sobrepasar los {{ limit }} dígitos."
     * )
     */
    private $tarifa;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", nullable=false)
      * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="text", nullable=true)
     */
    private $tipo;

    /**
     * @var \Plantilla
     *
     * @ORM\ManyToOne(targetEntity="Plantilla")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="encargado", referencedColumnName="id", onDelete="CASCADE")
     * })
      * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $rrhh;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Institucion", inversedBy="servicio")
     * @ORM\JoinTable(name="insitucion_servicios_cientico_tecnicos",
     *   joinColumns={
     *     @ORM\JoinColumn(name="servicio", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="insitucion", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
     */
    private $insitucion;

     /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TematicaSct", inversedBy="servicio")
     * @ORM\JoinTable(name="servicio_tematica",
     *   joinColumns={
     *     @ORM\JoinColumn(name="servicio", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="tematica", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    public $tematica;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->insitucion = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tematica = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return ServiciosCienticoTecnicos
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set tarifa
     *
     * @param float $tarifa
     * @return ServiciosCienticoTecnicos
     */
    public function setTarifa($tarifa)
    {
        $this->tarifa = $tarifa;

        return $this;
    }

    /**
     * Get tarifa
     *
     * @return float 
     */
    public function getTarifa()
    {
        return $this->tarifa;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return ServiciosCienticoTecnicos
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Add insitucion
     *
     * @param \GEDELTUR\Bundle\Entity\Institucion $insitucion
     * @return ServiciosCienticoTecnicos
     */
    public function addInsitucion(\GEDELTUR\Bundle\Entity\Institucion $insitucion)
    {
        $this->insitucion[] = $insitucion;

        return $this;
    }

    /**
     * Remove insitucion
     *
     * @param \GEDELTUR\Bundle\Entity\Institucion $insitucion
     */
    public function removeInsitucion(\GEDELTUR\Bundle\Entity\Institucion $insitucion)
    {
        $this->insitucion->removeElement($insitucion);
    }

    /**
     * Get insitucion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInsitucion()
    {
        return $this->insitucion;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return ServiciosCienticoTecnicos
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    public function __toString()
    {
        return $this->nombre;
    }

   

    /**
     * Add tematica
     *
     * @param \GEDELTUR\Bundle\Entity\TematicaSct $tematica
     * @return ServiciosCienticoTecnicos
     */
    public function addTematica(\GEDELTUR\Bundle\Entity\TematicaSct $tematica)
    {
        $this->tematica[] = $tematica;

        return $this;
    }

    /**
     * Remove tematica
     *
     * @param \GEDELTUR\Bundle\Entity\TematicaSct $tematica
     */
    public function removeTematica(\GEDELTUR\Bundle\Entity\TematicaSct $tematica)
    {
        $this->tematica->removeElement($tematica);
    }

    /**
     * Get tematica
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTematica()
    {
        return $this->tematica;
    }

    /**
     * Set rrhh
     *
     * @param \GEDELTUR\Bundle\Entity\Plantilla $rrhh
     * @return ServiciosCienticoTecnicos
     */
    public function setRrhh(\GEDELTUR\Bundle\Entity\Plantilla $rrhh = null)
    {
        $this->rrhh = $rrhh;

        return $this;
    }

    /**
     * Get rrhh
     *
     * @return \GEDELTUR\Bundle\Entity\Plantilla
     */
    public function getRrhh()
    {
        return $this->rrhh;
    }
}

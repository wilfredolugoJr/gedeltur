<?php

namespace GEDELTUR\Bundle\Entity;


use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * ProgramaAcademico
 *
 * @ORM\Table(name="programa_academico")
 * @ORM\Entity
 * @UniqueEntity(fields = {"titulo"}, message="Ya este Programa Académico se encuentra registrado")
 */
class ProgramaAcademico
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=100, nullable=false)
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="perfil_egresado", type="string", nullable=true)
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
      */
    private $perfilEgresado;

    /**
     * @var string
     *
     * @ORM\Column(name="objetivos", type="string", nullable=true)
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
      */
    private $objetivos;



    /**
     * @var string
     *
     * @ORM\Column(name="requisitos", type="text", nullable=false)
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     */
    private $requisitos;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_egresado", type="string", length=100, nullable=false)
    * @Assert\NotNull(message = "El campo no debe estar en blanco.")
    * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     */
    private $tituloEgresado;

    /**
     * @var float
     *
     * @ORM\Column(name="tarifa", type="float", precision=8, scale=2, nullable=true)
        * @Assert\Type(type="float", message=" El campo debe tener valor de tipo número flotante.")
     */
    private $tarifa;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_inicio", type="date", nullable=false)
      * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
     */
    private $inicio;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_fin", type="date", nullable=false)
     * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
     */
    private $fin;



    /**
     * @var \Plantilla
     *
     * @ORM\ManyToOne(targetEntity="Plantilla")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="encargado", referencedColumnName="id", onDelete="CASCADE")
     * })
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $encargado;

      /**
     * @var string
     *
     * @ORM\Column(name="TIPO", type="string", length=15, nullable=true)
    */
    private $tipo;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return ProgramaAcademico
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }


    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

     public function __toString()
    {
        return $this->titulo;
    }

    /**
     * Set perfilEgresado
     *
     * @param string $perfilEgresado
     * @return ProgramaAcademico
     */
    public function setPerfilEgresado($perfilEgresado)
    {
        $this->perfilEgresado = $perfilEgresado;

        return $this;
    }

    /**
     * Get perfilEgresado
     *
     * @return string 
     */
    public function getPerfilEgresado()
    {
        return $this->perfilEgresado;
    }

    /**
     * Set objetivos
     *
     * @param string $objetivos
     * @return ProgramaAcademico
     */
    public function setObjetivos($objetivos)
    {
        $this->objetivos = $objetivos;

        return $this;
    }

    /**
     * Get objetivos
     *
     * @return string 
     */
    public function getObjetivos()
    {
        return $this->objetivos;
    }

    /**
     * Set tiempo
     *
     * @param string $tiempo
     * @return ProgramaAcademico
     */
    public function setTiempo($tiempo)
    {
        $this->tiempo = $tiempo;

        return $this;
    }

    /**
     * Get tiempo
     *
     * @return string 
     */
    public function getTiempo()
    {
        return $this->tiempo;
    }

    /**
     * Set requisitos
     *
     * @param string $requisitos
     * @return ProgramaAcademico
     */
    public function setRequisitos($requisitos)
    {
        $this->requisitos = $requisitos;

        return $this;
    }

    /**
     * Get requisitos
     *
     * @return string 
     */
    public function getRequisitos()
    {
        return $this->requisitos;
    }

    /**
     * Set tituloEgresado
     *
     * @param string $tituloEgresado
     * @return ProgramaAcademico
     */
    public function setTituloEgresado($tituloEgresado)
    {
        $this->tituloEgresado = $tituloEgresado;

        return $this;
    }

    /**
     * Get tituloEgresado
     *
     * @return string 
     */
    public function getTituloEgresado()
    {
        return $this->tituloEgresado;
    }

    /**
     * Set tarifa
     *
     * @param float $tarifa
     * @return ProgramaAcademico
     */
    public function setTarifa($tarifa)
    {
        $this->tarifa = $tarifa;

        return $this;
    }

    /**
     * Get tarifa
     *
     * @return float 
     */
    public function getTarifa()
    {
        return $this->tarifa;
    }

    /**
     * Set encargado
     *
     * @param \GEDELTUR\Bundle\Entity\Plantilla $encargado
     * @return ProgramaAcademico
     */
    public function setEncargado(\GEDELTUR\Bundle\Entity\Plantilla $encargado = null)
    {
        $this->encargado = $encargado;

        return $this;
    }

    /**
     * Get encargado
     *
     * @return \GEDELTUR\Bundle\Entity\Plantilla
     */
    public function getEncargado()
    {
        return $this->encargado;
    }

    /**
     * Set inicio
     *
     * @param \DateTime $inicio
     * @return ProgramaAcademico
     */
    public function setInicio($inicio)
    {
        $this->inicio = $inicio;

        return $this;
    }

    /**
     * Get inicio
     *
     * @return \DateTime 
     */
    public function getInicio()
    {
        return $this->inicio;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return ProgramaAcademico
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set fin
     *
     * @param \DateTime $fin
     * @return ProgramaAcademico
     */
    public function setFin($fin)
    {
        $this->fin = $fin;

        return $this;
    }

    /**
     * Get fin
     *
     * @return \DateTime 
     */
    public function getFin()
    {
        return $this->fin;
    }
}

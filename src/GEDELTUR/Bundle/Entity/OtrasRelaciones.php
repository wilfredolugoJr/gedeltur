<?php

namespace GEDELTUR\Bundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OtrasRelaciones
 *
 * @ORM\Table(name="otras_relaciones", indexes={@ORM\Index(name="RefInstitucion26", columns={"institucion"})})
 * @ORM\Entity
 */
class OtrasRelaciones
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="concepto", type="string", length=200, nullable=false)
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
      * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     */
    private $concepto;

    /**
     * @var \Institucion
     *
     * @ORM\ManyToOne(targetEntity="Institucion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="institucion", referencedColumnName="id", onDelete="CASCADE")
     * })
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $institucion;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set concepto
     *
     * @param string $concepto
     * @return OtrasRelaciones
     */
    public function setConcepto($concepto)
    {
        $this->concepto = $concepto;

        return $this;
    }

    /**
     * Get concepto
     *
     * @return string 
     */
    public function getConcepto()
    {
        return $this->concepto;
    }

    /**
     * Set institucion
     *
     * @param \GEDELTUR\Bundle\Entity\Institucion $institucion
     * @return OtrasRelaciones
     */
    public function setInstitucion(\GEDELTUR\Bundle\Entity\Institucion $institucion = null)
    {
        $this->institucion = $institucion;

        return $this;
    }

    /**
     * Get institucion
     *
     * @return \GEDELTUR\Bundle\Entity\Institucion 
     */
    public function getInstitucion()
    {
        return $this->institucion;
    }
}

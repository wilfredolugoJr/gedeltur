<?php

namespace GEDELTUR\Bundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Norma
 *
 * @ORM\Table(name="norma")
 * @ORM\Entity
 * @UniqueEntity(fields = {"nombre"}, message="Ya esta norma está registrada")
 */
class Norma
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="estructura_trabajo", type="text", nullable=false)
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
      * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     */
    private $estructuraTrabajo;

    /**
     * @var string
     *
     * @ORM\Column(name="pautas", type="text", nullable=false)
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
    */
    private $pautas;


    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Idioma", inversedBy="norma")
     * @ORM\JoinTable(name="norma_idioma",
     *   joinColumns={
     *     @ORM\JoinColumn(name="norma", referencedColumnName="id",onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="idioma", referencedColumnName="id",onDelete="CASCADE")
     *   }
     * )
     */
    private $idioma;



/**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=30, nullable=false)
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
       * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
         */
    private $nombre;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    /**
     * Set estructuraTrabajo
     *
     * @param string $estructuraTrabajo
     * @return Norma
     */
    public function setEstructuraTrabajo($estructuraTrabajo)
    {
        $this->estructuraTrabajo = $estructuraTrabajo;

        return $this;
    }

    /**
     * Get estructuraTrabajo
     *
     * @return string 
     */
    public function getEstructuraTrabajo()
    {
        return $this->estructuraTrabajo;
    }

    /**
     * Set pautas
     *
     * @param string $pautas
     * @return Norma
     */
    public function setPautas($pautas)
    {
        $this->pautas = $pautas;

        return $this;
    }

    /**
     * Get pautas
     *
     * @return string 
     */
    public function getPautas()
    {
        return $this->pautas;
    }



    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Norma
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idioma = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add idioma
     *
     * @param \GEDELTUR\Bundle\Entity\Idioma $idioma
     * @return Norma
     */
    public function addIdioma(\GEDELTUR\Bundle\Entity\Idioma $idioma)
    {
        $this->idioma[] = $idioma;

        return $this;
    }

    /**
     * Remove idioma
     *
     * @param \GEDELTUR\Bundle\Entity\Idioma $idioma
     */
    public function removeIdioma(\GEDELTUR\Bundle\Entity\Idioma $idioma)
    {
        $this->idioma->removeElement($idioma);
    }

    /**
     * Get idioma
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdioma()
    {
        return $this->idioma;
    }
}

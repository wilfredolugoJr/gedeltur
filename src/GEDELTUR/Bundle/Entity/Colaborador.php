<?php

namespace GEDELTUR\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Colaborador
 *
 * @ORM\Table(name="colaborador")
 * @ORM\Entity
 */
class Colaborador
{
    /**
     * @var string
     *
     * @ORM\Column(name="Centro", type="string", length=100, nullable=true)
    * @Assert\NotBlank(message = "El campo no debe estar en blanco.")
    * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     */
    private $centro;

/**
     * @var string
     *
     * @ORM\Column(name="cat_doc", type="string", length=10, nullable=false)
     */
    private $catDoc;

    /**
     * @var string
     *
     * @ORM\Column(name="cat_cien", type="string", length=10, nullable=false)
     */
    private $catCien;

    /**
     * @var \RecursosHumanos
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="RecursosHumanos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id", referencedColumnName="id", onDelete="CASCADE")
     * })
     * @Assert\Valid()
     */
    private $id;



    /**
     * Set centro
     *
     * @param string $centro
     * @return Colaborador
     */
    public function setCentro($centro)
    {
        $this->centro = $centro;

        return $this;
    }

    /**
     * Get centro
     *
     * @return string 
     */
    public function getCentro()
    {
        return $this->centro;
    }

    /**
     * Set id
     *
     * @param \GEDELTUR\Bundle\Entity\RecursosHumanos $id
     * @return Colaborador
     */
    public function setId(\GEDELTUR\Bundle\Entity\RecursosHumanos $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return \GEDELTUR\Bundle\Entity\RecursosHumanos 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set catDoc
     *
     * @param string $catDoc
     * @return Colaborador
     */
    public function setCatDoc($catDoc)
    {
        $this->catDoc = $catDoc;

        return $this;
    }

    /**
     * Get catDoc
     *
     * @return string 
     */
    public function getCatDoc()
    {
        return $this->catDoc;
    }

    /**
     * Set catCien
     *
     * @param string $catCien
     * @return Colaborador
     */
    public function setCatCien($catCien)
    {
        $this->catCien = $catCien;

        return $this;
    }

    /**
     * Get catCien
     *
     * @return string 
     */
    public function getCatCien()
    {
        return $this->catCien;
    }
}

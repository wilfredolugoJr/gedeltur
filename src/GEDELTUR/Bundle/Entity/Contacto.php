<?php

namespace GEDELTUR\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Contacto
 *
 * @ORM\Entity
 * @UniqueEntity(fields = {"rrhh"}, message="Ya el contacto de esta persona está almacenado.")
 * @UniqueEntity(fields = {"facebook"}, message="No debe tener la misma dirección de facebook.")
 * @UniqueEntity(fields = {"twiter"}, message="No debe tener la misma dirección de twiter.")
 * @UniqueEntity(fields = {"correo"}, message="No debe tener la misma dirección correo.")
 * @UniqueEntity(fields = {"fijo"}, message="Ya existe un contacto con este número.")
 * @UniqueEntity(fields = {"movil"}, message="Ya existe un contacto con este número.")
 */
class Contacto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;




    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=100, nullable=true)
     * @Assert\Email(message = "Debe ser una direcciòn de correo.")
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="twiter", type="string", length=100, nullable=true)
     * @Assert\Email(message = "Debe ser una direcciòn de correo.")
     */
    private $twiter;

    /**
     * @var string
     *
     * @ORM\Column(name="correo", type="string", length=100, nullable=false)
     * @Assert\Email(message = "Debe ser una direcciòn de correo.")
     */
    private $correo;





    /**
     * @var float
     *
     * @ORM\Column(name="movil", type="float", length=8, nullable=true)
     * @Assert\Length(
     *      min = "8",
     *      max = "8",
     *      exactMessage = "Los números telefónicos  deben ser de 8 dígitos.",
     *      exactMessage = "Los números telefónicos  deben ser de 8 dígitos."
     * )
     */
    private $movil;

    /**
     * @var float
     *
     * @ORM\Column(name="fijo", type="float", length=8, nullable=false)
     * @Assert\Length(
     *      min = "8",
     *      max = "8",
     *      exactMessage = "Los números telefónicos  deben ser de 8 dígitos.",
     *      exactMessage = "Los números telefónicos  deben ser de 8 dígitos."
     * )
     */

    private $fijo;

   



    /**
     * @var \RecursosHumanos
     *
     * @ORM\OneToOne(targetEntity="RecursosHumanos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rrhh", referencedColumnName="id",onDelete="CASCADE")
     * })
     * @Assert\NotBlank(message = "Por favor, elije el trabajador al cual pertenece este contacto.")
     */
    private $rrhh;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

   


    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Contacto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }



    /**
     * Set facebook
     *
     * @param string $facebook
     * @return Contacto
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string 
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set twiter
     *
     * @param string $twiter
     * @return Contacto
     */
    public function setTwiter($twiter)
    {
        $this->twiter = $twiter;

        return $this;
    }

    /**
     * Get twiter
     *
     * @return string 
     */
    public function getTwiter()
    {
        return $this->twiter;
    }

    /**
     * Set correo
     *
     * @param string $correo
     * @return Contacto
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string 
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Set fijo
     *
     * @param integer $fijo
     * @return Contacto
     */
    public function setFijo($fijo)
    {
        $this->fijo = $fijo;

        return $this;
    }

    /**
     * Get fijo
     *
     * @return integer 
     */
    public function getFijo()
    {
        return $this->fijo;
    }

    /**
     * Set movil
     *
     * @param integer $movil
     * @return Contacto
     */
    public function setMovil($movil)
    {
        $this->movil = $movil;

        return $this;
    }

    /**
     * Get movil
     *
     * @return integer 
     */
    public function getMovil()
    {
        return $this->movil;
    }

    /**
     * Set rrhh
     *
     * @param \GEDELTUR\Bundle\Entity\RecursosHumanos $rrhh
     * @return Contacto
     */
    public function setRrhh(\GEDELTUR\Bundle\Entity\RecursosHumanos $rrhh = null)
    {
        $this->rrhh = $rrhh;

        return $this;
    }

    /**
     * Get rrhh
     *
     * @return \GEDELTUR\Bundle\Entity\RecursosHumanos 
     */
    public function getRrhh()
    {
        return $this->rrhh;
    }
}

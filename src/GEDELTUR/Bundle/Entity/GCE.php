<?php

namespace GEDELTUR\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * GCE
 *
 * @ORM\Table(name="g_c_e")
 * @ORM\Entity
 */
class GCE
{
    /**
     * @var string
     *
     * @ORM\Column(name="facultad", type="string", length=100, nullable=false)
     */
    private $facultad;

    /**
     * @var integer
     *
     * @ORM\Column(name="anyo", type="integer", nullable=false)
     */
    private $anyo;

    /**
     * @var string
     *
     * @ORM\Column(name="carrera", type="string", length=100, nullable=false)
     */
    private $carrera;

    /**
     * @var \RecursosHumanos
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="RecursosHumanos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id", referencedColumnName="id", onDelete="CASCADE")
     * })
     * @Assert\Valid()
     */
    private $id;



    /**
     * Set facultad
     *
     * @param string $facultad
     * @return GCE
     */
    public function setFacultad($facultad)
    {
        $this->facultad = $facultad;

        return $this;
    }

    /**
     * Get facultad
     *
     * @return string 
     */
    public function getFacultad()
    {
        return $this->facultad;
    }

    /**
     * Set anyo
     *
     * @param integer $anyo
     * @return GCE
     */
    public function setAnyo($anyo)
    {
        $this->anyo = $anyo;

        return $this;
    }

    /**
     * Get anyo
     *
     * @return integer 
     */
    public function getAnyo()
    {
        return $this->anyo;
    }

    /**
     * Set carrera
     *
     * @param string $carrera
     * @return GCE
     */
    public function setCarrera($carrera)
    {
        $this->carrera = $carrera;

        return $this;
    }

    /**
     * Get carrera
     *
     * @return string 
     */
    public function getCarrera()
    {
        return $this->carrera;
    }

    /**
     * Set id
     *
     * @param \GEDELTUR\Bundle\Entity\RecursosHumanos $id
     * @return GCE
     */
    public function setId(\GEDELTUR\Bundle\Entity\RecursosHumanos $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return \GEDELTUR\Bundle\Entity\RecursosHumanos 
     */
    public function getId()
    {
        return $this->id;
    }
}

<?php

namespace GEDELTUR\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * TematicaSct
 *
 * @ORM\Table(name="tematica_sct")
 *
 * @ORM\Entity
  * @UniqueEntity(fields = {"nombre"}, message="El valor del campo nombre ya exsiste en la base de datos")
 */
class TematicaSct
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=false)
     * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     */
    private $nombre;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ServiciosCienticoTecnicos", mappedBy="tematica")
     */
    private $servicio;

    /**
     * @var string
     *
     * @ORM\Column(name="descrpcion", type="text", nullable=true)
     * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     */
    private $descrpcion;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Plantilla", inversedBy="tematica")
     * @ORM\JoinTable(name="plantilla_tematica_sctnew",
     *   joinColumns={
     *     @ORM\JoinColumn(name="tematica", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="plantilla", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $plantilla;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->plantilla = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    public function __toString()
    {
        return $this->nombre;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return TematicaSct
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set servicio
     *
     * @param \GEDELTUR\Bundle\Entity\ServiciosCienticoTecnicos $servicio
     * @return TematicaSct
     */
   

    /**
     * Add plantilla
     *
     * @param \GEDELTUR\Bundle\Entity\Plantilla $plantilla
     * @return TematicaSct
     */
    public function addPlantilla(\GEDELTUR\Bundle\Entity\Plantilla $plantilla)
    {
        $this->plantilla[] = $plantilla;

        return $this;
    }

    /**
     * Remove plantilla
     *
     * @param \GEDELTUR\Bundle\Entity\Plantilla $plantilla
     */
    public function removePlantilla(\GEDELTUR\Bundle\Entity\Plantilla $plantilla)
    {
        $this->plantilla->removeElement($plantilla);
    }

    /**
     * Get plantilla
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlantilla()
    {
        return $this->plantilla;
    }

    /**
     * Add servicio
     *
     * @param \GEDELTUR\Bundle\Entity\ServiciosCienticoTecnicos $servicio
     * @return TematicaSct
     */
    public function addServicio(\GEDELTUR\Bundle\Entity\ServiciosCienticoTecnicos $servicio)
    {
        $this->servicio[] = $servicio;

        return $this;
    }

    /**
     * Remove servicio
     *
     * @param \GEDELTUR\Bundle\Entity\ServiciosCienticoTecnicos $servicio
     */
    public function removeServicio(\GEDELTUR\Bundle\Entity\ServiciosCienticoTecnicos $servicio)
    {
        $this->servicio->removeElement($servicio);
    }

    /**
     * Get servicio
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getServicio()
    {
        return $this->servicio;
    }

    /**
     * Set descrpcion
     *
     * @param string $descrpcion
     * @return TematicaSct
     */
    public function setDescrpcion($descrpcion)
    {
        $this->descrpcion = $descrpcion;

        return $this;
    }

    /**
     * Get descrpcion
     *
     * @return string 
     */
    public function getDescrpcion()
    {
        return $this->descrpcion;
    }

    /**
     * Set plantilla
     *
     * @param \GEDELTUR\Bundle\Entity\Plantilla $plantilla
     * @return Evento
     */
    public function setEncargado(\GEDELTUR\Bundle\Entity\Plantilla $plantilla = null)
    {
        $this->plantilla = $plantilla;

        return $this;
    }
}

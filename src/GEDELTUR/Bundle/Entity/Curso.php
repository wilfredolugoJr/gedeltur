<?php

namespace GEDELTUR\Bundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Curso
 *
 * @ORM\Table(name="curso")
 * @ORM\Entity
 *  @UniqueEntity(fields = {"nombre"}, message="Por favor , ya se ingresaron los datos de este curso")
 */
class Curso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=false)
      * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $nombre;

    /**
     * @var \Plantilla
     *
     * @ORM\ManyToOne(targetEntity="Plantilla")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="plantilla", referencedColumnName="id", onDelete="CASCADE")
     * })
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $plantilla;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
      * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
      * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="objetivos", type="text", nullable=true)
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $objetivos;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Evento", mappedBy="curso")
     */
    private $evento;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Diplomado", mappedBy="curso")
     */
    private $diplomado;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Especialidad", mappedBy="curso")
     */
    private $especialidad;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->diplomado = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Curso
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    public function __toString()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Curso
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }



    /**
     * Add diplomado
     *
     * @param \GEDELTUR\Bundle\Entity\Diplomado $diplomado
     * @return Curso
     */
    public function addDiplomado(\GEDELTUR\Bundle\Entity\Diplomado $diplomado)
    {
        $this->diplomado[] = $diplomado;

        return $this;
    }

    /**
     * Remove diplomado
     *
     * @param \GEDELTUR\Bundle\Entity\Diplomado $diplomado
     */
    public function removeDiplomado(\GEDELTUR\Bundle\Entity\Diplomado $diplomado)
    {
        $this->diplomado->removeElement($diplomado);
    }

    /**
     * Get diplomado
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDiplomado()
    {
        return $this->diplomado;
    }

    /**
     * Add evento
     *
     * @param \GEDELTUR\Bundle\Entity\Evento $evento
     * @return Curso
     */
    public function addEvento(\GEDELTUR\Bundle\Entity\Evento $evento)
    {
        $this->evento[] = $evento;

        return $this;
    }

    /**
     * Remove evento
     *
     * @param \GEDELTUR\Bundle\Entity\Evento $evento
     */
    public function removeEvento(\GEDELTUR\Bundle\Entity\Evento $evento)
    {
        $this->evento->removeElement($evento);
    }

    /**
     * Get evento
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvento()
    {
        return $this->evento;
    }

    /**
     * Set objetivos
     *
     * @param string $objetivos
     * @return Curso
     */
    public function setObjetivos($objetivos)
    {
        $this->objetivos = $objetivos;

        return $this;
    }

    /**
     * Get objetivos
     *
     * @return string 
     */
    public function getObjetivos()
    {
        return $this->objetivos;
    }

    /**
     * Set plantilla
     *
     * @param \GEDELTUR\Bundle\Entity\Plantilla $plantilla
     * @return Curso
     */
    public function setPlantilla(\GEDELTUR\Bundle\Entity\Plantilla $plantilla = null)
    {
        $this->plantilla = $plantilla;

        return $this;
    }

    /**
     * Get plantilla
     *
     * @return \GEDELTUR\Bundle\Entity\Plantilla 
     */
    public function getPlantilla()
    {
        return $this->plantilla;
    }

    /**
     * Add especialidad
     *
     * @param \GEDELTUR\Bundle\Entity\Especialidad $especialidad
     * @return Curso
     */
    public function addEspecialidad(\GEDELTUR\Bundle\Entity\Especialidad $especialidad)
    {
        $this->especialidad[] = $especialidad;

        return $this;
    }

    /**
     * Remove especialidad
     *
     * @param \GEDELTUR\Bundle\Entity\Especialidad $especialidad
     */
    public function removeEspecialidad(\GEDELTUR\Bundle\Entity\Especialidad $especialidad)
    {
        $this->especialidad->removeElement($especialidad);
    }

    /**
     * Get especialidad
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEspecialidad()
    {
        return $this->especialidad;
    }
}

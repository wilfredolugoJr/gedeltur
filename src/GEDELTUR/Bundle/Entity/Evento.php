<?php

namespace GEDELTUR\Bundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Evento
 *
 * @ORM\Table(name="evento", indexes={@ORM\Index(name="RefNorma1", columns={"norma"})})
 * @ORM\Entity
 * * @UniqueEntity(fields={"nombre"}, message="Este Evento ya está registrado.")
 */
class Evento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=15, nullable=false)
     * @Assert\NotBlank(message = "El campo no debe estar en blanco.")
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     * @Assert\Length(
     *
     *      max = "15",
     *      exactMessage = "El  nombre del evento debe tener como máximo 15 caracteres."
     *
     * )
     */
    private $nombre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_resumen", type="date", nullable=false)
      * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
     */
    private $fechaResumen;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_aceptacion", type="date", length=10, nullable=false)
      * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
     */
    private $fechaAceptacion;

    /**
     * @var string
     *
     * @ORM\Column(name="convocatoria", type="string", nullable=false)
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
       * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     */
    private $convocatoria;

    /**
     * @var float
     *
     * @ORM\Column(name="cuota_inscripcion", type="float", precision=18, scale=2, nullable=false)
     * @Assert\NotBlank(message = "El campo no debe estar en blanco.")
     * @Assert\Type(type="float", message="El campo debe tener valor de tipo número flotante.")
     */
    private $cuotaInscripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $fecha;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fin", type="date", nullable=false)
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $fin;

    /**
     * @var string
     *
     * @ORM\Column(name="lugar", type="string", length=100, nullable=false)
      * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $lugar;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=110, nullable=true)
     * @Assert\Image(mimeTypesMessage = "Por favor, suba una imagen.")
     */
    private $image;

    /**
     * @var \Norma
     *
     * @ORM\ManyToOne(targetEntity="Norma")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="norma", referencedColumnName="id",onDelete="CASCADE")
     * })
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     */

    private $norma;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Plantilla", inversedBy="evento")
     * @ORM\JoinTable(name="organizadores_eventos",
     *   joinColumns={
     *     @ORM\JoinColumn(name="evento", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="rrhh", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $encargado;


     /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Taller", inversedBy="evento")
     * @ORM\JoinTable(name="taller_evento",
     *   joinColumns={
     *     @ORM\JoinColumn(name="evento", referencedColumnName="id",onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="taller", referencedColumnName="id",onDelete="CASCADE")
     *   }
     * )
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $taller;


    /**
 * @var \Doctrine\Common\Collections\Collection
 *
 * @ORM\ManyToMany(targetEntity="Curso", inversedBy="evento")
 * @ORM\JoinTable(name="evento_curso",
 *   joinColumns={
 *     @ORM\JoinColumn(name="evento", referencedColumnName="id",onDelete="CASCADE")
 *   },
 *   inverseJoinColumns={
 *     @ORM\JoinColumn(name="curso", referencedColumnName="id",onDelete="CASCADE")
 *   }
 * )
 * @Assert\NotNull(message = "El campo no debe estar en blanco.")
 */
    private $curso;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Evento
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    /**
     * Set fechaResumen
     *
     * @param \DateTime $fechaResumen
     * @return Evento
     */
    public function setFechaResumen($fechaResumen)
    {
        $this->fechaResumen = $fechaResumen;

        return $this;
    }

    /**
     * Get fechaResumen
     *
     * @return \DateTime 
     */
    public function getFechaResumen()
    {
        return $this->fechaResumen;
    }

    /**
     * Set fechaAceptacion
     *
     * @param string $fechaAceptacion
     * @return Evento
     */
    public function setFechaAceptacion($fechaAceptacion)
    {
        $this->fechaAceptacion = $fechaAceptacion;

        return $this;
    }

    /**
     * Get fechaAceptacion
     *
     * @return string 
     */
    public function getFechaAceptacion()
    {
        return $this->fechaAceptacion;
    }

    /**
     * Set convocatoria
     *
     * @param string $convocatoria
     * @return Evento
     */
    public function setConvocatoria($convocatoria)
    {
        $this->convocatoria = $convocatoria;

        return $this;
    }

    /**
     * Get convocatoria
     *
     * @return string 
     */
    public function getConvocatoria()
    {
        return $this->convocatoria;
    }

    /**
     * Set cuotaInscripcion
     *
     * @param float $cuotaInscripcion
     * @return Evento
     */
    public function setCuotaInscripcion($cuotaInscripcion)
    {
        $this->cuotaInscripcion = $cuotaInscripcion;

        return $this;
    }

    /**
     * Get cuotaInscripcion
     *
     * @return float 
     */
    public function getCuotaInscripcion()
    {
        return $this->cuotaInscripcion;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Evento
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set lugar
     *
     * @param string $lugar
     * @return Evento
     */
    public function setLugar($lugar)
    {
        $this->lugar = $lugar;

        return $this;
    }

    /**
     * Get lugar
     *
     * @return string 
     */
    public function getLugar()
    {
        return $this->lugar;
    }

    /**
     * Set norma
     *
     * @param \GEDELTUR\Bundle\Entity\Norma $norma
     * @return Evento
     */
    public function setNorma(\GEDELTUR\Bundle\Entity\Norma $norma = null)
    {
        $this->norma = $norma;

        return $this;
    }

    /**
     * Get norma
     *
     * @return \GEDELTUR\Bundle\Entity\Norma 
     */
    public function getNorma()
    {
        return $this->norma;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->taller = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add taller
     *
     * @param \GEDELTUR\Bundle\Entity\Taller $taller
     * @return Evento
     */
    public function addTaller(\GEDELTUR\Bundle\Entity\Taller $taller)
    {
        $this->taller[] = $taller;

        return $this;
    }

    /**
     * Remove taller
     *
     * @param \GEDELTUR\Bundle\Entity\Taller $taller
     */
    public function removeTaller(\GEDELTUR\Bundle\Entity\Taller $taller)
    {
        $this->taller->removeElement($taller);
    }

    /**
     * Get taller
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTaller()
    {
        return $this->taller;
    }
    public function subirFoto()
    {
        if (null === $this->image) {
            return;
        }
        $directorioDestino = 'uploads/gedeltur/fotos/';
        $nombreArchivo = uniqid('w').'.jpg';

        $this->getImage()->move($directorioDestino, $nombreArchivo);
        $this->setImage($nombreArchivo);
    }

    public function removeUpload($file)
    {
        unlink($file);
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Evento
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add curso
     *
     * @param \GEDELTUR\Bundle\Entity\Curso $curso
     * @return Evento
     */
    public function addCurso(\GEDELTUR\Bundle\Entity\Curso $curso)
    {
        $this->curso[] = $curso;

        return $this;
    }

    /**
     * Remove curso
     *
     * @param \GEDELTUR\Bundle\Entity\Curso $curso
     */
    public function removeCurso(\GEDELTUR\Bundle\Entity\Curso $curso)
    {
        $this->curso->removeElement($curso);
    }

    /**
     * Get curso
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * Set encargado
     *
     * @param \GEDELTUR\Bundle\Entity\Plantilla $encargado
     * @return Evento
     */
    public function setEncargado(\GEDELTUR\Bundle\Entity\Plantilla $encargado = null)
    {
        $this->encargado = $encargado;

        return $this;
    }

    /**
     * Get encargado
     *
     * @return \GEDELTUR\Bundle\Entity\Plantilla
     */
    public function getEncargado()
    {
        return $this->encargado;
    }

    /**
     * Add encargado
     *
     * @param \GEDELTUR\Bundle\Entity\Plantilla $encargado
     * @return Evento
     */
    public function addEncargado(\GEDELTUR\Bundle\Entity\Plantilla $encargado)
    {
        $this->encargado[] = $encargado;

        return $this;
    }

    /**
     * Remove encargado
     *
     * @param \GEDELTUR\Bundle\Entity\Plantilla $encargado
     */
    public function removeEncargado(\GEDELTUR\Bundle\Entity\Plantilla $encargado)
    {
        $this->encargado->removeElement($encargado);
    }

    /**
     * Set fin
     *
     * @param \DateTime $fin
     * @return Evento
     */
    public function setFin($fin)
    {
        $this->fin = $fin;

        return $this;
    }

    /**
     * Get fin
     *
     * @return \DateTime 
     */
    public function getFin()
    {
        return $this->fin;
    }
}

<?php

namespace GEDELTUR\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Maestria
 *
 * @ORM\Table(name="maestria")
 * @ORM\Entity
 */
class Maestria
{
    /**
     * @var string
     *
     * @ORM\Column(name="programa", type="text", nullable=true)
      * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     */
    private $programa;

    /**
     * @var boolean
     *
     * @ORM\Column(name="acreditada", type="boolean", nullable=true)
     */
    private $acreditada;


    

    /**
     * @var \ProgramaAcademico
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="ProgramaAcademico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id", referencedColumnName="id",onDelete="CASCADE")
     * })
      * @Assert\Valid()
     */
    private $id;



    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Diplomado", inversedBy="maestria")
     * @ORM\JoinTable(name="maestria_diplomado",
     *   joinColumns={
     *     @ORM\JoinColumn(name="maestria", referencedColumnName="id",onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="diplomado", referencedColumnName="id",onDelete="CASCADE")
     *   }
     * )
     */
    private $diplomado;



    /**
     * Set programa
     *
     * @param string $programa
     * @return Maestria
     */
    public function setPrograma($programa)
    {
        $this->programa = $programa;

        return $this;
    }

    /**
     * Get programa
     *
     * @return string 
     */
    public function getPrograma()
    {
        return $this->programa;
    }

    /**
     * Set acreditada
     *
     * @param boolean $acreditada
     * @return Maestria
     */
    public function setAcreditada($acreditada)
    {
        $this->acreditada = $acreditada;

        return $this;
    }

    /**
     * Get acreditada
     *
     * @return boolean 
     */
    public function getAcreditada()
    {
        return $this->acreditada;
    }

    /**
     * Set id
     *
     * @param \GEDELTUR\Bundle\Entity\ProgramaAcademico $id
     * @return Maestria
     */
    public function setId(\GEDELTUR\Bundle\Entity\ProgramaAcademico $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return \GEDELTUR\Bundle\Entity\ProgramaAcademico 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Maestria
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->curso = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add curso
     *
     * @param \GEDELTUR\Bundle\Entity\Curso $curso
     * @return Maestria
     */
    public function addCurso(\GEDELTUR\Bundle\Entity\Curso $curso)
    {
        $this->curso[] = $curso;

        return $this;
    }

    /**
     * Remove curso
     *
     * @param \GEDELTUR\Bundle\Entity\Curso $curso
     */
    public function removeCurso(\GEDELTUR\Bundle\Entity\Curso $curso)
    {
        $this->curso->removeElement($curso);
    }

    /**
     * Get curso
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * Add diplomado
     *
     * @param \GEDELTUR\Bundle\Entity\Diplomado $diplomado
     * @return Maestria
     */
    public function addDiplomado(\GEDELTUR\Bundle\Entity\Diplomado $diplomado)
    {
        $this->diplomado[] = $diplomado;

        return $this;
    }

    /**
     * Remove diplomado
     *
     * @param \GEDELTUR\Bundle\Entity\Diplomado $diplomado
     */
    public function removeDiplomado(\GEDELTUR\Bundle\Entity\Diplomado $diplomado)
    {
        $this->diplomado->removeElement($diplomado);
    }

    /**
     * Get diplomado
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDiplomado()
    {
        return $this->diplomado;
    }
}

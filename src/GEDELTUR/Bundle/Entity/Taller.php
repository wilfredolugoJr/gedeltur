<?php

namespace GEDELTUR\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Taller
 *
 * @ORM\Table(name="taller")
 * @ORM\Entity
 * @UniqueEntity(fields = {"nombre"}, message="Ya este taller está registrado")
 */
class Taller
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=150, nullable=false)
     * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
        * @Assert\NotBlank(message = "Por favor, no puede estar vacio.")
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
          */
    private $descripcion;

     /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Evento", mappedBy="taller")
     */
    private $evento;
    

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Tematica", inversedBy="taller")
     * @ORM\JoinTable(name="taller_tematica",
     *   joinColumns={
     *     @ORM\JoinColumn(name="taller", referencedColumnName="id",onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="tematica", referencedColumnName="id")
     *   }
     * )
     */
    private $tematica;


    /**
     * @var \Plantilla
     *
     * @ORM\ManyToOne(targetEntity="Plantilla")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="encargado", referencedColumnName="id", onDelete="CASCADE")
     * })
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $encargado;


     

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tematica = new \Doctrine\Common\Collections\ArrayCollection();
    }

     public function __toString()
    {
        return $this->nombre;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Taller
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Taller
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set evento
     *
     * @param \GEDELTUR\Bundle\Entity\Evento $evento
     * @return Taller
     */
    
    /**
     * Add tematica
     *
     * @param \GEDELTUR\Bundle\Entity\Tematica $tematica
     * @return Taller
     */
    public function addTematica(\GEDELTUR\Bundle\Entity\Tematica $tematica)
    {
        $this->tematica[] = $tematica;

        return $this;
    }

    /**
     * Remove tematica
     *
     * @param \GEDELTUR\Bundle\Entity\Tematica $tematica
     */
    public function removeTematica(\GEDELTUR\Bundle\Entity\Tematica $tematica)
    {
        $this->tematica->removeElement($tematica);
    }

    /**
     * Get tematica
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTematica()
    {
        return $this->tematica;
    }

    /**
     * Add evento
     *
     * @param \GEDELTUR\Bundle\Entity\Evento $evento
     * @return Taller
     */
    public function addEvento(\GEDELTUR\Bundle\Entity\Evento $evento)
    {
        $this->evento[] = $evento;

        return $this;
    }

    /**
     * Remove evento
     *
     * @param \GEDELTUR\Bundle\Entity\Evento $evento
     */
    public function removeEvento(\GEDELTUR\Bundle\Entity\Evento $evento)
    {
        $this->evento->removeElement($evento);
    }

    /**
     * Get evento
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvento()
    {
        return $this->evento;
    }

    /**
     * Set encargado
     *
     * @param \GEDELTUR\Bundle\Entity\Plantilla $encargado
     * @return Taller
     */
    public function setEncargado(\GEDELTUR\Bundle\Entity\Plantilla $encargado = null)
    {
        $this->encargado = $encargado;

        return $this;
    }

    /**
     * Get encargado
     *
     * @return \GEDELTUR\Bundle\Entity\Plantilla
     */
    public function getEncargado()
    {
        return $this->encargado;
    }
}

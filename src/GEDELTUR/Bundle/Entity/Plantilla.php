<?php

namespace GEDELTUR\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Plantilla
 *
 * @ORM\Table(name="plantilla")
 * @ORM\Entity
 */
class Plantilla
{
    /**
     * @var string
     *
     * @ORM\Column(name="cargo", type="string", length=10, nullable=false)
     */
    private $cargo;

    /**
     * @var string
     *
     * @ORM\Column(name="cat_doc", type="string", length=10, nullable=false)
     */
    private $catDoc;

    /**
     * @var string
     *
     * @ORM\Column(name="cat_cien", type="string", length=10, nullable=false)
     */
    private $catCien;

    /**
     * @var \RecursosHumanos
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="RecursosHumanos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id", referencedColumnName="id", onDelete="CASCADE")
     * })
     * @Assert\Valid()
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Evento", mappedBy="encargado")
     */
    private $evento;


    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TematicaSct", mappedBy="plantilla")
     */
    private $tematica;

    

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tematica = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set cargo
     *
     * @param string $cargo
     * @return Plantilla
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * Get cargo
     *
     * @return string 
     */
    public function getCargo()
    {
        return $this->cargo;
    }
 public function __toString()
    {
        return $this->id->getNombre().' '.$this->id->getApellidos();
    }



    

    /**
     * Set catDoc
     *
     * @param string $catDoc
     * @return Plantilla
     */
    public function setCatDoc($catDoc)
    {
        $this->catDoc = $catDoc;

        return $this;
    }

    /**
     * Get catDoc
     *
     * @return string 
     */
    public function getCatDoc()
    {
        return $this->catDoc;
    }

    /**
     * Set id
     *
     * @param \GEDELTUR\Bundle\Entity\RecursosHumanos $id
     * @return Plantilla
     */
    public function setId(\GEDELTUR\Bundle\Entity\RecursosHumanos $id)
    {
        $this->id = $id;

        return $this;
    }


    /**
     * Get id
     *
     * @return \GEDELTUR\Bundle\Entity\RecursosHumanos 
     */
    public function getId()
    {
        return $this->id;
    }

    

    /**
     * Set catCien
     *
     * @param string $catCien
     * @return Plantilla
     */
    public function setCatCien($catCien)
    {
        $this->catCien = $catCien;

        return $this;
    }

    /**
     * Get catCien
     *
     * @return string 
     */
    public function getCatCien()
    {
        return $this->catCien;
    }

    /**
     * Add evento
     *
     * @param \GEDELTUR\Bundle\Entity\Evento $evento
     * @return Plantilla
     */
    public function addEvento(\GEDELTUR\Bundle\Entity\Evento $evento)
    {
        $this->evento[] = $evento;

        return $this;
    }

    /**
     * Remove evento
     *
     * @param \GEDELTUR\Bundle\Entity\Evento $evento
     */
    public function removeEvento(\GEDELTUR\Bundle\Entity\Evento $evento)
    {
        $this->evento->removeElement($evento);
    }

    /**
     * Get evento
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvento()
    {
        return $this->evento;
    }

    /**
     * Add tematica
     *
     * @param \GEDELTUR\Bundle\Entity\TematicaSct $tematica
     * @return Plantilla
     */
    public function addTematica(\GEDELTUR\Bundle\Entity\TematicaSct $tematica)
    {
        $this->tematica[] = $tematica;

        return $this;
    }

    /**
     * Remove tematica
     *
     * @param \GEDELTUR\Bundle\Entity\TematicaSct $tematica
     */
    public function removeTematica(\GEDELTUR\Bundle\Entity\TematicaSct $tematica)
    {
        $this->tematica->removeElement($tematica);
    }

    /**
     * Get tematica
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTematica()
    {
        return $this->tematica;
    }
}

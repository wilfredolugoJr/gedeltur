<?php

namespace GEDELTUR\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * LineaInvestigacion
 *
 * @ORM\Table(name="linea_investigacion")
 * @ORM\Entity
 * @UniqueEntity(fields = {"titulo"}, message="Ya esta Linea de Investigación existe")
 */
class LineaInvestigacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="resumen", type="string", length=10, nullable=false)
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     */
    private $resumen;

    /**
     * @var string
     *
     * @ORM\Column(name="objetivo", type="string", length=100, nullable=false)
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     */
    private $objetivo;

    /**
     * @var string
     *
     * @ORM\Column(name="impacto", type="text", nullable=false)
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá.,0-9\s]*$/", match=true, message="No puede contener caracteres raros.")
     */
    private $impacto;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=100, nullable=false)
     * @Assert\Regex(pattern="/^[a-zA-Zñíóúéá\s]*$/", match=true, message="Solo puede contener letras y espacios en blanco.")
     */
    private $titulo;





    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Proyecto", mappedBy="lineas")
     */
    private $proyecto;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set resumen
     *
     * @param string $resumen
     * @return LineaInvestigacion
     */
    public function setResumen($resumen)
    {
        $this->resumen = $resumen;

        return $this;
    }

    /**
     * Get resumen
     *
     * @return string 
     */
    public function getResumen()
    {
        return $this->resumen;
    }

    /**
     * Set objetivo
     *
     * @param string $objetivo
     * @return LineaInvestigacion
     */
    public function setObjetivo($objetivo)
    {
        $this->objetivo = $objetivo;

        return $this;
    }

    /**
     * Get objetivo
     *
     * @return string 
     */
    public function getObjetivo()
    {
        return $this->objetivo;
    }

    /**
     * Set impacto
     *
     * @param string $impacto
     * @return LineaInvestigacion
     */
    public function setImpacto($impacto)
    {
        $this->impacto = $impacto;

        return $this;
    }

    /**
     * Get impacto
     *
     * @return string 
     */
    public function getImpacto()
    {
        return $this->impacto;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return LineaInvestigacion
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }
 public function  __toString()
    {
        return $this->titulo;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rrhh = new \Doctrine\Common\Collections\ArrayCollection();
    }



    /**
     * Add proyecto
     *
     * @param \GEDELTUR\Bundle\Entity\Proyecto $proyecto
     * @return LineaInvestigacion
     */
    public function addProyecto(\GEDELTUR\Bundle\Entity\Proyecto $proyecto)
    {
        $this->proyecto[] = $proyecto;

        return $this;
    }

    /**
     * Remove proyecto
     *
     * @param \GEDELTUR\Bundle\Entity\Proyecto $proyecto
     */
    public function removeProyecto(\GEDELTUR\Bundle\Entity\Proyecto $proyecto)
    {
        $this->proyecto->removeElement($proyecto);
    }

    /**
     * Get proyecto
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProyecto()
    {
        return $this->proyecto;
    }
}

<?php

namespace GEDELTUR\Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Diplomado
 *
 * @ORM\Table(name="diplomado")
 * @ORM\Entity
 */
class Diplomado
{
    /**
     * @var \ProgramaAcademico
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="ProgramaAcademico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id", referencedColumnName="id", onDelete="CASCADE")
     * })
     * @Assert\Valid()
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Curso", inversedBy="diplomado")
     * @ORM\JoinTable(name="diplomado_curso",
     *   joinColumns={
     *     @ORM\JoinColumn(name="diplomado", referencedColumnName="id",onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="curso", referencedColumnName="id",onDelete="CASCADE")
     *   }
     * )
     */
    private $curso;



    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Maestria", mappedBy="diplomado")
     */
    private $maestria;






   

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->curso = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set id
     *
     * @param \GEDELTUR\Bundle\Entity\ProgramaAcademico $id
     * @return Diplomado
     */
    public function setId(\GEDELTUR\Bundle\Entity\ProgramaAcademico $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return \GEDELTUR\Bundle\Entity\ProgramaAcademico 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add curso
     *
     * @param \GEDELTUR\Bundle\Entity\Curso $curso
     * @return Diplomado
     */
    public function addCurso(\GEDELTUR\Bundle\Entity\Curso $curso)
    {
        $this->curso[] = $curso;

        return $this;
    }

    /**
     * Remove curso
     *
     * @param \GEDELTUR\Bundle\Entity\Curso $curso
     */
    public function removeCurso(\GEDELTUR\Bundle\Entity\Curso $curso)
    {
        $this->curso->removeElement($curso);
    }

    public function __toString()
    {
        return $this->id->getTitulo();
    }

    /**
     * Get curso
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Diplomado
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Add maestria
     *
     * @param \GEDELTUR\Bundle\Entity\Maestria $maestria
     * @return Diplomado
     */
    public function addMaestrium(\GEDELTUR\Bundle\Entity\Maestria $maestria)
    {
        $this->maestria[] = $maestria;

        return $this;
    }

    /**
     * Remove maestria
     *
     * @param \GEDELTUR\Bundle\Entity\Maestria $maestria
     */
    public function removeMaestrium(\GEDELTUR\Bundle\Entity\Maestria $maestria)
    {
        $this->maestria->removeElement($maestria);
    }

    /**
     * Get maestria
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMaestria()
    {
        return $this->maestria;
    }
}

<?php

namespace GEDELTUR\Bundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Contenido
 *
 * @ORM\Table(name="contenido")
 * @ORM\Entity
 */
class Contenido
{
    /**
     * @var \ServiciosCienticoTecnicos
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="ServiciosCienticoTecnicos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id", referencedColumnName="id", onDelete="CASCADE")
     * })
     * @Assert\Valid()
     */
    private $id;



    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="text", nullable=false)
     * @Assert\NotNull(message = "El campo no debe estar en blanco.")
     */
    private $contenido;

    
 



    

    /**
     * Set contenido
     *
     * @param string $contenido
     * @return Contenido
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return string 
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    

   

    /**
     * Set id
     *
     * @param \GEDELTUR\Bundle\Entity\ServiciosCienticoTecnicos $id
     * @return Contenido
     */
    public function setId(\GEDELTUR\Bundle\Entity\ServiciosCienticoTecnicos $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return \GEDELTUR\Bundle\Entity\ServiciosCienticoTecnicos 
     */
    public function getId()
    {
        return $this->id;
    }
}
